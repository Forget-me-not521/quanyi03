#!/bin/sh

set -eo pipefail

if [ "$CLASSPATH" ]
then
    if [ "$JAVA_OPTS" ]
    then
        exec /usr/bin/java $JAVA_OPTS -classpath $CLASSPATH
    else
        exec /usr/bin/java -Xdebug -Xms1g -Xmx3g -classpath $CLASSPATH
    fi
else
   echo 'Please set env $CLASSPATH'
   exit 1
fi
