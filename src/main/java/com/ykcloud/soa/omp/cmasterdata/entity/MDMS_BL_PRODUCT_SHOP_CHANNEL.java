package com.ykcloud.soa.omp.cmasterdata.entity;


public class MDMS_BL_PRODUCT_SHOP_CHANNEL extends BaseEntity {


    private Long RESERVED_NO;//永久调价单号

    private Long SUB_UNIT_NUM_ID;//数字id的代替单位

    private Integer CHANNCEL_NUM_ID;//改变的id

    private Long ITEM_NUM_ID;//物品id

    public MDMS_BL_PRODUCT_SHOP_CHANNEL() {
        // TODO Auto-generated constructor stub
    }

    public Long getRESERVED_NO() {
        return RESERVED_NO;
    }

    public void setRESERVED_NO(Long rESERVED_NO) {
        RESERVED_NO = rESERVED_NO;
    }

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long sUB_UNIT_NUM_ID) {
        SUB_UNIT_NUM_ID = sUB_UNIT_NUM_ID;
    }

    public Integer getCHANNCEL_NUM_ID() {
        return CHANNCEL_NUM_ID;
    }

    public void setCHANNCEL_NUM_ID(Integer cHANNCEL_NUM_ID) {
        CHANNCEL_NUM_ID = cHANNCEL_NUM_ID;
    }

    public Long getITEM_NUM_ID() {
        return ITEM_NUM_ID;
    }

    public void setITEM_NUM_ID(Long iTEM_NUM_ID) {
        ITEM_NUM_ID = iTEM_NUM_ID;
    }

    @Override
    public String toString() {
        return "MDMS_BI_PRODUCT_SHOP_CHANNEL [RESERVED_NO=" + RESERVED_NO
                + ", SUB_UNIT_NUM_ID=" + SUB_UNIT_NUM_ID + ", CHANNCEL_NUM_ID="
                + CHANNCEL_NUM_ID + ", ITEM_NUM_ID=" + ITEM_NUM_ID + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MDMS_BL_PRODUCT_SHOP_CHANNEL)) return false;

        MDMS_BL_PRODUCT_SHOP_CHANNEL that = (MDMS_BL_PRODUCT_SHOP_CHANNEL) o;

        return ITEM_NUM_ID != null ? ITEM_NUM_ID.equals(that.ITEM_NUM_ID) : that.ITEM_NUM_ID == null;
    }

    @Override
    public int hashCode() {
        return ITEM_NUM_ID != null ? ITEM_NUM_ID.hashCode() : 0;
    }
}
