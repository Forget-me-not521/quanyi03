package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;

import java.util.Objects;
import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

/**
 * @Auther: Dan
 * @Date: 2019/8/24 19:56
 * @Description:
 */
@Repository
public class MdmsWmsPhysicalStorageDao {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    public boolean checkStatus(Long tenantNumId, Long dataSign, Long subUnitNumId, Long physicalNumId,
                               Long storageNumId, Long typeNumId) {
        StringBuilder sql = new StringBuilder("SELECT count(1) from mdms_wms_physical_storage where tenant_num_id = ? AND data_sign = ? AND sub_unit_num_id = ?");
        sql.append(" AND physical_num_id = ? AND storage_num_id = ? AND is_wms = 1");
        if (Objects.nonNull(typeNumId) && typeNumId.equals(1L)) {
            sql.append(" and direct_type = 1");
        }
        Long count = jdbcTemplate.queryForObject(sql.toString(), new Object[]{tenantNumId, dataSign, subUnitNumId, physicalNumId, storageNumId}, Long.class);
        if (count > 0l) {
            return true;
        } else {
            return false;
        }
    }


    public boolean checkConfigExist(Long tenantNumId, Long dataSign, Long subUnitNumId) {
        StringBuilder sql = new StringBuilder("SELECT count(1) from mdms_wms_physical_storage where tenant_num_id = ? AND data_sign = ? AND sub_unit_num_id = ?");
        sql.append(" AND is_wms = 1");
        Integer count = jdbcTemplate.queryForObject(sql.toString(), new Object[]{tenantNumId, dataSign, subUnitNumId}, Integer.class);
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }
}
