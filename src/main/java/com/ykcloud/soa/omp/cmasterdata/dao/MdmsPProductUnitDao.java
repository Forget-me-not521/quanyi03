package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.ProductUnitInfo;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_P_PRODUCT_UNIT;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;


@Repository
public class MdmsPProductUnitDao extends Dao<MDMS_P_PRODUCT_UNIT> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public List<ProductUnitInfo> getUnitProductList(Long tenantNumId, Long dataSign, String cortNumId, String itemName, String factory, Integer pageNum, Integer pageSize) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_p_product_basic b INNER JOIN mdms_p_product_unit u on b.item_num_id=u.item_num_id and b.tenant_num_id=u.tenant_num_id and b.data_sign=u.data_sign  ");
        sb.append(" where b.tenant_num_id=? and b.data_sign=? and  b.cancelsign='N' and u.cort_num_id=?  ");
        if(!isNullOrZero(itemName)){
            sb.append(" and b.item_name like '%"+itemName+"%'");
        }
        if(!isNullOrZero(factory)){
            sb.append(" and b.factory="+factory );
        }
        Integer start = (pageNum-1)*pageSize;
        sb.append(" limit ?,? ");
        List<ProductUnitInfo> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, cortNumId,start,pageSize}, new BeanPropertyRowMapper<>(ProductUnitInfo.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return  list;
    }

    public Integer getUnitProductCount(Long tenantNumId, Long dataSign, String cortNumId,String itemName,  String factory) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from mdms_p_product_basic b INNER JOIN mdms_p_product_unit u on b.item_num_id=u.item_num_id and b.tenant_num_id=u.tenant_num_id and b.data_sign=u.data_sign  ");
        sb.append(" where b.tenant_num_id=? and b.data_sign=? and  b.cancelsign='N' and u.cort_num_id=?  ");

        if(!isNullOrZero(itemName)){
            sb.append(" and b.item_name like '%"+itemName+"%'");
        }

        if(!isNullOrZero(factory)){
            sb.append(" and b.factory="+factory );
        }

        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign,cortNumId}, Integer.class);
    }
    private static boolean isNullOrZero(Object obj) {
        if (obj == null || "0".equals(obj.toString()) || "0.0".equals(obj.toString()) || "".equals(obj.toString())) {
            return true;
        }
        return false;

    }
}
