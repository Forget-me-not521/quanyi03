package com.ykcloud.soa.omp.cmasterdata.util;

/**
 * @author shirong.chen
 * @date 2018年6月8日上午11:43:50
 */
public class DaoUtil {
    /**
     * mycat查询限制条数，定义查询条数
     */
    public final static String LIMIT_STR = " limit 0,1000000";

    /**
     * 批量插入汇总成功笔数
     *
     * @author shirong.chen
     * @date 2018年6月8日上午11:44:43
     */
    public static int sum(int[] row) {
        int sum = 0;
        for (int i : row) {
            sum += i;
        }
        return sum;
    }
}
