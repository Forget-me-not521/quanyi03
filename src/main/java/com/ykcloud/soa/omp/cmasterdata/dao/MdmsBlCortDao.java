package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsBlCort;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_CORT;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

@Repository
public class MdmsBlCortDao {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_BL_CORT.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_BL_CORT.class, ",");
    private static final String TABLE_NAME = "MDMS_BL_CORT";

    public void insertMdmsBlCompanyDao(MDMS_BL_CORT mdms_bl_CORT){
        StringBuilder sb = new StringBuilder();
        sb.append("insert into ");
        sb.append(TABLE_NAME);
        sb.append("(");
        sb.append(SQL_COLS);
        sb.append(") values (");
        sb.append(WILDCARDS);
        sb.append(")");
        int row = jdbcTemplate.update(sb.toString(), EntityFieldUtil.fieldSplitValue(MDMS_BL_CORT.class, mdms_bl_CORT));
        if (row <= 0) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35002, "插入失败！");
        }
    }

    public void updateByReservedNo(Long tenantNumId, Long dataSign, String reservedNo, MDMS_BL_CORT mdms_bl_CORT) {
        String cols = EntityFieldUtil.wildCardSplitUpdate(mdms_bl_CORT, ",");
        List<Object> params = new ArrayList<>(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_BL_CORT.class, mdms_bl_CORT)));
        params.add(tenantNumId);
        params.add(dataSign);
        params.add(reservedNo);
        StringBuffer sb = new StringBuffer();
        sb.append("update mdms_bl_cort set ");
        sb.append(cols);
        sb.append(" where tenant_num_id=? and data_sign=? and reserved_no=? and cancelsign='N'");

        int row = jdbcTemplate.update(sb.toString(), params.toArray());
        if (row == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "更新供应商变更单信息失败!");
        }
    }

    public MDMS_BL_CORT selectByReservedNo(Long tenantNumId, Long dataSign, String reservedNo) {
        String sql = "select * from MDMS_BL_CORT WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  RESERVED_NO = ? AND cancelsign = 'N'";
        MDMS_BL_CORT mdms_bl_CORT = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(MDMS_BL_CORT.class));
        if (null == mdms_bl_CORT) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "不存在系统单号为" + reservedNo + "的表单数据！");
        }
        return mdms_bl_CORT;
    }

    public List<MDMS_BL_CORT> selectByUnifiedSocialCreditCode(Long tenantNumId, Long dataSign, String unifiedSocialCreditCode) {
        String sql = "select series as SERIES, reserved_no as RESERVED_NO from MDMS_BL_CORT WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  UNIFIED_SOCIAL_CREDIT_CODE = ? AND cancelsign = 'N'";
        List<MDMS_BL_CORT> mdmsBlManagerHdrList = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, unifiedSocialCreditCode}, new BeanPropertyRowMapper<>(MDMS_BL_CORT.class));
        return mdmsBlManagerHdrList;
    }

    public MdmsBlCort selectEntityByReservedNo(Long tenantNumId, Long dataSign, String reservedNo) {
        String sql = "select * from MDMS_BL_CORT WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  RESERVED_NO = ? AND cancelsign = 'N'";
        MdmsBlCort mdmsBlCort = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(MdmsBlCort.class));
        if (null == mdmsBlCort) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "不存在系统单号为" + reservedNo + "的表单数据！");
        }
        return mdmsBlCort;
    }

    public boolean checkCortExist(Long tenantNumId, Long dataSign, String unifiedSocialCreditCode) {
        String sql = "select count(1) from mdms_bl_cort where tenant_num_id = ? and data_sign = ? and  unified_social_credit_code = ? AND cancelsign = 'N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign,
                unifiedSocialCreditCode}, int.class) >0 ? true : false;
    }

}