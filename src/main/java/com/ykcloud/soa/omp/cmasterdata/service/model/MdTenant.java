package com.ykcloud.soa.omp.cmasterdata.service.model;

public class MdTenant {
    private String configValue;

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

}
