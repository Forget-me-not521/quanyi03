package com.ykcloud.soa.omp.cmasterdata.enums;

public enum BillTypeEnums {
    COMPANY(1, "公司"),;

    private int id;
    private String value;

    BillTypeEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (BillTypeEnums item : BillTypeEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
