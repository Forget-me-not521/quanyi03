package com.ykcloud.soa.omp.cmasterdata.service.impl;


import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.JsonUtil;
import com.gb.soa.omp.cmessagecenter.util.ExceptionUtils;
import com.ykcloud.soa.omp.cmasterdata.api.request.UnitGenerateRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.UnitOrgDeleteRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.UnitOrgSaveRequest;
import com.ykcloud.soa.omp.cmasterdata.api.response.UnitGenerateResponse;
import com.ykcloud.soa.omp.cmasterdata.api.response.UnitOrgDeleteResponse;
import com.ykcloud.soa.omp.cmasterdata.api.response.UnitOrgSaveResponse;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdUnitService;
import com.ykcloud.soa.omp.cmasterdata.dao.MdmsOUnitDao;
import com.ykcloud.soa.omp.cmasterdata.dao.MdmsOUnitOrgDao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_UNIT;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_UNIT_ORG;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Objects;

@Service("mdUnitService")
@RestController
public class MdUnitServiceImpl implements MdUnitService {
    private static Logger log = LoggerFactory.getLogger(MdUnitServiceImpl.class);
    @Resource
    private MdmsOUnitDao mdmsOUnitDao;
    @Resource
    private MdmsOUnitOrgDao mdmsOUnitOrgDao;
    @Override
    public UnitGenerateResponse generateUnit(UnitGenerateRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin generateUnit request:{}", JsonUtil.toJson(request));
        }
        UnitGenerateResponse response = new UnitGenerateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId(),
                    dataSign = request.getDataSign(),
                    userNumId = request.getUserNumId();
            if(mdmsOUnitDao.checkUnitExistByUnitNumId(tenantNumId,dataSign,request.getCortNumId(),request.getUnitNumId())){
                mdmsOUnitDao.updateEntity(tenantNumId,dataSign,userNumId,request.getCortNumId(),request.getUnitNumId(),request.getUnitName());
            }else{
                MDMS_O_UNIT entity=createUnit(tenantNumId,dataSign,userNumId,request.getCortNumId(),request.getUnitName(),request.getUnitType(),request.getSubType(),request.getUnitNumId());
                mdmsOUnitDao.insert(entity);
            }
        } catch (Exception e) {
            ExceptionUtils.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end generateUnit response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public UnitOrgSaveResponse saveUnitOrg(UnitOrgSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveUnitOrg request:{}", JsonUtil.toJson(request));
        }
        UnitOrgSaveResponse response = new UnitOrgSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId(),
                    dataSign = request.getDataSign(),
                    userNumId = request.getUserNumId();
            MDMS_O_UNIT_ORG entity=this.buildUnitOrg(tenantNumId,dataSign,userNumId,request.getUnitNumId(),request.getUnitType(),request.getPurOrg(),request.getSalOrg(),request.getInvOrg(),request.getFinOrg(),request.getBusOrg());
            if(ValidatorUtils.isNullOrZero(request.getSeries())){
                entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_UNIT_ORG_SERIES));
                mdmsOUnitOrgDao.insert(entity);
            }else{
                MDMS_O_UNIT_ORG oldEntity=mdmsOUnitOrgDao.getUnitOrgEntity(tenantNumId,dataSign,request.getSeries());
                if(!Objects.isNull(oldEntity)){
                    entity.setCREATE_DTME(oldEntity.getCREATE_DTME());
                    entity.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                    mdmsOUnitOrgDao.update(entity,tenantNumId,dataSign,request.getSeries());
                }
            }
        } catch (Exception e) {
            ExceptionUtils.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveUnitOrg response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public UnitOrgDeleteResponse deleteUnitOrg(UnitOrgDeleteRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin deleteUnitOrg request:{}", JsonUtil.toJson(request));
        }
        UnitOrgDeleteResponse response = new UnitOrgDeleteResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId(),
                    dataSign = request.getDataSign(),
                    userNumId = request.getUserNumId();
            for(String series : request.getSeriesList()){
                MDMS_O_UNIT_ORG oldEntity=mdmsOUnitOrgDao.getUnitOrgEntity(tenantNumId,dataSign,series);
                if(!Objects.isNull(oldEntity)){
                    mdmsOUnitOrgDao.deleteUnitOrg(tenantNumId,dataSign,series);
                }
            }

        } catch (Exception e) {
            ExceptionUtils.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteUnitOrg response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    private MDMS_O_UNIT createUnit(Long tenantNumId, Long dataSign, Long usrNumId,String cortNumId,String unitName,Integer unitType,Integer subType,String unitNumId){
        MDMS_O_UNIT entity = new MDMS_O_UNIT();
        entity.setCORT_NUM_ID(cortNumId);
        entity.setUNIT_NUM_ID(unitNumId);
        entity.setUNIT_NAME(unitName);
        entity.setUNIT_TYPE(unitType);
        entity.setSUB_TYPE(subType);
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_UNIT_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }

    private MDMS_O_UNIT_ORG buildUnitOrg(Long tenantNumId, Long dataSign, Long usrNumId,Integer unitNumId,Integer unitType,Integer purOrg,Integer salOrg,Integer invOrg,Integer finOrg,Integer busOrg){
        MDMS_O_UNIT_ORG entity = new MDMS_O_UNIT_ORG();
        entity.setUNIT_NUM_ID(unitNumId);
        entity.setUNIT_TYPE(unitType);
        entity.setPUR_ORG(purOrg);
        entity.setSAL_ORG(salOrg);
        entity.setINV_ORG(invOrg);
        entity.setFIN_ORG(finOrg);
        entity.setBUS_ORG(busOrg);
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;

    }
}
