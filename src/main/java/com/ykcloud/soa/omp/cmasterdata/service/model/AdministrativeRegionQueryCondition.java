package com.ykcloud.soa.omp.cmasterdata.service.model;

public class AdministrativeRegionQueryCondition {
    private Long prvNumId;

    private String prvSimNo;

    private String prvName;

    private Long cityNumId;

    private String citySimNo;

    private String cityName;

    private Long cityAreaNumId;

    private String cityAreaSimNo;

    private String cityAreaName;

    private Long townNumId;

    private String townSimNo;

    private String townName;

    public Long getPrvNumId() {
        return prvNumId;
    }

    public void setPrvNumId(Long prvNumId) {
        this.prvNumId = prvNumId;
    }

    public String getPrvSimNo() {
        return prvSimNo;
    }

    public void setPrvSimNo(String prvSimNo) {
        this.prvSimNo = prvSimNo;
    }

    public Long getCityNumId() {
        return cityNumId;
    }

    public void setCityNumId(Long cityNumId) {
        this.cityNumId = cityNumId;
    }

    public String getCitySimNo() {
        return citySimNo;
    }

    public void setCitySimNo(String citySimNo) {
        this.citySimNo = citySimNo;
    }

    public Long getCityAreaNumId() {
        return cityAreaNumId;
    }

    public void setCityAreaNumId(Long cityAreaNumId) {
        this.cityAreaNumId = cityAreaNumId;
    }

    public String getCityAreaSimNo() {
        return cityAreaSimNo;
    }

    public void setCityAreaSimNo(String cityAreaSimNo) {
        this.cityAreaSimNo = cityAreaSimNo;
    }

    public Long getTownNumId() {
        return townNumId;
    }

    public void setTownNumId(Long townNumId) {
        this.townNumId = townNumId;
    }

    public String getTownSimNo() {
        return townSimNo;
    }

    public void setTownSimNo(String townSimNo) {
        this.townSimNo = townSimNo;
    }

    public String getPrvName() {
        return prvName;
    }

    public void setPrvName(String prvName) {
        this.prvName = prvName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityAreaName() {
        return cityAreaName;
    }

    public void setCityAreaName(String cityAreaName) {
        this.cityAreaName = cityAreaName;
    }

    public String getTownName() {
        return townName;
    }

    public void setTownName(String townName) {
        this.townName = townName;
    }

}
