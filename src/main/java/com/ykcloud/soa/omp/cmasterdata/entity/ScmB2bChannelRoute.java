package com.ykcloud.soa.omp.cmasterdata.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
@Data
public class ScmB2bChannelRoute implements Serializable {
    private static final long serialVersionUID=1L;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Long series;

    /** 租户 */
    @ApiModelProperty(value = "租户")
    private Long tenant_num_id;

    /** 状态标识，0正式，1测试，3删除，4作废 */
    @ApiModelProperty(value = "状态标识，0正式，1测试，3删除，4作废")
    private Long data_sign;

    /** 公司 */
    @ApiModelProperty(value = "公司")
    private String cort_num_id;

    /** 业务单元 */
    @ApiModelProperty(value = "业务单元")
    private String unit_num_id;

    /** 组织 */
    @ApiModelProperty(value = "组织")
    private String org_unit;

    /** 渠道 */
    @ApiModelProperty(value = "渠道")
    private String channel_num_id;

    /** 逻辑仓库 */
    @ApiModelProperty(value = "逻辑仓库")
    private Long storage_num_id;

    /** 优先级 */
    @ApiModelProperty(value = "优先级")
    private Byte storage_lv;

    /** 是否生效。0不生效，1生效 */
    @ApiModelProperty(value = "是否生效。0不生效，1生效")
    private Byte effect_sign;

    private Long create_user_id;

    private Date create_dtme;

    private Long last_update_user_id;

    private Date last_updtme;

    /** 删除 */
    @ApiModelProperty(value = "删除")
    private String cancelsign;


}
