package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class MDMS_BL_CUST_UNIT extends BaseEntity {
    private String RESERVED_NO;
    private String THEME;
    private Integer TYPE_NUM_ID;
    private Integer STATUS_NUM_ID;
    private Integer SO_FROM_TYPE;
    private Date ORDER_DATE;
    private Long AUD_USER_ID;
    private Date AUD_DTME;
    private Long FI_USER_ID;
    private Date FI_DTME;
    private Long UNIT_NUM_ID;
    private String UNIT_NAME;
    private Long CORT_NUM_ID;
    private String CORT_NAME;
    private Integer UNIT_STATUS_NUM_ID;
    private String CUS_NAME;
    private String CUS_TEL;
    private String EMERGENCY_CONT;
    private String EMERGENCY_CONT_TEL;
    private Long PRV_NUM_ID;
    private Long CITY_NUM_ID;
    private Long CITY_AREA_NUM_ID;
    private Long TOWN_NUM_ID;
    private String ADR;
    private String LEGAL_BEHF;
    private String BEHF_TELEPHONE;
    private String BEHF_ID;
    private String BEHF_FAX;
    private String BEHF_ADR;
    private String ORG_CODE;
    private String REGISTERED_NAME;
    private String TRADING_CERTIFICATES;
    private String TAX_ACCOUNT;
    private String TAXPAYER_TYPE;
    private String REG_CAPITAL;
    private String REG_ADR;
    private String BILLING_BANK;
    private String BILLING_BANK_ACCOUNT;
    private Long BANK_PRV_NUM_ID;
    private Long BANK_CITY_NUM_ID;
    private String CUS_CATEGORY;
    private String CUS_AREA;
    private Integer CUS_LEVEL;
    private String REMARK;
    private Integer SETTLEMENT_CYCLE;
    private Integer PAYMENT_DAYS;
    private Integer PAYMENT_METHOD;
    private Integer ACCOUNT_PERIOD;

    public Integer getACCOUNT_PERIOD() {
        return ACCOUNT_PERIOD;
    }

    public void setACCOUNT_PERIOD(Integer ACCOUNT_PERIOD) {
        this.ACCOUNT_PERIOD = ACCOUNT_PERIOD;
    }

    public String getRESERVED_NO() {
        return RESERVED_NO;
    }

    public void setRESERVED_NO(String RESERVED_NO) {
        this.RESERVED_NO = RESERVED_NO;
    }

    public String getTHEME() {
        return THEME;
    }

    public void setTHEME(String THEME) {
        this.THEME = THEME;
    }

    public Integer getTYPE_NUM_ID() {
        return TYPE_NUM_ID;
    }

    public void setTYPE_NUM_ID(Integer TYPE_NUM_ID) {
        this.TYPE_NUM_ID = TYPE_NUM_ID;
    }

    public Integer getSTATUS_NUM_ID() {
        return STATUS_NUM_ID;
    }

    public void setSTATUS_NUM_ID(Integer STATUS_NUM_ID) {
        this.STATUS_NUM_ID = STATUS_NUM_ID;
    }

    public Integer getSO_FROM_TYPE() {
        return SO_FROM_TYPE;
    }

    public void setSO_FROM_TYPE(Integer SO_FROM_TYPE) {
        this.SO_FROM_TYPE = SO_FROM_TYPE;
    }

    public Date getORDER_DATE() {
        return ORDER_DATE;
    }

    public void setORDER_DATE(Date ORDER_DATE) {
        this.ORDER_DATE = ORDER_DATE;
    }

    public Long getAUD_USER_ID() {
        return AUD_USER_ID;
    }

    public void setAUD_USER_ID(Long AUD_USER_ID) {
        this.AUD_USER_ID = AUD_USER_ID;
    }

    public Date getAUD_DTME() {
        return AUD_DTME;
    }

    public void setAUD_DTME(Date AUD_DTME) {
        this.AUD_DTME = AUD_DTME;
    }

    public Long getFI_USER_ID() {
        return FI_USER_ID;
    }

    public void setFI_USER_ID(Long FI_USER_ID) {
        this.FI_USER_ID = FI_USER_ID;
    }

    public Date getFI_DTME() {
        return FI_DTME;
    }

    public void setFI_DTME(Date FI_DTME) {
        this.FI_DTME = FI_DTME;
    }

    public Long getUNIT_NUM_ID() {
        return UNIT_NUM_ID;
    }

    public void setUNIT_NUM_ID(Long UNIT_NUM_ID) {
        this.UNIT_NUM_ID = UNIT_NUM_ID;
    }

    public String getUNIT_NAME() {
        return UNIT_NAME;
    }

    public void setUNIT_NAME(String UNIT_NAME) {
        this.UNIT_NAME = UNIT_NAME;
    }

    public Long getCORT_NUM_ID() {
        return CORT_NUM_ID;
    }

    public void setCORT_NUM_ID(Long CORT_NUM_ID) {
        this.CORT_NUM_ID = CORT_NUM_ID;
    }

    public String getCORT_NAME() {
        return CORT_NAME;
    }

    public void setCORT_NAME(String CORT_NAME) {
        this.CORT_NAME = CORT_NAME;
    }

    public Integer getUNIT_STATUS_NUM_ID() {
        return UNIT_STATUS_NUM_ID;
    }

    public void setUNIT_STATUS_NUM_ID(Integer UNIT_STATUS_NUM_ID) {
        this.UNIT_STATUS_NUM_ID = UNIT_STATUS_NUM_ID;
    }

    public String getCUS_NAME() {
        return CUS_NAME;
    }

    public void setCUS_NAME(String CUS_NAME) {
        this.CUS_NAME = CUS_NAME;
    }

    public String getCUS_TEL() {
        return CUS_TEL;
    }

    public void setCUS_TEL(String CUS_TEL) {
        this.CUS_TEL = CUS_TEL;
    }

    public String getEMERGENCY_CONT() {
        return EMERGENCY_CONT;
    }

    public void setEMERGENCY_CONT(String EMERGENCY_CONT) {
        this.EMERGENCY_CONT = EMERGENCY_CONT;
    }

    public String getEMERGENCY_CONT_TEL() {
        return EMERGENCY_CONT_TEL;
    }

    public void setEMERGENCY_CONT_TEL(String EMERGENCY_CONT_TEL) {
        this.EMERGENCY_CONT_TEL = EMERGENCY_CONT_TEL;
    }

    public Long getPRV_NUM_ID() {
        return PRV_NUM_ID;
    }

    public void setPRV_NUM_ID(Long PRV_NUM_ID) {
        this.PRV_NUM_ID = PRV_NUM_ID;
    }

    public Long getCITY_NUM_ID() {
        return CITY_NUM_ID;
    }

    public void setCITY_NUM_ID(Long CITY_NUM_ID) {
        this.CITY_NUM_ID = CITY_NUM_ID;
    }

    public Long getCITY_AREA_NUM_ID() {
        return CITY_AREA_NUM_ID;
    }

    public void setCITY_AREA_NUM_ID(Long CITY_AREA_NUM_ID) {
        this.CITY_AREA_NUM_ID = CITY_AREA_NUM_ID;
    }

    public Long getTOWN_NUM_ID() {
        return TOWN_NUM_ID;
    }

    public void setTOWN_NUM_ID(Long TOWN_NUM_ID) {
        this.TOWN_NUM_ID = TOWN_NUM_ID;
    }

    public String getADR() {
        return ADR;
    }

    public void setADR(String ADR) {
        this.ADR = ADR;
    }

    public String getLEGAL_BEHF() {
        return LEGAL_BEHF;
    }

    public void setLEGAL_BEHF(String LEGAL_BEHF) {
        this.LEGAL_BEHF = LEGAL_BEHF;
    }

    public String getBEHF_TELEPHONE() {
        return BEHF_TELEPHONE;
    }

    public void setBEHF_TELEPHONE(String BEHF_TELEPHONE) {
        this.BEHF_TELEPHONE = BEHF_TELEPHONE;
    }

    public String getBEHF_ID() {
        return BEHF_ID;
    }

    public void setBEHF_ID(String BEHF_ID) {
        this.BEHF_ID = BEHF_ID;
    }

    public String getBEHF_FAX() {
        return BEHF_FAX;
    }

    public void setBEHF_FAX(String BEHF_FAX) {
        this.BEHF_FAX = BEHF_FAX;
    }

    public String getBEHF_ADR() {
        return BEHF_ADR;
    }

    public void setBEHF_ADR(String BEHF_ADR) {
        this.BEHF_ADR = BEHF_ADR;
    }

    public String getORG_CODE() {
        return ORG_CODE;
    }

    public void setORG_CODE(String ORG_CODE) {
        this.ORG_CODE = ORG_CODE;
    }

    public String getREGISTERED_NAME() {
        return REGISTERED_NAME;
    }

    public void setREGISTERED_NAME(String REGISTERED_NAME) {
        this.REGISTERED_NAME = REGISTERED_NAME;
    }

    public String getTRADING_CERTIFICATES() {
        return TRADING_CERTIFICATES;
    }

    public void setTRADING_CERTIFICATES(String TRADING_CERTIFICATES) {
        this.TRADING_CERTIFICATES = TRADING_CERTIFICATES;
    }

    public String getTAX_ACCOUNT() {
        return TAX_ACCOUNT;
    }

    public void setTAX_ACCOUNT(String TAX_ACCOUNT) {
        this.TAX_ACCOUNT = TAX_ACCOUNT;
    }

    public String getTAXPAYER_TYPE() {
        return TAXPAYER_TYPE;
    }

    public void setTAXPAYER_TYPE(String TAXPAYER_TYPE) {
        this.TAXPAYER_TYPE = TAXPAYER_TYPE;
    }

    public String getREG_CAPITAL() {
        return REG_CAPITAL;
    }

    public void setREG_CAPITAL(String REG_CAPITAL) {
        this.REG_CAPITAL = REG_CAPITAL;
    }

    public String getREG_ADR() {
        return REG_ADR;
    }

    public void setREG_ADR(String REG_ADR) {
        this.REG_ADR = REG_ADR;
    }

    public String getBILLING_BANK() {
        return BILLING_BANK;
    }

    public void setBILLING_BANK(String BILLING_BANK) {
        this.BILLING_BANK = BILLING_BANK;
    }

    public String getBILLING_BANK_ACCOUNT() {
        return BILLING_BANK_ACCOUNT;
    }

    public void setBILLING_BANK_ACCOUNT(String BILLING_BANK_ACCOUNT) {
        this.BILLING_BANK_ACCOUNT = BILLING_BANK_ACCOUNT;
    }

    public Long getBANK_PRV_NUM_ID() {
        return BANK_PRV_NUM_ID;
    }

    public void setBANK_PRV_NUM_ID(Long BANK_PRV_NUM_ID) {
        this.BANK_PRV_NUM_ID = BANK_PRV_NUM_ID;
    }

    public Long getBANK_CITY_NUM_ID() {
        return BANK_CITY_NUM_ID;
    }

    public void setBANK_CITY_NUM_ID(Long BANK_CITY_NUM_ID) {
        this.BANK_CITY_NUM_ID = BANK_CITY_NUM_ID;
    }

    public String getCUS_CATEGORY() {
        return CUS_CATEGORY;
    }

    public void setCUS_CATEGORY(String CUS_CATEGORY) {
        this.CUS_CATEGORY = CUS_CATEGORY;
    }

    public Integer getCUS_LEVEL() {
        return CUS_LEVEL;
    }

    public void setCUS_LEVEL(Integer CUS_LEVEL) {
        this.CUS_LEVEL = CUS_LEVEL;
    }

    public String getCUS_AREA() {
        return CUS_AREA;
    }

    public void setCUS_AREA(String CUS_AREA) {
        this.CUS_AREA = CUS_AREA;
    }


    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public Integer getSETTLEMENT_CYCLE() {
        return SETTLEMENT_CYCLE;
    }

    public void setSETTLEMENT_CYCLE(Integer SETTLEMENT_CYCLE) {
        this.SETTLEMENT_CYCLE = SETTLEMENT_CYCLE;
    }

    public Integer getPAYMENT_DAYS() {
        return PAYMENT_DAYS;
    }

    public void setPAYMENT_DAYS(Integer PAYMENT_DAYS) {
        this.PAYMENT_DAYS = PAYMENT_DAYS;
    }

    public Integer getPAYMENT_METHOD() {
        return PAYMENT_METHOD;
    }

    public void setPAYMENT_METHOD(Integer PAYMENT_METHOD) {
        this.PAYMENT_METHOD = PAYMENT_METHOD;
    }
}
