package com.ykcloud.soa.omp.cmasterdata.dao;


import com.gb.soa.omp.ccommon.util.EntityFieldUtil;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.erp.common.entity.PageQueryResult;
import com.ykcloud.soa.omp.cmasterdata.api.model.PageQueryResultDTO;
import com.ykcloud.soa.omp.cmasterdata.api.request.MdmsB2bBlCsPolicyHdrRequest;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_B2B_BL_CS_POLICY_HDR;
import com.ykcloud.soa.omp.cmasterdata.util.MdDaoUtil;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @author
 * @date 2021/10/22 23:33
 */
@Repository
public class MdmsB2bCsPolicyHdrDao extends Dao<MDMS_B2B_BL_CS_POLICY_HDR> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_B2B_BL_CS_POLICY_HDR.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_B2B_BL_CS_POLICY_HDR.class, ",");
    private static final String TABLE_NAME = "MDMS_B2B_BL_CS_POLICY_HDR";

    public PageQueryResultDTO<MDMS_B2B_BL_CS_POLICY_HDR> pageQuery(MdmsB2bBlCsPolicyHdrRequest request) {
        PageQueryResultDTO<MDMS_B2B_BL_CS_POLICY_HDR> response = new PageQueryResultDTO<>();
        Long tenantNumId=request.getTenantNumId();
        Long dataSign=request.getDataSign();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT");
        sb.append(SQL_COLS);
        sb.append(" FROM ");
        sb.append(TABLE_NAME);
        sb.append(" WHERE 1=1");
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("org_unit",request.getORG_UNIT()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("channel_num_id",request.getCHANNEL_NUM_ID()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("reserved_no",request.getRESERVED_NO()));
        sb.append(MdDaoUtil.DATE_COL_SQL(null,"orderDate",request.getOrderDate1(),request.getOrderDate2()));
        sb.append(MdDaoUtil.TENANT_NUM_ID_DATA_SIGN_SQL);
        PageQueryResult<MDMS_B2B_BL_CS_POLICY_HDR> result = pageQuery(sb.toString(), request.getPageNum(), request.getPageSize(), MDMS_B2B_BL_CS_POLICY_HDR.class,
                MdDaoUtil.THUE_OBJECT(new Object[]{request.getORG_UNIT(),request.getCHANNEL_NUM_ID(),tenantNumId,dataSign}));
        response.setPageNum(result.getPageNum());
        response.setPageSize(result.getPageSize());
        response.setPageCount(result.getPageCount());
        response.setRecordCount(result.getRecordCount());
        response.setData(result.getData());
        return response;
    }

    public MDMS_B2B_BL_CS_POLICY_HDR getMdmsB2bCsPolicyHdrEntity(Long tenantNumId, Long dataSign, String series) {
        String sql = "select * from MDMS_B2B_BL_CS_POLICY_HDR where " +
                " tenant_num_id = ? and data_sign = ? and series = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, MDMS_B2B_BL_CS_POLICY_HDR.class);
    }

    public int deleteMdmsB2bCsPolicyHdr(Long tenantNumId, Long dataSign,String series){
        String sql = "delete from MDMS_B2B_BL_CS_POLICY_HDR WHERE tenant_num_id = ? and data_sign = ? and series = ? ";
        return jdbcTemplate.update(sql, new Object[]{tenantNumId, dataSign, series});
    }






}
