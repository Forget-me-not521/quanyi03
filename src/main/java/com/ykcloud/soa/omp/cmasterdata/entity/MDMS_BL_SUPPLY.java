package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class MDMS_BL_SUPPLY extends BaseEntity {

    private String RESERVED_NO;

    private String SUPPLY_NAME;

    private Integer PARTNER_ROLE_GROUP;

    private Integer SUPPLY_CLASSIFY;

    private String MNEMONIC_CODE;

    private String FINANCE_NAME;

    private String FINANCE_TEL;

    private String ADR;

    private Integer SUPPLY_TYPE;

    private String BUSINESS_SCOPE;

    private String BUSINESS_PERMIT;

    private String PRODUCTION_SCOPE;

    private String UNIFIED_SOCIAL_CREDIT_CODE;

    private String REGISTER_PLACE;

    private Date REGISTER_DATE;

    private Integer ABC_MARK;

    private String TAX_ACCOUNT;

    private Integer BASIC_BANK_NUM;

    private String BASIC_BANK_NAME;

    private String BASIC_BANK_ACCOUNT;

    private Integer SUPPLY_STATUS;

    private Integer GOVERNMENT_RELATED_SUPPLIER;

    private Integer POTENTIAL_LICHONG_SUPPLIER;

    private String GOVERNMENT_RELATED_SUPPLIER_REMARK;

    private String POTENTIAL_LICHONG_SUPPLIER_REMARK;

    private Double REGISTERED_CAPITAL;

    private Double PAID_IN_CAPITAL;

    private String OLD_SUPPLY_ID;
    private String FILE_NO;
    private int RECEIVING_BANK_NUM;

    private String RECEIVING_BANK_NAME;

    private String RECEIVING_BANK_ACCOUNT;
    //--------------------分部---------------------------------
    private Integer CONTROL_SUBJECT;//统驭科目（下拉选择）',
    private String REMARK;//备注',
    private Integer MONEY_TYPE;//币别',
    private String PURCHASE_LEADER;//采购负责人',
    private Integer PAYMENT_CONDITION;//付款条件（下拉选择）',
    private Integer CLOSE_DAYS;//关闭天数',
    private Integer FORBID_PURCHASE;//禁采(1-是,0-否)',
    private Integer FORBID_RETURN_FACTORY;//禁退厂(1-是,0-否)',
    private Integer FORBID_DISTRIBUTION;//禁配(1-是,0-否)',
    private Integer FORBID_RETURN_STORE;//禁退仓(1-是,0-否)',
    private Integer FORBID_SALE;//禁售(1-是,0-否)',
    private Integer FORBID_PAYMENT;//禁付款(1-是,0-否)',
    private Integer PRE_DISTRIBUTION;//送货前置期（天）',
    private Integer DIRECT_DELIVERY_FLAG;//直配标志(1-是,0-否)',
    private String SNNUAL_ANNOUNCEMEN;//年度公示',
    private String STORAGE_ADR;//仓库地址',
    private String CORPORAT_PROXY_STAT_IDCARD;//法人委托人的身份证号码',
    private Date CORPORAT_PROXY_IDCARD_BEGIN;//法人委托人的身份证有效期开始',
    private Date CORPORAT_PROXY_IDCARD_END;//法人委托人的身份证有效期结束',
    private Integer SUB_SUPPLY_STATUS;//分部供应商状态ID',
    private String CORPORAT;//法人',
    private String CORPORAT_PROXY_NAME;//法人委托人姓名',
    private Date CORPORAT_PROXY_STAT_BEGIN;//法人委托书效期开始',
    private Date CORPORAT_PROXY_STAT_END;//法人委托书效期结束',
    private String QUALITY_LEADER;//质量负责人',
    private String BUSINESS_CONTACT;//业务联系人',
    private String BUSINESS_CONTACT_TEL;//业务联系人电话',
    private String BUSINESS_CONTACT_IDCARD;//业务联系人身份证号码',
    private Date SPDA_RECORD_BEGIN;//省药监局备案效期开始',
    private Date SPDA_RECORD_END;//省药监局备案效期结束',
    private String SPDA_RECORD_STAFF;//省药监局备案业务员',
    private String SPDA_RECORD_IDCARD;//省药监局备案业务员身份证号码',
    private Date BUSINESS_LICENSE_BEGIN;//营业执照有效期',
    private Date BUSINESS_LICENSE_END;//营业执照有效期',
    private Date QUALITY_AGREEMENT_BEGIN;//质量协议书有效期开始',
    private Date QUALITY_AGREEMENT_END;//质量协议书有效期结束',
    private Integer LICENCE_WARN_DAYS;//证照预警天数',
    private String REPLENISHER;//补货员',
    private String DRUG_BUSINESS_LICENSE;//药品经营许可证',
    private Date DRUG_BUSINESS_LICENSE_BEGIN;//药品经营许可证有效期',
    private Date DRUG_BUSINESS_LICENSE_END;//药品经营许可证有效期',
    private String DRUG_PRODUCT_LICENSE;//药品生产许可证',
    private Date DRUG_PRODUCT_LICENSE_BEGIN;//药品生产许可证有效期开始',
    private Date DRUG_PRODUCT_LICENSE_END;//药品生产许可证有效期结束',
    private String HYGIENE_LICENSE_SCOPE;//消毒产品生产企业卫生许可证生产范围',
    private Date HYGIENE_LICENSE_BEGIN;//消毒产品生产企业卫生许可证有效期',
    private Date HYGIENE_LICENSE_END;//消毒产品生产企业卫生许可证有效期',
    private String MEDICAL_DEVICE_PRO_LINCENSE;//医疗器械生产许可证',
    private String MEDICAL_DEVICE_SCOPE;//医疗器械生产范围',
    private Date MEDICAL_DEVICE_BEGIN;//医疗器械生产许可证有效期开始',
    private Date MEDICAL_DEVICE_END;//医疗器械生产许可证有效期结束',
    private String MEDICAL_DEVICE_PRO_REC1;//一类医疗器械生产备案',
    private String MEDICAL_DEVICE_PRO_REC1_SCOPE;//一类医疗器械生产备案范围',
    private String MEDICAL_DEVICE_PRO_REC2;//二类医疗器械经营备案',
    private String MEDICAL_DEVICE_PRO_REC2_SCOPE;//二类医疗器械经营备案范围',
    private String MEDICAL_DEVICE_BUS_LINCENSE;//医疗器械经营许可证',
    private String MEDICAL_DEVICE_BUS_SCOPE;//医疗器械经营范围',
    private Date MEDICAL_DEVICE_BUS_BEGIN;//医疗器械经营许可证有效期开始',
    private Date MEDICAL_DEVICE_BUS_END;//医疗器械经营许可证有效期结束',
    private String FOOD_BUS_LICENSE;//食品经营许可证',
    private String FOOD_BUS_LICENSE_SCOPE;//食品经营许可证范围',
    private Date FOOD_BUS_LICENSE_BEGIN;//食品经营许可证有效期开始',
    private Date FOOD_BUS_LICENSE_END;//食品经营许可证有效期结束',
}