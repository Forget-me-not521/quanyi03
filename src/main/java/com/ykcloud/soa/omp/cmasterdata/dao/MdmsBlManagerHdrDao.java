package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.api.model.ManagerHdrAndCompany;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsBlManagerHdr;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_MANAGER_HDR;
import com.ykcloud.soa.omp.cmasterdata.service.model.ManagerHdrInfo;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

/**
 * @Author Hewei
 * @Date 2018/6/27 16:43
 */
@Repository
public class MdmsBlManagerHdrDao {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_BL_MANAGER_HDR.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_BL_MANAGER_HDR.class, ",");
    private static final String TABLE_NAME = "MDMS_BL_MANAGER_HDR";

    public void updateStatusNumIdByReservedNo(Long tenantNumId, Long dataSign, Long reservedNo) {
        String sql = "UPDATE MDMS_BL_MANAGER_HDR SET STATUS_NUM_ID = 3 WHERE TENANT_NUM_ID = ? and DATA_SIGN = ? AND RESERVED_NO = ? and STATUS_NUM_ID=2";
        if (jdbcTemplate.update(sql, new Object[]{tenantNumId, dataSign, reservedNo}) < 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "DM单号" + reservedNo + "更新STATUS_NUM_ID = 3失败!");
        }
    }

    public MDMS_BL_MANAGER_HDR selectEntity(Long tenantNumId, Long dataSign, String reservedNo) {
        String sql = "select * from MDMS_BL_MANAGER_HDR WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  RESERVED_NO = ?";
        List<MDMS_BL_MANAGER_HDR> mdmsBlManagerHdrList = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(MDMS_BL_MANAGER_HDR.class));
        if (CollectionUtils.isEmpty(mdmsBlManagerHdrList)) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "不存在系统单号为" + reservedNo + "的变更单表单数据！");
        } else if (mdmsBlManagerHdrList.size() > 1) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "存在多笔系统单号为" + reservedNo + "的变更单表单数据！");
        }
        return mdmsBlManagerHdrList.get(0);
    }

    public ManagerHdrInfo getStatusNumIdAndTypeNumIdNotMust(Long tenantNumId, Long dataSign, Long reservedNo) {
        String sql = "select status_num_id,type_num_id from mdms_bl_manager_hdr where tenant_num_id=? and data_sign=? and reserved_no=? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(ManagerHdrInfo.class));
    }

    public void insertMdmsBlManagerHdrDao(MDMS_BL_MANAGER_HDR mdms_bl_manager_hdr){
        StringBuilder sb = new StringBuilder();
        sb.append("insert into ");
        sb.append(TABLE_NAME);
        sb.append("(");
        sb.append(SQL_COLS);
        sb.append(") values (");
        sb.append(WILDCARDS);
        sb.append(")");
        int row = jdbcTemplate.update(sb.toString(), EntityFieldUtil.fieldSplitValue(MDMS_BL_MANAGER_HDR.class, mdms_bl_manager_hdr));
        if (row <= 0) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35002, "插入失败！");
        }
    }

    public void updateRemarkByReservedNo(Long tenantNumId, Long dataSign, String reservedNo, String remark, Long userNumId, Date updateDate) {
        String sql = "UPDATE MDMS_BL_MANAGER_HDR SET REMARK = ?, LAST_UPDATE_USER_ID = ?, LAST_UPDTME = ? WHERE TENANT_NUM_ID = ? and DATA_SIGN = ? AND RESERVED_NO = ?";
        if (jdbcTemplate.update(sql, new Object[]{remark, userNumId, updateDate, tenantNumId, dataSign, reservedNo}) < 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "DM单号" + reservedNo + "更新REMARK = " + remark + "失败!");
        }
    }

    public void updateManagerCompanyHdrStatus(Long tenantNumId, Long dataSign, String reservedNo, Integer statusNumId, Long userNumId, Date updateDate) {
        String sql = "UPDATE MDMS_BL_MANAGER_HDR SET STATUS_NUM_ID = ?, LAST_UPDATE_USER_ID = ?, LAST_UPDTME = ? WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  RESERVED_NO = ?";
        int row = jdbcTemplate.update(sql, statusNumId, userNumId, updateDate, tenantNumId, dataSign, reservedNo);
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "DM单号" + reservedNo + "更新STATUS_NUM_ID = " + statusNumId + "失败!");
        }
    }

    public ManagerHdrAndCompany getManagerHdrAndCompany(Long tenantNumId, Long dataSign, String reservedNo) {
        String sql = "select a.*, b.* from MDMS_BL_MANAGER_HDR a left join MDMS_BL_CORT b on a.RESERVED_NO = b.RESERVED_NO where a.tenant_num_id=? and a.data_sign=? and a.reserved_no=? and a.cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(ManagerHdrAndCompany.class));
    }

    public MdmsBlManagerHdr selectEntityByReservedNo(Long tenantNumId, Long dataSign, String reservedNo) {
        String sql = "select * from MDMS_BL_MANAGER_HDR WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  RESERVED_NO = ?";
        List<MdmsBlManagerHdr> mdmsBlManagerHdrList = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, reservedNo}, new BeanPropertyRowMapper<>(MdmsBlManagerHdr.class));
        if (CollectionUtils.isEmpty(mdmsBlManagerHdrList)) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "不存在系统单号为" + reservedNo + "的变更单表单数据！");
        } else if (mdmsBlManagerHdrList.size() > 1) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "存在多笔系统单号为" + reservedNo + "的变更单表单数据！");
        }
        return mdmsBlManagerHdrList.get(0);
    }
}
