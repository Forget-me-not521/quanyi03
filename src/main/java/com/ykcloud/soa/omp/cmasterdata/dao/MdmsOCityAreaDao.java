package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOCityArea;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_CITY_AREA;
import com.ykcloud.soa.omp.cmasterdata.service.model.AdministrativeRegionQueryCondition;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

import javax.annotation.Resource;

@Repository
public class MdmsOCityAreaDao {

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_CITY_AREA.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_CITY_AREA.class, ",");
    private static final String TABLE_NAME = "mdms_o_city_area";

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    public Long getCityAreaNumId(Long tenantNumId, Long dataSign, Long cityAreaNumId) {
        String sql = "select count(*) from mdms_o_city_area  " +
                "where tenant_num_id=? and data_sign=? and city_area_num_id = ? ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, cityAreaNumId}, Long.class);
    }

    public Long getCityAreaNumIdMate(Long tenantNumId, Long dataSign, Long cityAreaNumId, Long cityNumId) {
        String sql = "select count(*) from mdms_o_city_area  " +
                "where tenant_num_id=? and data_sign=? and city_area_num_id = ? and city_num_id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, cityAreaNumId, cityNumId}, Long.class);
    }

    public int selectExists(Long tenantNumId, Long dataSign, Long cityAreaNumId) {
        return jdbcTemplate.queryForObject("select count(*) from mdms_o_city_area where tenant_num_id=? and data_sign=? and city_area_num_id=? and cancelsign='N'", new Object[]{tenantNumId, dataSign, cityAreaNumId}, Integer.class);
    }

    public boolean checkExistCityArea(Long tenantNumId, Long dataSign, Long prvNumId, Long cityNumId, Long cityAreaNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1)");
        sb.append(" from ");
        sb.append(" mdms_o_city_area ");
        sb.append(" where tenant_num_id=? and data_sign=? and prv_num_id=? and city_num_id = ? and city_area_num_id = ? and cancelsign='N'");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, prvNumId, cityNumId, cityAreaNumId}, Integer.class) == 0 ? true : false;
    }

    public int insertmdmsOCityArea(MDMS_O_CITY_AREA area) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into ");
        sb.append(TABLE_NAME);
        sb.append(" ( ");
        sb.append(SQL_COLS);
        sb.append(") values (");
        sb.append(WILDCARDS);
        sb.append(")");
        System.out.println(sb.toString());
        System.out.println(Arrays.asList(EntityFieldUtil.fieldSplitValue(MDMS_O_CITY_AREA.class, area)));

        return jdbcTemplate.update(sb.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_CITY_AREA.class, area));
    }

    public Long checkExistSeries(String series, Long tenantNumId, Long dataSign) {

        String sql = "select city_area_num_id from mdms_o_city_area where SERIES = ? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        Long cityAreaNumId = jdbcTemplate.queryForObject(sql, Long.class, new Object[]{series, tenantNumId, dataSign});
        return cityAreaNumId;

    }

    public int updateMdmsOCityArea(Long cityAreaNumId, Long cityNumId, Long prvNumId, String cityAreaName,
                                   String cityAreaSimNo, Long userNumId, String series, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city_area set city_area_num_id=?,city_num_id=?,prv_num_id=?,city_area_name=?,city_area_sim_no=?,last_update_user_id=? ,last_updtme = now(),updatedata='Y' where series =? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.update(sql, new Object[]{cityAreaNumId, cityNumId, prvNumId, cityAreaName, cityAreaSimNo, userNumId, series, tenantNumId, dataSign});
    }

    public int deteteMdmsOCityAreaBySeries(Long userNumId, String series, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city_area set cancelsign ='Y' ,last_update_user_id= ?,last_updtme=now() where series= ? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.update(sql, new Object[]{userNumId, series, tenantNumId, dataSign});
    }

    public MDMS_O_CITY_AREA getMdmsOCityAreaBySeries(String series, Long tenantNumId, Long dataSign) {
        String sql = "select * from mdms_o_city_area where SERIES = ? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(MDMS_O_CITY_AREA.class), new Object[]{series, tenantNumId, dataSign});
    }

    public List<Long> getAreaIdListByCityAndPrv(Long city_NUM_ID, Long prv_NUM_ID, Long tenantNumId, Long dataSign) {
        String sql = "select city_area_num_id from mdms_o_city_area where city_num_id = ? and prv_num_id =? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Long.class), new Object[]{city_NUM_ID, prv_NUM_ID, tenantNumId, dataSign});
    }

    public int deleteByCityAndPrv(Long userNumId, Long city_NUM_ID, Long prv_NUM_ID, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city_area set cancelsign ='Y' ,last_update_user_id= ?,last_updtme=now() where city_num_id =? and prv_num_id =? and tenant_num_id=? and data_sign=? and cancelsign ='N' ";
        return jdbcTemplate.update(sql, new Object[]{userNumId, city_NUM_ID, prv_NUM_ID, tenantNumId, dataSign});
    }

    public int deleteByprvId(Long userNumId, Long prvId, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_city_area set cancelsign ='Y' ,last_update_user_id= ?,last_updtme=now() where prv_num_id =? and tenant_num_id=? and data_sign=? and cancelsign ='N' ";
        return jdbcTemplate.update(sql, new Object[]{userNumId, prvId, tenantNumId, dataSign});
    }

    public int checkExistAreabyCityIdAndPrvNumId(Long city_NUM_ID, Long prv_NUM_ID, Long tenantNumId, Long dataSign) {
        String sql = "select count(1) from mdms_o_city_area where city_num_id=? and prv_num_id=? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{city_NUM_ID, prv_NUM_ID, tenantNumId, dataSign});
    }

    public Integer countCityAreaInfo(Long tenantNumId, Long dataSign, AdministrativeRegionQueryCondition condition) {
        StringBuilder sql = new StringBuilder();
        sql.append("select count(1) " +
                " from mdms_o_city_area area " +
                " LEFT JOIN mdms_o_city city " +
                "	on area.tenant_num_id = city.tenant_num_id " +
                "	AND area.data_sign = city.data_sign " +
                "	and area.cancelsign = city.cancelsign " +
                "	and area.city_num_id = city.city_num_id" +
                " LEFT JOIN mdms_o_province prv " +
                "	on area.tenant_num_id = prv.tenant_num_id " +
                "	AND area.data_sign = prv.data_sign " +
                "	and area.cancelsign = prv.cancelsign " +
                "	and area.prv_num_id = prv.prv_num_id " +
                " where  " +
                "area.tenant_num_id = :tenantNumId " +
                "AND area.data_sign = :dataSign " +
                "AND area.cancelsign = 'N'");

        Map<String, Object> params = new HashMap<>();
        params.put("tenantNumId", tenantNumId);
        params.put("dataSign", dataSign);

        if (Objects.nonNull(condition.getPrvNumId())) {
            sql.append(" AND area.prv_num_id = :prvNumId ");
            params.put("prvNumId", condition.getPrvNumId());
        }
        if (Objects.nonNull(condition.getCityNumId())) {
            sql.append(" AND area.city_num_id = :cityNumId ");
            params.put("cityNumId", condition.getCityNumId());

        }
        if (Objects.nonNull(condition.getCityAreaNumId())) {
            sql.append(" and area.city_area_num_id = :cityAreaNumId ");
            params.put("cityAreaNumId", condition.getCityAreaNumId());
        }
        if (Objects.nonNull(condition.getCityAreaSimNo())) {
            sql.append(" AND locate(:cityAreaSimNo, area.city_area_sim_no) > 0 ");
            params.put("cityAreaSimNo", condition.getCityAreaSimNo());
        }
        if (Objects.nonNull(condition.getCityName())) {
            sql.append(" AND locate(:cityName, city.city_name) > 0 ");
            params.put("cityName", condition.getCityName());
        }
        if (Objects.nonNull(condition.getPrvName())) {
            sql.append(" AND locate(:prvName, prv.prv_name) > 0 ");
            params.put("prvName", condition.getPrvName());
        }
        if (Objects.nonNull(condition.getCityAreaName())) {
            sql.append(" AND locate(:cityAreaName, area.city_area_name) > 0 ");
            params.put("cityAreaName", condition.getCityAreaName());
        }

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.TYPE);
    }

    public List<MdmsOCityArea> queryCityAreaInfo(Long tenantNumId, Long dataSign, AdministrativeRegionQueryCondition condition, Integer start, Integer pageSize) {
        StringBuilder sql = new StringBuilder();
        sql.append("select prv.prv_num_id, prv.prv_name, city.city_num_id, city.city_name, area.city_area_num_id, area.city_area_name, area.city_area_sim_no, area.create_dtme, area.create_user_id, area.last_update_user_id, area.last_updtme " +
                "from mdms_o_city_area area " +
                "LEFT JOIN mdms_o_city city " +
                "	on area.tenant_num_id = city.tenant_num_id AND area.data_sign = city.data_sign and area.cancelsign = city.cancelsign and area.city_num_id = city.city_num_id " +
                "LEFT JOIN mdms_o_province prv " +
                "	on area.tenant_num_id = prv.tenant_num_id AND area.data_sign = prv.data_sign and area.cancelsign = prv.cancelsign and area.prv_num_id = prv.prv_num_id " +
                "where  " +
                "area.tenant_num_id = :tenantNumId " +
                "AND area.data_sign = :dataSign " +
                "AND area.cancelsign = 'N' ");

        Map<String, Object> params = new HashMap<>();
        params.put("tenantNumId", tenantNumId);
        params.put("dataSign", dataSign);

        if (Objects.nonNull(condition.getPrvNumId())) {
            sql.append(" AND area.prv_num_id = :prvNumId ");
            params.put("prvNumId", condition.getPrvNumId());
        }
        if (Objects.nonNull(condition.getCityNumId())) {
            sql.append(" AND area.city_num_id = :cityNumId ");
            params.put("cityNumId", condition.getCityNumId());

        }
        if (Objects.nonNull(condition.getCityAreaNumId())) {
            sql.append(" and area.city_area_num_id = :cityAreaNumId ");
            params.put("cityAreaNumId", condition.getCityAreaNumId());
        }
        if (Objects.nonNull(condition.getCityAreaSimNo())) {
            sql.append(" AND locate(:cityAreaSimNo, area.city_area_sim_no) > 0 ");
            params.put("cityAreaSimNo", condition.getCityAreaSimNo());
        }
        if (Objects.nonNull(condition.getCityName())) {
            sql.append(" AND locate(:cityName, city.city_name) > 0 ");
            params.put("cityName", condition.getCityName());
        }
        if (Objects.nonNull(condition.getPrvName())) {
            sql.append(" AND locate(:prvName, prv.prv_name) > 0 ");
            params.put("prvName", condition.getPrvName());
        }
        if (Objects.nonNull(condition.getCityAreaName())) {
            sql.append(" AND locate(:cityAreaName, area.city_area_name) > 0 ");
            params.put("cityAreaName", condition.getCityAreaName());
        }

        sql.append(" limit :start, :pageSize");
        params.put("start", start);
        params.put("pageSize", pageSize);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(MdmsOCityArea.class));

    }

}
