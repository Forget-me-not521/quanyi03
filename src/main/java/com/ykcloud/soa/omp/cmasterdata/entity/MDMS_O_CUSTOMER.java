package com.ykcloud.soa.omp.cmasterdata.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @ author
 * @date: 2021年09月10日19:46
 * @describtion:
 */
@Data
public class MDMS_O_CUSTOMER extends BaseEntity{
    private String CUSTOMER_NUM_ID;//客户编码
    private String CUSTOMER_NAME;//客户名称',
    private String CUSTOMER_SIM_NAME;//客户简称',
    private String MNEMONIC_CODE;//助记码',
    private String POSTAL_CODE;//邮政编码',
    private String CONTACT_NUM;//联系电话',
    private Integer CUSTOMER_CLASSIFY;//客户分类(零售企业,批发企业,盈利性医疗机构,非盈利性医疗机构，单体药店,其他)',
    private String ADR;//联系地址',
    private String DEFAULT_HARVEST_ADR;//默认收获地址',
    private String BUSINESS_SCOPE;//经营范围',
    private Integer BUSINESS_PERMIT;//经营许可',

    private String UNIFIED_SOCIAL_CREDIT_CODE;//统一社会信用代码',
    private Integer CUSTOMER_STATUS;//客户状态-总部',
    private Date REGISTR_BEGIN_DATE;//注册日期',
    private Date REGISTR_END_DATE;//注册日期',
    private String REGISTER_PLACE;//注册地点',

    private String TAX_ACCOUNT;//税号',
    private Long PRV_NUM_ID;//省',
    private Long CITY_NUM_ID;//市',
    private Long CITY_AREA_NUM_ID;//县',
    private Long TOWN_NUM_ID;//镇',
    private Double CREDIT_LINE;//授信额度',
    private Double REGISTERED_CAPITAL;//注册资本',
    private Double PAID_IN_CAPITAL;//实缴资本',
    private Integer ABC_MARK;//ABC标识（下拉选择，ABC）',
    private Integer BASIC_BANK_NUM;//基本户银行联行号',
    private String BASIC_BANK_NAME;//基本户银行名称',
    private String BASIC_BANK_ACCOUNT;//基本户银行账号',
    private Integer GOVERNMENT_RELATED_CUSTOMER;//政府相关客户(是否)',
    private Integer POTENTIAL_LICHONG_CUSTOMER;//潜在利冲客户(是否)',
    private String GOVERNMENT_RELATED_CUSTOMER_REMARK;//政府相关客户-备注',
    private String POTENTIAL_LICHONG_CUSTOMER_REMARK;//潜在利冲客户-备注',
    private int IS_MONOMER_DRUGSTORE;//是否单体药店
}
