package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

@Data
public class MDMS_W_ROUTE_HDR extends BaseEntity{

    //公司
    private String CORT_NUM_ID;

    //门店
    private String SUB_UNIT_NUM_ID;

    //大仓编码
    private String PHYSICAL_NUM_ID;

    //装运点编号
    private String SHIPMENT_NUM_ID;

    //装运点名称
    private String SHIPMENT_NAME;

    //路线id
    private String ROUTE_NUM_ID;

    //路线名称
    private String ROUTE_NAME;

    //1-7 周一到周日
    private Long DAY_SIGN;

    //备注
    private String REMARK;
}
