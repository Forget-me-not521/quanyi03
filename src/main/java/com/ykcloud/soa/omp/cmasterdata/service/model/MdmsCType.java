package com.ykcloud.soa.omp.cmasterdata.service.model;

public class MdmsCType {

    private int typeNumId;

    private String typeName;


    public int getTypeNumId() {
        return typeNumId;
    }

    public void setTypeNumId(int typeNumId) {
        this.typeNumId = typeNumId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
