package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class COMMON_QUERY {

    private String SERIES;
    private String SQL_NAME;
    private String SQL_ID;
    private String SQL_CONTENT;
    private String PARAM_CONTENT;
    private String JDBC_NAME;
    private Date CREATE_DTME;
    private Date LAST_UPDTME;
    private Long CREATE_USER_ID;
    private Long LAST_UPDATE_USER_ID;
    private String CANCEL_SIGN;
    private Long TENANT_NUM_ID;
    private Long DATA_SIGN;
    private String DB_TYPE;
    private String ANNOTATE_PREFIX;
    private String SUB_SQL_ID;
    private String NO_DATA_EXCEPTION;
    private Integer CACHE_SIGN;
    private String METHOD_NAME;
    private Long CACHE_LIVE_TIME;
    private String RETURN_HANDLE_CONTENT;
    private Long _DBLE_OP_TIME;
    private String CANCELSIGN;
    private String REMARK;

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String sERIES) {
        SERIES = sERIES;
    }

    public String getSQL_NAME() {
        return SQL_NAME;
    }

    public void setSQL_NAME(String sQL_NAME) {
        SQL_NAME = sQL_NAME;
    }

    public String getSQL_ID() {
        return SQL_ID;
    }

    public void setSQL_ID(String sQL_ID) {
        SQL_ID = sQL_ID;
    }

    public String getSQL_CONTENT() {
        return SQL_CONTENT;
    }

    public void setSQL_CONTENT(String sQL_CONTENT) {
        SQL_CONTENT = sQL_CONTENT;
    }

    public String getPARAM_CONTENT() {
        return PARAM_CONTENT;
    }

    public void setPARAM_CONTENT(String pARAM_CONTENT) {
        PARAM_CONTENT = pARAM_CONTENT;
    }

    public String getJDBC_NAME() {
        return JDBC_NAME;
    }

    public void setJDBC_NAME(String jDBC_NAME) {
        JDBC_NAME = jDBC_NAME;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date cREATE_DTME) {
        CREATE_DTME = cREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date lAST_UPDTME) {
        LAST_UPDTME = lAST_UPDTME;
    }

    public Long getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(Long cREATE_USER_ID) {
        CREATE_USER_ID = cREATE_USER_ID;
    }

    public Long getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(Long lAST_UPDATE_USER_ID) {
        LAST_UPDATE_USER_ID = lAST_UPDATE_USER_ID;
    }

    public String getCANCEL_SIGN() {
        return CANCEL_SIGN;
    }

    public void setCANCEL_SIGN(String cANCEL_SIGN) {
        CANCEL_SIGN = cANCEL_SIGN;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long tENANT_NUM_ID) {
        TENANT_NUM_ID = tENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long dATA_SIGN) {
        DATA_SIGN = dATA_SIGN;
    }

    public String getDB_TYPE() {
        return DB_TYPE;
    }

    public void setDB_TYPE(String dB_TYPE) {
        DB_TYPE = dB_TYPE;
    }

    public String getANNOTATE_PREFIX() {
        return ANNOTATE_PREFIX;
    }

    public void setANNOTATE_PREFIX(String aNNOTATE_PREFIX) {
        ANNOTATE_PREFIX = aNNOTATE_PREFIX;
    }

    public String getSUB_SQL_ID() {
        return SUB_SQL_ID;
    }

    public void setSUB_SQL_ID(String sUB_SQL_ID) {
        SUB_SQL_ID = sUB_SQL_ID;
    }

    public String getNO_DATA_EXCEPTION() {
        return NO_DATA_EXCEPTION;
    }

    public void setNO_DATA_EXCEPTION(String nO_DATA_EXCEPTION) {
        NO_DATA_EXCEPTION = nO_DATA_EXCEPTION;
    }

    public Integer getCACHE_SIGN() {
        return CACHE_SIGN;
    }

    public void setCACHE_SIGN(Integer cACHE_SIGN) {
        CACHE_SIGN = cACHE_SIGN;
    }

    public String getMETHOD_NAME() {
        return METHOD_NAME;
    }

    public void setMETHOD_NAME(String mETHOD_NAME) {
        METHOD_NAME = mETHOD_NAME;
    }

    public Long getCACHE_LIVE_TIME() {
        return CACHE_LIVE_TIME;
    }

    public void setCACHE_LIVE_TIME(Long cACHE_LIVE_TIME) {
        CACHE_LIVE_TIME = cACHE_LIVE_TIME;
    }

    public String getRETURN_HANDLE_CONTENT() {
        return RETURN_HANDLE_CONTENT;
    }

    public void setRETURN_HANDLE_CONTENT(String rETURN_HANDLE_CONTENT) {
        RETURN_HANDLE_CONTENT = rETURN_HANDLE_CONTENT;
    }

    public Long get_DBLE_OP_TIME() {
        return _DBLE_OP_TIME;
    }

    public void set_DBLE_OP_TIME(Long _DBLE_OP_TIME) {
        this._DBLE_OP_TIME = _DBLE_OP_TIME;
    }

    public String getCANCELSIGN() {
        return CANCELSIGN;
    }

    public void setCANCELSIGN(String cANCELSIGN) {
        CANCELSIGN = cANCELSIGN;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String rEMARK) {
        REMARK = rEMARK;
    }

}
