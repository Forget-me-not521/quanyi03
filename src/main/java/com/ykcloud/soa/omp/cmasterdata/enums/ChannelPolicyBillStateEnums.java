package com.ykcloud.soa.omp.cmasterdata.enums;

public enum ChannelPolicyBillStateEnums {
    CREATE(1, "未确认"),
    COMMIT(2, "确认"),
    SUBMIT(3, "提交"),
    REJECT(4, "驳回"),
    APPROVAL(5, "审批中"),
    COMPLETED(6, "已完成");
    private int id;
    private String value;

    ChannelPolicyBillStateEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (ProcessMasterBillStatusEnums item : ProcessMasterBillStatusEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
