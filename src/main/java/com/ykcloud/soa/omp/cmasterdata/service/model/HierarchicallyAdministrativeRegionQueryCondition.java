package com.ykcloud.soa.omp.cmasterdata.service.model;

public class HierarchicallyAdministrativeRegionQueryCondition {
    private String regionName;

    private String regionSimNo;

    private Long regionNumId;

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionSimNo() {
        return regionSimNo;
    }

    public void setRegionSimNo(String regionSimNo) {
        this.regionSimNo = regionSimNo;
    }

    public Long getRegionNumId() {
        return regionNumId;
    }

    public void setRegionNumId(Long regionNumId) {
        this.regionNumId = regionNumId;
    }
}
