package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.ExceptionUtil;
import com.gb.soa.omp.ccommon.util.JsonUtil;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsB2bSubRelation;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOCustomerShop;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOSubCustomer;
import com.ykcloud.soa.omp.cmasterdata.api.request.*;
import com.ykcloud.soa.omp.cmasterdata.api.response.*;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdmsCustomerArchivesService;
import com.ykcloud.soa.omp.cmasterdata.dao.MdmsCustomerArchivesQueryDao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_B2B_SUB_RELATION;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_CUSTOMER_SHOP;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUB_CUSTOMER;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import feign.Param;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author wangcong
 * @date 2021/11/2 14:16
 */
@Service("mdmsCustomerArchivesService")
@RestController
@Slf4j
public class MdmsCustomerArchivesServiceImpl implements MdmsCustomerArchivesService {
    @Resource
    MdmsCustomerArchivesQueryDao mdmsCustomerArchivesQueryDao;

    @Override
    public MdmsCustomerArchivesListResponse getCustomerArchivesListPage(MdmsCustomerArchivesListRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCustomerArchivesListPage request:{}", JsonUtil.toJson(request));
        }
        MdmsCustomerArchivesListResponse response = new MdmsCustomerArchivesListResponse();
        try {
            List<MdmsOSubCustomer> rows = new ArrayList<>();
            List<MDMS_O_SUB_CUSTOMER> customers = mdmsCustomerArchivesQueryDao.selectMdmsOSubCustomerListPage(
                    request.getTenantNumId(),
                    request.getDataSign(),
                    request.getSaleOrg(),
                    request.getDistChannel(),
                    request.getCustomerNumId(),
                    request.getValidContract(),
                    request.getFirstSaleDate(),
                    request.getPageNum(),
                    request.getPageSize());
            int rowCount = mdmsCustomerArchivesQueryDao.selectMdmsOSubCustomerListPageTotal(
                    request.getTenantNumId(),
                    request.getDataSign(),
                    request.getSaleOrg(),
                    request.getDistChannel(),
                    request.getCustomerNumId(),
                    request.getValidContract(),
                    request.getFirstSaleDate());

            if (customers != null) {
                customers.forEach(r -> {
                    rows.add(entity2Model(r));
                });
            }
            response.setRows(rows);
            response.setTotal(rowCount);
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerArchivesListPage request:{}", JsonUtil.toJson(request));
        }
        return response;
    }

    @Override
    public MdmsCustomerArchivesResponse getCustomerArchives(MdmsCustomerArchivesRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCustomerArchives request:{}", JsonUtil.toJson(request));
        }
        MdmsCustomerArchivesResponse response = new MdmsCustomerArchivesResponse();
        try {
            response.setCustomerInfo(entity2Model(mdmsCustomerArchivesQueryDao.selectMdmsOSubCustomer(request.getTenantNumId(),
                    request.getDataSign(), request.getSeries())));
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerArchives response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsCustomerShopListResponse getCustomerShopListPage(MdmsCustomerShopListRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCustomerShopListPage request:{}", JsonUtil.toJson(request));
        }
        MdmsCustomerShopListResponse response = new MdmsCustomerShopListResponse();
        try {
            List<MdmsOCustomerShop> rows = new ArrayList<>();
            List<MDMS_O_CUSTOMER_SHOP> shops = mdmsCustomerArchivesQueryDao.selectMdmsOCustomerShopListPage(request.getTenantNumId(),
                    request.getDataSign(), request.getPageNum(), request.getPageSize());
            if (shops != null) {
                shops.forEach(r -> {
                    rows.add(entity2Model(r));
                });
            }
            response.setRows(rows);
            response.setTotal(mdmsCustomerArchivesQueryDao.selectMdmsOCustomerShopListPageTotal(request.getTenantNumId(),
                    request.getDataSign()));
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerShopListPage request:{}", JsonUtil.toJson(request));
        }
        return response;
    }

    @Override
    public MdmsCustomerShopResponse getCustomerShop(MdmsCustomerShopRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCustomerShop request:{}", JsonUtil.toJson(request));
        }
        MdmsCustomerShopResponse response = new MdmsCustomerShopResponse();
        try {
            response.setShopInfo(entity2Model(mdmsCustomerArchivesQueryDao.selectMdmsOCustomerShop(request.getTenantNumId(),
                    request.getDataSign(), request.getSeries())));
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerShop response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsCustomerShopRelListResponse getCustomerShopRelListPage(MdmsCustomerShopRelListRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCustomerShopRelListPage request:{}", JsonUtil.toJson(request));
        }
        MdmsCustomerShopRelListResponse response = new MdmsCustomerShopRelListResponse();
        try {
            List<MdmsB2bSubRelation> rows = new ArrayList<>();
            List<MDMS_B2B_SUB_RELATION> rels = mdmsCustomerArchivesQueryDao.selectMdmsB2bSubRelationListPage(
                    request.getTenantNumId(),
                    request.getDataSign(),
                    request.getSaleOrg(),
                    request.getDistChannel(),
                    request.getCustomerNumId(),
                    request.getEffectSign(),
                    request.getSubUnitName(),
                    request.getPageNum(),
                    request.getPageSize());
            int rowCount = mdmsCustomerArchivesQueryDao.selectMdmsB2bSubRelationListPageTotal(
                    request.getTenantNumId(),
                    request.getDataSign(),
                    request.getSaleOrg(),
                    request.getDistChannel(),
                    request.getCustomerNumId(),
                    request.getEffectSign(),
                    request.getSubUnitName());
            if (rels != null) {
                rels.forEach(r -> {
                    rows.add(entity2Model(r));
                });
            }
            response.setRows(rows);
            response.setTotal(rowCount);
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerShopRelListPage request:{}", JsonUtil.toJson(request));
        }
        return response;
    }

    @Override
    public MdmsCustomerShopRelResponse getCustomerShopRel(MdmsCustomerShopRelRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCustomerShopRel request:{}", JsonUtil.toJson(request));
        }
        MdmsCustomerShopRelResponse response = new MdmsCustomerShopRelResponse();
        try {
            response.setRelInfo(entity2Model(mdmsCustomerArchivesQueryDao.selectMdmsB2bSubRelation(request.getTenantNumId(),
                    request.getDataSign(), request.getSeries())));
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerShopRel response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsCustomerShopRelSaveResponse saveOrUpdateCustomerShopRel(MdmsCustomerShopRelSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveOrUpdateCustomerShopRel request:{}", JsonUtil.toJson(request));
        }
        MdmsCustomerShopRelSaveResponse response = new MdmsCustomerShopRelSaveResponse();
        try {
            MDMS_B2B_SUB_RELATION entity = model2Entity(request);
            if (StringUtils.isBlank(request.getSeries())) {
                entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_B2B_SUB_RELATION_SERIES));
//                entity.setSERIES("2021110302");
                if (mdmsCustomerArchivesQueryDao.insertMdmsB2bSubRelation(entity) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                            "客户门店关系新增失败");
                }
            } else {
                entity.setSERIES(request.getSeries());
                if (mdmsCustomerArchivesQueryDao.updateMdmsB2bSubRelation(entity) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                            "客户门店关系更新失败");
                }
            }
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveOrUpdateCustomerShopRel response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    private MdmsOSubCustomer entity2Model(MDMS_O_SUB_CUSTOMER entity) {
        MdmsOSubCustomer model = new MdmsOSubCustomer();
        model.setAnnualReport(entity.getANNUAL_REPORT());
        model.setAnnualReportBegin(entity.getANNUAL_REPORT_BEGIN());
        model.setAnnualReportEnd(entity.getANNUAL_REPORT_END());
        model.setBusinessContact(entity.getBUSINESS_CONTACT());
        model.setBusinessContactIdcard(entity.getBUSINESS_CONTACT_IDCARD());
        model.setBusinessContactTel(entity.getBUSINESS_CONTACT_TEL());
        model.setBusinessLicenseBeginDate(entity.getBUSINESS_LICENSE_BEGIN_DATE());
        model.setBusinessLicenseEndDate(entity.getBUSINESS_LICENSE_END_DATE());
        model.setCancelsign(entity.getCANCELSIGN());
        model.setCloseDays(entity.getCLOSE_DAYS());
        model.setCorporat(entity.getCORPORAT());
        model.setCorporatProxyIdcardBegin(entity.getCORPORAT_PROXY_IDCARD_BEGIN());
        model.setCorporatProxyIdcardEnd(entity.getCORPORAT_PROXY_IDCARD_END());
        model.setCorporatProxyName(entity.getCORPORAT_PROXY_NAME());
        model.setCorporatProxyStatBegin(entity.getCORPORAT_PROXY_STAT_BEGIN());
        model.setCorporatProxyStatEnd(entity.getCORPORAT_PROXY_STAT_END());
        model.setCorporatProxyStatIdcard(entity.getCORPORAT_PROXY_STAT_IDCARD());
        model.setCortNumId(entity.getCORT_NUM_ID());
        model.setCreateDtme(entity.getCREATE_DTME());
        model.setCreateUserId(entity.getCREATE_USER_ID());
        model.setCustomerNumId(entity.getCUSTOMER_NUM_ID());
        model.setDataSign(entity.getDATA_SIGN());
        model.setDirectDeliveryFlag(entity.getDIRECT_DELIVERY_FLAG());
        model.setDrugBusinessLicense(entity.getDRUG_BUSINESS_LICENSE());
        model.setDrugBusinessLicenseBegin(entity.getDRUG_BUSINESS_LICENSE_BEGIN());
        model.setDrugBusinessLicenseEnd(entity.getDRUG_BUSINESS_LICENSE_END());
        model.setDrugProductLicense(entity.getDRUG_PRODUCT_LICENSE());
        model.setDrugProductLicenseBegin(entity.getDRUG_PRODUCT_LICENSE_BEGIN());
        model.setDrugProductLicenseEnd(entity.getDRUG_PRODUCT_LICENSE_END());
        model.setFileNo(entity.getFILE_NO());
        model.setFinanceName(entity.getFINANCE_NAME());
        model.setFinanceTel(entity.getFINANCE_TEL());
        model.setFoodBusLicense(entity.getFOOD_BUS_LICENSE());
        model.setFoodBusLicenseBegin(entity.getFOOD_BUS_LICENSE_BEGIN());
        model.setFoodBusLicenseEnd(entity.getFOOD_BUS_LICENSE_END());
        model.setFoodBusLicenseScope(entity.getFOOD_BUS_LICENSE_SCOPE());
        model.setForbidReturnFactory(entity.getFORBID_RETURN_FACTORY());
        model.setForbidSale(entity.getFORBID_SALE());
        model.setLastUpdateUserId(entity.getLAST_UPDATE_USER_ID());
        model.setLastUpdtme(entity.getLAST_UPDTME());
        model.setLicenceWarnDays(entity.getLICENCE_WARN_DAYS());
        model.setMedicalDeviceBusBegin(entity.getMEDICAL_DEVICE_BUS_BEGIN());
        model.setMedicalDeviceBusEnd(entity.getMEDICAL_DEVICE_BUS_END());
        model.setMedicalDeviceBusLincense(entity.getMEDICAL_DEVICE_BUS_LINCENSE());
        model.setMedicalDeviceBusScope(entity.getMEDICAL_DEVICE_BUS_SCOPE());
        model.setMedicalDeviceProRec2(entity.getMEDICAL_DEVICE_PRO_REC2());
        model.setMedicalDeviceProRec2Scope(entity.getMEDICAL_DEVICE_PRO_REC2_SCOPE());
        model.setMedicalInstitutionBegin(entity.getMEDICAL_INSTITUTION_BEGIN());
        model.setMedicalInstitutionEnd(entity.getMEDICAL_INSTITUTION_END());
        model.setMedicalInstitutionLicense(entity.getMEDICAL_INSTITUTION_LICENSE());
        model.setMoneyType(entity.getMONEY_TYPE());
        model.setOldCustomerId(entity.getOLD_CUSTOMER_ID());
        model.setPaymentTerm(entity.getPAYMENT_TERM());
        model.setPreDistribution(entity.getPRE_DISTRIBUTION());
        model.setPurchaseLeader(entity.getPURCHASE_LEADER());
        model.setQualityAgreemnetBeginDate(entity.getQUALITY_AGREEMENT_BEGIN_DATE());
        model.setQualityAgreemnetEndDate(entity.getQUALITY_AGREEMENT_BEGIN_DATE());
        model.setReceivingBankAccount(entity.getRECEIVING_BANK_ACCOUNT());
        model.setReceivingBankName(entity.getRECEIVING_BANK_NAME());
        model.setReceivingBankNum(entity.getRECEIVING_BANK_NUM());
        model.setRemark(entity.getREMARK());
        model.setReplenisher(entity.getREPLENISHER());
        model.setSaleCode("");
        model.setSaleLimitDays(entity.getSALE_LIMIT_DAYS());
        model.setSeries(entity.getSERIES());
        model.setStorageAdr(entity.getSTORAGE_ADR());
        model.setSubCreditLine(entity.getSUB_CREDIT_LINE());
        model.setSubCustomerStatus(entity.getSUB_CUSTOMER_STATUS());
        model.setTenantNumId(entity.getTENANT_NUM_ID());
        return model;
    }

    private MdmsOCustomerShop entity2Model(MDMS_O_CUSTOMER_SHOP entity) {
        MdmsOCustomerShop model = new MdmsOCustomerShop();
        model.setAdr(entity.getADR());
        model.setBusinessLicense(entity.getBUSINESS_LICENSE());
        model.setCancelsign(entity.getCANCELSIGN());
        model.setCityAreaNumId(entity.getCITY_AREA_NUM_ID());
        model.setCityNumId(entity.getCITY_NUM_ID());
        model.setCreateDtme(entity.getCREATE_DTME());
        model.setCortNumId(entity.getCORT_NUM_ID());
        model.setCreateUserId(entity.getCREATE_USER_ID());
        model.setCustomerNumId(entity.getCUSTOMER_NUM_ID());
        model.setDataSign(entity.getDATA_SIGN());
        model.setDrugBusinessLicense(entity.getDRUG_BUSINESS_LICENSE());
        model.setDrugBusinessScope(entity.getDRUG_BUSINESS_SCOPE());
        model.setFoodBusLicense(entity.getFOOD_BUS_LICENSE());
        model.setFoodBusLicenseBegin(entity.getFOOD_BUS_LICENSE_BEGIN());
        model.setFoodBusLicenseEnd(entity.getFOOD_BUS_LICENSE_END());
        model.setFoodBusLicenseScope(entity.getFOOD_BUS_LICENSE_SCOPE());
        model.setIsChineseMedicineAptitude(entity.getIS_CHINESE_MEDICINE_APTITUDE());
        model.setIsColdChainAptitude(entity.getIS_COLD_CHAIN_APTITUDE());
        model.setIsDualChannel(entity.getIS_DUAL_CHANNEL());
        model.setLastUpdateUserId(entity.getLAST_UPDATE_USER_ID());
        model.setLastUpdtme(entity.getLAST_UPDTME());
        model.setMaplocationX(entity.getMAPLOCATION_X());
        model.setMaplocationY(entity.getMAPLOCATION_Y());
        model.setMedicalDeviceBusBegin(entity.getMEDICAL_DEVICE_BUS_BEGIN());
        model.setMedicalDeviceBusEnd(entity.getMEDICAL_DEVICE_BUS_END());
        model.setMedicalDeviceBusLincense(entity.getMEDICAL_DEVICE_BUS_LINCENSE());
        model.setMedicalDeviceBusScope(entity.getMEDICAL_DEVICE_BUS_SCOPE());
        model.setMedicalDeviceProRec2(entity.getMEDICAL_DEVICE_PRO_REC2());
        model.setMedicalDeviceProRec2Scope(entity.getMEDICAL_DEVICE_PRO_REC2_SCOPE());
        model.setPrvNumId(entity.getPRV_NUM_ID());
        model.setQualityLeader(entity.getQUALITY_LEADER());
        model.setRemark(entity.getREMARK());
        model.setSaleCode(entity.getSALE_CODE());
        model.setSaleLimitDays(entity.getSALE_LIMIT_DAYS());
        model.setSeries(entity.getSERIES());
        model.setShopCategory(entity.getSHOP_CATEGORY());
        model.setShopName(entity.getSHOP_NAME());
        model.setShopNumId(entity.getSHOP_NUM_ID());
        model.setSigningDate(entity.getSIGNING_DATE());
        model.setSimShopName(entity.getSIM_SHOP_NAME());
        model.setStoreDistributionCycle(entity.getSTORE_DISTRIBUTION_CYCLE());
        model.setStoreHeadLeader(entity.getSTORE_HEAD_LEADER());
        model.setTenantNumId(entity.getTENANT_NUM_ID());
        model.setTownNumId(entity.getTOWN_NUM_ID());
        model.setUnifiedSocialCreditCode(entity.getUNIFIED_SOCIAL_CREDIT_CODE());
        return model;
    }

    private MdmsB2bSubRelation entity2Model(MDMS_B2B_SUB_RELATION entity) {
        MdmsB2bSubRelation model = new MdmsB2bSubRelation();
        model.setAnnex(entity.getANNEX());
        model.setChannelNumId(entity.getCHANNEL_NUM_ID());
        model.setCortName(entity.getCORT_NAME());
        model.setCortNumId(entity.getCORT_NUM_ID());
        model.setCreateDtme(entity.getCREATE_DTME());
        model.setCreateUserId(entity.getCREATE_USER_ID());
        model.setDataSign(entity.getDATA_SIGN());
        model.setEffectSign(entity.getEFFECT_SIGN());
        model.setLastUpdateUserId(entity.getLAST_UPDATE_USER_ID());
        model.setLastUpdtme(entity.getLAST_UPDTME());
        model.setOrgUnit(entity.getORG_UNIT());
        model.setRemark(entity.getREMARK());
        model.setSeries(entity.getSERIES());
        model.setSubUnitName(entity.getSUB_UNIT_NAME());
        model.setSubUnitNumId(entity.getSUB_UNIT_NUM_ID());
        model.setTenantNumId(entity.getTENANT_NUM_ID());
        model.setUnitNumId(entity.getUNIT_NUM_ID());
        return model;
    }

    private MDMS_B2B_SUB_RELATION model2Entity(MdmsCustomerShopRelSaveRequest request) {
        MDMS_B2B_SUB_RELATION entity = new MDMS_B2B_SUB_RELATION();
        entity.setANNEX(request.getAnnex());
        entity.setCHANNEL_NUM_ID(request.getChannelNumId());
        entity.setCORT_NAME(request.getCortName());
        entity.setCORT_NUM_ID(request.getCortNumId());
        entity.setCREATE_USER_ID(request.getCreateUserId());
        entity.setDATA_SIGN(request.getDataSign());
        entity.setEFFECT_SIGN(request.getEffectSign());
        entity.setLAST_UPDATE_USER_ID(request.getLastUpdateUserId());
        entity.setORG_UNIT(request.getOrgUnit());
        entity.setREMARK(request.getRemark());
        entity.setSUB_UNIT_NAME(request.getSubUnitName());
        entity.setSUB_UNIT_NUM_ID(request.getSubUnitNumId());
        entity.setTENANT_NUM_ID(request.getTenantNumId());
        entity.setUNIT_NUM_ID(request.getUnitNumId());
        return entity;
    }

}
