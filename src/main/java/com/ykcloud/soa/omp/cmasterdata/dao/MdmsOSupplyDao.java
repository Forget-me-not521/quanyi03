package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsBlSupply;
import com.ykcloud.soa.omp.cmasterdata.api.model.SupplyInfoForQuery;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUPPLY;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@Repository
public class MdmsOSupplyDao extends Dao<MDMS_O_SUPPLY> {
    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_SUPPLY.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_SUPPLY.class, ",");
    private static final String TABLE_NAME = "MDMS_O_SUPPLY";
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    public List<MDMS_O_SUPPLY> selectByUnifiedCreditCode(Long tenantNumId, Long dataSign, String unifiedCreditCode) {
        String sql = "select * from MDMS_O_SUPPLY WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  unified_social_credit_code = ? AND cancelsign = 'N'";
        List<MDMS_O_SUPPLY> mdmsOSupplyList = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, unifiedCreditCode}, new BeanPropertyRowMapper<>(MDMS_O_SUPPLY.class));
        if (CollectionUtils.isNotEmpty(mdmsOSupplyList) && mdmsOSupplyList.size() > 1) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "存在多笔表单数据！");
        }
        return mdmsOSupplyList;
    }

    public List<MdmsBlSupply> getSupplyPage(Long tenantNumId, Long dataSign, String unifiedCreditCode, String cortNumId, String supplyNumId, String supplyName, Long createUserId, Date createDateBegin, Date createDateEnd, Integer pageNum, Integer pageSize) {
        String sql = "select c.*,s.* from mdms_o_supply c inner join mdms_o_sub_supply s on c.supply_num_id=s.supply_num_id " +
                "and c.tenant_num_id=s.tenant_num_id and c.data_sign=s.data_sign where c.tenant_num_id =? and c.data_sign =? and s.cort_num_id=? ";
        if(StringUtils.isNotEmpty(unifiedCreditCode)){
            sql+=" and c.unified_social_credit_code='"+unifiedCreditCode+"' ";
        }
        if(StringUtils.isNotEmpty(supplyNumId)){
            sql+=" and c.supply_num_id='"+supplyNumId+"' ";
        }
        if(StringUtils.isNotEmpty(supplyName)){
            sql+=" and c.supply_name='"+supplyName+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createUserId)){
            sql+=" and c.create_user_id='"+createUserId+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateBegin)){
            sql+=" and c.create_dtme>'"+createDateBegin+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateEnd)){
            sql+=" and c.create_dtme<'"+createDateEnd+"' ";
        }
        Integer start = (pageNum-1)*pageSize;

        sql+=" limit ?,? ";
        List<MdmsBlSupply> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign,cortNumId,start,pageSize}, new BeanPropertyRowMapper<>(MdmsBlSupply.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    public Integer getSupplyPageCount(Long tenantNumId, Long dataSign, String unifiedCreditCode, String cortNumId, String supplyNumId, String supplyName, Long createUserId, Date createDateBegin,Date createDateEnd) {
        String sql = "select count(1) from mdms_o_supply c inner join mdms_o_sub_supply s on c.supply_num_id=s.supply_num_id " +
                "and c.tenant_num_id=s.tenant_num_id and c.data_sign=s.data_sign  where c.tenant_num_id =? and c.data_sign =? and s.cort_num_id=? ";
        if(StringUtils.isNotEmpty(unifiedCreditCode)){
            sql+=" and c.unified_social_credit_code='"+unifiedCreditCode+"' ";
        }
        if(StringUtils.isNotEmpty(supplyNumId)){
            sql+=" and c.supply_num_id='"+supplyNumId+"' ";
        }
        if(StringUtils.isNotEmpty(supplyName)){
            sql+=" and c.supply_name='"+supplyName+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createUserId)){
            sql+=" and c.create_user_id='"+createUserId+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateBegin)){
            sql+=" and c.create_dtme>'"+createDateBegin+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateEnd)){
            sql+=" and c.create_dtme<'"+createDateEnd+"' ";
        }
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign,cortNumId}, Integer.class);
    }
    public boolean checkSupplyExist(Long tenantNumId, Long dataSign, String unifiedSocialCreditCode) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from mdms_o_supply ");
        sb.append(" where tenant_Num_id=? and data_sign=? and unified_social_credit_code=?  and cancelsign='N'  ");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign,
                unifiedSocialCreditCode}, int.class) >0 ? true : false;
    }
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }


    public SupplyInfoForQuery getSupplyBySupplyNumId(Long tenantNumId, Long dataSign, String supplyNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_o_supply ");
        sb.append(" where tenant_Num_id=? and data_sign=? and  supply_num_id=?  and cancelsign='N'  ");
        List<SupplyInfoForQuery> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,supplyNumId}, new BeanPropertyRowMapper<>(SupplyInfoForQuery.class));
        if(CollectionUtils.isEmpty(list)){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                    "该供应商不存在!");
        }
        if(list.size()>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                    "查出多条数据!");
        }
        return  list.get(0);
    }
}
