package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUB_UNIT;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUPPLY_ANNUAL_REPORT_DTL;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @Author stark.jiang
 * @Date 2021/10/22/9:35
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsOSupplyAnnualReportDtlDao extends Dao<MDMS_O_SUPPLY_ANNUAL_REPORT_DTL> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public MDMS_O_SUPPLY_ANNUAL_REPORT_DTL getReportDtlEntity(Long tenantNumId, Long dataSign, String series) {
        String sql = "select * from mdms_o_supply_annual_report_dtl where " +
                " tenant_num_id = ? and data_sign = ? and series = ?  and cancelsign = 'N' ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, MDMS_O_SUPPLY_ANNUAL_REPORT_DTL.class);
    }

    public int updateEntity(Long tenantNumId, Long dataSign,Long userNumId,String series,Integer confirmStatus,String remark) {
        String sql="update mdms_o_supply_annual_report_dtl set confirm_status=?,last_update_user_id=? ,last_updtme=now() ";
        if(confirmStatus==3){
            sql+=",remark='"+remark+"'";
        }
        sql+=" where tenant_num_id=? and data_sign=? and series=? and cancelsign='N'";
        int row = jdbcTemplate.update(sql, new Object[]{confirmStatus,userNumId,tenantNumId, dataSign,series});
        if (row <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, String.format("更新供应商年报明细失败!行号:%d", series));
        }
        return row;
    }

}
