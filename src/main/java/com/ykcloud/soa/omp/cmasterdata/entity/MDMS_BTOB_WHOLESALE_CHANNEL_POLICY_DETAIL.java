package com.ykcloud.soa.omp.cmasterdata.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 批销-渠道政策明细对象 scm_wholesale_channel_policy_detail
 *
 */
@Data
public class MDMS_BTOB_WHOLESALE_CHANNEL_POLICY_DETAIL implements Serializable {
    private static final long serialVersionUID=1L;

    /** 行号 */
    @ApiModelProperty(value = "行号")
    private Long series;

    /** 租户 */
    @ApiModelProperty(value = "租户")
    private Long tenantNumId;

    /** 生产或测试标识 */
    @ApiModelProperty(value = "生产或测试标识")
    private Long dataSign;

    /** 单据号 */
    @ApiModelProperty(value = "单据号")
    private String billNumId;

    /** 公司编码 */
    @ApiModelProperty(value = "公司编码")
    private Integer cortNumId;

    /** 销售组织 */
    @ApiModelProperty(value = "销售组织")
    private Integer unitNumId;

    /** 分销渠道 */
    @ApiModelProperty(value = "分销渠道")
    private Byte channel;

    /** 是否启用(0:否 1:是) */
    @ApiModelProperty(value = "是否启用(0:否 1:是)")
    private Byte isEnable;

    /** 商品编码 */
    @ApiModelProperty(value = "商品编码")
    private String itemNumId;

    /** 商品名称 */
    @ApiModelProperty(value = "商品名称")
    private String itemName;

    /** 商品小类 */
    @ApiModelProperty(value = "商品小类")
    private Long pty3NumId;

    /** 规格 */
    @ApiModelProperty(value = "规格")
    private String styleDesc;

    /** 批发价格 */
    @ApiModelProperty(value = "批发价格")
    private BigDecimal wholesalePrice;

    @ApiModelProperty(value = "原取价模式")
    private BigDecimal originalPriceModel;

    /** 新取价模式 */
    @ApiModelProperty(value = "新取价模式")
    private BigDecimal newPriceModel;

    /** 原加价率 */
    @ApiModelProperty(value = "原加价率")
    private BigDecimal originalMarkupRate;

    /** 新加价率 */
    @ApiModelProperty(value = "新加价率")
    private BigDecimal newMarkupRate;

    /** 原固定加价 */
    @ApiModelProperty(value = "原固定加价")
    private BigDecimal originalFixedMarkup;

    /** 新固定加价 */
    @ApiModelProperty(value = "新固定加价")
    private BigDecimal newFixedMarkup;

    /** 预估价格 */
    @ApiModelProperty(value = "预估价格")
    private BigDecimal estimatedPrice;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createDtme;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "最后更新时间")
    private Date lastUpdtme;

    /** 用户 */
    @ApiModelProperty(value = "用户")
    private Long createUserId;

    /** 更新用户 */
    @ApiModelProperty(value = "更新用户")
    private Long lastUpdateUserId;

    /** 删除 */
    @ApiModelProperty(value = "删除")
    private String cancelsign;

    @ApiModelProperty(value = "备注")
    private String remark;
}
