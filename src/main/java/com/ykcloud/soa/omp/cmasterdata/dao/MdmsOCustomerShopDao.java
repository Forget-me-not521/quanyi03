package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.CustomerShopInfo;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOSubSupply;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_CUSTOMER_SHOP;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUB_UNIT;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author stark.jiang
 * @Date 2021/10/11/8:40
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsOCustomerShopDao extends Dao<MDMS_O_CUSTOMER_SHOP> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }


    public boolean checkCustomerShopExist(Long tenantNumId, Long dataSign, String customerNumId,String cortNumId,String unifiedSocialCreditCode) {
        String sql = "select count(1) from mdms_o_customer_shop where " +
                " tenant_num_id = ? and data_sign = ? and customer_num_id = ? and cort_num_id=? and unified_social_credit_code=? and cancelsign = 'N' ";
        Integer count = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, customerNumId,cortNumId,unifiedSocialCreditCode}, Integer.class);
        if(ValidatorUtils.isNullOrZero(count)){
            return false;
        }
        if (count > 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据社会统一信用码" + unifiedSocialCreditCode + "找到多个门店");
        }
        return true;
    }
    public MDMS_O_CUSTOMER_SHOP getCustomerShopEntity(Long tenantNumId, Long dataSign, String customerNumId,String cortNumId,String unifiedSocialCreditCode) {
        String sql = "select * from mdms_o_sub_unit where " +
                " tenant_num_id = ? and data_sign = ? and customer_num_id = ? and cort_num_id=? and unified_social_credit_code=? and cancelsign = 'N' ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, customerNumId,cortNumId,unifiedSocialCreditCode}, MDMS_O_CUSTOMER_SHOP.class);
    }


    public int updateEntity(Long tenantNumId, Long dataSign, MDMS_O_CUSTOMER_SHOP entity) {
        String cols = EntityFieldUtil.wildCardSplitUpdate(entity, ",");
        List<Object> list = new ArrayList<>(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_O_CUSTOMER_SHOP.class, entity)));
        list.add(tenantNumId);
        list.add(dataSign);
        list.add(entity.getCUSTOMER_NUM_ID());
        list.add(entity.getCORT_NUM_ID());
        list.add(entity.getUNIFIED_SOCIAL_CREDIT_CODE());
        StringBuffer sb = new StringBuffer();
        sb.append("update mdms_o_customer_shop set ");
        sb.append(cols);
        sb.append(" where tenant_num_id=? and data_sign=? and customer_num_id=? and cort_num_id=? and unified_social_credit_code=? and cancelsign='N'");
        int row = jdbcTemplate.update(sb.toString(), list.toArray());
        if (row <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, String.format("更新门店资料失败!门店统一信用编码:%d", entity.getUNIFIED_SOCIAL_CREDIT_CODE()));
        }
        return row;
    }


    public List<CustomerShopInfo> getCustomerShopList(Long tenantNumId, Long dataSign, String cortNumId,String customerNumId) {
        String sql = "select * from mdms_o_customer_shop  " +
                "where tenant_num_id = ? and data_sign = ? and cort_num_id=? and customer_num_id=? and CANCELSIGN = 'N'  ";
        List<CustomerShopInfo> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, cortNumId,customerNumId}, new BeanPropertyRowMapper<>(CustomerShopInfo.class));
        return list;
    }

}
