package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.ExceptionUtil;
import com.gb.soa.omp.ccommon.util.JsonUtil;

import com.ykcloud.soa.erp.common.enums.BillTypeStatusEnum;
import com.ykcloud.soa.erp.common.enums.UnitSubTypeEnum;
import com.ykcloud.soa.erp.common.enums.UnitTypeEnum;
import com.ykcloud.soa.omp.cmasterdata.api.model.ConfigSwitchInfo;
import com.ykcloud.soa.omp.cmasterdata.api.request.*;

import com.ykcloud.soa.erp.common.utils.RedisUtil;
import com.ykcloud.soa.omp.cmasterdata.api.request.PhysicalSaveOrUpdateRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.StorageSaveOrUpdateRequest;
import com.ykcloud.soa.omp.cmasterdata.api.response.*;

import com.ykcloud.soa.omp.cmasterdata.api.service.MdUnitService;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdWarehouseDataService;
import com.ykcloud.soa.omp.cmasterdata.dao.*;
import com.ykcloud.soa.omp.cmasterdata.entity.*;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Objects;

/**
 * @author
 * @date 2021/10/12 14:27
 */
@Service("mdWarehouseDataService")
@RestController
public class MdWarehouseDataServiceImpl implements MdWarehouseDataService {
    private static Logger log = LoggerFactory.getLogger(MdWarehouseDataServiceImpl.class);


    @Resource
    private MdmsWPhysicalDao mdmsWPhysicalDao;

    @Autowired
    private MdmsWStorageDao mdmsWStorageDao;

    @Resource
    private MdmsWShipmentDao mdmsWShipmentDao;

    @Resource
    private MdmsWRouteHdrDao mdmsWRouteHdrDao;

    @Resource
    private MdmsWRouteDtlDao mdmsWRouteDtlDao;

    @Resource
    private MdUnitService mdUnitService;

    @Resource(name = "redisObjectTemplate")
    private RedisTemplate redisTemplate;

    @Resource
    private MdmsWPhysicalConfigSwitchDao mdmsWPhysicalConfigSwitchDao;


    @Override
    public PhysicalSaveOrUpdateResponse physicalSaveOrUpdate(PhysicalSaveOrUpdateRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin physicalSaveOrUpdate request: {}", JsonUtil.toJson(request));
        }
        PhysicalSaveOrUpdateResponse response = new PhysicalSaveOrUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);

            // TODO 根据公司判断是否可以虚拟
            MDMS_W_PHYSICAL mdmsWPhysical = new MDMS_W_PHYSICAL();
            if (StringUtils.isBlank(request.getSeries())) {
                if (mdmsWPhysicalDao.checkPhyNumIdExist(request.getTenantNumId(), request.getDataSign(), request.getPhysicalNumId())) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "当前仓库编号已存在");
                }
                mdmsWPhysical.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_W_PHYSICAL_SERIES));
                mdmsWPhysical.setPHYSICAL_NUM_ID(StringUtils.trim(request.getPhysicalNumId()));
                mdmsWPhysical.setCORT_NUM_ID(StringUtils.trim(request.getCortNumId()));
                mdmsWPhysical.setTENANT_NUM_ID(request.getTenantNumId());
                mdmsWPhysical.setDATA_SIGN(request.getDataSign());
                mdmsWPhysical.setCANCELSIGN(Constant.CANCEL_SIGN_N);
                // 所属组织
                mdmsWPhysical.setSTORAGE_DEPT_NUM_ID(request.getStorageDeptNumId());
                mdmsWPhysical.setVIRTUAL_SIGN(request.getVirtualSign());
                mdmsWPhysical.setSUB_UNIT_NUM_ID(request.getSubUnitNumId());
                mdmsWPhysical.setCREATE_DTME(new Date());
                mdmsWPhysical.setCREATE_USER_ID(request.getUserNumId());
                mdmsWPhysical.setEnable_sign(BillTypeStatusEnum.PHYSICAL_ENABLE_SIGN_1.getNumId());
            }
            mdmsWPhysical.setPHYSICAL_NAME(StringUtils.trim(request.getPhysicalName()));
            //地址
            mdmsWPhysical.setPRV_NUM_ID(request.getPrvNumId());
            mdmsWPhysical.setCITY_NUM_ID(request.getCityNumId());
            mdmsWPhysical.setCITY_AREA_NUM_ID(request.getCityAreaNumId());
            mdmsWPhysical.setAREA_ADR(request.getAreaAdr());
            mdmsWPhysical.setCONT_EMPE(request.getContEmpe());
            mdmsWPhysical.setTELEPHONE(request.getTelephone());
            mdmsWPhysical.setREMARK(request.getRemark());

            mdmsWPhysical.setLAST_UPDTME(new Date());
            mdmsWPhysical.setLAST_UPDATE_USER_ID(request.getUserNumId());
            if (StringUtils.isBlank(request.getSeries())) {
                if(mdmsWPhysicalDao.insert(mdmsWPhysical) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "新增大仓信息失败");
                }
            } else {
                if(mdmsWPhysicalDao.updateEntity(mdmsWPhysical,request.getTenantNumId(),request.getDataSign(), request.getCortNumId(),
                        request.getSubUnitNumId() ,request.getSeries()) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "更新大仓信息失败");
                }
//                UpdatePhyOrStorageNameRequest updateRequest = new UpdatePhyOrStorageNameRequest();
//                updateRequest.setTenantNumId(request.getTenantNumId());
//                updateRequest.setDataSign(request.getDataSign());
//                updateRequest.setCortNumId(request.getCortNumId());
//                updateRequest.setSubUnitNumId(request.getSubUnitNumId());
//                updateRequest.setPhysicalNumId(request.getPhysicalNumId());
//                updateRequest.setPhysicalName(request.getPhysicalName());
//                ExceptionUtil.checkDubboException(warehouseDataService.updatePhyOrStorageName(updateRequest));
            }
            UnitGenerateRequest unitReq=buildUnitReq(request.getTenantNumId(),request.getDataSign(),request.getUserNumId(),request.getCortNumId(),request.getPhysicalName(),request.getPhysicalNumId());
            mdUnitService.generateUnit(unitReq);
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end physicalSaveOrUpdate response: {}", JsonUtil.toJson(response));
        }
        return response;
    }



    @Override
    public StorageSaveOrUpdateResponse storageSaveOrUpdate(StorageSaveOrUpdateRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("begin storageSaveOrUpdate request: {}", JsonUtil.toJson(request));
        }
        StorageSaveOrUpdateResponse response = new StorageSaveOrUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);

            MDMS_W_STORAGE mdmsWStorage = new MDMS_W_STORAGE();
            String existSeries = mdmsWStorageDao.checkExistSeries(request.getTenantNumId(), request.getDataSign(),
                    request.getStorageNumId());
            if ((StringUtils.isBlank(request.getSeries()) && StringUtils.isNotBlank(existSeries))
                || (StringUtils.isNotBlank(request.getSeries()) && StringUtils.isNotBlank(existSeries) &&
                    !request.getSeries().equals(existSeries))) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "仓库编码重复");
            }
            if (StringUtils.isBlank(request.getSeries())) {
                mdmsWStorage.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_W_STORAGE_SERIES));
                mdmsWStorage.setCREATE_DTME(new Date());
                mdmsWStorage.setCANCELSIGN(Constant.CANCEL_SIGN_N);
                mdmsWStorage.setTENANT_NUM_ID(request.getTenantNumId());
                mdmsWStorage.setDATA_SIGN(request.getDataSign());
                mdmsWStorage.setCREATE_USER_ID(request.getUserNumId());
                mdmsWStorage.setCORT_NUM_ID(StringUtils.trim(request.getCortNumId()));
                mdmsWStorage.setSUB_UNIT_NUM_ID(request.getSubUnitNumId());
                mdmsWStorage.setSTORAGE_NUM_ID(StringUtils.trim(request.getStorageNumId()));

                // TODO 不允许为空 插入时报错 set默认值
                mdmsWStorage.setPRV_NUM_ID(0L);
                mdmsWStorage.setCITY_NUM_ID(0L);
                mdmsWStorage.setCITY_AREA_NUM_ID(0L);
                mdmsWStorage.setTOWN_NUM_ID(0L);
                mdmsWStorage.setADR("");
            }

            mdmsWStorage.setSTORAGE_NAME(request.getStorageName());
            mdmsWStorage.setBUSINESS_TYPE(request.getBusinessType());
            mdmsWStorage.setMANAGE_TYPE(request.getManageType());
            mdmsWStorage.setPHYSICAL_NUM_ID(StringUtils.trim(request.getPhysicalNumId()));

            mdmsWStorage.setREMARK(request.getRemark());
            mdmsWStorage.setLAST_UPDATE_USER_ID(request.getUserNumId());
            mdmsWStorage.setLAST_UPDTME(new Date());

            if (StringUtils.isBlank(request.getSeries())) {
                if(mdmsWStorageDao.insert(mdmsWStorage) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "新增仓库信息失败");
                }
            } else {
                if(mdmsWStorageDao.updateEntity(mdmsWStorage, request.getTenantNumId(), request.getDataSign(),
                        request.getCortNumId(), request.getSubUnitNumId(), request.getSeries()) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "更新仓库信息失败");
                }
//                UpdatePhyOrStorageNameRequest updateRequest = new UpdatePhyOrStorageNameRequest();
//                updateRequest.setTenantNumId(request.getTenantNumId());
//                updateRequest.setDataSign(request.getDataSign());
//                updateRequest.setCortNumId(request.getCortNumId());
//                updateRequest.setSubUnitNumId(request.getSubUnitNumId());
//                updateRequest.setStorageNumId(request.getStorageNumId());
//                updateRequest.setStorageName(request.getStorageName());
//                ExceptionUtil.checkDubboException(warehouseDataService.updatePhyOrStorageName(updateRequest));
            }
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end storageSaveOrUpdate response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ShipmentSaveOrUpdateResponse shipmentSaveOrUpdate(ShipmentSaveOrUpdateRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin shipmentSaveOrUpdate request: {}", JsonUtil.toJson(request));
        }
        ShipmentSaveOrUpdateResponse response = new ShipmentSaveOrUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            String existSeries = mdmsWShipmentDao.checkExistShipment(request.getTenantNumId(), request.getCortNumId(),
                    request.getDataSign(), request.getPhysicalNumId(), request.getShipmentNumId());
            if ((StringUtils.isBlank(request.getSeries()) && StringUtils.isNotBlank(existSeries))
                    || (StringUtils.isNotBlank(request.getSeries()) && StringUtils.isNotBlank(existSeries) &&
                    !request.getSeries().equals(existSeries))) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "装运点编号重复");
            }
            MDMS_W_SHIPMENT shipment = new MDMS_W_SHIPMENT();
            if (StringUtils.isBlank(request.getSeries())) {
                shipment.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_W_SHIPMENT_SERIES));
                shipment.setCREATE_DTME(new Date());
                shipment.setCANCELSIGN(Constant.CANCEL_SIGN_N);
                shipment.setTENANT_NUM_ID(request.getTenantNumId());
                shipment.setDATA_SIGN(request.getDataSign());
                shipment.setCREATE_USER_ID(request.getUserNumId());
                shipment.setCORT_NUM_ID(StringUtils.trim(request.getCortNumId()));
                shipment.setSUB_UNIT_NUM_ID(request.getSubUnitNumId());
                shipment.setPHYSICAL_NUM_ID(request.getPhysicalNumId());
                shipment.setPHYSICAL_NAME(request.getPhysicalName());
                shipment.setSHIPMENT_NUM_ID(StringUtils.trim(request.getShipmentNumId()));
            }
            shipment.setSHIPMENT_NAME(request.getShipmentName());
            shipment.setENABLE_SIGN(request.getEnableSign());
            shipment.setREMARK(request.getRemark());
            shipment.setLAST_UPDATE_USER_ID(request.getUserNumId());
            shipment.setLAST_UPDTME(new Date());
            if (StringUtils.isBlank(request.getSeries())) {
                if (mdmsWShipmentDao.insert(shipment) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "新增装运点信息失败");
                }
            } else {
                if (mdmsWShipmentDao.updateEntity(shipment, request.getTenantNumId(), request.getDataSign(),
                        request.getCortNumId(), request.getSubUnitNumId(), request.getSeries()) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "更新装运点信息失败");
                }
            }
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end shipmentSaveOrUpdate response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public RouteHdrSaveOrUpdateResponse routeHdrSaveOrUpdate(RouteHdrSaveOrUpdateRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin routeHdrSaveOrUpdate request: {}", JsonUtil.toJson(request));
        }
        RouteHdrSaveOrUpdateResponse response = new RouteHdrSaveOrUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            String existSeries = mdmsWRouteHdrDao.checkExistRouteDaySign(request.getTenantNumId(), request.getCortNumId(),
                    request.getDataSign(), request.getPhysicalNumId(), request.getDaySign());
            if ((StringUtils.isBlank(request.getSeries()) && StringUtils.isNotBlank(existSeries))
                    || (StringUtils.isNotBlank(request.getSeries()) && StringUtils.isNotBlank(existSeries) &&
                    !request.getSeries().equals(existSeries))) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "线路周期重复");
            }
            if (mdmsWRouteHdrDao.checkExistRouteNumId(request.getTenantNumId(), request.getCortNumId(),
                    request.getDataSign(), request.getPhysicalNumId(), request.getRouteNumId())) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "线路编码重复");
            }
            MDMS_W_ROUTE_HDR routeHdr = new MDMS_W_ROUTE_HDR();
            if (StringUtils.isBlank(request.getSeries())) {
                routeHdr.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_W_ROUTE_HDR_SERIES));
                routeHdr.setCREATE_DTME(new Date());
                routeHdr.setCANCELSIGN(Constant.CANCEL_SIGN_N);
                routeHdr.setTENANT_NUM_ID(request.getTenantNumId());
                routeHdr.setDATA_SIGN(request.getDataSign());
                routeHdr.setCREATE_USER_ID(request.getUserNumId());
                routeHdr.setCORT_NUM_ID(StringUtils.trim(request.getCortNumId()));
                routeHdr.setSUB_UNIT_NUM_ID(request.getSubUnitNumId());
                routeHdr.setPHYSICAL_NUM_ID(request.getPhysicalNumId());
                routeHdr.setROUTE_NUM_ID(StringUtils.trim(request.getRouteNumId()));
            }
            routeHdr.setSHIPMENT_NUM_ID(StringUtils.trim(request.getShipmentNumId()));
            routeHdr.setSHIPMENT_NAME(request.getShipmentName());
            routeHdr.setROUTE_NAME(request.getRouteName());
            routeHdr.setDAY_SIGN(request.getDaySign());
            routeHdr.setREMARK(request.getRemark());
            routeHdr.setLAST_UPDATE_USER_ID(request.getUserNumId());
            routeHdr.setLAST_UPDTME(new Date());
            if (StringUtils.isBlank(request.getSeries())) {
                if (mdmsWRouteHdrDao.insert(routeHdr) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "新增线路头信息失败");
                }
            } else {
                if (mdmsWRouteHdrDao.updateEntity(routeHdr, request.getTenantNumId(), request.getDataSign(),
                        request.getCortNumId(), request.getSubUnitNumId(), request.getSeries()) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "更新线路头信息失败");
                }
            }
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end routeHdrSaveOrUpdate response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public RouteDtlSaveOrUpdateResponse routeDtlSaveOrUpdate(RouteDtlSaveOrUpdateRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin routeDtlSaveOrUpdate request: {}", JsonUtil.toJson(request));
        }
        RouteDtlSaveOrUpdateResponse response = new RouteDtlSaveOrUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);

            MDMS_W_ROUTE_DTL routeDtl = new MDMS_W_ROUTE_DTL();
            if (StringUtils.isBlank(request.getSeries())) {
                routeDtl.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_W_ROUTE_DTL_SERIES));
                routeDtl.setCREATE_DTME(new Date());
                routeDtl.setCANCELSIGN(Constant.CANCEL_SIGN_N);
                routeDtl.setTENANT_NUM_ID(request.getTenantNumId());
                routeDtl.setDATA_SIGN(request.getDataSign());
                routeDtl.setCREATE_USER_ID(request.getUserNumId());
                routeDtl.setCORT_NUM_ID(StringUtils.trim(request.getCortNumId()));
                routeDtl.setSUB_UNIT_NUM_ID(request.getSubUnitNumId());
                routeDtl.setPHYSICAL_NUM_ID(request.getPhysicalNumId());
                routeDtl.setROUTE_NUM_ID(request.getRouteNumId());
            }
            routeDtl.setCUST_SUB_UNIT_NUM_ID(request.getCustSubUnitNumId());
            routeDtl.setREMARK(request.getRemark());
            routeDtl.setLAST_UPDATE_USER_ID(request.getUserNumId());
            routeDtl.setLAST_UPDTME(new Date());
            if (StringUtils.isBlank(request.getSeries())) {
                if (mdmsWRouteDtlDao.insert(routeDtl) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "新增线路详情信息失败");
                }
            } else {
                if (mdmsWRouteDtlDao.updateEntity(routeDtl, request.getTenantNumId(), request.getDataSign(),
                        request.getCortNumId(), request.getSubUnitNumId(), request.getSeries()) != 1) {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                            "更新线路详情信息失败");
                }
            }
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end routeDtlSaveOrUpdate response: {}", JsonUtil.toJson(response));
        }
        return response;
    }


    @Override
    public GetPhysicalConfigSwitchResponse getPhysicalConfigSwitch(GetPhysicalConfigSwitchRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getPhysicalConfigSwitch request: {}", JsonUtil.toJson(request));
        }
        GetPhysicalConfigSwitchResponse response = new GetPhysicalConfigSwitchResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long statusNumId = mdmsWPhysicalConfigSwitchDao.getOnOffStatusByKey(request.getTenantNumId(), request.getDataSign(),
                    request.getCortNumId(), request.getSubUnitNumId(), request.getPhysicalNumId(), request.getSwitchKey());
            if(Objects.nonNull(statusNumId)) {
                redisTemplate.opsForHash().put(RedisUtil.getPhysicalConfigSwitchPrefix(request.getTenantNumId(), request.getDataSign(),
                        request.getCortNumId(), request.getPhysicalNumId()), request.getSwitchKey(),  statusNumId);
            }
            response.setStatus(statusNumId);
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getPhysicalConfigSwitch response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public PhysicalConfigSwitchUpdateResponse updatePhysicalConfigSwitch(PhysicalConfigSwitchUpdateRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updatePhysicalConfigSwitch request: {}", JsonUtil.toJson(request));
        }
        PhysicalConfigSwitchUpdateResponse response = new PhysicalConfigSwitchUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            try {
                mdmsWPhysicalConfigSwitchDao.updateOnOffStatusByKey(request.getTenantNumId(), request.getDataSign(),
                        request.getCortNumId(), request.getSubUnitNumId(), request.getPhysicalNumId(), request.getConfigSwitchInfos());
                for (ConfigSwitchInfo configSwitchInfo : request.getConfigSwitchInfos()) {
                    redisTemplate.opsForHash().put(RedisUtil.getPhysicalConfigSwitchPrefix(request.getTenantNumId(), request.getDataSign(),
                            request.getCortNumId(), request.getPhysicalNumId()), configSwitchInfo.getSwitchKey(), configSwitchInfo);
                }

            } catch (Exception ex) {

            }

            //response.setStatus(statusNumId);
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end updatePhysicalConfigSwitch response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public GetPhysicalByStorageResponse getPhysicalByStorage(GetPhysicalByStorageRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getPhysicalByStorage request: {}", JsonUtil.toJson(request));
        }
        GetPhysicalByStorageResponse response = new GetPhysicalByStorageResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            //StorageInfo
            String physicalNumId = mdmsWStorageDao.getPhysicalByStorage(request.getTenantNumId(),
                    request.getDataSign(), request.getCortNumId(), request.getSubUnitNumId(),
                    request.getStorageNumId());
            response.setPhsicalNumId(physicalNumId);
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getPhysicalByStorage response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public GetStorageByNumIdResponse getStorageByNumId(GetStorageByNumIdRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getStorageByNumId request: {}", JsonUtil.toJson(request));
        }
        GetStorageByNumIdResponse response = new GetStorageByNumIdResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            MDMS_W_STORAGE wStorage = mdmsWStorageDao.selectMdmsWStorage(request.getTenantNumId(),
                    request.getDataSign(), request.getStorageNumId());
            if (wStorage != null) {
                response.setStorageNumId(wStorage.getSTORAGE_NUM_ID());
                response.setBusinessType(wStorage.getBUSINESS_TYPE());
                response.setManageType(wStorage.getMANAGE_TYPE());
            }
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getStorageByNumId response: {}", JsonUtil.toJson(request));
        }
        return response;
    }

    private UnitGenerateRequest buildUnitReq(Long tenantNumId, Long dataSign, Long usrNumId, String cortNumId, String physicalName, String physicalNumId){
        UnitGenerateRequest unitRequest = new UnitGenerateRequest();
        unitRequest.setCortNumId(cortNumId);
        unitRequest.setUnitName(physicalName);
        unitRequest.setUnitType(UnitTypeEnum.UNIT_ORG.getTypeNumId());
        unitRequest.setSubType(UnitSubTypeEnum.STORAGE.getSubType());
        unitRequest.setUnitNumId(physicalNumId);
        unitRequest.setUserNumId(usrNumId);
        unitRequest.setTenantNumId(tenantNumId);
        unitRequest.setDataSign(dataSign);
        return unitRequest;
    }
}
