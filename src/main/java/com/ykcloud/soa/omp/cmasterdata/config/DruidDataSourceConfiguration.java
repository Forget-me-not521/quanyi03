package com.ykcloud.soa.omp.cmasterdata.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class DruidDataSourceConfiguration {
    @Value("${db.annotate.prefix}")
    private String dbAnnotatePrefix;
	

    @Bean(name = "masterDataDataSource")
    @ConfigurationProperties("spring.datasource.druid.masterdata")
    public DruidDataSource masterDataDataSource() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name = "platFormDataSource")
    @ConfigurationProperties("spring.datasource.druid.platform")
    public DruidDataSource platFormDataSource() {
        return DruidDataSourceBuilder.create().build();
    }


    @Bean(name = "dataexchangeDataSource")
    @ConfigurationProperties("spring.datasource.druid.dataexchange")
    public DruidDataSource dataexchangeDataSource() {
        return DruidDataSourceBuilder.create().build();
    }



    @Bean(name = "masterDataJdbcTemplate")
    public MyJdbcTemplate masterDataJdbcTemplate() {
        MyJdbcTemplate jdbcTemplate = new MyJdbcTemplate();
        jdbcTemplate.setDataSource(masterDataDataSource());
        jdbcTemplate.setDbAnnotatePrefix("");
        return jdbcTemplate;
    }

    @Bean(name = "leadMasterDataJdbcTemplate")
    public MyJdbcTemplate leadMasterDataJdbcTemplate() {
        MyJdbcTemplate jdbcTemplate = new MyJdbcTemplate();
        jdbcTemplate.setDataSource(masterDataDataSource());
        jdbcTemplate.setDbAnnotatePrefix(dbAnnotatePrefix);
        return jdbcTemplate;
    }
	


    @Bean(name = "masterDataTransactionManager")
    public DataSourceTransactionManager masterDataTransactionManager() {
        DataSourceTransactionManager tm = new DataSourceTransactionManager();
        tm.setDataSource(masterDataDataSource());
        tm.setNestedTransactionAllowed(true);
        return tm;
    }


    @Bean(name = "platFormJdbcTemplate")
    public MyJdbcTemplate platFormJdbcTemplate() {
        MyJdbcTemplate jdbcTemplate = new MyJdbcTemplate();
        jdbcTemplate.setDataSource(platFormDataSource());
        jdbcTemplate.setDbAnnotatePrefix(dbAnnotatePrefix);
        return jdbcTemplate;
    }

    @Bean(name = "platFormTransactionManager")
    public DataSourceTransactionManager platFormTransactionManager() {
        DataSourceTransactionManager tm = new DataSourceTransactionManager();
        tm.setDataSource(platFormDataSource());
        tm.setNestedTransactionAllowed(true);
        return tm;
    }

    @Bean(name = "dataexchangeJdbcTemplate")
    public MyJdbcTemplate dataexchangeJdbcTemplate() {
        MyJdbcTemplate jdbcTemplate = new MyJdbcTemplate();
        jdbcTemplate.setDataSource(dataexchangeDataSource());
        jdbcTemplate.setDbAnnotatePrefix("");
        return jdbcTemplate;
    }

    @Bean(name = "dataexchangeTransactionManager")
    public DataSourceTransactionManager dataexchangeTransactionManager() {
        DataSourceTransactionManager tm = new DataSourceTransactionManager();
        tm.setDataSource(dataexchangeDataSource());
        tm.setNestedTransactionAllowed(true);
        return tm;
    }


}