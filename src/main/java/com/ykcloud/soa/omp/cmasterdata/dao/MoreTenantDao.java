package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.entity.*;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class MoreTenantDao {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate masterDataJdbcTemplate;

    //@Resource(name = "platFormJdbcTemplate")
    private MyJdbcTemplate platFormJdbcTemplate;

    //@Resource(name = "financeJdbcTemplate")
    private MyJdbcTemplate financeJdbcTemplate;



    private static final String BRAND_SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_P_BRAND.class, ",");
    private static final String BRAND_TABLE_NAME = "MDMS_P_BRAND";
    private static final String BRAND_WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_P_BRAND.class, ",");


    private static final String AUTO_SEQUENCE_SQL_COLS = EntityFieldUtil.fieldSplit(PLATFORM_AUTO_SEQUENCE.class, ",");
    private static final String AUTO_SEQUENCE_TABLE_NAME = "PLATFORM_AUTO_SEQUENCE";
    private static final String AUTO_SEQUENCE_WILDCARDS = EntityFieldUtil.wildcardSplit(PLATFORM_AUTO_SEQUENCE.class, ",");





    public void insertBrand(MDMS_P_BRAND entity) {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append("insert into ");
        stringBuffer.append(BRAND_TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(BRAND_SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(BRAND_WILDCARDS);
        stringBuffer.append(")");
        int raw = masterDataJdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_P_BRAND.class, entity));
        if (raw <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "失败！");
        }
    }


    public List<MDMS_P_BRAND> getBrandList(Long tenantNumId, Long dataSign) {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append("select ");
        stringBuffer.append(BRAND_SQL_COLS);
        stringBuffer.append(" from ");
        stringBuffer.append(BRAND_TABLE_NAME);
        stringBuffer.append(" where tenant_num_id =? and data_sign=?  and  cancelsign='N'");
        return masterDataJdbcTemplate.query(stringBuffer.toString(), new Object[]{tenantNumId, dataSign}, new BeanPropertyRowMapper<MDMS_P_BRAND>(MDMS_P_BRAND.class));
    }


    public void insertAutoSeq(PLATFORM_AUTO_SEQUENCE entity) {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append("insert into ");
        stringBuffer.append(AUTO_SEQUENCE_TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(AUTO_SEQUENCE_SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(AUTO_SEQUENCE_WILDCARDS);
        stringBuffer.append(")");
        int raw = masterDataJdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(PLATFORM_AUTO_SEQUENCE.class, entity));
        if (raw <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    "失败！");
        }
    }


    public List<PLATFORM_AUTO_SEQUENCE> getAutoSeqList(Long tenantNumId, Long dataSign) {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append("select ");
        stringBuffer.append(AUTO_SEQUENCE_SQL_COLS);
        stringBuffer.append(" from ");
        stringBuffer.append(AUTO_SEQUENCE_TABLE_NAME);
        stringBuffer.append(" where tenant_num_id =? and data_sign=?  and  cancelsign='N'");
        return masterDataJdbcTemplate.query(stringBuffer.toString(), new Object[]{tenantNumId, dataSign}, new BeanPropertyRowMapper<PLATFORM_AUTO_SEQUENCE>(PLATFORM_AUTO_SEQUENCE.class));
    }
}
