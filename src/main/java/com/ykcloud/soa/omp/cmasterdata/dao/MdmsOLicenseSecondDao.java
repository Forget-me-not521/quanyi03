package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.erp.common.exception.ErpExceptionType;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOLicenseSecond;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_LICENSE_SECOND;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;


@Repository
public class MdmsOLicenseSecondDao extends Dao<MDMS_O_LICENSE_SECOND> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public MDMS_O_LICENSE_SECOND getBySeries(Long tenantNumId, Integer dataSign, Long series) {
        String sql = "select * from mdms_o_license_second where tenant_num_id = ? and data_sign = ? " +
                " and series = ? and cancelsign='N'";
        List<MDMS_O_LICENSE_SECOND> list = jdbcTemplate.query(sql,
                new Object[]{tenantNumId, dataSign, series}, new BeanPropertyRowMapper<>(MDMS_O_LICENSE_SECOND.class));
        if (CollectionUtils.isEmpty(list)) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "找不到当前信息 ！行号:" + series);
        }
        if (list.size() > 1) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "当前信息存在多条 ！行号:" + series);
        }
        return list.get(0);
    }

    public MDMS_O_LICENSE_SECOND getLastByFirstNumId(Long tenantNumId, Integer dataSign, String firstNumId) {
        String sql = "select * from mdms_o_license_second where tenant_num_id = ? and data_sign = ? " +
                " and first_num_id = ? and cancelsign='N' order by series desc";
        List<MDMS_O_LICENSE_SECOND> list = jdbcTemplate.query(sql,
                new Object[]{tenantNumId, dataSign, firstNumId}, new BeanPropertyRowMapper<>(MDMS_O_LICENSE_SECOND.class));
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    public List<MDMS_O_LICENSE_SECOND> getByFirstNumId(Long tenantNumId, Integer dataSign, String firstNumId) {
        String sql = "select * from mdms_o_license_second where tenant_num_id = ? and data_sign = ? " +
                " and first_num_id = ? and cancelsign='N'";
        List<MDMS_O_LICENSE_SECOND> list = jdbcTemplate.query(sql,
                new Object[]{tenantNumId, dataSign, firstNumId}, new BeanPropertyRowMapper<>(MDMS_O_LICENSE_SECOND.class));
        return list;
    }

    public void updateBySeries(Long tenantNumId, Integer dataSign, List<String> seriesList) {
        String sql = "update mdms_o_license_second set cancelsign = 'Y' where tenant_num_id = ? and data_sign = ?  " +
                "and cancelsign='N' and series in (" + StringUtils.join(seriesList, ",") + ")";
        int rows = jdbcTemplate.update(sql, tenantNumId, dataSign);
        if (rows != seriesList.size()) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ErpExceptionType.DOE33003,
                    "更新失败!");
        }
    }

    public List<MdmsOLicenseSecond> getByFirstNumIds(Long tenantNumId, Long dataSign, List<String> firstNumIds) {
        String sql = "select * from mdms_o_license_second where tenant_num_id = ? and data_sign = ? " +
                " and first_num_id in (" + StringUtils.join(firstNumIds, ",") + ") and cancelsign='N'";
        List<MdmsOLicenseSecond> list = jdbcTemplate.query(sql,
                new Object[]{tenantNumId, dataSign}, new BeanPropertyRowMapper<>(MdmsOLicenseSecond.class));
        return list;
    }

}
