package com.ykcloud.soa.omp.cmasterdata.entity;

import com.ykcloud.soa.erp.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class MDMS_B2B_BL_CS_POLICY_HDR extends BaseEntity {

    /** 公司 */
    @ApiModelProperty(value = "公司")
    private String CORT_NUM_ID;

    /** 业务单元 */
    @ApiModelProperty(value = "业务单元")
    private String UNIT_NUM_ID;

    /** 组织 */
    @ApiModelProperty(value = "组织")
    private String ORG_UNIT;

    /** 渠道 */
    @ApiModelProperty(value = "渠道")
    private Integer CHANNEL_NUM_ID;

    /** 单据号 */
    @ApiModelProperty(value = "单据号")
    private String RESERVED_NO;

    /** 变更主主题 */
    @ApiModelProperty(value = "变更主主题")
    private String THEME;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private Integer STATUS_NUM_ID;

    /** 类型 */
    @ApiModelProperty(value = "类型")
    private Integer TYPE_NUM_ID;

    /** 来源 */
    @ApiModelProperty(value = "来源")
    private Integer SO_FROM_TYPE;

    /** 制单日期 */
    @ApiModelProperty(value = "制单日期")
    private Date ORDER_DATE;

    /** 生效日期 */
    @ApiModelProperty(value = "生效日期")
    private Date EFFECT_DATE;

    /** 附件 */
    @ApiModelProperty(value = "附件")
    private String ANNEX;

    /** $column.columnComment */
    @ApiModelProperty(value = "$column.columnComment")
    private Long CREATE_USER_ID;

    /** $column.columnComment */
    @ApiModelProperty(value = "$column.columnComment")
    private Date CREATE_DTME;

    /** $column.columnComment */
    @ApiModelProperty(value = "$column.columnComment")
    private Long UPDATE_USER_ID;

    /** $column.columnComment */
    @ApiModelProperty(value = "$column.columnComment")
    private Date UPDATE_DTME;

    /** 删除 */
    @ApiModelProperty(value = "删除")
    private String CANCELSIGN;

    @ApiModelProperty("明细")
    private List<MDMS_B2B_BL_CS_POLICY_DTL> mdmsB2bBlCsPolicyDtlList;


    @ApiModelProperty(value = "批量删除使用的id")
    private List<String> seriesList;
}
