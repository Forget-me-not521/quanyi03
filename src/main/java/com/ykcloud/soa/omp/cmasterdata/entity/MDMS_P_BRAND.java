package com.ykcloud.soa.omp.cmasterdata.entity;

public class MDMS_P_BRAND extends BaseEntity {
    private Long BRAND_NUM_ID;//品牌',
    private String BRAND_NAME;//品牌中文名称',
    private String EN_BRAND_NAME;//品牌英文名称',
    private String BRAND_SIM_NO;//编码规则',
    private String BRAND_SIGN;//品牌标识',
    private String BRAND_URL;//品牌网址',
    private String BRAND_LOGO;//品牌图片',
    private String BRAND_DESC;//品牌描述',

    public Long getBRAND_NUM_ID() {
        return BRAND_NUM_ID;
    }

    public void setBRAND_NUM_ID(Long BRAND_NUM_ID) {
        this.BRAND_NUM_ID = BRAND_NUM_ID;
    }

    public String getBRAND_NAME() {
        return BRAND_NAME;
    }

    public void setBRAND_NAME(String BRAND_NAME) {
        this.BRAND_NAME = BRAND_NAME;
    }

    public String getEN_BRAND_NAME() {
        return EN_BRAND_NAME;
    }

    public void setEN_BRAND_NAME(String EN_BRAND_NAME) {
        this.EN_BRAND_NAME = EN_BRAND_NAME;
    }

    public String getBRAND_SIM_NO() {
        return BRAND_SIM_NO;
    }

    public void setBRAND_SIM_NO(String BRAND_SIM_NO) {
        this.BRAND_SIM_NO = BRAND_SIM_NO;
    }

    public String getBRAND_SIGN() {
        return BRAND_SIGN;
    }

    public void setBRAND_SIGN(String BRAND_SIGN) {
        this.BRAND_SIGN = BRAND_SIGN;
    }

    public String getBRAND_URL() {
        return BRAND_URL;
    }

    public void setBRAND_URL(String BRAND_URL) {
        this.BRAND_URL = BRAND_URL;
    }

    public String getBRAND_LOGO() {
        return BRAND_LOGO;
    }

    public void setBRAND_LOGO(String BRAND_LOGO) {
        this.BRAND_LOGO = BRAND_LOGO;
    }

    public String getBRAND_DESC() {
        return BRAND_DESC;
    }

    public void setBRAND_DESC(String BRAND_DESC) {
        this.BRAND_DESC = BRAND_DESC;
    }
}
