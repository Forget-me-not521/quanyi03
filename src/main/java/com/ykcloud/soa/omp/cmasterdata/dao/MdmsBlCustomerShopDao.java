package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_CUSTOMER_SHOP;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_SUB_UNIT;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

/**
 * @Author stark.jiang
 * @Date 2021/10/11/8:48
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsBlCustomerShopDao extends Dao<MDMS_BL_CUSTOMER_SHOP> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public boolean checkBlCustomerShopExist(Long tenantNumId, Long dataSign,String reservedNo, String unifiedSocialCreditCode,String series) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from mdms_bl_customer_shop ");
        sb.append(" where tenant_Num_id=? and data_sign=? and reserved_no=? and unified_social_credit_code=?  and cancelsign='N'  ");
        if(!Objects.isNull(series)){
            sb.append(" and series!="+series );
        }
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign,reservedNo,unifiedSocialCreditCode }, int.class) >0 ? true : false;
    }


    public MDMS_BL_CUSTOMER_SHOP getOldCustomerShop(Long tenantNumId, Long dataSign, String reservedNo, String series) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_customer_shop ");
        sb.append(" where tenant_Num_id=? and data_sign=? and reserved_no=? and series=?  and cancelsign='N'  ");

        List<MDMS_BL_CUSTOMER_SHOP> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,reservedNo,series }, new BeanPropertyRowMapper<>(MDMS_BL_CUSTOMER_SHOP.class));
        if(list.size()>1){
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据单号和明细行号查出多条数据!");
        }
        if(CollectionUtils.isNotEmpty(list)){
            return  list.get(0);
        }
        return  null;
    }


    public MDMS_BL_CUSTOMER_SHOP getCustomerShopByReservedNo(Long tenantNumId, Long dataSign, String reservedNo,String cortNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_customer_shop ");
        sb.append(" where tenant_Num_id=? and data_sign=? and reserved_no=? and cort_num_id=? and cancelsign='N'  ");

        List<MDMS_BL_CUSTOMER_SHOP> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,reservedNo,cortNumId}, new BeanPropertyRowMapper<>(MDMS_BL_CUSTOMER_SHOP.class));
        if(list.size()>1){
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据单号和明细行号查出多条数据!");
        }
        if(CollectionUtils.isNotEmpty(list)){
            return  list.get(0);
        }
        return  null;
    }
}
