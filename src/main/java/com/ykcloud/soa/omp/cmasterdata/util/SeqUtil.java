package com.ykcloud.soa.omp.cmasterdata.util;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_UNIT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.gb.soa.sequence.util.SeqGetUtil;
import com.ykcloud.soa.omp.cmasterdata.entity.EX_ARC_SUB_PRICE_TAG_PRINT_DTL;

@Component()
@Lazy(false)
@Scope("singleton")
public class SeqUtil {
    @Resource
    NacosConfigManager nacosConfigManager;

    @PostConstruct
    public void init() {
        SeqGetUtil.initeSequenceConfig(nacosConfigManager);
    }

    public static String getSeqNextValue(String SeqName) {
        return String.valueOf(SeqGetUtil.getNoSubSequence(SeqName));
    }

    public static String getSeqAutoNextValue(Long tenantNumId, Long dataSign, String SeqName) {
        return SeqGetUtil.getAutomicSequence(SeqName, tenantNumId, dataSign);
    }

    public static final String EX_ARC_POS_CONFIG_SERIES = "ex_arc_pos_config_series";

    public static final String MDMS_W_LOC_SERIES = "mdms_w_loc_series";
    public static final String MDMS_W_ZONE_SERIES = "mdms_w_zone_series";
    public static final String MDMS_W_STORAGE_SERIES = "mdms_w_storage_series";
    public static final String MDMS_W_SHIPMENT_SERIES = "mdms_w_shipment_series";
    public static final String MDMS_W_ROUTE_HDR_SERIES = "mdms_w_route_hdr_series";
    public static final String MDMS_W_ROUTE_DTL_SERIES = "mdms_w_route_dtl_series";
    public static final String MDMS_W_PHYSICAL_SERIES = "mdms_w_physical_series";
    public static final String MDMS_O_SUB_UNIT_SERIES = "mdms_o_sub_unit_series";
    public static final String MDMS_O_CUSTOMER_SHOP_SERIES = "mdms_o_customer_shop_series";
    public static final String MDMS_O_TAG_ITEM_SERIES = "mdms_o_tag_item_series";
    public static final String MDMS_O_TAG_SERIES = "mdms_o_tag_series";
    public static final String MDMS_O_TAG_GROUP_SERIES = "mdms_o_tag_group_series";
    public static final String AUTO_MDMS_O_TAG_GROUP_GROUP_TAG_NUM_ID = "auto_mdms_o_tag_group_group_tag_num_id";
    public static final String AUTO_MDMS_O_TAG_TAG_NUM_ID = "mdms_o_tag_tag_num_id";
    public static final String AUTO_MDMS_W_PHYSICAL_PHYSICAL_NUM_ID = "auto_mdms_w_physical_physical_num_id";
    public static final String AUTO_MDMS_W_STORAGE_STORAGE_DEPT_NUM_ID = "auto_mdms_w_storage_storage_dept_num_id";
    public static final String AUTO_MDMS_W_STORAGE_STORAGE_NUM_ID = "auto_mdms_w_storage_storage_num_id";
    public static final String AUTO_MDMS_W_LOC_LOC_NUM_ID = "auto_mdms_w_loc_loc_num_id";
    public static final String AUTO_MDMS_O_SUB_UNIT_SUB_UNIT_NUM_ID= "auto_mdms_o_sub_unit_sub_unit_num_id";
    public static final String AUTO_MDMS_O_SUB_UNIT_SUB_UNIT_NUM_ID_13 = "auto_mdms_o_sub_unit_sub_unit_num_id_13";
    public static final String AUTO_MDMS_O_CUSTOMER_CUSTOMER_NUM_ID = "auto_mdms_o_customer_customer_num_id";
    // 客户合同生成行号
    public static final String AUTO_MDMS_O_CORT_CORT_NUM_ID = "auto_mdms_o_cort_cort_num_id";
    public static final String AUTO_MDMS_O_CUSTOMER_SHOP_NUM_ID = "auto_mdms_o_customer_shop_num_id";
    // 客户单元生成行号
    public static final String MDMS_O_UNIT_SERIES = "mdms_o_unit_series";
    public static final String MDMS_O_UNIT_ORG_SERIES = "mdms_o_unit_org_series";
    public static final String AUTO_MDMS_O_UNIT_UNIT_NUM_ID = "auto_mdms_o_unit_unit_num_id";

    public static final String EX_ARC_CORT_SUB_UNIT_TML_SERIES = "ex_arc_cort_sub_unit_tml_series";

    public static final String MDMS_P_RETURN_CONFIG_SERIES = "mdms_p_return_config_series";
    public static final String MDMS_BL_UNIT_SERIES = "mdms_bl_unit_series";

    public static final String EX_ARC_SUB_PRICE_TAG_PRINT_HDR_SERIES = "ex_arc_sub_price_tag_print_hdr_series";
    public static final String EX_ARC_SUB_PRICE_TAG_PRINT_DTL_SERIES = "ex_arc_sub_price_tag_print_dtl_series";
    public static final String EX_ARC_SUB_PRICE_TAG_PRINT_HDR_RESERVED = "ex_arc_sub_price_tag_print_hdr_reserved";
    public static final String MDMS_S_SYNC_PROGRESS_SERIES = "mdms_s_sync_progress_series";
    public static final String EX_ARC_EMPE_SERIES = "ex_arc_empe_series";
    public static final String EX_ARC_SUB_PRICE_TAG_SERIES = "ex_arc_sub_price_tag_series";
    public static final String AUTO_EX_ARC_SUB_PRICE_TAG_TAG_NUM_ID = "auto_ex_arc_sub_price_tag_tag_num_id";
    public static final String AUTO_MDMS_O_DEPART_DEPART_NUM_ID = "auto_mdms_o_depart_depart_num_id";
    public static final String MDMS_O_SUB_UNIT_PAY_TYPE_SERIES = "mdms_o_sub_unit_pay_type_series";

    public static final String MDMS_O_SUB_UNIT_RECSTORAGE_SERIES = "mdms_o_sub_unit_recstorage_series";
    public static final String AUTO_T_BUSINESS_UNIT_ID = "auto_t_business_unit_id";
    public static final String T_USER_ID = "t_user_id";
    public static final String T_ORG_ID = "t_org_id";
    public static final String MDMS_C_TYPE_SERIES = "mdms_c_type_series";
    public static final String SYS_POS_POWER_SERIES = "sys_pos_power_series";
    public static final String MMDMS_S_CONFIG_SERIES = "mdms_s_config_series";


    public static final String MDMS_O_DEPART_SERIES = "mdms_o_depart_series";

    public static final String MDMS_O_UNIT_STATUS_SERIES = "mdms_o_unit_status_series";
    public static final String MDMS_O_SUB_UNIT_STATUS_SERIES = "mdms_o_sub_unit_status_series";
    public static final String MDMS_O_PRODUCT_ORIGIN_SERIES = "mdms_o_product_origin_series";
    public static final String MDMS_P_ITEM_STATUS_SERIES = "mdms_p_item_status_series";
    public static final String MDMS_P_ITEM_STATUS_CONFIG_SERIES = "mdms_p_item_status_config_series";
    public static final String MDMS_P_BRAND_SERIES = "mdms_p_brand_series";
    public static final String T_USER_MANGAGE_ORG_ID = "t_user_mangage_org_id";
    public static final String PLATFORM_AUTO_SEQUENCE_SERIES = "platform_auto_sequence_series";
    public static final String EX_ARC_CHANNEL_SERIES = "ex_arc_channel_series";

    public static final String COMMONCALLTABLE_SERIES = "commoncalltable_series";
    public static final String PLATFORM_SEQUENCE_SERIES = "platform_sequence_series";


    public static final String MDMS_O_PROVINCE_SERIES = "mdms_o_province_series";
    public static final String MDMS_O_CITY_SERIES = "mdms_o_city_series";
    public static final String MDMS_O_CITY_AREA_SERIES = "mdms_o_city_area_series";
    public static final String MDMS_O_CITY_TOWN_SERIES = "mdms_o_city_town_series";
    public static final String COMMON_QUERY_SERIES = "common_query_series";
    public static final String EC_CACHE_METHOD_SCHEMA_DEFINE_SERIES = "ec_cache_method_schema_define_series";
    public static final String MDMS_BL_CUST_UNIT_SERIES = "mdms_bl_cust_unit_series";
    public static final String MDMS_O_SUPPLY_ANNUAL_REPORT_LOG_SERIES = "mdms_o_supply_annual_report_log_series";

    public static final String MDMS_O_SUPPLY_ANNUAL_REPORT_HDR_SERIES = "mdms_o_supply_annual_report_hdr_series";
    public static final String MDMS_O_SUPPLY_ANNUAL_REPORT_DTL_SERIES = "mdms_o_supply_annual_report_dtl_series";


    public static final String MDMS_O_LICENSE_RESOURCES_SERIES = "mdms_o_license_resources_series";
    public static final String MDMS_BL_PROCESS_OPERATE_LOG_SERIES = "mdms_bl_process_operate_log_series";

    public static final String MDMS_BL_MANAGER_HDR_SERIES = "mdms_bl_manager_hdr_series";
    public static final String MDMS_BL_CORT_SERIES = "mdms_bl_cort_series";
    public static final String MDMS_O_CORT_SERIES = "mdms_o_cort_series";
    public static final String MDMS_BL_PROCESS_MASTER_HDR_SERIES = "mdms_bl_process_master_hdr_series";

    public static final String MDMS_BL_PARTNER_SERIES = "mdms_bl_partner_series";
    public static final String MDMS_BL_CUSTOMER_SERIES = "mdms_bl_customer_series";
    public static final String MDMS_O_PARTNER_SERIES = "mdms_o_partner_series";
    public static final String MDMS_O_SUB_PARTNER_SERIES = "mdms_o_sub_partner_series";
    public static final String MDMS_BL_SUPPLY_SERIES = "mdms_bl_supply_series";
    public static final String MDMS_O_SUPPLY_SERIES = "mdms_o_supply_series";
    public static final String MDMS_O_SUB_SUPPLY_SERIES = "mdms_o_sub_supply_series";
    public static final String MDMS_BL_MANAGER_HDR_RESERVED_NO = "mdms_bl_manager_hdr_reserved_no";
    public static final String AUTO_MDMS_O_PARTNER_PARTNER_NUM_ID = "auto_mdms_o_partner_partner_num_id";
    public static final String AUTO_MDMS_O_SUB_PARTNER_PARTNER_NUM_ID = "auto_mdms_o_sub_partner_partner_num_id";
    public static final String AUTO_MDMS_O_SUPPLY_SUPPLY_NUM_ID = "auto_mdms_o_supply_supply_num_id";
    public static final String AUTO_MDMS_O_SUB_SUPPLY_SUPPLY_NUM_ID = "auto_mdms_o_sub_supply_supply_num_id";
    public static final String MDMS_BL_SUB_UNIT_SERIES = "mdms_bl_sub_unit_series";
    public static final String MDMS_O_CUSTOMER_SERIES = "mdms_o_customer_series";
    public static final String MDMS_BL_CUSTOMER_SHOP_SERIES = "mdms_bl_customer_shop_series";
    public static final String MDMS_O_SUB_CUSTOMER_SERIES = "mdms_o_sub_customer_series";


    public static final String MDMS_BL_PROCESS_MASTER_HDR_RESERVED_NO = "mdms_bl_process_master_hdr_reserved_no";

    public static final String MDMS_ASSIST_ON_OFF_SERIES = "mdms_assist_on_off_series";

    // 公告
    public static final String MDMS_O_NOTICE_SERIES = "mdms_o_notice_series";
    public static final String AUTO_MDMS_O_NOTICE_NOTICE_NUM_ID = "auto_mdms_o_notice_notice_num_id";

    // 公告
    public static final String MDMS_O_LICENSE_FIRST_SERIES = "mdms_o_license_first_series";
    public static final String MDMS_O_LICENSE_SECOND_SERIES = "mdms_o_license_second_series";
    public static final String MDMS_O_LICENSE_ALLOW_SERIES = "mdms_o_license_allow_series";
    //BTOB
    public static final String MDMS_SCM_B2B_BL_CHANNEL_POLICY_HDR_SERIES="scm_b2b_bl_channel_policy_hdr_series";
    public static final String MDMS_SCM_B2B_BL_CHANNEL_POLICY_HDR_RESERVED_NO="scm_b2b_bl_channel_policy_hdr_reserved_no";
    public static final String MDMS_SCM_B2B_BL_CHANNEL_POLICY_DTL_SERIES="scm_b2b_bl_channel_policy_dtl_series";
    public static final String MDMS_SCM_B2B_BL_CHANNEL_POLICY_SERIES="scm_b2b_bl_channel_policy_series";
    public static final String MDMS_B2B_CS_PRODUCT_SERIES="mdms_b2b_cs_product_series";
    //价格政策
    public final static String MDMS_B2B_BL_CS_POLICY_HDR_SERIES = "fi_bl_credit_hdr_series";
    public final static String MDMS_B2B_BL_CS_POLICY_DTL_SERIES = "fi_bl_credit_dtl_series";
    public final static String MDMS_B2B_BL_CS_POLICY_HDR_RESERVED_NO = "fi_bl_credit_hdr_reserved_no";

    public final static String MDMS_B2B_SUB_RELATION_SERIES = "mdms_b2b_sub_relation_series";

}
