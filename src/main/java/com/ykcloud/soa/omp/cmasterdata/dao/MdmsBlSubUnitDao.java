package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.BlSubUnit;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_SUB_UNIT;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;


@Repository
public class MdmsBlSubUnitDao extends Dao<MDMS_BL_SUB_UNIT> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_BL_SUB_UNIT.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_BL_SUB_UNIT.class, ",");
    private static final String TABLE_NAME = "MDMS_BL_SUB_UNIT";

    public boolean checkBlSubUnitExist(Long tenantNumId, Long dataSign,String reservedNo, String subUnitId,String series) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from MDMS_BL_SUB_UNIT ");
        sb.append(" where tenant_Num_id=? and data_sign=? and reserved_no=? and sub_unit_id=?  and cancelsign='N'  ");
        if(!Objects.isNull(series)){
            sb.append(" and series!="+series );
        }
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign,reservedNo,subUnitId }, int.class) >0 ? true : false;
    }
    public MDMS_BL_SUB_UNIT getOldBlSubUnit(Long tenantNumId, Long dataSign,String reservedNo,String series) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from MDMS_BL_SUB_UNIT ");
        sb.append(" where tenant_Num_id=? and data_sign=? and reserved_no=? and series=?  and cancelsign='N'  ");

        List<MDMS_BL_SUB_UNIT> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,reservedNo,series }, new BeanPropertyRowMapper<>(MDMS_BL_SUB_UNIT.class));
        if(list.size()>1){
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据单号和明细行号查出多条数据!");
        }
        if(CollectionUtils.isNotEmpty(list)){
            return  list.get(0);
        }
        return  null;
    }
    public MDMS_BL_SUB_UNIT getBlSubUnitEntity(Long tenantNumId, Long dataSign,String reservedNo) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from MDMS_BL_SUB_UNIT ");
        sb.append(" where tenant_Num_id=? and data_sign=? and reserved_no=? and cancelsign='N'  ");

        List<MDMS_BL_SUB_UNIT> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,reservedNo }, new BeanPropertyRowMapper<>(MDMS_BL_SUB_UNIT.class));
        if(list.size()>1){
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据单号和明细行号查出多条数据!");
        }
        if(CollectionUtils.isNotEmpty(list)){
            return  list.get(0);
        }
        return  null;
    }
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }


    public BlSubUnit getBlEntity(Long tenantNumId, Long dataSign, String reservedNo,String cortNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from MDMS_BL_SUB_UNIT ");
        sb.append(" where tenant_Num_id=? and data_sign=? and reserved_no=? and cort_num_id=? and cancelsign='N'  ");
        List<BlSubUnit> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,reservedNo,cortNumId}, new BeanPropertyRowMapper<>(BlSubUnit.class));
        if(CollectionUtils.isNotEmpty(list)){
            if(list.size()>1){
                throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                        "根据单号和明细行号查出多条数据!");
            }
            return  list.get(0);
        }
        return  null;
    }
}
