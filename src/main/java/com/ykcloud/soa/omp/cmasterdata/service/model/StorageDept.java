package com.ykcloud.soa.omp.cmasterdata.service.model;

/**
 * @author tz.x
 * @date 2020/11/30 22:01
 */
public class StorageDept {

    private Long groupNumId;

    private Long storageDeptNumId;

    public Long getGroupNumId() {
        return groupNumId;
    }

    public void setGroupNumId(Long groupNumId) {
        this.groupNumId = groupNumId;
    }

    public Long getStorageDeptNumId() {
        return storageDeptNumId;
    }

    public void setStorageDeptNumId(Long storageDeptNumId) {
        this.storageDeptNumId = storageDeptNumId;
    }
}
