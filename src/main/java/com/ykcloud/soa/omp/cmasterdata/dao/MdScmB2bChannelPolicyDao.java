package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.request.BtobChannelPolicyGetRequest;
import com.ykcloud.soa.omp.cmasterdata.entity.ScmB2bChannelPolicy;
import com.ykcloud.soa.omp.cmasterdata.util.MdDaoUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
@Repository
public class MdScmB2bChannelPolicyDao  extends Dao<ScmB2bChannelPolicy> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }
    private static final String TABLE_NAME = "mdms_b2b_channel_policy";

    public List<ScmB2bChannelPolicy> getChannelPolicyListPage(BtobChannelPolicyGetRequest r) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT r.* FROM ");
        sb.append(TABLE_NAME + " r where 1=1");
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("r","unit_num_id",r.getUnitNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("r","channel_num_id",r.getChannelNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("r","update_dtme",r.getUpdateDtme()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("r","item_num_id",r.getItemNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("r","pty3_num_id",r.getPty3NumId()));
        sb.append(MdDaoUtil.TENANT_NUM_ID_DATA_SIGN_SQL("r"));
        Integer start = (r.getPageNum()-1)*r.getPageSize();
        sb.append(" limit ?,? ");
        List<ScmB2bChannelPolicy> list = jdbcTemplate.query(sb.toString(), new Object[]{r.getUnitNumId(), r.getChannelNumId(),r.getUpdateDtme(),r.getItemNumId(),r.getPty3NumId(), r.getTenantNumId(), r.getDataSign(),start,r.getPageSize()}, new BeanPropertyRowMapper<>(ScmB2bChannelPolicy.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    public Integer getChannelPolicyListPageCount(BtobChannelPolicyGetRequest r) {
        String sb = "SELECT count(1) FROM " +
                TABLE_NAME + " where cancelsign='N' and tenant_num_id =? and data_sign = ?" +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("unit_num_id", r.getUnitNumId()) +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("channel_num_id", r.getChannelNumId()) +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("update_dtme", r.getUpdateDtme()) +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("item_num_id", r.getItemNumId()) +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("pty3_num_id", r.getPty3NumId());
        return jdbcTemplate.queryForObject(sb, new Object[]{r.getTenantNumId(), r.getDataSign(),r.getUnitNumId(), r.getChannelNumId(), r.getItemNumId(),r.getPty3NumId(), r.getTenantNumId()}, Integer.class);
    }

}
