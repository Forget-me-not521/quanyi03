package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.ykcloud.soa.erp.common.utils.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.api.model.EmpeInfo;
import com.ykcloud.soa.omp.cmasterdata.entity.EX_ARC_EMPE;
import com.ykcloud.soa.omp.cmasterdata.entity.T_ORG;
import com.ykcloud.soa.omp.cmasterdata.entity.T_USER;
import com.ykcloud.soa.omp.cmasterdata.service.model.BussinessUnit;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

/**
 * @Author stark.jiang
 * @Date 2021/09/18/14:10
 * @Description:
 * @Version 1.0
 */
@Repository
public class ExArcEmpeDao {
    @Resource(name = "platFormJdbcTemplate")
    private JdbcTemplate jdbcTemplate;


    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(EX_ARC_EMPE.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(EX_ARC_EMPE.class, ",");
    private static final String TABLE_NAME = "EX_ARC_EMPE";

    /**
     * 根据行号查询工牌号码
     */
    public String checkExistWorkNums(Long tenantNumId, Long dataSign, String series) {
        String sql = " SELECT work_nums from ex_arc_empe WHERE tenant_num_id=? and data_sign=? and series=? ; ";
        String workNums = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, String.class);
        if (Objects.isNull(workNums)) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "用户不存在!行号：" + series);
        }
        return workNums;
    }



    //获取分页总条数
    public Integer getTotalCountByEmpeIdOrNameOrWorkNum(Long tenantNumId, Long dataSign, String empeIdOrNameOrWorkNum) {
        String sql = null;
        Integer count = null;
        if (StringUtils.isEmpty(empeIdOrNameOrWorkNum)) {
            sql = "select count(1) from ex_arc_empe where tenant_num_id = ? AND data_sign = ? AND cancelsign = 'N'";
            count = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign}, Integer.class);
        } else {
            sql = " SELECT count(1) " +
                    " FROM ex_arc_empe WHERE tenant_num_id = ? AND data_sign = ? and (empe_num_id = ? " +
                    " OR work_nums like ? OR empe_name LIKE ?) AND cancelsign = 'N' ";
            count = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, empeIdOrNameOrWorkNum, "%" + empeIdOrNameOrWorkNum + "%",
                    "%" + empeIdOrNameOrWorkNum + "%"}, Integer.class);
        }
        return count;
    }

    public boolean checkExistLoginName(Long tenantNumId, Long dataSign, String loginName) {
        String sql = "select Login_name as loginName from t_user where  " +
                "Login_name=? and is_valid='Y' ";
        String loginNames = jdbcTemplate.queryForObject(sql, new Object[]{loginName}, String.class);
        if (StringUtils.isNotEmpty(loginNames)) {
            return true;
        }
        return false;
    }

    public void insertBusinessEntity(Long id, String code, String name, String rootCode, String pid) {
        String safesql = "INSERT INTO t_business_unit(" + //
                "id," + //行号
                "code," + //租户ID
                "name," + //0: 正式  1：测试
                "rootCode,pid" +
                ") VALUES( ?, ?, ?, ?,?)";
        int result = jdbcTemplate.update(safesql, //
                id,
                code,
                name,
                rootCode, pid
        );
        if (result == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "插入组织表失败！");
        }
    }

    public void insertBusinessEntityNew(Long tenantNumId, Long dataSign, Long id, String code, String name, String rootCode, String pid) {
        String safesql = "INSERT INTO t_business_unit(" + //
                "id," + //行号
                "code," + //租户ID
                "name," + //0: 正式  1：测试
                "rootCode,pid, tenant_num_id, data_sign" +
                ") VALUES( ?, ?, ?, ?,?, ?, ?)";
        int result = jdbcTemplate.update(safesql, //
                id,
                code,
                name,
                rootCode, pid, tenantNumId, dataSign
        );
        if (result == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "插入组织表失败！");
        }
    }


    public void insertExArcEmpe(EX_ARC_EMPE ex_arc_empe) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(EX_ARC_EMPE.class, ex_arc_empe));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "  数据插入失败! ");
        }
    }

    public boolean checkExistBySeries(Long series, Long tenantNumId, Long dataSign) {
        return jdbcTemplate.queryForObject("select count(*) from EX_ARC_EMPE where tenant_num_id=? and data_sign=? and series=? and cancelsign='N'", new Object[]{tenantNumId, dataSign, series}, Integer.class) > 0;
    }

    public void updateEntity(EX_ARC_EMPE entity) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE EX_ARC_EMPE SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(entity, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND series = ?");
        List<Object> objects = new ArrayList(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(EX_ARC_EMPE.class, entity)));
        objects.add(entity.getDATA_SIGN());
        objects.add(entity.getTENANT_NUM_ID());
        objects.add(entity.getSERIES());
        int row = jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "EX_ARC_EMPE更新失败!");
        }
    }

    /**
     * 更新用户表
     */
    public void updateUserEntity(EX_ARC_EMPE entity) {
        String sql = " UPDATE t_user SET Phone=?,Email=?,update_user=? ,update_date= NOW() " +
                " WHERE tenant_num_id=? and data_sign=? and Login_name= ? ";
        int row = jdbcTemplate.update(sql, new Object[]{entity.getEMPE_TELEPHONE(), entity.getEMPE_MAIL(), entity.getLAST_UPDATE_USER_ID(),
                entity.getTENANT_NUM_ID(), entity.getDATA_SIGN(), entity.getWORK_NUMS()});
        if (row <= 0) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "更新用户表失败!");
        }
    }

    public void deleteRecordBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "delete from EX_ARC_EMPE where TENANT_NUM_ID = ? AND DATA_SIGN = ? and series=? and cancelsign='N'";
        int row = jdbcTemplate.update(sql, tenantNumId, dataSign, series);
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "EX_ARC_EMPE删除失败!");
        }
    }

    public void insertEntity(EX_ARC_EMPE entity) {
        String safesql = "INSERT INTO EX_ARC_EMPE(" + //
                "SERIES," + //行号
                "TENANT_NUM_ID," + //租户ID
                "DATA_SIGN," + //0: 正式  1：测试
                "DIV_NUM_ID," +
                "work_nums," +
                "empe_mail," +
                "EMPE_NUM_ID," +
                "EMPE_NAME," +
                "EN_EMPE_NAME," +
                "CUY_NUM_ID," +
                "DEPART_NUM_ID," +
                "UNIT_NUM_ID," +
                "SUB_UNIT_NUM_ID," +
                "SUB_UNIT_NAME," +
                "SUB_UNIT_TYPE," +
                "PURCHASE_TYPE_NUM_ID," +
                "DEFAULT_SUPPLY," +
                "CREATE_USER_ID," + //用户
                "LAST_UPDATE_USER_ID," + //更新用户
                "depart_name," +
                "CREATE_DTME," + //创建时间
                "LAST_UPDTME," + //最后更新时间
                "CANCELSIGN" + //删除
                ") VALUES( ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,0,now(),now(),'N')";

        int result = jdbcTemplate.update(safesql, //
                entity.getSERIES(),//行号
                entity.getTENANT_NUM_ID(),//租户ID
                entity.getDATA_SIGN(),//0: 正式  1：测试
                entity.getDIV_NUM_ID(),
                entity.getWORK_NUMS(),
                entity.getEMPE_MAIL(),
                entity.getEMPE_NUM_ID(),//
                entity.getEMPE_NAME(),//
                entity.getEN_EMPE_NAME(),//
                entity.getCUY_NUM_ID(),//
                entity.getDEPART_NUM_ID(),
                entity.getUNIT_NUM_ID(),
                entity.getSUB_UNIT_NUM_ID(),
                entity.getSUB_UNIT_NAME(),
                entity.getSUB_UNIT_TYPE(),
                entity.getPURCHASE_TYPE_NUM_ID(),
                entity.getDEFAULT_SUPPLY(),
                entity.getCREATE_USER_ID(),//用户
                entity.getLAST_UPDATE_USER_ID()
        );
        if (result == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "插入员工表失败！");
        }
    }

    public void insertUserEntity(T_USER entity) {
        String safesql = "INSERT INTO T_USER(" + //
                "id," + //行号
                "name," + //租户ID
                "Login_name," + //0: 正式  1：测试
                "salt," +
                "password," +
                "Main_org," +
                "Gender," +
                "Birthday," +
                "Phone," +
                "Email," +
                "Photo," +
                "English_name," +
                "passwordModifyTime," +
                "comments," +
                "create_user," + //用户
                "update_user," + //更新用户
                "create_date," + //创建时间
                "update_date," + //最后更新时间
                "is_valid," + //删除
                "tenant_num_id," +
                "data_sign" +
                ") VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?, ?,?,?,?,?,now(),now(),?,?,?)";
        int result = jdbcTemplate.update(safesql, //
                entity.getID(),//行号
                entity.getNAME(),//租户ID
                entity.getLOGIN_NAME(),//0: 正式  1：测试
                entity.getSALT(),
                entity.getPASSWORD(),//
                entity.getMAIN_ORG(),//
                entity.getGENDER(),//
                entity.getBIRTHDAY(),//
                entity.getPHONE(),
                entity.getEMAIL(),
                entity.getPHOTO(),
                entity.getENGLISH_NAME(),
                entity.getPASSWORDMODIFYTIME(),
                entity.getCOMMENTS(),
                entity.getCREATE_USER(),//用户
                entity.getUPDATE_USER(), entity.getIS_VALID(), entity.getTENANT_NUM_ID(), entity.getDATA_SIGN()
        );
        if (result == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "插入员工表失败！");
        }
    }

    public void insertOrgEntity(T_ORG entity) {
        String safesql = "INSERT INTO T_ORG(" + //
                "id," + //行号
                "org_name," + //租户ID
                "Org_kind," + //0: 正式  1：测试
                "parent_id," +
                "path_Id," +
                "path_Name," +
                "User_id," +
                "seq," +
                "enabled," +
                "create_date," + //创建时间
                "update_date," + //最后更新时间
                "is_valid," + //删除
                "tenant_num_id," +
                "data_sign" +
                ") VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?,now(),now(),?,?,?)";


        int result = jdbcTemplate.update(safesql, //
                entity.getID(),//行号
                entity.getORG_NAME(),//租户ID
                entity.getORG_KIND(),//0: 正式  1：测试
                entity.getPARENT_ID(),
                entity.getPATH_ID(),//
                entity.getPATH_NAME(),//
                entity.getUSER_ID(),//
                entity.getSEQ(),//
                entity.getENABLED(), entity.getIS_VALID(), entity.getTENANT_NUM_ID(), entity.getDATA_SIGN()
        );
        if (result == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "插入员工组织表失败！");
        }
    }


    public void insertRoleEntity(Long tenant_num_id, Long empe_num_id, String tenant_name) {
        String safesql = "INSERT INTO t_role(" + //
                "id," + //行号
                "name," + //租户ID
                "role_kind," + //0: 正式  1：测试
                "seq," +
                "enabled," +
                "specialAuthorization," +
                "comments," +
                "create_user," +
                "update_user," + //创建时间
                "tenant_num_id," + //删除
                "data_sign," +
                "is_valid," +
                "create_date," +
                "update_date" + //最后更新时间
                ") VALUES( ?, ?, ?, ?, ?, ?, ?, ?,?,?,?, ?,now(),now())";
        int result = jdbcTemplate.update(safesql,
                empe_num_id,
                tenant_name + "管理员",
                "按功能划分",
                empe_num_id,
                "Y",
                "1",
                tenant_name + "管理员",
                empe_num_id,
                empe_num_id, tenant_num_id, 0L, "Y"
        );
        if (result == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "插入员工组织表失败！");
        }
    }

    public void insertRoleOrgEntity(Long tenant_num_id, Long empe_num_id) {
        String safesql = "INSERT INTO t_role_org(id,Role_id,org_id,tenant_num_id) VALUES( ?, ?, ?, ?)";
        int result = jdbcTemplate.update(safesql, empe_num_id, empe_num_id, empe_num_id, tenant_num_id);
        if (result == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "插入员工组织表失败！");
        }
    }

    public void insertRoleResourceEntity(Long roleResourceId, String resource_id, Long tenant_num_id, Long empe_num_id) {
        String safesql = "INSERT INTO t_role_resource(id,Role_id,resource_id,tenant_num_id) VALUES( ?, ?, ?, ?)";
        int result = jdbcTemplate.update(safesql, roleResourceId, empe_num_id, resource_id, tenant_num_id);
        if (result == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "插入员工组织表失败！");
        }
    }

    public Long getNextResourceId() {
        String sql = " SELECT max(id)+1 from t_role_resource   ";
        return jdbcTemplate.queryForObject(sql, new Object[]{}, Long.class);
    }

    public Long getNextArcEmpeNumId() {
        String sql = " SELECT max(empe_num_id)+1 from ex_arc_empe   ";
        return jdbcTemplate.queryForObject(sql, new Object[]{}, Long.class);
    }

    public void insertArcTenant(Long tenant_num_id, String tenant_name, String tenant_name_short, String corporation, String telphone) {
        String safesql = "INSERT INTO ex_arc_tenant(" + //
                "series,tenant_num_id,data_sign,tenant_name,tenant_name_short,corporation,telphone,status_num_id,create_dtme,last_updtme,create_user_id,last_update_user_id,cancelsign " +
                ") VALUES( ?, ?, ?, ?,? ,?, ?, ?, now(),now(), 1, 1,'N')";
        int result = jdbcTemplate.update(safesql, tenant_num_id, tenant_num_id, 0, tenant_name, tenant_name_short, corporation, telphone, 1);
        if (result == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "插入租户表失败！");
        }
    }

    public String getPidByCode(Long tenantNumId, Long dataSign, String code) {
        String sql = "select id from t_business_unit " +
                "where tenant_num_id = ? and data_sign = ? " +
                "and code = ?";
        String pid = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, code}, String.class);
        if (pid == null) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "根据code未查询到对应的pid");
        }
        return pid;
    }

    public Long getOrgByUsrNumId(Long tenantNumId, Long dataSign, Long usrNumId) {
        String sql = "select id from t_org " +
                "where tenant_num_id = ? and data_sign = ? and user_id = ?";
        Long pid = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, usrNumId}, Long.class);
        if (pid == null) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "根据用户编码未查询到对应的组织");
        }
        return pid;
    }

    public void updateManagerOrg(Long tenantNumId, Long dataSign, Long orgId, String orgIds, String orgCodes) {
        String sql = " UPDATE t_user_mangage_org SET org_ids=? " +
                " WHERE tenant_num_id=? and data_sign=? and user_id= ? and dbname=?  ";
        int row = jdbcTemplate.update(sql, new Object[]{orgIds, tenantNumId, dataSign, orgId, orgCodes});
        if (row <= 0) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "更新用户组织表失败!");
        }
    }

    public boolean checkExistManagerOrg(Long tenantNumId, Long dataSign, Long orgId, String orgCodes) {
        String sql = "select count(1) from t_user_mangage_org  WHERE tenant_num_id=? and data_sign=? and user_id= ? and dbname=? ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, orgId, orgCodes}, Integer.class) > 0 ? true : false;
    }

    public BussinessUnit getBusinessUnit(Long tenantNumId, Long dataSign, String code, String rootCode, String pid) {
        String sql = "select id,code,rootCode from t_business_unit where tenant_num_id = ? and data_sign = ? and code = ? and rootCode=? and pid=? ";
        List<BussinessUnit> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, code, rootCode, pid}, new BeanPropertyRowMapper<BussinessUnit>(BussinessUnit.class));
        System.out.println("getBusinessUnit===" + list.size());
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    public EmpeInfo getSubUnitNumIdByUsrNumId(Long tenantNumId, Long dataSign, Long usrNumId) {
        String sql = "select empe_num_id , work_nums ,empe_name,sub_unit_num_id,head_sub_unit_num_id  from ex_arc_empe where tenant_num_id = ? and data_sign = ? and empe_num_id = ?";
        List<EmpeInfo> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, usrNumId}, new BeanPropertyRowMapper<EmpeInfo>(EmpeInfo.class));
        if (CollectionUtils.isNotEmpty(list)) {
            if (list.size() > 1) {
                throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "根据用户编码查询到多条用户信息");
            } else {
                return list.get(0);
            }
        }
        return null;
    }



    public String getBusinessIdByRootCode(Long tenantNumId, Long dataSign, String rootCode) {
        String ids = null;
        String sql = "select id from t_business_unit where rootCode=? and pid>0 limit 100000";
        List<String> list = jdbcTemplate.queryForList(sql, new Object[]{rootCode}, String.class);
        if (!list.isEmpty()) {
            ids = String.join(",", list);
        }
        return ids;
    }

    public void updateBusinessIdByRootCode(Long tenantNumId, Long dataSign, String dbname, Long userId) {
        String sql = "update t_user_mangage_org set org_ids=null where tenant_num_id = ? and data_sign = ? and user_id=? and dbname=? ";
        jdbcTemplate.update(sql, new Object[]{tenantNumId, dataSign, userId, dbname});
    }

    public String getOrgIdsByOrgIdNotMustExist(Long tenantNumId, Long dataSign, Long orgId, String tableName, String fieldName) {
        String sql = "select mg.org_ids from  t_org o  " +
                "inner join t_user_mangage_org mg on o.id=mg.user_id and o.tenant_num_id=mg.tenant_num_id and o.data_sign=mg.data_sign " +
                "inner join t_data_permission_config pc on mg.dbname = pc.dbname and pc.tenant_num_id=mg.tenant_num_id and pc.data_sign =mg.data_sign  " +
                "where o.id=? and pc.tablename = ? and pc.searchfields=? and o.tenant_num_id=? and o.data_sign=?  limit 1 ";
        String orgIds = jdbcTemplate.queryForObject(sql, new Object[]{orgId, tableName, fieldName, tenantNumId, dataSign}, String.class);
        return orgIds;
    }

    public List<String> getCodeListByIds(List<Long> ids) {
        String sql = "select code from t_business_unit WHERE id in (:ids) and pid<>0 limit 100000";
        NamedParameterJdbcTemplate namedJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        Map<String, Object> params = new HashMap<>();
        params.put("ids", ids);
        List<String> list = namedJdbcTemplate.queryForList(sql, params, String.class);
        return list;
    }


    public void insertManagerEntity(Long tenantNumId, Long dataSign, Long userId, String org_ids, String org_codes) {
        String safesql = "INSERT INTO t_user_mangage_org(" + //
                "id," + //行号
                "user_id," + //租户ID
                "org_ids," + //0: 正式  1：测试
                "org_codes," +
                "level," +
                "dbname, " +
                "tenant_num_id, " +
                "data_sign " +
                ") VALUES( ?, ?, ?, ?,0,?,?,?)";
        Long id = Long.valueOf(SeqUtil.getSeqNextValue(SeqUtil.T_USER_MANGAGE_ORG_ID));
        int result = jdbcTemplate.update(safesql, //
                id,//行号
                userId,//租户ID
                org_ids,//0: 正式  1：测试
                org_codes,
                org_codes, tenantNumId, dataSign
        );
        if (result == 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE30034, "插入组织表失败！");
        }
    }

    public String getWorkNums(Long tenantNumId, Long dataSign, Long userId) {
        String sql = " SELECT work_nums from ex_arc_empe where tenant_num_id=? and data_sign=? and empe_num_id=?   ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, userId}, String.class);
    }

}
