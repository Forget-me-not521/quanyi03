package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.entity.COMMONCALLTABLE;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_W_PHYSICAL;
import com.ykcloud.soa.omp.cmasterdata.entity.T_MENU;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class CommoncalltableDao {
    @Resource(name = "platFormJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(COMMONCALLTABLE.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(COMMONCALLTABLE.class, ",");
    private static final String TABLE_NAME = "COMMONCALLTABLE";

    public boolean checkExistCmd(String cmd) {
        String sql = "select count(*) from commoncalltable where cmd = ? ";
        int number = jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{cmd});
        return number == 0 ? false : true;
    }

    public boolean checkExistFuncname(String funcname) {
        String sql = "select count(*) from commoncalltable where funcname = ?";
        int number = jdbcTemplate.queryForObject(sql, Integer.class, funcname);
        return number == 0 ? false : true;
    }

    public boolean checkExistBeanidMethod(String beanid, String method) {
        String sql = "select count(*) from commoncalltable where beanid = ? and method=? ";
        int number = jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{beanid, method});
        return number == 0 ? false : true;
    }

    public boolean checkExistSeries(String series) {
        String sql = "select count(*) from commoncalltable where series = ?";
        int number = jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{series});
        return number == 0 ? false : true;
    }

    public COMMONCALLTABLE findCommoncallBySeries(String series) throws Exception {
        String sql = "select * from commoncalltable where series = ?";
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(COMMONCALLTABLE.class), series);
    }

    public int insertCommoncalltableNew(String cmd, String funcname, String beanid, String method, String request_sample, String remark, String series) {

        String sql = "insert into commoncalltable(cmd,funcname,beanid,method,request_sample,remark,series) values(?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql, new Object[]{cmd, funcname, beanid, method, request_sample, remark, series});

    }

    public int updateCommoncalltable(String cmd, String funcname, String beanid, String method, String request_sample, String remark, String series) {

        String sql = "update commoncalltable set cmd=?,funcname=?,beanid=?,method=?,request_sample=?,remark=? where series = ?";
        return jdbcTemplate.update(sql, new Object[]{cmd, funcname, beanid, method, request_sample, remark, series});

    }
}
