package com.ykcloud.soa.omp.cmasterdata.entity;

public class EX_ARC_SUB_PRICE_TAG extends BaseEntity {
    private Long SUB_UNIT_NUM_ID;// 门店编号
    private Long TAG_NUM_ID;// 价签编号
    private String TAG_NAME;// 价签名称
    private Integer TYPE_NUM_ID;// 价签类型,0正常，1促销 2会员
    private Integer IS_DESIGN;// 是否已设计，0否1是
    private String TAG_TEMPLATE;// 价签模板

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long sUB_UNIT_NUM_ID) {
        SUB_UNIT_NUM_ID = sUB_UNIT_NUM_ID;
    }

    public Long getTAG_NUM_ID() {
        return TAG_NUM_ID;
    }

    public void setTAG_NUM_ID(Long tAG_NUM_ID) {
        TAG_NUM_ID = tAG_NUM_ID;
    }

    public String getTAG_NAME() {
        return TAG_NAME;
    }

    public void setTAG_NAME(String tAG_NAME) {
        TAG_NAME = tAG_NAME;
    }

    public Integer getTYPE_NUM_ID() {
        return TYPE_NUM_ID;
    }

    public void setTYPE_NUM_ID(Integer tYPE_NUM_ID) {
        TYPE_NUM_ID = tYPE_NUM_ID;
    }

    public Integer getIS_DESIGN() {
        return IS_DESIGN;
    }

    public void setIS_DESIGN(Integer iS_DESIGN) {
        IS_DESIGN = iS_DESIGN;
    }

    public String getTAG_TEMPLATE() {
        return TAG_TEMPLATE;
    }

    public void setTAG_TEMPLATE(String tAG_TEMPLATE) {
        TAG_TEMPLATE = tAG_TEMPLATE;
    }

}
