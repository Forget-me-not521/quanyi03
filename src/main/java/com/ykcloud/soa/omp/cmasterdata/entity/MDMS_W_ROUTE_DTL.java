package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

@Data
public class MDMS_W_ROUTE_DTL extends BaseEntity{

    //公司
    private String CORT_NUM_ID;

    //门店
    private String SUB_UNIT_NUM_ID;

    //大仓编码
    private String PHYSICAL_NUM_ID;

    //路线id
    private String ROUTE_NUM_ID;

    //配送门店
    private String CUST_SUB_UNIT_NUM_ID;

    //备注
    private String REMARK;
}
