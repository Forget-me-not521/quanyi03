package com.ykcloud.soa.omp.cmasterdata.entity;

public class MDMS_O_SUB_UNIT_STATUS extends BaseEntity {
    private Long STATUS_NUM_ID;//状态编号',
    private String STATUS_NAME;//状态名称',
    private String EN_STATUS_NAME;//状态英文名称',
    private int PURCHASE_SIGN;//是否采购',
    private int REPLENISH_SIGN;//是否允许补货',
    private int RETURN_SIGN;//是否允许退货',
    private int TRANSFER_SIGN;//是否允许调拨',

    public Long getSTATUS_NUM_ID() {
        return STATUS_NUM_ID;
    }

    public void setSTATUS_NUM_ID(Long STATUS_NUM_ID) {
        this.STATUS_NUM_ID = STATUS_NUM_ID;
    }

    public String getSTATUS_NAME() {
        return STATUS_NAME;
    }

    public void setSTATUS_NAME(String STATUS_NAME) {
        this.STATUS_NAME = STATUS_NAME;
    }

    public String getEN_STATUS_NAME() {
        return EN_STATUS_NAME;
    }

    public void setEN_STATUS_NAME(String EN_STATUS_NAME) {
        this.EN_STATUS_NAME = EN_STATUS_NAME;
    }

    public int getPURCHASE_SIGN() {
        return PURCHASE_SIGN;
    }

    public void setPURCHASE_SIGN(int PURCHASE_SIGN) {
        this.PURCHASE_SIGN = PURCHASE_SIGN;
    }

    public int getREPLENISH_SIGN() {
        return REPLENISH_SIGN;
    }

    public void setREPLENISH_SIGN(int REPLENISH_SIGN) {
        this.REPLENISH_SIGN = REPLENISH_SIGN;
    }

    public int getRETURN_SIGN() {
        return RETURN_SIGN;
    }

    public void setRETURN_SIGN(int RETURN_SIGN) {
        this.RETURN_SIGN = RETURN_SIGN;
    }

    public int getTRANSFER_SIGN() {
        return TRANSFER_SIGN;
    }

    public void setTRANSFER_SIGN(int TRANSFER_SIGN) {
        this.TRANSFER_SIGN = TRANSFER_SIGN;
    }
}
