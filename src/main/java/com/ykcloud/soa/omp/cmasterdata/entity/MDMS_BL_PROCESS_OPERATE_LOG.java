package com.ykcloud.soa.omp.cmasterdata.entity;

/**
 * @Author stark.jiang
 * @Date 2021/09/11/13:59
 * @Description:
 * @Version 1.0
 */
public class MDMS_BL_PROCESS_OPERATE_LOG extends BaseEntity{
    private String CORT_NUM_ID;
    private String RESERVED_NO;
    private int BUSINESS_TYPE;
    private int BILL_TYPE;
    private String OPERATE_CONTENT;
    private int SOURCE_TYPE;
    private String BUSINESS_ID;

    public int getBUSINESS_TYPE() {
        return BUSINESS_TYPE;
    }

    public void setBUSINESS_TYPE(int BUSINESS_TYPE) {
        this.BUSINESS_TYPE = BUSINESS_TYPE;
    }

    public String getCORT_NUM_ID() {
        return CORT_NUM_ID;
    }

    public void setCORT_NUM_ID(String CORT_NUM_ID) {
        this.CORT_NUM_ID = CORT_NUM_ID;
    }

    public String getRESERVED_NO() {
        return RESERVED_NO;
    }

    public void setRESERVED_NO(String RESERVED_NO) {
        this.RESERVED_NO = RESERVED_NO;
    }

    public int getBILL_TYPE() {
        return BILL_TYPE;
    }

    public void setBILL_TYPE(int BILL_TYPE) {
        this.BILL_TYPE = BILL_TYPE;
    }

    public String getOPERATE_CONTENT() {
        return OPERATE_CONTENT;
    }

    public void setOPERATE_CONTENT(String OPERATE_CONTENT) {
        this.OPERATE_CONTENT = OPERATE_CONTENT;
    }

    public int getSOURCE_TYPE() {
        return SOURCE_TYPE;
    }

    public void setSOURCE_TYPE(int SOURCE_TYPE) {
        this.SOURCE_TYPE = SOURCE_TYPE;
    }

    public String getBUSINESS_ID() {
        return BUSINESS_ID;
    }

    public void setBUSINESS_ID(String BUSINESS_ID) {
        this.BUSINESS_ID = BUSINESS_ID;
    }
}
