package com.ykcloud.soa.omp.cmasterdata.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 【商品条款】对象 mdms_b2b_cs_product
 *
 */
@Data
public class MDMS_B2B_CS_PRODUCT implements Serializable {
    private static final long serialVersionUID=1L;

    /** 行号 */
    @ApiModelProperty(value = "行号")
    private Long series;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private Long tenant_num_id;

    /** 0: 正式  1：测试 */
    @ApiModelProperty(value = "0: 正式  1：测试")
    private Long data_sign;

    /** 公司 */
    @ApiModelProperty(value = "公司")
    private String cort_num_id;

    /** 业务单元 */
    @ApiModelProperty(value = "业务单元")
    private String unit_num_id;

    /** 客户 */
    @ApiModelProperty(value = "客户")
    private String customer_num_id;

    /** 渠道 */
    @ApiModelProperty(value = "渠道")
    private Byte channel_num_id;

    /** 组织 */
    @ApiModelProperty(value = "组织")
    private String org_unit;

    /** 商品编码 */
    @ApiModelProperty(value = "商品编码")
    private String item_num_id;

    /** 客户门店编码 */
    @ApiModelProperty(value = "客户门店编码")
    private String sub_unit_num_id;

    /** 是否生效  0不生效 1生效 */
    @ApiModelProperty(value = "是否生效  0不生效 1生效")
    private Byte effect_sign;

    /** 0未分配1专供2协议 */
    @ApiModelProperty(value = "0未分配1专供2协议")
    private Byte type_num_id;

    /** 是否合同，0否，1合同 */
    @ApiModelProperty(value = "是否合同，0否，1合同")
    private Byte contract_sign;

    /** 合同号 */
    @ApiModelProperty(value = "合同号")
    private String contract_code;

    /** 合同价 */
    @ApiModelProperty(value = "合同价")
    private BigDecimal contract_price;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private Date create_dtme;

    /** 最后更新时间 */
    @ApiModelProperty(value = "最后更新时间")
    private Date last_updtme;

    /** 用户 */
    @ApiModelProperty(value = "用户")
    private Long create_user_id;

    /** 更新用户 */
    @ApiModelProperty(value = "更新用户")
    private Long last_update_user_id;

    /** 删除 */
    @ApiModelProperty(value = "删除")
    private String cancelsign;

    private String item_name;

    private String style_desc;

    private String customer_name;


}
