package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

/**
 * @author gaoyun.shen
 * @date 2018年7月14日 下午4:53:18
 * @Description 价签打印申请单表体
 */
public class EX_ARC_SUB_PRICE_TAG_PRINT_DTL extends BaseEntity {

    private String RESERVED_NO;
    private Long SUB_UNIT_NUM_ID;
    private Date ORDER_DATE;
    private Long ITEM_NUM_ID;
    private String ITEMID;
    private String BARCODE;
    private Long PRINT_TIMES;
    private Long CARRY_SIGN;

    public String getRESERVED_NO() {
        return RESERVED_NO;
    }

    public void setRESERVED_NO(String rESERVED_NO) {
        RESERVED_NO = rESERVED_NO;
    }

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long sUB_UNIT_NUM_ID) {
        SUB_UNIT_NUM_ID = sUB_UNIT_NUM_ID;
    }

    public Date getORDER_DATE() {
        return ORDER_DATE;
    }

    public void setORDER_DATE(Date oRDER_DATE) {
        ORDER_DATE = oRDER_DATE;
    }

    public Long getITEM_NUM_ID() {
        return ITEM_NUM_ID;
    }

    public void setITEM_NUM_ID(Long iTEM_NUM_ID) {
        ITEM_NUM_ID = iTEM_NUM_ID;
    }

    public String getITEMID() {
        return ITEMID;
    }

    public void setITEMID(String iTEMID) {
        ITEMID = iTEMID;
    }

    public String getBARCODE() {
        return BARCODE;
    }

    public void setBARCODE(String bARCODE) {
        BARCODE = bARCODE;
    }

    public Long getPRINT_TIMES() {
        return PRINT_TIMES;
    }

    public void setPRINT_TIMES(Long pRINT_TIMES) {
        PRINT_TIMES = pRINT_TIMES;
    }

    public Long getCARRY_SIGN() {
        return CARRY_SIGN;
    }

    public void setCARRY_SIGN(Long cARRY_SIGN) {
        CARRY_SIGN = cARRY_SIGN;
    }


}
