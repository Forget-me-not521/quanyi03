package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

/**
 * 基础实体Entity, 其他实体类可复用公共属性
 */
public class BaseEntity {
    // 行号
    private String SERIES;
    // 租户编号
    private Long TENANT_NUM_ID;
    // 0: 正式 1：测试
    private Long DATA_SIGN;
    // 创建时间
    private Date CREATE_DTME;
    // 最后更新时间
    private Date LAST_UPDTME;
    // 创建用户
    private Long CREATE_USER_ID;
    // 更新用户
    private Long LAST_UPDATE_USER_ID;
    // 删除标识
    private String CANCELSIGN;

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String sERIES) {
        SERIES = sERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long tENANT_NUM_ID) {
        TENANT_NUM_ID = tENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long dATA_SIGN) {
        DATA_SIGN = dATA_SIGN;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date cREATE_DTME) {
        CREATE_DTME = cREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date lAST_UPDTME) {
        LAST_UPDTME = lAST_UPDTME;
    }

    public Long getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(Long cREATE_USER_ID) {
        CREATE_USER_ID = cREATE_USER_ID;
    }

    public Long getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(Long lAST_UPDATE_USER_ID) {
        LAST_UPDATE_USER_ID = lAST_UPDATE_USER_ID;
    }

    public String getCANCELSIGN() {
        return CANCELSIGN;
    }

    public void setCANCELSIGN(String cANCELSIGN) {
        CANCELSIGN = cANCELSIGN;
    }
}
