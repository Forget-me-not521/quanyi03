package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.gb.soa.omp.ccommon.api.exception.BusinessException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.*;
import com.ykcloud.soa.omp.cmasterdata.api.request.*;
import com.ykcloud.soa.omp.cmasterdata.api.response.*;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdNoticeService;
import com.ykcloud.soa.omp.cmasterdata.dao.*;
import com.ykcloud.soa.omp.cmasterdata.entity.*;
import com.ykcloud.soa.omp.cmasterdata.enums.*;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Objects;

@Service("mdNoticeService")
@RestController
public class MdNoticeServiceImpl implements MdNoticeService {
    private static final Logger log = LoggerFactory.getLogger(MdNoticeServiceImpl.class);

    @Resource
    private MdmsONoticeDao mdmsONoticeDao;

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate masterDataJdbcTemplate;


    @Resource(name = "masterDataTransactionManager")
    private PlatformTransactionManager masterDataTransactionManager;

    @Override
    public NoticeSaveResponse saveNotice(NoticeSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveNotice request:{}", JsonUtil.toJson(request));
        }
        NoticeSaveResponse response = new NoticeSaveResponse();
        try {
            if(!checkDate(request.getBeginDate(), request.getEndDate())) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "有效期时间有误");
            }
            MDMS_O_NOTICE oldEntity = null;
            if (null != request.getSeries()) {
                oldEntity = mdmsONoticeDao.getBySeries(request.getTenantNumId(), request.getDataSign().intValue(), request.getSeries());
                if (!Objects.equals(NoticeStatusEnums.INACTIVE, oldEntity.getNOTICE_STATUS())) {
                    throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "此状态不能进行此操作");
                }
            }
            MDMS_O_NOTICE mdmsONotice = createMdmsONotice(request);
            if (null != oldEntity) {
                mdmsONotice.setSERIES(oldEntity.getSERIES());
                mdmsONotice.setNOTICE_NUM_ID(oldEntity.getNOTICE_NUM_ID());
                mdmsONotice.setCREATE_DTME(oldEntity.getCREATE_DTME());
                mdmsONotice.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                mdmsONoticeDao.update(mdmsONotice, request.getTenantNumId(), request.getDataSign().longValue(), mdmsONotice.getSERIES().toString());
            } else {
                mdmsONoticeDao.insert(mdmsONotice);
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveNotice response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public NoticeStatusResponse deleteNotice(NoticeStatusRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin deleteNotice request:{}", JsonUtil.toJson(request));
        }
        NoticeStatusResponse response = new NoticeStatusResponse();
        try {
            if (null == request.getSeries()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "参数错误[缺少行号]");
            }
            MDMS_O_NOTICE oldEntity = mdmsONoticeDao.getBySeries(request.getTenantNumId(), request.getDataSign().intValue(), request.getSeries());
            if (Objects.equals(NoticeStatusEnums.ACTIVATION, oldEntity.getNOTICE_STATUS())) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "此状态不能删除");
            }
            MDMS_O_NOTICE mdmsONotice = new MDMS_O_NOTICE();
            mdmsONotice.setSERIES(oldEntity.getSERIES());
            mdmsONotice.setCANCELSIGN("Y");
            mdmsONoticeDao.update(mdmsONotice, request.getTenantNumId(), request.getDataSign(), oldEntity.getSERIES().toString());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteNotice response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public NoticeStatusResponse activatNotice(NoticeStatusRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin activatNotice request:{}", JsonUtil.toJson(request));
        }
        NoticeStatusResponse response = new NoticeStatusResponse();
        try {
            if (null == request.getSeries()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "参数错误[缺少行号]");
            }
            MDMS_O_NOTICE oldEntity = mdmsONoticeDao.getBySeries(request.getTenantNumId(), request.getDataSign().intValue(), request.getSeries());
            if (!Objects.equals(NoticeStatusEnums.INACTIVE, oldEntity.getNOTICE_STATUS())) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "此状态不能激活");
            }
            MDMS_O_NOTICE mdmsONotice = new MDMS_O_NOTICE();
            mdmsONotice.setSERIES(oldEntity.getSERIES());
            mdmsONotice.setNOTICE_STATUS(NoticeStatusEnums.ACTIVATION.getId());
            mdmsONoticeDao.update(mdmsONotice, request.getTenantNumId(), request.getDataSign(), oldEntity.getSERIES().toString());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end activatNotice response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public NoticeStatusResponse failureNotice(NoticeStatusRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin failureNotice request:{}", JsonUtil.toJson(request));
        }
        NoticeStatusResponse response = new NoticeStatusResponse();
        try {
            if (null == request.getSeries()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "参数错误[缺少行号]");
            }
            MDMS_O_NOTICE oldEntity = mdmsONoticeDao.getBySeries(request.getTenantNumId(), request.getDataSign().intValue(), request.getSeries());
            MDMS_O_NOTICE mdmsONotice = new MDMS_O_NOTICE();
            mdmsONotice.setSERIES(oldEntity.getSERIES());
            mdmsONotice.setNOTICE_STATUS(NoticeStatusEnums.FAILURE.getId());
            mdmsONoticeDao.update(mdmsONotice, request.getTenantNumId(), request.getDataSign(), oldEntity.getSERIES().toString());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end failureNotice response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public NoticeDateSaveResponse saveNoticeDate(NoticeDateSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveNoticeDate request:{}", JsonUtil.toJson(request));
        }
        NoticeDateSaveResponse response = new NoticeDateSaveResponse();
        try {
            if (null == request.getSeries()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "参数错误[缺少行号]");
            }
            if(!checkDate(request.getBeginDate(), request.getEndDate())) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "有效期时间有误");
            }
            MDMS_O_NOTICE oldEntity = mdmsONoticeDao.getBySeries(request.getTenantNumId(), request.getDataSign().intValue(), request.getSeries());
            if (Objects.equals(NoticeStatusEnums.EXPIRE, oldEntity.getNOTICE_STATUS()) ||
                    Objects.equals(NoticeStatusEnums.FAILURE, oldEntity.getNOTICE_STATUS())) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "此状态不能修改有效期");
            }
            MDMS_O_NOTICE mdmsONotice = new MDMS_O_NOTICE();
            mdmsONotice.setSERIES(oldEntity.getSERIES());
            mdmsONotice.setBEGIN_DATE(request.getBeginDate());
            mdmsONotice.setEND_DATE(request.getEndDate());
            mdmsONoticeDao.update(mdmsONotice, request.getTenantNumId(), request.getDataSign(), oldEntity.getSERIES().toString());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveNoticeDate response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public NoticeStatusResponse saveNoticeReadType(NoticeStatusRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveNoticeReadType request:{}", JsonUtil.toJson(request));
        }
        NoticeStatusResponse response = new NoticeStatusResponse();
        try {
            if (null == request.getSeries()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "参数错误[缺少行号]");
            }
            MDMS_O_NOTICE oldEntity = mdmsONoticeDao.getBySeries(request.getTenantNumId(), request.getDataSign().intValue(), request.getSeries());
            MDMS_O_NOTICE mdmsONotice = new MDMS_O_NOTICE();
            mdmsONotice.setSERIES(oldEntity.getSERIES());
            mdmsONotice.setREAD_TYPE(NoticeReadTypeEnums.READ.getId());
            mdmsONoticeDao.update(mdmsONotice, request.getTenantNumId(), request.getDataSign(), oldEntity.getSERIES().toString());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveNoticeReadType response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    public Boolean checkDate(Date beginDate, Date endDate) {
        Boolean bo = true;
        if (System.currentTimeMillis() > beginDate.getTime()) {
            bo = false;
        }
        if (beginDate.getTime() > endDate.getTime()) {
            bo = false;
        }
        return bo;
    }

    public MDMS_O_NOTICE createMdmsONotice(NoticeSaveRequest request) {
        MDMS_O_NOTICE en = new MDMS_O_NOTICE();
        en.setSERIES(Long.valueOf(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_NOTICE_SERIES)));
        en.setTENANT_NUM_ID(request.getTenantNumId());
        en.setDATA_SIGN(request.getDataSign().intValue());
        en.setNOTICE_NUM_ID(SeqUtil.getSeqAutoNextValue(request.getTenantNumId(), request.getDataSign(), SeqUtil.AUTO_MDMS_O_NOTICE_NOTICE_NUM_ID));
        en.setNOTICE_TYPE(request.getNoticeType());
        en.setNOTICE_TITLE(request.getNoticeTitle());
        en.setNOTICE_CONTENT(request.getNoticeContent());
        en.setSHOW_TYPE(request.getShowType());
        en.setBEGIN_DATE(request.getBeginDate());
        en.setEND_DATE(request.getEndDate());
        en.setNOTICE_REASON(request.getNoticeReason());
        en.setNOTICE_STATUS(request.getNoticeStatus());
        en.setNOTICE_OBJECT(request.getNoticeObject());
        en.setREAD_TYPE(NoticeReadTypeEnums.UNREAD.getId());
        en.setCREATE_DTME(new Date());
        en.setLAST_UPDTME(new Date());
        en.setCREATE_USER_ID(request.getUserNumId());
        en.setLAST_UPDATE_USER_ID(request.getUserNumId());
        en.setCANCELSIGN("N");
        return en;
    }
}