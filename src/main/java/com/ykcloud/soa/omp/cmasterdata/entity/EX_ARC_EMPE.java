package com.ykcloud.soa.omp.cmasterdata.entity;

public class EX_ARC_EMPE extends BaseEntity {
    private String EMPE_NAME;
    private Long UNIT_NUM_ID;
    private Long SUB_UNIT_NUM_ID;
    private String SUB_UNIT_NAME;
    private Long EMPE_NUM_ID;// 雇员编号',
    private Long CUY_NUM_ID;// 国家',
    private Long SUB_UNIT_TYPE;
    private Long PURCHASE_TYPE_NUM_ID;
    private Long DEFAULT_SUPPLY;
    private Long DEPART_NUM_ID;
    private String DEPART_NAME;
    private String EMPE_TELEPHONE;
    private String EN_EMPE_NAME;// 英文姓名',
    private Long DIV_NUM_ID;// 事业部',
    private String EMPE_ADR;
    private String EMPE_MAIL;
    //工牌号码
    private String WORK_NUMS;

    public Long getEMPE_NUM_ID() {
        return EMPE_NUM_ID;
    }

    public void setEMPE_NUM_ID(Long EMPE_NUM_ID) {
        this.EMPE_NUM_ID = EMPE_NUM_ID;
    }

    public Long getCUY_NUM_ID() {
        return CUY_NUM_ID;
    }

    public void setCUY_NUM_ID(Long CUY_NUM_ID) {
        this.CUY_NUM_ID = CUY_NUM_ID;
    }

    public String getEN_EMPE_NAME() {
        return EN_EMPE_NAME;
    }

    public void setEN_EMPE_NAME(String EN_EMPE_NAME) {
        this.EN_EMPE_NAME = EN_EMPE_NAME;
    }

    public Long getDIV_NUM_ID() {
        return DIV_NUM_ID;
    }

    public void setDIV_NUM_ID(Long DIV_NUM_ID) {
        this.DIV_NUM_ID = DIV_NUM_ID;
    }

    public String getEMPE_NAME() {
        return EMPE_NAME;
    }

    public void setEMPE_NAME(String EMPE_NAME) {
        this.EMPE_NAME = EMPE_NAME;
    }

    public Long getUNIT_NUM_ID() {
        return UNIT_NUM_ID;
    }

    public void setUNIT_NUM_ID(Long UNIT_NUM_ID) {
        this.UNIT_NUM_ID = UNIT_NUM_ID;
    }

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long SUB_UNIT_NUM_ID) {
        this.SUB_UNIT_NUM_ID = SUB_UNIT_NUM_ID;
    }

    public String getSUB_UNIT_NAME() {
        return SUB_UNIT_NAME;
    }

    public void setSUB_UNIT_NAME(String SUB_UNIT_NAME) {
        this.SUB_UNIT_NAME = SUB_UNIT_NAME;
    }

    public Long getSUB_UNIT_TYPE() {
        return SUB_UNIT_TYPE;
    }

    public void setSUB_UNIT_TYPE(Long SUB_UNIT_TYPE) {
        this.SUB_UNIT_TYPE = SUB_UNIT_TYPE;
    }

    public Long getPURCHASE_TYPE_NUM_ID() {
        return PURCHASE_TYPE_NUM_ID;
    }

    public void setPURCHASE_TYPE_NUM_ID(Long PURCHASE_TYPE_NUM_ID) {
        this.PURCHASE_TYPE_NUM_ID = PURCHASE_TYPE_NUM_ID;
    }

    public Long getDEFAULT_SUPPLY() {
        return DEFAULT_SUPPLY;
    }

    public void setDEFAULT_SUPPLY(Long DEFAULT_SUPPLY) {
        this.DEFAULT_SUPPLY = DEFAULT_SUPPLY;
    }

    public Long getDEPART_NUM_ID() {
        return DEPART_NUM_ID;
    }

    public void setDEPART_NUM_ID(Long DEPART_NUM_ID) {
        this.DEPART_NUM_ID = DEPART_NUM_ID;
    }

    public String getDEPART_NAME() {
        return DEPART_NAME;
    }

    public void setDEPART_NAME(String DEPART_NAME) {
        this.DEPART_NAME = DEPART_NAME;
    }

    public String getEMPE_TELEPHONE() {
        return EMPE_TELEPHONE;
    }

    public void setEMPE_TELEPHONE(String EMPE_TELEPHONE) {
        this.EMPE_TELEPHONE = EMPE_TELEPHONE;
    }

    public String getEMPE_ADR() {
        return EMPE_ADR;
    }

    public void setEMPE_ADR(String EMPE_ADR) {
        this.EMPE_ADR = EMPE_ADR;
    }

    public String getEMPE_MAIL() {
        return EMPE_MAIL;
    }

    public void setEMPE_MAIL(String EMPE_MAIL) {
        this.EMPE_MAIL = EMPE_MAIL;
    }

    public String getWORK_NUMS() {
        return WORK_NUMS;
    }

    public void setWORK_NUMS(String WORK_NUMS) {
        this.WORK_NUMS = WORK_NUMS;
    }
}
