package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class EX_ARC_CHANNEL {
    private String SERIES;
    private Long TENANT_NUM_ID;
    private Long EC_CHANNEL_NUM_ID;
    private String EC_CHANNEL_NAME;
    private Long CHANNEL_NUM_ID;
    private String CHANNEL_NAME;
    private Date CREATE_DTME;
    private Date LAST_UPDTME;
    private Long CREATE_USER_ID;
    private Long LAST_UPDATE_USER_ID;
    private Long DATA_SIGN;
    private int ISONLINE;
    private int REFUND_SIGN;
    private String CANCELSIGN;
    private int DEFAULT_TRAN_TYPE_NUM_ID;
    private int PLATFORM_ID;
    private int MEMO_AUDIT;
    private int SELF_DELIVERY_SIGN;
    private int USR_CHANNEL_NUM_ID;
    private int IS_REPORT_ONLINE;
    private int BX_SORT_NUM;
    private int EN_ABLED;
    private int CHANNEL_TYPE;
    private int CHANNEL_FROM_TYPE;

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String SERIES) {
        this.SERIES = SERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long TENANT_NUM_ID) {
        this.TENANT_NUM_ID = TENANT_NUM_ID;
    }

    public Long getEC_CHANNEL_NUM_ID() {
        return EC_CHANNEL_NUM_ID;
    }

    public void setEC_CHANNEL_NUM_ID(Long EC_CHANNEL_NUM_ID) {
        this.EC_CHANNEL_NUM_ID = EC_CHANNEL_NUM_ID;
    }

    public String getEC_CHANNEL_NAME() {
        return EC_CHANNEL_NAME;
    }

    public void setEC_CHANNEL_NAME(String EC_CHANNEL_NAME) {
        this.EC_CHANNEL_NAME = EC_CHANNEL_NAME;
    }

    public Long getCHANNEL_NUM_ID() {
        return CHANNEL_NUM_ID;
    }

    public void setCHANNEL_NUM_ID(Long CHANNEL_NUM_ID) {
        this.CHANNEL_NUM_ID = CHANNEL_NUM_ID;
    }

    public String getCHANNEL_NAME() {
        return CHANNEL_NAME;
    }

    public void setCHANNEL_NAME(String CHANNEL_NAME) {
        this.CHANNEL_NAME = CHANNEL_NAME;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date CREATE_DTME) {
        this.CREATE_DTME = CREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date LAST_UPDTME) {
        this.LAST_UPDTME = LAST_UPDTME;
    }

    public Long getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(Long CREATE_USER_ID) {
        this.CREATE_USER_ID = CREATE_USER_ID;
    }

    public Long getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(Long LAST_UPDATE_USER_ID) {
        this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long DATA_SIGN) {
        this.DATA_SIGN = DATA_SIGN;
    }

    public int getISONLINE() {
        return ISONLINE;
    }

    public void setISONLINE(int ISONLINE) {
        this.ISONLINE = ISONLINE;
    }

    public int getREFUND_SIGN() {
        return REFUND_SIGN;
    }

    public void setREFUND_SIGN(int REFUND_SIGN) {
        this.REFUND_SIGN = REFUND_SIGN;
    }

    public String getCANCELSIGN() {
        return CANCELSIGN;
    }

    public void setCANCELSIGN(String CANCELSIGN) {
        this.CANCELSIGN = CANCELSIGN;
    }

    public int getDEFAULT_TRAN_TYPE_NUM_ID() {
        return DEFAULT_TRAN_TYPE_NUM_ID;
    }

    public void setDEFAULT_TRAN_TYPE_NUM_ID(int DEFAULT_TRAN_TYPE_NUM_ID) {
        this.DEFAULT_TRAN_TYPE_NUM_ID = DEFAULT_TRAN_TYPE_NUM_ID;
    }

    public int getPLATFORM_ID() {
        return PLATFORM_ID;
    }

    public void setPLATFORM_ID(int PLATFORM_ID) {
        this.PLATFORM_ID = PLATFORM_ID;
    }

    public int getMEMO_AUDIT() {
        return MEMO_AUDIT;
    }

    public void setMEMO_AUDIT(int MEMO_AUDIT) {
        this.MEMO_AUDIT = MEMO_AUDIT;
    }

    public int getSELF_DELIVERY_SIGN() {
        return SELF_DELIVERY_SIGN;
    }

    public void setSELF_DELIVERY_SIGN(int SELF_DELIVERY_SIGN) {
        this.SELF_DELIVERY_SIGN = SELF_DELIVERY_SIGN;
    }

    public int getUSR_CHANNEL_NUM_ID() {
        return USR_CHANNEL_NUM_ID;
    }

    public void setUSR_CHANNEL_NUM_ID(int USR_CHANNEL_NUM_ID) {
        this.USR_CHANNEL_NUM_ID = USR_CHANNEL_NUM_ID;
    }

    public int getIS_REPORT_ONLINE() {
        return IS_REPORT_ONLINE;
    }

    public void setIS_REPORT_ONLINE(int IS_REPORT_ONLINE) {
        this.IS_REPORT_ONLINE = IS_REPORT_ONLINE;
    }

    public int getBX_SORT_NUM() {
        return BX_SORT_NUM;
    }

    public void setBX_SORT_NUM(int BX_SORT_NUM) {
        this.BX_SORT_NUM = BX_SORT_NUM;
    }

    public int getEN_ABLED() {
        return EN_ABLED;
    }

    public void setEN_ABLED(int EN_ABLED) {
        this.EN_ABLED = EN_ABLED;
    }

    public int getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    public void setCHANNEL_TYPE(int CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    public int getCHANNEL_FROM_TYPE() {
        return CHANNEL_FROM_TYPE;
    }

    public void setCHANNEL_FROM_TYPE(int CHANNEL_FROM_TYPE) {
        this.CHANNEL_FROM_TYPE = CHANNEL_FROM_TYPE;
    }
}
