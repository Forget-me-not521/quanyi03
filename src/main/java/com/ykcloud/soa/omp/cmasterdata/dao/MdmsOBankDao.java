package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOBank;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_BANK;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author songanqi
 * @Date 2021/11/1/14:01
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsOBankDao extends Dao<MDMS_O_BANK> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public Integer getCount(Long tenantNumId, Long dataSign, String bankNumId, String bankName) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1)  from mdms_o_bank ");
        sb.append(" where tenant_Num_id = ? and data_sign = ?  and cancelsign = 'N' ");
        if(StringUtils.isNotBlank(bankNumId)){
            sb.append(" and bank_num_id="+bankNumId);
        }
        if(StringUtils.isNotBlank(bankName)){
            sb.append(" and bank_name="+bankName);
        }
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign}, Integer.class);
    }
    public List<MdmsOBank> getPage(Long tenantNumId, Long dataSign, String bankNumId, String bankName, Integer pageNum, Integer pageSize) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_o_bank where tenant_Num_id = ? and data_sign = ? and cancelsign = 'N'");
        if(StringUtils.isNotBlank(bankNumId)){
            sb.append(" and bank_num_id="+bankNumId);
        }
        if(StringUtils.isNotBlank(bankName)){
            sb.append(" and bank_name="+bankName);
        }
        Integer start = (pageNum-1)*pageSize;
        sb.append(" limit ?,? ");
        List<MdmsOBank> list = jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,start,pageSize}, new BeanPropertyRowMapper<>(MdmsOBank.class));
        return list;
    }
}
