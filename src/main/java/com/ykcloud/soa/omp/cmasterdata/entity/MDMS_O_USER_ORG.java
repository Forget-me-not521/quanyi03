package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

/**
 * @Author stark.jiang
 * @Date 2021/10/11/20:07
 * @Description:
 * @Version 1.0
 */
@Data
public class MDMS_O_USER_ORG extends BaseEntity{
    private String WORK_NUM;// 业务单元编码',
    private int PUR_ORG;//采购组织',
    private int SAL_ORG;//销售组织',
    private int INV_ORG;//库存组织',
    private int FIN_ORG;//财务组织',
    private int BUS_ORG;//业务组织',
}
