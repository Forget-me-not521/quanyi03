package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.BlCustomerInfo;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOSubSupply;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_CUSTOMER;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUB_UNIT;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @ author
 * @date: 2021年09月10日20:32
 * @describtion:
 */
@Repository
public class MdmsOCustomerDao extends Dao<MDMS_O_CUSTOMER> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public boolean checkCustomerExist(Long tenantNumId, Long dataSign, String unifiedSocialCreditCode) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from mdms_o_customer ");
        sb.append(" where tenant_Num_id=? and data_sign=? and unified_social_credit_code=?  and cancelsign='N'  ");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign,
                unifiedSocialCreditCode}, int.class) >0 ? true : false;
    }

    public MDMS_O_CUSTOMER getCustomerEntity(Long tenantNumId, Long dataSign, String unifiedSocialCreditCode) {
        String sql = "select * from mdms_o_customer where " +
                " tenant_num_id = ? and data_sign = ? and unified_social_credit_code = ?  and cancelsign = 'N' ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, unifiedSocialCreditCode}, MDMS_O_CUSTOMER.class);
    }


    public List<BlCustomerInfo> getCustomerList(Long tenantNumId, Long dataSign, String unifiedSocialCreditCode, String cortNumId, String customerNumId, String customerName, Long createUserId, Date createDateBegin,Date createDateEnd,Integer pageNum,Integer pageSize) {
        String sql = "select c.*,s.* from mdms_o_customer c inner join mdms_o_sub_customer s on c.customer_num_id=s.customer_num_id and c.tenant_num_id=s.tenant_num_id and c.data_sign=s.data_sign  where c.tenant_num_id =? and c.data_sign =? and s.cort_num_id=? ";
        if(StringUtils.isNotEmpty(unifiedSocialCreditCode)){
            sql+=" and c.unified_social_credit_code='"+unifiedSocialCreditCode+"' ";
        }
        if(StringUtils.isNotEmpty(customerNumId)){
            sql+=" and c.customer_num_id='"+customerNumId+"' ";
        }
        if(StringUtils.isNotEmpty(customerName)){
            sql+=" and c.customer_name='"+customerName+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createUserId)){
            sql+=" and c.create_user_id='"+createUserId+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateBegin)){
            sql+=" and c.create_dtme>'"+createDateBegin+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateEnd)){
            sql+=" and c.create_dtme<'"+createDateEnd+"' ";
        }
        Integer start = (pageNum-1)*pageSize;

        sql+=" limit ?,? ";
        List<BlCustomerInfo> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign,cortNumId,start,pageSize}, new BeanPropertyRowMapper<>(BlCustomerInfo.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    public Integer getCustomerCount(Long tenantNumId, Long dataSign, String unifiedSocialCreditCode, String cortNumId, String customerNumId, String customerName, Long createUserId, Date createDateBegin,Date createDateEnd) {
        String sql = "select count(1) from mdms_o_customer c inner join mdms_o_sub_customer s on c.customer_num_id=s.customer_num_id and c.tenant_num_id=s.tenant_num_id and c.data_sign=s.data_sign  where c.tenant_num_id =? and c.data_sign =? and s.cort_num_id=? ";
        if(StringUtils.isNotEmpty(unifiedSocialCreditCode)){
            sql+=" and c.unified_social_credit_code='"+unifiedSocialCreditCode+"' ";
        }
        if(StringUtils.isNotEmpty(customerNumId)){
            sql+=" and c.customer_num_id='"+customerNumId+"' ";
        }
        if(StringUtils.isNotEmpty(customerName)){
            sql+=" and c.customer_name='"+customerName+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createUserId)){
            sql+=" and c.create_user_id='"+createUserId+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateBegin)){
            sql+=" and c.create_dtme>'"+createDateBegin+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(createDateEnd)){
            sql+=" and c.create_dtme<'"+createDateEnd+"' ";
        }
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign,cortNumId}, Integer.class);
    }
}
