package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.EntityFieldUtil;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_C_TYPE;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_DEPART;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class MdmsODepartDao {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_DEPART.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_DEPART.class, ",");
    private static final String TABLE_NAME = "MDMS_O_DEPART";

    public Long checkExistByDepartNumId(Long tenantNumId, Long dataSign, Long departNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select depart_num_id");
        sb.append(" from ");
        sb.append(TABLE_NAME);
        sb.append(" where tenant_Num_id=? and data_sign=? and depart_num_id=? and cancelsign='N'");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, departNumId},
                Long.class);
    }

    public String getDivName(Long tenantNumId, Long dataSign, Long divNumId) {
        String sql = "select div_name from mdms_p_div where tenant_Num_id=? and data_sign=? and div_num_id=? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, divNumId},
                String.class);
    }

    public String getPty2Name(Long tenantNumId, Long dataSign, Long pty2NumId) {
        String sql = "select pty2_name from mdms_p_catelog_pty2 where tenant_Num_id=? and data_sign=? and pty2_num_id=? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, pty2NumId},
                String.class);
    }

    public String getSubUnitName(Long tenantNumId, Long dataSign, Long subUnitNumId) {
        String sql = "select sub_unit_name from mdms_o_sub_unit where tenant_Num_id=? and data_sign=? and sub_unit_num_id=? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, subUnitNumId},
                String.class);
    }

    public String getPhysicalName(Long tenantNumId, Long dataSign, Long physicalNumId) {
        String sql = "select physical_name from mdms_w_physical where tenant_Num_id=? and data_sign=? and physical_num_id=? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, physicalNumId},
                String.class);
    }

    public void insertEntity(MDMS_O_DEPART o) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_DEPART.class, o));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "  数据插入失败! ");
        }
    }

    public void updateEntity(MDMS_O_DEPART entity) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE MDMS_O_DEPART SET ");
        stringBuilder.append(EntityFieldUtil.wildCardSplitUpdate(entity, ","));
        stringBuilder.append(" WHERE DATA_SIGN = ? AND TENANT_NUM_ID = ? AND series = ?");
        List<Object> objects = new ArrayList(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_O_DEPART.class, entity)));
        objects.add(entity.getDATA_SIGN());
        objects.add(entity.getTENANT_NUM_ID());
        objects.add(entity.getSERIES());
        int row = jdbcTemplate.update(stringBuilder.toString(), objects.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_O_DEPART更新失败!");
        }
    }

    public boolean checkExistByDepartName(Long tenantNumId, Long dataSign, String departName) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) ");
        sb.append(" from ");
        sb.append(TABLE_NAME);
        sb.append(" where tenant_Num_id=? and data_sign=? and depart_name=? and cancelsign='N'");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, departName}, Long.class) > 0 ? true : false;
    }

    public boolean checkExistBySeries(Long tenantNumId, Long dataSign, Long series) {
        return jdbcTemplate.queryForObject("select count(*) from MDMS_O_DEPART where tenant_num_id=? and data_sign=? and series=? and cancelsign='N'", new Object[]{tenantNumId, dataSign, series}, Integer.class) > 0;
    }

    public void deleteRecordBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "delete from MDMS_O_DEPART where TENANT_NUM_ID = ? AND DATA_SIGN = ? and series=? and cancelsign='N'";
        int row = jdbcTemplate.update(sql, tenantNumId, dataSign, series);
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "MDMS_O_DEPART删除失败!");
        }
    }


    public void batchInsert(List<MDMS_O_DEPART> list) {
        for (MDMS_O_DEPART info : list) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("insert into ");
            stringBuffer.append(TABLE_NAME);
            stringBuffer.append(" (");
            stringBuffer.append(SQL_COLS);
            stringBuffer.append(") values (");
            stringBuffer.append(WILDCARDS);
            stringBuffer.append(")");
            jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_DEPART.class, info));
        }
    }
}
