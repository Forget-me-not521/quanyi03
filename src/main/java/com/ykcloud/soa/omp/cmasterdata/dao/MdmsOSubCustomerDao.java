package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_CUSTOMER;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUB_CUSTOMER;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * @Author stark.jiang
 * @Date 2021/09/14/22:07
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsOSubCustomerDao extends Dao<MDMS_O_SUB_CUSTOMER> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }


    public MDMS_O_SUB_CUSTOMER getSubCustomerEntity(Long tenantNumId, Long dataSign, String customerNumId,String cortNumId) {
        String sql = "select * from mdms_o_sub_customer where " +
                " tenant_num_id = ? and data_sign = ? and customer_num_id = ? and cort_num_id=?  and cancelsign = 'N' ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, customerNumId,cortNumId}, MDMS_O_SUB_CUSTOMER.class);
    }
}
