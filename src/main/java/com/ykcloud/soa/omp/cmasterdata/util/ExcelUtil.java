package com.ykcloud.soa.omp.cmasterdata.util;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

public class ExcelUtil {


    private ExcelUtil() {
    }
    
    /**
     * @Description: 创建单个工作簿 xlsx 
     * @param excelParam	表格参数
     * @param response
     * @param remark	工作簿说明
     * @throws IOException
     * 2018年6月21日 上午11:12:11
     */
    public static void exportOneBookForXlsx(ExcelParam excelParam, HttpServletResponse response, String remark) throws IOException {
    	if (excelParam.widths == null) {
    		excelParam.widths = new int[excelParam.headers.length];
    		for (int i = 0; i < excelParam.headers.length; i++) {
    			excelParam.widths[i] = excelParam.width;
    		}
    	}
    	if (excelParam.ds_format == null) {
    		excelParam.ds_format = new int[excelParam.headers.length];
    		for (int i = 0; i < excelParam.headers.length; i++) {
    			excelParam.ds_format[i] = 1;
    		}
    	}
    	//创建一个工作薄
		SXSSFWorkbook wb = new SXSSFWorkbook(100);
    	int rowCount = 0;
    	//创建一个sheet
		SXSSFSheet sheet = wb.createSheet(remark);
    	if (excelParam.headers != null) {
			SXSSFRow row = sheet.createRow(rowCount);
    		//表头样式
			CellStyle style = wb.createCellStyle();
			Font font = wb.createFont();
			font.setBold(true);
    	//	font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
    		font.setFontHeightInPoints((short) 11);
    		style.setFont(font);
    		style.setAlignment(HorizontalAlignment.CENTER);

    		for (int i = 0; i < excelParam.headers.length; i++) {
    			sheet.setColumnWidth(i, excelParam.widths[i]);
				SXSSFCell cell = row.createCell(i);
    			cell.setCellValue(excelParam.headers[i]);
    			cell.setCellStyle(style);
    		}
    		rowCount++;
    	}
		CellStyle style = wb.createCellStyle();
    	style.setAlignment(HorizontalAlignment.CENTER);
    	//表格主体  解析list
    	for (int i = 0; i < excelParam.data.size(); i++) {  //行数
			SXSSFRow row = sheet.createRow(rowCount);
    		for (int j = 0; j < excelParam.headers.length; j++) {  //列数
				SXSSFCell cell = row.createCell(j);
    			cell.setCellValue(excelParam.data.get(i)[j]);
    			cell.setCellStyle(style);
    		}
    		rowCount++;
    	}
    	//设置文件名
    	String fileName = excelParam.name + ".xlsx";
    	response.setContentType("application/vnd.ms-excel");
    	response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
    	response.setHeader("Pragma", "No-cache");
    	OutputStream outputStream = response.getOutputStream();
    	wb.write(outputStream);
    	outputStream.flush();
    	outputStream.close();
    }

}
