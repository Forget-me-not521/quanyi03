package com.ykcloud.soa.omp.cmasterdata.entity;

public class MDMS_BL_MANAGER_SHOP extends BaseEntity {

    private Long RESERVED_NO;//调价单号
    private Long SUB_UNIT_NUM_ID;//门店编码

    public MDMS_BL_MANAGER_SHOP() {
        // TODO Auto-generated constructor stub
    }

    public Long getRESERVED_NO() {
        return RESERVED_NO;
    }

    public void setRESERVED_NO(Long rESERVED_NO) {
        RESERVED_NO = rESERVED_NO;
    }

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long sUB_UNIT_NUM_ID) {
        SUB_UNIT_NUM_ID = sUB_UNIT_NUM_ID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MDMS_BL_MANAGER_SHOP)) return false;

        MDMS_BL_MANAGER_SHOP that = (MDMS_BL_MANAGER_SHOP) o;

        return SUB_UNIT_NUM_ID != null ? SUB_UNIT_NUM_ID.equals(that.SUB_UNIT_NUM_ID) : that.SUB_UNIT_NUM_ID == null;
    }

    @Override
    public int hashCode() {
        return SUB_UNIT_NUM_ID != null ? SUB_UNIT_NUM_ID.hashCode() : 0;
    }
}
