package com.ykcloud.soa.omp.cmasterdata.entity;


import lombok.Data;

/**
 * @Author stark.jiang
 * @Date 2021/10/22/9:31
 * @Description:
 * @Version 1.0
 */
@Data
public class MDMS_O_SUPPLY_ANNUAL_REPORT_LOG extends BaseEntity{
    private String RELATE_SERIES;//关联行号',
    private String SUPPLY_NUM_ID;//供应商编码 ',
    private String SUPPLY_NAME;//供应商名称',
    private String UNIFIED_SOCIAL_CREDIT_CODE;//统一社会信用代码',
    private Integer CONFIRM_STATUS;//确认状态(1:待确认,2:已上传,3:无需上传)',
}
