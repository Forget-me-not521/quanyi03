package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

/**
 * @Author songanqi
 * @Date 2021/11/1/13:59
 * @Description:
 * @Version 1.0
 */
@Data
public class MDMS_O_BANK extends BaseEntity {
    private String BANK_NUM_ID;
    private String BANK_NAME;
}
