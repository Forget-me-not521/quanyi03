package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.erp.common.exception.ErpExceptionType;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_UNIT_ORG;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author stark.jiang
 * @Date 2021/10/14/14:55
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsOUnitOrgDao extends Dao<MDMS_O_UNIT_ORG> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public MDMS_O_UNIT_ORG getUnitOrgEntity(Long tenantNumId, Long dataSign, String series){
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_o_unit_org ");
        sb.append(" where tenant_Num_id=? and data_sign=? and series=? and cancelsign='N'  ");

        List<MDMS_O_UNIT_ORG> list=jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign,series}, new BeanPropertyRowMapper<>(MDMS_O_UNIT_ORG.class));
        if(CollectionUtils.isNotEmpty(list)){
            if(list.size()>1){
                throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                        String.format("根据行号查询出多条，行号：%s", series));
            }
            return list.get(0);
        }
        return null;
    }

    public void deleteUnitOrg(Long tenantNumId,Long dataSign,String series) {
        String sql = "delete from mdms_o_unit_org "
                + " where tenant_num_id=? and data_sign=? and series = ? and cancelsign='N' ";
        int row = jdbcTemplate.update(sql, new Object[]{tenantNumId,dataSign,series});
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "删除失败!");
        }
    }
}
