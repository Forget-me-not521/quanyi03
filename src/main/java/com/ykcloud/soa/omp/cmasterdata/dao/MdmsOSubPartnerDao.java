package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUB_PARTNER;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class MdmsOSubPartnerDao extends Dao<MDMS_O_SUB_PARTNER> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_SUB_PARTNER.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_SUB_PARTNER.class, ",");
    private static final String TABLE_NAME = "MDMS_O_UNIT_PARTNER";

    public List<MDMS_O_SUB_PARTNER> selectByPartnerNumIdAndCortNumId(Long tenantNumId, Long dataSign, String partnerNumId, String cortNumId) {
        String sql = "select * from MDMS_O_SUB_PARTNER WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  partner_num_id = ? AND cort_num_id = ? AND cancelsign = 'N'";
        List<MDMS_O_SUB_PARTNER> mdmsOSubPartnerList = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, partnerNumId, cortNumId}, new BeanPropertyRowMapper<>(MDMS_O_SUB_PARTNER.class));
        if (CollectionUtils.isNotEmpty(mdmsOSubPartnerList) && mdmsOSubPartnerList.size() > 1) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "存在多笔表单数据！");
        }
        return mdmsOSubPartnerList;
    }

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }


}