package com.ykcloud.soa.omp.cmasterdata.entity;

import java.io.Serializable;
import java.util.Date;

public class MDMS_BL_PRODUCT_SHOP implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long SERIES;
    private Long TENANT_NUM_ID = 0l;
    private Long DATA_SIGN;
    private Long ITEM_NUM_ID;
    // 分账类型;
    private Long PARTITIONING_TYPE = 0l;
    // 分账比例
    private Long PARTITIONING_VALUE = 0l;
    private Date CREATE_DTME = new Date();
    private Date LAST_UPDTME = new Date();
    // 永久调价单号;
    private Long RESERVED_NO;
    // 变更原因
    private String REMARK;
    // 商品适用季节''123456789abc'',表示全年销售,1200000000bc 标识 11至次年2月销售
    private String SEASON_MONTH_FLAG;
    private Long STOCK_TYPE = 0l;
    // 商品状态ID
    private Long ITEM_STATUS_ID = 0l;
    private Long STORAGE_DEPT_NUM_ID = 0l;
    private Long BOM_NUM_ID = 0l;
    // 部门编号
    private Long DEPART_NUM_ID = 0l;

    private Long USR_NUM_ID = 0l;

    private Long SUB_UNIT_NUM_ID = 0l;

    // 附加字段

    private Long DEFAULT_SIGN = 0l;
    // 产地编码
    private Long PRODUCT_ORIGIN_NUM_ID = 0l;

    // 供应商
    private Long SUPPLY_UNIT_NUM_ID = 0l;

    // 是否可以改价 1是 0否
    private Integer MODIFY_PRICE;

    public Integer getMODIFY_PRICE() {
        return MODIFY_PRICE;
    }

    public void setMODIFY_PRICE(Integer mODIFY_PRICE) {
        MODIFY_PRICE = mODIFY_PRICE;
    }

    public Long getDEFAULT_SIGN() {
        return DEFAULT_SIGN;
    }

    public void setDEFAULT_SIGN(Long dEFAULT_SIGN) {
        DEFAULT_SIGN = dEFAULT_SIGN;
    }

    public Long getPRODUCT_ORIGIN_NUM_ID() {
        return PRODUCT_ORIGIN_NUM_ID;
    }

    public void setPRODUCT_ORIGIN_NUM_ID(Long pRODUCT_ORIGIN_NUM_ID) {
        PRODUCT_ORIGIN_NUM_ID = pRODUCT_ORIGIN_NUM_ID;
    }

    public Long getSUPPLY_UNIT_NUM_ID() {
        return SUPPLY_UNIT_NUM_ID;
    }

    public void setSUPPLY_UNIT_NUM_ID(Long sUPPLY_UNIT_NUM_ID) {
        SUPPLY_UNIT_NUM_ID = sUPPLY_UNIT_NUM_ID;
    }

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long sUB_UNIT_NUM_ID) {
        SUB_UNIT_NUM_ID = sUB_UNIT_NUM_ID;
    }

    public Long getUSR_NUM_ID() {
        return USR_NUM_ID;
    }

    public void setUSR_NUM_ID(Long uSR_NUM_ID) {
        USR_NUM_ID = uSR_NUM_ID;
    }

    public Long getSERIES() {
        return SERIES;
    }

    public void setSERIES(Long sERIES) {
        SERIES = sERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long tENANT_NUM_ID) {
        TENANT_NUM_ID = tENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long dATA_SIGN) {
        DATA_SIGN = dATA_SIGN;
    }

    public Long getITEM_NUM_ID() {
        return ITEM_NUM_ID;
    }

    public void setITEM_NUM_ID(Long iTEM_NUM_ID) {
        ITEM_NUM_ID = iTEM_NUM_ID;
    }

    public Long getPARTITIONING_TYPE() {
        return PARTITIONING_TYPE;
    }

    public void setPARTITIONING_TYPE(Long pARTITIONING_TYPE) {
        PARTITIONING_TYPE = pARTITIONING_TYPE;
    }

    public Long getPARTITIONING_VALUE() {
        return PARTITIONING_VALUE;
    }

    public void setPARTITIONING_VALUE(Long pARTITIONING_VALUE) {
        PARTITIONING_VALUE = pARTITIONING_VALUE;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date cREATE_DTME) {
        CREATE_DTME = cREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date lAST_UPDTME) {
        LAST_UPDTME = lAST_UPDTME;
    }

    public Long getRESERVED_NO() {
        return RESERVED_NO;
    }

    public void setRESERVED_NO(Long rESERVED_NO) {
        RESERVED_NO = rESERVED_NO;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String rEMARK) {
        REMARK = rEMARK;
    }

    public String getSEASON_MONTH_FLAG() {
        return SEASON_MONTH_FLAG;
    }

    public void setSEASON_MONTH_FLAG(String sEASON_MONTH_FLAG) {
        SEASON_MONTH_FLAG = sEASON_MONTH_FLAG;
    }

    public Long getSTOCK_TYPE() {
        return STOCK_TYPE;
    }

    public void setSTOCK_TYPE(Long sTOCK_TYPE) {
        STOCK_TYPE = sTOCK_TYPE;
    }

    public Long getITEM_STATUS_ID() {
        return ITEM_STATUS_ID;
    }

    public void setITEM_STATUS_ID(Long iTEM_STATUS_ID) {
        ITEM_STATUS_ID = iTEM_STATUS_ID;
    }

    public Long getSTORAGE_DEPT_NUM_ID() {
        return STORAGE_DEPT_NUM_ID;
    }

    public void setSTORAGE_DEPT_NUM_ID(Long sTORAGE_DEPT_NUM_ID) {
        STORAGE_DEPT_NUM_ID = sTORAGE_DEPT_NUM_ID;
    }

    public Long getBOM_NUM_ID() {
        return BOM_NUM_ID;
    }

    public void setBOM_NUM_ID(Long bOM_NUM_ID) {
        BOM_NUM_ID = bOM_NUM_ID;
    }

    public Long getDEPART_NUM_ID() {
        return DEPART_NUM_ID;
    }

    public void setDEPART_NUM_ID(Long dEPART_NUM_ID) {
        DEPART_NUM_ID = dEPART_NUM_ID;
    }

}
