package com.ykcloud.soa.omp.cmasterdata;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@ImportResource(locations = {"classpath:application_dubbo.xml"})
@Configuration
@EnableAutoConfiguration
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.ykcloud.soa.omp.cmasterdata", "com.gb.soa.omp.cmessagecenter.util", "com.gb.soa.omp.ccache.client.util", "com.gb.soa.omp.cuniversal"})
public class MdMain {

    public static void main(String[] args) {
        SpringApplication.run(MdMain.class, args);
        System.out.println("spring 启动好了");
    }

    @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        jsonConverter.setObjectMapper(objectMapper);
        return jsonConverter;
    }

}
