package com.ykcloud.soa.omp.cmasterdata.service.model;

public class ManagerHdrInfo {

    private Long statusNumId;

    private Long typeNumId;

    public Long getStatusNumId() {
        return statusNumId;
    }

    public void setStatusNumId(Long statusNumId) {
        this.statusNumId = statusNumId;
    }

    public Long getTypeNumId() {
        return typeNumId;
    }

    public void setTypeNumId(Long typeNumId) {
        this.typeNumId = typeNumId;
    }
}
