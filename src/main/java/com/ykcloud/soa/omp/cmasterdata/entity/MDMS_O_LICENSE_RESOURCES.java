package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.util.Date;

/**
 * @Author stark.jiang
 * @Date 2021/09/06/19:04
 * @Description:
 * @Version 1.0
 */
@Data
public class MDMS_O_LICENSE_RESOURCES extends BaseEntity{
    private Integer BUSINESS_TYPE;
    private Integer SECOND_LICENSE_TYPE_ID;
    private String CORT_NUM_ID;
    private String RELATION_ID;//编号
    private String TITLE;//标题
    private Integer IMAGE_TYPE;//证照类型(1:六面图，2:说明书)
    private String LICENSE_NO;//证件编号
    private Date LICENSE_BEGIN_DATE;//证照开始日期
    private Date LICENSE_END_DATE;//证照结束日期
    private String URL;//附件地址
}
