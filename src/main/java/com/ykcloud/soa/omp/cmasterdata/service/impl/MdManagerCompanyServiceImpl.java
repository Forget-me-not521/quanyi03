package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.gb.soa.omp.ccommon.api.exception.BusinessException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.*;
import com.google.common.collect.Lists;
import com.ykcloud.cache.spring.boot.starter.RedisService;
import com.ykcloud.soa.erp.common.enums.BlProcessTypeEnum;
import com.ykcloud.soa.erp.common.enums.UnitSubTypeEnum;
import com.ykcloud.soa.erp.common.enums.UnitTypeEnum;
import com.ykcloud.soa.erp.common.utils.ObjectAssignmentUtil;
import com.ykcloud.soa.omp.cmasterdata.api.model.*;
import com.ykcloud.soa.omp.cmasterdata.api.request.*;
import com.ykcloud.soa.omp.cmasterdata.api.response.*;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdManagerCompanyService;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdTenantService;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdUnitService;
import com.ykcloud.soa.omp.cmasterdata.dao.*;
import com.ykcloud.soa.omp.cmasterdata.entity.*;
import com.ykcloud.soa.omp.cmasterdata.enums.*;
import com.ykcloud.soa.omp.cmasterdata.service.model.SupplyAnnualReport;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import net.sf.json.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service("mdManagerCompanyService")
@RestController
public class MdManagerCompanyServiceImpl implements MdManagerCompanyService {
    private static final Logger log = LoggerFactory.getLogger(MdManagerCompanyServiceImpl.class);

    @Resource
    private MdmsBlManagerHdrDao mdmsBlManagerHdrDao;

    @Resource
    private MdmsBlCortDao mdmsBlCortDao;
    @Resource
    private MdmsBlCustomerDao mdmsBlCustomerDao;
    @Resource
    private MdmsOCustomerDao mdmsOCustomerDao;
    @Resource
    private MdmsOSubCustomerDao mdmsOSubCustomerDao;
    @Resource
    private ExArcEmpeDao exArcEmpeDao;
    @Resource
    private MdmsOCortDao mdmsOCortDao;
    @Resource
    private MdUnitService mdUnitService;
    @Resource
    private MdmOperateLogDao mdmOperateLogDao;

    @Resource
    private MdmsBlPartnerDao mdmsBlPartnerDao;

    @Resource
    private MdmsBlProcessMasterHdrDao mdmsBlProcessMasterHdrDao;
    @Resource
    private MdmsOSupplyAnnualReportHdrDao mdmsOSupplyAnnualReportHdrDao;
    @Resource
    private MdmsOSupplyAnnualReportDtlDao mdmsOSupplyAnnualReportDtlDao;
    @Resource
    private MdmsOSupplyAnnualReportLogDao mdmsOSupplyAnnualReportLogDao;
    @Resource
    private MdmsOPartnerDao mdmsOPartnerDao;

    @Resource
    private MdmsOSubPartnerDao mdmsOSubPartnerDao;

    @Resource
    private MdmsOSupplyDao mdmsOSupplyDao;

    @Resource
    private MdmsOSubSupplyDao mdmsOSubSupplyDao;
    @Resource
    private MdmsWStorageDao mdmsWStorageDao;
    @Resource
    private MdmsBlSupplyDao mdmsBlSupplyDao;

    @Resource
    private MdmsAssistOnOffDao mdmsAssistOnOffDao;

    @Resource
    private MdmsBlProcessOperateLogDao mdmsBlProcessOperateLogDao;

    @Resource
    private MdmsOLicenseResourcesDao mdmsOLicenseResourcesDao;

    @Resource  //
    private MdmsOWholesaleChainDao mdmsOWholesaleChainDao;

    @Resource
    private MdTenantService mdTenantService;

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate masterDataJdbcTemplate;

    @Autowired
    private RedisService redisService;

    @Resource(name = "masterDataTransactionManager")
    private PlatformTransactionManager masterDataTransactionManager;

    public MdManagerCompanyServiceImpl() {
    }

    @Override
    public ManagerCompanyCreateResponse addManagerCompany(ManagerCortSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin addManagerCompany request:{}", JsonUtil.toJson(request));
        }
        ManagerCompanyCreateResponse response = new ManagerCompanyCreateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            String reservedNo = SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_MANAGER_HDR_RESERVED_NO);
            // 通过统一信用码查询公司是否已存在
            List<MDMS_BL_CORT> mdmsBlCompanyList = mdmsBlCortDao.selectByUnifiedSocialCreditCode(request.getTenantNumId(), request.getDataSign(), request.getUnifiedSocialCreditCode());
            if (CollectionUtils.isEmpty(mdmsBlCompanyList)) {
                Long series = Long.valueOf(SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_MANAGER_HDR_SERIES));
                MDMS_BL_MANAGER_HDR mdms_bl_manager_hdr = createMdmsBlManagerHdr(series, reservedNo, request);
                mdmsBlManagerHdrDao.insertMdmsBlManagerHdrDao(mdms_bl_manager_hdr);
                // 插入详情
                String dtlSeries = SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_CORT_SERIES);
                MDMS_BL_CORT mdms_bl_CORT = createMdmsBlCort(dtlSeries, reservedNo, request);
                mdmsBlCortDao.insertMdmsBlCompanyDao(mdms_bl_CORT);

                response.setSeries(series);
                response.setDtlSeries(Long.valueOf(dtlSeries));
                response.setReservedNo(reservedNo);
                EmpeInfo empeInfo = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), request.getUserNumId());
                response.setCreateUserId(request.getUserNumId());
                response.setCreateUserName(null != empeInfo ? empeInfo.getEmpeName() : "");
                response.setLastUpdateUserId(request.getUserNumId());
                response.setLastUpdateUserName(null != empeInfo ? empeInfo.getEmpeName() : "");
            } else {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "公司已存在!");
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end addManagerCompany response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ManagerCompanyUpdateResponse saveManagerCompany(ManagerCortSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveManagerCompany request:{}", JsonUtil.toJson(request));
        }
        ManagerCompanyUpdateResponse response = new ManagerCompanyUpdateResponse();

        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            MDMS_BL_MANAGER_HDR mdmsBlManagerHdr = mdmsBlManagerHdrDao.selectEntity(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
            if (ManagerHdrStatusNumEnums.AUDITED.getId() == mdmsBlManagerHdr.getSTATUS_NUM_ID()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "已审核通过不能修改!");
            }
            String reservedNo = SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_MANAGER_HDR_RESERVED_NO);
            List<MDMS_BL_CORT> mdmsBlCompanyList = mdmsBlCortDao.selectByUnifiedSocialCreditCode(request.getTenantNumId(), request.getDataSign(), request.getUnifiedSocialCreditCode());
            if (CollectionUtils.isNotEmpty(mdmsBlCompanyList) && !Objects.equals(mdmsBlCompanyList.get(0).getRESERVED_NO(), reservedNo)) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "公司已存在!");
            }
            Long series = Long.valueOf(SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_MANAGER_HDR_SERIES));
            MDMS_BL_MANAGER_HDR mdms_bl_manager_hdr = createMdmsBlManagerHdr(series, reservedNo, request);
            mdmsBlManagerHdrDao.insertMdmsBlManagerHdrDao(mdms_bl_manager_hdr);
            // 更新详情
            String dtlSeries = SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_CORT_SERIES);
            MDMS_BL_CORT mdms_bl_CORT = createMdmsBlCort(dtlSeries, reservedNo, request);
            mdmsBlCortDao.insertMdmsBlCompanyDao(mdms_bl_CORT);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveManagerCompany response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ManagerCompanyGetResponse getManagerCompany(ManagerCortGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getManagerCompany request:{}", JsonUtil.toJson(request));
        }
        ManagerCompanyGetResponse response = new ManagerCompanyGetResponse();
        request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
        try {
            MdmsBlManagerHdr head = mdmsBlManagerHdrDao.selectEntityByReservedNo(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
            EmpeInfo cInfo = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), head.getCreateUserId());
            head.setCreateUserName(cInfo.getEmpeName());
            EmpeInfo uInfo = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), head.getLastUpdateUserId());
            head.setLastUpdateUserName(uInfo.getEmpeName());
            response.setHead(head);

            // 查详情
            MdmsBlCort detail = mdmsBlCortDao.selectEntityByReservedNo(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
            EmpeInfo cEmpe = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), detail.getCreateUserId());
            detail.setCreateUserName(cEmpe.getEmpeName());
            EmpeInfo uEmpe = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), detail.getLastUpdateUserId());
            detail.setLastUpdateUserName(uEmpe.getEmpeName());
            response.setDetail(detail);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }

        if (log.isDebugEnabled()) {
            log.debug("end getManagerCompany response: {}", JsonUtil.toJson(response));        }
        return response;
    }

    @Override
    public ManagerCompanyHdrSubmitAuditResponse saveSubmitAuditManagerCompanyHdr(ManagerCortHdrSubmitAuditRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveSubmitAuditManagerCompanyHdr request:{}", JsonUtil.toJson(request));
        }
        ManagerCompanyHdrSubmitAuditResponse response = new ManagerCompanyHdrSubmitAuditResponse();
        request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
        try {
            mdmsBlManagerHdrDao.updateManagerCompanyHdrStatus(request.getTenantNumId(), request.getDataSign(), request.getReservedNo(), ManagerHdrStatusNumEnums.WAITING_AUDIT.getId(), request.getUserNumId(), new Date());
            insertLog(request.getTenantNumId(), request.getDataSign(), request.getUserNumId(), BillTypeEnums.COMPANY.getId(), Long.valueOf(request.getReservedNo()), "提交审核");
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveSubmitAuditManagerCompanyHdr response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ManagerCortHdrAuditResponse saveAuditManagerCompanyHdr(ManagerCortHdrAuditRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveAuditManagerCompanyHdr request:{}", JsonUtil.toJson(request));
        }
        ManagerCortHdrAuditResponse response = new ManagerCortHdrAuditResponse();

        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId(),
                    dataSign = request.getDataSign(),
                    userNumId = request.getUserNumId();
            MDMS_BL_MANAGER_HDR mdmsBlManagerHdr = mdmsBlManagerHdrDao.selectEntity(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
            if (ManagerHdrStatusNumEnums.WAITING_AUDIT.getId() != mdmsBlManagerHdr.getSTATUS_NUM_ID()) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        "单号:" + request.getReservedNo() + "状态不符合审核要求！");
            }
            MDMS_BL_CORT blEntity = mdmsBlCortDao.selectByReservedNo(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
            if(mdmsOCortDao.checkCortExist(tenantNumId,dataSign,blEntity.getUNIFIED_SOCIAL_CREDIT_CODE())){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        "该公司统一信用码已存在！");
            }
            TransactionStatus txStatus = masterDataTransactionManager.getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                mdmsBlManagerHdrDao.updateManagerCompanyHdrStatus(request.getTenantNumId(), request.getDataSign(), request.getReservedNo(), ManagerHdrStatusNumEnums.AUDITED.getId(), request.getUserNumId(), new Date());

                MDMS_O_CORT entity = new MDMS_O_CORT();
                entity.setSERIES(Long.valueOf(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_CORT_SERIES)));
                entity.setTENANT_NUM_ID(request.getTenantNumId().intValue());
                entity.setDATA_SIGN(request.getDataSign().byteValue());
                entity.setPARENT_CORT_NUM_ID("0");
                entity.setCORT_NUM_ID(SeqUtil.getSeqAutoNextValue(request.getTenantNumId(), request.getDataSign(), SeqUtil.AUTO_MDMS_O_CORT_CORT_NUM_ID));
                entity.setCORT_ID(blEntity.getCORT_ID());
                entity.setCORT_NAME(blEntity.getCORT_NAME());
                entity.setUNIFIED_SOCIAL_CREDIT_CODE(blEntity.getUNIFIED_SOCIAL_CREDIT_CODE());
                entity.setTYPE_NUM_ID(blEntity.getTYPE_NUM_ID());
                entity.setLEGAL_REPRESENTATIVE(blEntity.getLEGAL_REPRESENTATIVE());
                entity.setREGISTR_STATUS(blEntity.getREGISTR_STATUS());
                entity.setINCORPOR_DATE(blEntity.getINCORPOR_DATE());
                entity.setREGISTERED_CAPITAL(blEntity.getREGISTERED_CAPITAL());
                entity.setPAID_IN_CAPITAL(blEntity.getPAID_IN_CAPITAL());
                entity.setAPPROVAL_DATE(blEntity.getAPPROVAL_DATE());
                entity.setORG_CODE(blEntity.getORG_CODE());
                entity.setBUSINESS_REGISTR_NO(blEntity.getBUSINESS_REGISTR_NO());
                entity.setTAXPAYER_NO(blEntity.getTAXPAYER_NO());
                entity.setCORT_TYPE(blEntity.getCORT_TYPE());
                entity.setBUSINESS_BEGIN_TERM(blEntity.getBUSINESS_BEGIN_TERM());
                entity.setBUSINESS_END_TERM(blEntity.getBUSINESS_END_TERM());
                entity.setTAXPAYER_QUALIFICAT(blEntity.getTAXPAYER_QUALIFICAT());
                entity.setINDUSTRY(blEntity.getINDUSTRY());
                entity.setREGION(blEntity.getREGION());
                entity.setPRV_NUM_ID(blEntity.getPRV_NUM_ID());
                entity.setCITY_NUM_ID(blEntity.getCITY_NUM_ID());
                entity.setCITY_AREA_NUM_ID(blEntity.getCITY_AREA_NUM_ID());
                entity.setTOWN_NUM_ID(blEntity.getTOWN_NUM_ID());
                entity.setREGISTR_AUTHORITY(blEntity.getREGISTR_AUTHORITY());
                entity.setOLD_CORT_NAME(blEntity.getOLD_CORT_NAME());
                entity.setIMPORT_ENTERPRISE_CODE(blEntity.getIMPORT_ENTERPRISE_CODE());
                entity.setEN_CORT_NAME(StringUtils.isNotBlank(blEntity.getEN_CORT_NAME()) ? blEntity.getEN_CORT_NAME() : "");
                entity.setREGISTR_ADR(blEntity.getREGISTR_ADR());
                entity.setBUSINESS_SCOPE(blEntity.getBUSINESS_SCOPE());
                entity.setPHONE(blEntity.getPHONE());
                entity.setEMAIL(blEntity.getEMAIL());
                entity.setBRIEF_INTRODUCTION(blEntity.getBRIEF_INTRODUCTION());
                entity.setCREATE_DTME(new Date());
                entity.setLAST_UPDTME(new Date());
                entity.setCREATE_USER_ID(request.getUserNumId());
                entity.setLAST_UPDATE_USER_ID(request.getUserNumId());
                entity.setCANCELSIGN("N");
                entity.setBANK_NAME(StringUtils.isNotBlank(blEntity.getBANK_NAME()) ? blEntity.getBANK_NAME() : "");
                entity.setBANK_ACCOUNT(StringUtils.isNotBlank(blEntity.getBANK_ACCOUNT()) ? blEntity.getBANK_ACCOUNT() : "");
                mdmsOCortDao.insertMdmsOCort(entity);

                insertLog(request.getTenantNumId(), request.getDataSign(), request.getUserNumId(), BillTypeEnums.COMPANY.getId(), Long.valueOf(request.getReservedNo()), "审核通过");
                //
                UnitGenerateRequest unitReq=buildUnitReq(tenantNumId,dataSign,userNumId,entity.getCORT_NUM_ID(),entity.getCORT_NAME());
                mdUnitService.generateUnit(unitReq);
                // 插入辅助首营开关
                MDMS_ASSIST_ON_OFF mdmsAssistOnOff = createMdmsAssistOnOff(entity, request.getTenantNumId(), request.getDataSign(), request.getUserNumId());
                mdmsAssistOnOffDao.insert(mdmsAssistOnOff);

                masterDataTransactionManager.commit(txStatus);
            } catch (Exception ex) {
                log.error(ex.getMessage());
                masterDataTransactionManager.rollback(txStatus);
                throw ex;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveAuditManagerCompanyHdr response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ManagerCompanyHdrRejectedResponse saveRejectedManagerCompanyHdr(ManagerCortHdrRejectedRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveRejectedManagerCompanyHdr request:{}", JsonUtil.toJson(request));
        }
        ManagerCompanyHdrRejectedResponse response = new ManagerCompanyHdrRejectedResponse();
        request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
        try {
            MDMS_BL_MANAGER_HDR mdmsBlManagerHdr = mdmsBlManagerHdrDao.selectEntity(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
            if (ManagerHdrStatusNumEnums.WAITING_AUDIT.getId() != mdmsBlManagerHdr.getSTATUS_NUM_ID()) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        "单号:" + request.getReservedNo() + "状态不符合驳回要求！");
            }
            mdmsBlManagerHdrDao.updateManagerCompanyHdrStatus(request.getTenantNumId(), request.getDataSign(), request.getReservedNo(), ManagerHdrStatusNumEnums.REJECTED.getId(), request.getUserNumId(), new Date());
            insertLog(request.getTenantNumId(), request.getDataSign(), request.getUserNumId(), BillTypeEnums.COMPANY.getId(), Long.valueOf(request.getReservedNo()), "审核驳回");
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveRejectedManagerCompanyHdr response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ManagerCompanyHdrInvalidResponse saveInvalidManagerCompanyHdr(ManagerCortHdrInvalidRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveInvalidManagerCompanyHdr request:{}", JsonUtil.toJson(request));
        }
        ManagerCompanyHdrInvalidResponse response = new ManagerCompanyHdrInvalidResponse();
        request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
        try {
            mdmsBlManagerHdrDao.updateManagerCompanyHdrStatus(request.getTenantNumId(), request.getDataSign(), request.getReservedNo(), ManagerHdrStatusNumEnums.INVALID.getId(), request.getUserNumId(), new Date());
            insertLog(request.getTenantNumId(), request.getDataSign(), request.getUserNumId(), BillTypeEnums.COMPANY.getId(), Long.valueOf(request.getReservedNo()), "作废");
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveInvalidManagerCompanyHdr response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ManagerCompanyHdrLogGetResponse getManagerCompanyHdrLog(ManagerCortHdrLogGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getManagerCompanyHdrLog request:{}", JsonUtil.toJson(request));
        }
        ManagerCompanyHdrLogGetResponse response = new ManagerCompanyHdrLogGetResponse();
        request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
        try {
            List<MdmOperateLog> mdmOperateLogList = mdmOperateLogDao.selectByReservedNo(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
            response.setMdmOperateLogList(mdmOperateLogList);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getManagerCompanyHdrLog response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public NewPartnerSaveResponse saveNewPartner(NewPartnerSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveNewPartner request:{}", JsonUtil.toJson(request));
        }
        NewPartnerSaveResponse response = new NewPartnerSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            ProcessHdr head = request.getHead();
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long userNumId = request.getUserNumId();
            MdmsBlPartner detail = request.getDetails();
            List<MDMS_BL_PARTNER> detailList = Lists.newArrayList();
            List<MDMS_BL_PARTNER> updateList = Lists.newArrayList();
            List<ProcessOperateLog> logList=Lists.newArrayList();
            String reservedNo=head.getReservedNo();
            MDMS_BL_PROCESS_MASTER_HDR headHdr = createProcessHdr(tenantNumId, dataSign, userNumId, head);
            if(ValidatorUtils.isNullOrZero(reservedNo)){
                reservedNo=SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_PROCESS_MASTER_HDR_RESERVED_NO);
                headHdr.setRESERVED_NO(reservedNo);
            }

            MDMS_BL_PARTNER oldEntity = null;
            List<MDMS_BL_PARTNER> mdmsBlPartnerList = mdmsBlPartnerDao.checkItemExist(tenantNumId, dataSign, detail.getUnifiedCreditCode(), reservedNo);
            if (CollectionUtils.isNotEmpty(mdmsBlPartnerList)) {
                if (null != detail.getSeries()) {
                    if (mdmsBlPartnerList.get(0).getSERIES() != detail.getSeries()) {
                        throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "合作商已存在");
                    }
                    oldEntity = mdmsBlPartnerList.get(0);
                } else {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "合作商已存在!");
                }
            }
            MDMS_BL_PARTNER partnerDetail = createMdmsBlPartner(tenantNumId, dataSign, userNumId, reservedNo, detail);
            if (!Objects.isNull(oldEntity)) {
                partnerDetail.setSERIES(oldEntity.getSERIES());
                updateList.add(partnerDetail);
                ObjectAssignmentUtil s=new ObjectAssignmentUtil();
                String logInfo="合作商修改" + s.assignment(oldEntity,partnerDetail);
                ProcessOperateLog operateLog=buildOperateLog(head.getCortNumId(),reservedNo, BlProcessTypeEnum.PARTNER_UPDATE.getBillType(),logInfo,head.getSourceType(),null);
                logList.add(operateLog);
            } else {
                detailList.add(partnerDetail);
                String logInfo= "合作商创建" + JSONObject.fromObject(partnerDetail).toString();
                ProcessOperateLog operateLog=buildOperateLog(head.getCortNumId(),reservedNo, BlProcessTypeEnum.PARTNER_CREATE.getBillType(),logInfo,head.getSourceType(),null);
                logList.add(operateLog);
            }
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                MDMS_BL_PROCESS_MASTER_HDR oldHdrEntity = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId, dataSign, reservedNo,headHdr.getCORT_NUM_ID());
                if (Objects.isNull(oldHdrEntity)) {
                    if (Objects.equals(ProcessMasterSourceTypeEnums.BUSINESS.getId(), headHdr.getSOURCE_TYPE())) {
                        headHdr.setBILL_STATUS(ProcessMasterBillStatusEnums.CREATE.getId());
                    }
                    mdmsBlProcessMasterHdrDao.insert(headHdr);
                } else {
                    headHdr.setSERIES(oldHdrEntity.getSERIES());
                    mdmsBlProcessMasterHdrDao.update(headHdr, tenantNumId, dataSign, headHdr.getSERIES());
                }
                if (CollectionUtils.isNotEmpty(detailList)) {
                    mdmsBlPartnerDao.batchInsert(detailList);
                }
                if (CollectionUtils.isNotEmpty(updateList)) {
                    mdmsBlPartnerDao.batchUpdateEntity(updateList);
                }
                if(CollectionUtils.isNotEmpty(logList)){
                    saveBlProcessOperatelog(tenantNumId,dataSign,userNumId,logList);
                }
                masterDataTransactionManager.commit(txStatus);
            } catch (Exception e) {
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
            response.setReservedNo(reservedNo);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }

        if (log.isDebugEnabled()) {
            log.debug("end saveNewPartner response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ProcessHdrCreateFinalStatusUpdateResponse updateProcessHdrPartnerCreateFinalStatus(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updateProcessHdrFinalStatus request:{}", JsonUtil.toJson(request));
        }
        ProcessHdrCreateFinalStatusUpdateResponse response = new ProcessHdrCreateFinalStatusUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            MDMS_BL_PROCESS_MASTER_HDR mdmsBlProcessMasterHdr= mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId, dataSign, request.getReservedNo(),request.getCortNumId());
            if (Objects.isNull(mdmsBlProcessMasterHdr)) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据不存在!");
            }
            // 获取mdms_o_partner
            MDMS_BL_PARTNER mdms_bl_partner = mdmsBlPartnerDao.selectByReservedNo(tenantNumId, dataSign, request.getReservedNo());

            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if (Objects.equals(ProcessMasterSourceTypeEnums.BUSINESS.getId(), mdmsBlProcessMasterHdr.getSOURCE_TYPE())) {
                    // 修改单头状态
                    mdmsBlProcessMasterHdr.setLAST_UPDTME(new Date());
                    mdmsBlProcessMasterHdr.setLAST_UPDATE_USER_ID(request.getUserNumId());
                    mdmsBlProcessMasterHdr.setBILL_STATUS(ProcessMasterBillStatusEnums.PASS.getId());
                    mdmsBlProcessMasterHdrDao.update(mdmsBlProcessMasterHdr, tenantNumId, dataSign, mdmsBlProcessMasterHdr.getSERIES());
                }
                // 添加mdms_o_partner
                MDMS_O_PARTNER mdms_o_partner = createMdmsOPartner(tenantNumId, dataSign, usrNumId, mdms_bl_partner);
                mdmsOPartnerDao.insert(mdms_o_partner);
                // 添加mdms_o_sub_partner
                MDMS_O_SUB_PARTNER mdms_o_unit_partner = createMdmsOSubPartner(tenantNumId, dataSign, usrNumId, mdmsBlProcessMasterHdr.getCORT_NUM_ID(), mdms_o_partner.getPARTNER_NUM_ID(), mdms_bl_partner);
                mdmsOSubPartnerDao.insert(mdms_o_unit_partner);
                masterDataTransactionManager.commit(txStatus);
            } catch (Exception e) {
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end updateProcessHdrFinalStatus response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ProcessHdrUpdateFinalStatusUpdateResponse updateProcessHdrPartnerUpdateFinalStatus(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updateProcessHdrPartnerUpdateFinalStatus request:{}", JsonUtil.toJson(request));
        }
        ProcessHdrUpdateFinalStatusUpdateResponse response = new ProcessHdrUpdateFinalStatusUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            MDMS_BL_PROCESS_MASTER_HDR mdmsBlProcessMasterHdr = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId, dataSign, request.getReservedNo(),request.getCortNumId());
            if (Objects.isNull(mdmsBlProcessMasterHdr)) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据不存在!");
            }
            // 获取mdms_bl_partner
            MDMS_BL_PARTNER mdms_bl_partner = mdmsBlPartnerDao.selectByReservedNo(tenantNumId, dataSign, request.getReservedNo());
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if (Objects.equals(ProcessMasterSourceTypeEnums.BUSINESS.getId(), mdmsBlProcessMasterHdr.getSOURCE_TYPE())) {
                    // 修改单头状态
                    mdmsBlProcessMasterHdr.setLAST_UPDTME(new Date());
                    mdmsBlProcessMasterHdr.setLAST_UPDATE_USER_ID(request.getUserNumId());
                    mdmsBlProcessMasterHdr.setBILL_STATUS(ProcessMasterBillStatusEnums.PASS.getId());
                    mdmsBlProcessMasterHdrDao.update(mdmsBlProcessMasterHdr, tenantNumId, dataSign, mdmsBlProcessMasterHdr.getSERIES());
                }
                // 更新mdms_o_partner
                List<MDMS_O_PARTNER> oldEntityList = mdmsOPartnerDao.selectByUnifiedCreditCode(tenantNumId, dataSign, mdms_bl_partner.getUNIFIED_CREDIT_CODE());
                MDMS_O_PARTNER mdms_o_partner = createMdmsOPartner(tenantNumId, dataSign, usrNumId, mdms_bl_partner);
                if (CollectionUtils.isNotEmpty(oldEntityList)) {
                    mdms_o_partner.setSERIES(oldEntityList.get(0).getSERIES());
                    mdms_o_partner.setPARTNER_NUM_ID(oldEntityList.get(0).getPARTNER_NUM_ID());
                    mdmsOPartnerDao.update(mdms_o_partner, tenantNumId, dataSign, mdms_o_partner.getSERIES().toString());
                } else {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据结果信息不存在");
                }
                // 更新mdms_o_sub_partner
                List<MDMS_O_SUB_PARTNER> oldUnitEntityList = mdmsOSubPartnerDao.selectByPartnerNumIdAndCortNumId(tenantNumId, dataSign, mdms_o_partner.getPARTNER_NUM_ID(), mdmsBlProcessMasterHdr.getCORT_NUM_ID());
                MDMS_O_SUB_PARTNER mdms_o_unit_partner = createMdmsOSubPartner(tenantNumId, dataSign, usrNumId, mdmsBlProcessMasterHdr.getCORT_NUM_ID(), mdms_o_partner.getPARTNER_NUM_ID(), mdms_bl_partner);
                if (CollectionUtils.isNotEmpty(oldUnitEntityList)) {
                    mdms_o_unit_partner.setSERIES(oldUnitEntityList.get(0).getSERIES());
                    mdmsOSubPartnerDao.update(mdms_o_unit_partner, tenantNumId, dataSign, mdms_o_unit_partner.getSERIES());
                } else {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据结果信息不存在!");
                }
                masterDataTransactionManager.commit(txStatus);
            } catch (Exception e) {
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end updateProcessHdrPartnerUpdateFinalStatus response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ProcessHdrUpdateFinalStatusUpdateResponse updatePartnerAuditStatus(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updateProcessHdrPartnerAuditStatus request:{}", JsonUtil.toJson(request));
        }
        ProcessHdrUpdateFinalStatusUpdateResponse response = new ProcessHdrUpdateFinalStatusUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            MDMS_BL_PROCESS_MASTER_HDR mdmsBlProcessMasterHdr = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId, dataSign, request.getReservedNo(),request.getCortNumId());
            if (Objects.isNull(mdmsBlProcessMasterHdr)) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据不存在!");
            }
            if(mdmsBlProcessMasterHdr.getTYPE_NUM_ID().compareTo(BlProcessTypeEnum.PARTNER_HEAD_UPDATE.getBillType())!=0){
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("该单据类型错误，请确认，单号[%s]...", request.getReservedNo()));
            }

            // 获取mdms_bl_partner
            MDMS_BL_PARTNER mdms_bl_partner = mdmsBlPartnerDao.selectByReservedNo(tenantNumId, dataSign, request.getReservedNo());
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if (Objects.equals(ProcessMasterSourceTypeEnums.BUSINESS.getId(), mdmsBlProcessMasterHdr.getSOURCE_TYPE())) {
                    // 修改单头状态
                    mdmsBlProcessMasterHdr.setLAST_UPDTME(new Date());
                    mdmsBlProcessMasterHdr.setLAST_UPDATE_USER_ID(request.getUserNumId());
                    mdmsBlProcessMasterHdr.setBILL_STATUS(ProcessMasterBillStatusEnums.PASS.getId());
                    mdmsBlProcessMasterHdrDao.update(mdmsBlProcessMasterHdr, tenantNumId, dataSign, mdmsBlProcessMasterHdr.getSERIES());
                }
                List<MDMS_O_PARTNER> oldEntityList = mdmsOPartnerDao.selectByUnifiedCreditCode(tenantNumId, dataSign, mdms_bl_partner.getUNIFIED_CREDIT_CODE());
                MDMS_O_PARTNER mdms_o_partner = createMdmsOPartner(tenantNumId, dataSign, usrNumId, mdms_bl_partner);
                if (CollectionUtils.isNotEmpty(oldEntityList)) {
                    mdms_o_partner.setSERIES(oldEntityList.get(0).getSERIES());
                    mdms_o_partner.setPARTNER_NUM_ID(oldEntityList.get(0).getPARTNER_NUM_ID());
                    mdmsOPartnerDao.update(mdms_o_partner, tenantNumId, dataSign, mdms_o_partner.getSERIES().toString());
                } else {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据结果信息不存在!");
                }
                masterDataTransactionManager.commit(txStatus);
            } catch (Exception e) {
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end updateProcessHdrPartnerAuditStatus response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ProcessHdrUpdateFinalStatusUpdateResponse updateSubPartnerAuditStatus(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updateProcessHdrPartnerAuditStatus request:{}", JsonUtil.toJson(request));
        }
        ProcessHdrUpdateFinalStatusUpdateResponse response = new ProcessHdrUpdateFinalStatusUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            MDMS_BL_PROCESS_MASTER_HDR mdmsBlProcessMasterHdr = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId, dataSign, request.getReservedNo(),request.getCortNumId());
            if (Objects.isNull(mdmsBlProcessMasterHdr)) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据不存在!");
            }
            if(mdmsBlProcessMasterHdr.getTYPE_NUM_ID().compareTo(BlProcessTypeEnum.PARTNER_SUB_UPDATE.getBillType())!=0){
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("该单据类型错误，请确认，单号[%s]...", request.getReservedNo()));
            }

            // 获取mdms_bl_partner
            MDMS_BL_PARTNER mdms_bl_partner = mdmsBlPartnerDao.selectByReservedNo(tenantNumId, dataSign, request.getReservedNo());
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if (Objects.equals(ProcessMasterSourceTypeEnums.BUSINESS.getId(), mdmsBlProcessMasterHdr.getSOURCE_TYPE())) {
                    // 修改单头状态
                    mdmsBlProcessMasterHdr.setLAST_UPDTME(new Date());
                    mdmsBlProcessMasterHdr.setLAST_UPDATE_USER_ID(request.getUserNumId());
                    mdmsBlProcessMasterHdr.setBILL_STATUS(ProcessMasterBillStatusEnums.PASS.getId());
                    mdmsBlProcessMasterHdrDao.update(mdmsBlProcessMasterHdr, tenantNumId, dataSign, mdmsBlProcessMasterHdr.getSERIES());
                }
                List<MDMS_O_PARTNER> oldEntityList = mdmsOPartnerDao.selectByUnifiedCreditCode(tenantNumId, dataSign, mdms_bl_partner.getUNIFIED_CREDIT_CODE());
                List<MDMS_O_SUB_PARTNER> oldUnitEntityList = mdmsOSubPartnerDao.selectByPartnerNumIdAndCortNumId(tenantNumId, dataSign, oldEntityList.get(0).getPARTNER_NUM_ID(), mdmsBlProcessMasterHdr.getCORT_NUM_ID());

                // 更新mdms_o_sub_partner
                MDMS_O_SUB_PARTNER mdms_o_sub_partner = createMdmsOSubPartner(tenantNumId, dataSign, usrNumId, mdmsBlProcessMasterHdr.getCORT_NUM_ID(), oldEntityList.get(0).getPARTNER_NUM_ID(), mdms_bl_partner);
                if (CollectionUtils.isNotEmpty(oldUnitEntityList)) {
                    mdms_o_sub_partner.setSERIES(oldUnitEntityList.get(0).getSERIES());
                    mdms_o_sub_partner.setPARTNER_NUM_ID(oldUnitEntityList.get(0).getPARTNER_NUM_ID());
                    mdmsOSubPartnerDao.update(mdms_o_sub_partner, tenantNumId, dataSign, mdms_o_sub_partner.getSERIES());
                } else {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据结果信息不存在!");
                }
                masterDataTransactionManager.commit(txStatus);
            } catch (Exception e) {
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end updateProcessHdrPartnerAuditStatus response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public PartnerGetResponse getPartner(PartnerGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getManagerCompany request:{}", JsonUtil.toJson(request));
        }
        PartnerGetResponse response = new PartnerGetResponse();
        request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
        try {
            // 单头信息
            List<ProcessHdr> processHdrList = mdmsBlProcessMasterHdrDao.getByReservedNo(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
            if (CollectionUtils.isNotEmpty(processHdrList)) {
                ProcessHdr head = processHdrList.get(0);
                EmpeInfo cInfo = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), head.getCreateUserId());
                head.setCreateUserName(cInfo.getEmpeName());
                EmpeInfo uInfo = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), head.getLastUpdateUserId());
                head.setLastUpdateUserName(uInfo.getEmpeName());
                response.setHead(head);

                // 查详情信息
                List<MdmsBlPartner> details = mdmsBlPartnerDao.selectInfoByReservedNo(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
                for (MdmsBlPartner bl : details) {
                    EmpeInfo cEmpe = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), bl.getCreateUserId());
                    bl.setCreateUserName(cEmpe.getEmpeName());
                    EmpeInfo uEmpe = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), bl.getLastUpdateUserId());
                    bl.setLastUpdateUserName(uEmpe.getEmpeName());
                }
                response.setDetails(details);
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }

        if (log.isDebugEnabled()) {
            log.debug("end getManagerCompany response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public NewSupplySaveResponse saveNewSupply(NewSupplySaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveNewSupply request:{}", JsonUtil.toJson(request));
        }
        NewSupplySaveResponse response = new NewSupplySaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            ProcessHdr head = request.getHead();
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long userNumId = request.getUserNumId();
            MdmsBlSupply detail = request.getDetails();
            List<MDMS_BL_SUPPLY> detailList = Lists.newArrayList();
            List<MDMS_BL_SUPPLY> updateList = Lists.newArrayList();
            List<ProcessOperateLog> logList=Lists.newArrayList();
            MDMS_BL_PROCESS_MASTER_HDR headHdr = createProcessHdr(tenantNumId, dataSign, userNumId, head);
            String reservedNo=head.getReservedNo();
            if(ValidatorUtils.isNullOrZero(reservedNo)){
                reservedNo=SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_PROCESS_MASTER_HDR_RESERVED_NO);
                headHdr.setRESERVED_NO(reservedNo);
            }
            MDMS_BL_SUPPLY oldEntity = null;
            List<MDMS_BL_SUPPLY> mdmsBlSupplyList = mdmsBlSupplyDao.checkItemExist(tenantNumId, dataSign, detail.getUnifiedSocialCreditCode(), reservedNo);
            if (CollectionUtils.isNotEmpty(mdmsBlSupplyList)) {
                if (null != detail.getSeries()) {
                    if (mdmsBlSupplyList.get(0).getSERIES() != detail.getSeries()) {
                        throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "供应商商已存在!");
                    }
                    oldEntity = mdmsBlSupplyList.get(0);
                } else {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "供应商已存在!");
                }
            }
            MDMS_BL_SUPPLY supplyDetail = createMdmsBlSupply(tenantNumId, dataSign, userNumId, reservedNo, detail);
            if (!Objects.isNull(oldEntity)) {
                supplyDetail.setSERIES(oldEntity.getSERIES());
                updateList.add(supplyDetail);
                ObjectAssignmentUtil s=new ObjectAssignmentUtil();
                String logInfo = "供应商修改" + s.assignment(oldEntity,supplyDetail);
                ProcessOperateLog operateLog=buildOperateLog(head.getCortNumId(),reservedNo,BlProcessTypeEnum.SUPPLY_UPDATE.getBillType(),logInfo,head.getSourceType(),null);
                logList.add(operateLog);
            } else {
                detailList.add(supplyDetail);
                String logInfo="供应商创建" + JSONObject.fromObject(supplyDetail).toString();
                ProcessOperateLog operateLog=buildOperateLog(head.getCortNumId(),reservedNo,BlProcessTypeEnum.SUPPLY_CREATE.getBillType(),logInfo,head.getSourceType(),null);
                logList.add(operateLog);
            }
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                MDMS_BL_PROCESS_MASTER_HDR oldHdrEntity = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId, dataSign, reservedNo,headHdr.getCORT_NUM_ID());
                if (Objects.isNull(oldHdrEntity)) {
                    if (Objects.equals(ProcessMasterSourceTypeEnums.BUSINESS.getId(), headHdr.getSOURCE_TYPE())) {
                        headHdr.setBILL_STATUS(ProcessMasterBillStatusEnums.CREATE.getId());
                    }
                    mdmsBlProcessMasterHdrDao.insert(headHdr);
                } else {
                    headHdr.setSERIES(oldHdrEntity.getSERIES());
                    mdmsBlProcessMasterHdrDao.update(headHdr, tenantNumId, dataSign, headHdr.getSERIES());
                }
                if (CollectionUtils.isNotEmpty(detailList)) {
                    mdmsBlSupplyDao.batchInsert(detailList);
                }
                if (CollectionUtils.isNotEmpty(updateList)) {
                    for (MDMS_BL_SUPPLY mdmsBlSupply : updateList) {
                        mdmsBlSupplyDao.update(mdmsBlSupply, tenantNumId, dataSign, mdmsBlSupply.getSERIES());
                    }
                }
                // 保存证照
                if (CollectionUtils.isNotEmpty(request.getLicenseList())) {
                    saveLicense(request.getLicenseList(), reservedNo, head.getTypeNumId(), head.getCortNumId());
                }

                if(CollectionUtils.isNotEmpty(logList)){
                    saveBlProcessOperatelog(tenantNumId,dataSign,userNumId,logList);
                }
                masterDataTransactionManager.commit(txStatus);
            } catch (Exception e) {
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
            response.setReservedNo(reservedNo);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }

        if (log.isDebugEnabled()) {
            log.debug("end saveNewSupply response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ProcessHdrCreateFinalStatusUpdateResponse updateProcessHdrSupplyCreateFinalStatus(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updateProcessHdrSupplyFinalStatus request:{}", JsonUtil.toJson(request));
        }
        ProcessHdrCreateFinalStatusUpdateResponse response = new ProcessHdrCreateFinalStatusUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            MDMS_BL_PROCESS_MASTER_HDR mdmsBlProcessMasterHdr = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId, dataSign, request.getReservedNo(),request.getCortNumId());
            if (Objects.isNull(mdmsBlProcessMasterHdr)) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据不存在!");
            }
            // 获取mdms_bl_supply
            MDMS_BL_SUPPLY mdms_bl_supply = mdmsBlSupplyDao.selectByReservedNo(tenantNumId, dataSign, request.getReservedNo());

            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if (Objects.equals(ProcessMasterSourceTypeEnums.BUSINESS.getId(), mdmsBlProcessMasterHdr.getSOURCE_TYPE())) {
                    // 修改单头状态
                    mdmsBlProcessMasterHdr.setLAST_UPDTME(new Date());
                    mdmsBlProcessMasterHdr.setLAST_UPDATE_USER_ID(request.getUserNumId());
                    mdmsBlProcessMasterHdr.setBILL_STATUS(ProcessMasterBillStatusEnums.PASS.getId());
                    mdmsBlProcessMasterHdrDao.update(mdmsBlProcessMasterHdr, tenantNumId, dataSign, mdmsBlProcessMasterHdr.getSERIES());
                }
                // 添加mdms_o_supply
                MDMS_O_SUPPLY entity = createMdmsOSupply(tenantNumId, dataSign, usrNumId, mdms_bl_supply);
                mdmsOSupplyDao.insert(entity);
                // 添加mdms_o_sub_supply
                MDMS_O_SUB_SUPPLY mdms_o_sub_supply = createMdmsOSubSupply(tenantNumId, dataSign, usrNumId, mdmsBlProcessMasterHdr.getCORT_NUM_ID(), entity.getSUPPLY_NUM_ID(), mdms_bl_supply);
                mdmsOSubSupplyDao.insert(mdms_o_sub_supply);
                // 证照
                List<MDMS_O_LICENSE_RESOURCES> licenseList = mdmsOLicenseResourcesDao.getLicenseListByRelationId(tenantNumId, dataSign, request.getReservedNo());
                if (CollectionUtils.isNotEmpty(licenseList)) {
                    List<MDMS_O_LICENSE_RESOURCES> lList = new ArrayList<>();
                    for (MDMS_O_LICENSE_RESOURCES lr: licenseList) {
                        lr.setRELATION_ID(entity.getSUPPLY_NUM_ID());
                        lList.add(lr);
                    }
                    if (CollectionUtils.isNotEmpty(lList)) {
                        mdmsOLicenseResourcesDao.batchInsert(lList);
                    }
                }
                /****
                 * 生成对应的逻辑仓
                 */
                this.generateStorage(tenantNumId,dataSign,usrNumId,request.getCortNumId(),entity.getSUPPLY_NUM_ID(),entity.getSUPPLY_NAME(),5L,null,null,null,null,entity.getADR());

                masterDataTransactionManager.commit(txStatus);
            } catch (Exception e) {
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end updateProcessHdrSupplyFinalStatus response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ProcessHdrUpdateFinalStatusUpdateResponse updateProcessHdrSupplyUpdateFinalStatus(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updateProcessHdrSupplyUpdateFinalStatus request:{}", JsonUtil.toJson(request));
        }
        ProcessHdrUpdateFinalStatusUpdateResponse response = new ProcessHdrUpdateFinalStatusUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            MDMS_BL_PROCESS_MASTER_HDR mdmsBlProcessMasterHdr = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId, dataSign, request.getReservedNo(),request.getCortNumId());
            if (Objects.isNull(mdmsBlProcessMasterHdr)) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据不存在!");
            }
            // 获取mdms_bl_supply
            MDMS_BL_SUPPLY mdms_bl_supply = mdmsBlSupplyDao.selectByReservedNo(tenantNumId, dataSign, request.getReservedNo());
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if (Objects.equals(ProcessMasterSourceTypeEnums.BUSINESS.getId(), mdmsBlProcessMasterHdr.getSOURCE_TYPE())) {
                    // 修改单头状态
                    mdmsBlProcessMasterHdr.setLAST_UPDTME(new Date());
                    mdmsBlProcessMasterHdr.setLAST_UPDATE_USER_ID(request.getUserNumId());
                    mdmsBlProcessMasterHdr.setBILL_STATUS(ProcessMasterBillStatusEnums.PASS.getId());
                    mdmsBlProcessMasterHdrDao.update(mdmsBlProcessMasterHdr, tenantNumId, dataSign, mdmsBlProcessMasterHdr.getSERIES());
                }
                // 更新mdms_o_supply
                List<MDMS_O_SUPPLY> oldEntityList = mdmsOSupplyDao.selectByUnifiedCreditCode(tenantNumId, dataSign, mdms_bl_supply.getUNIFIED_SOCIAL_CREDIT_CODE());
                MDMS_O_SUPPLY mdms_o_supply = createMdmsOSupply(tenantNumId, dataSign, usrNumId, mdms_bl_supply);
                if (CollectionUtils.isNotEmpty(oldEntityList)) {
                    mdms_o_supply.setSERIES(oldEntityList.get(0).getSERIES());
                    mdms_o_supply.setSUPPLY_NUM_ID(oldEntityList.get(0).getSUPPLY_NUM_ID());
                    mdmsOSupplyDao.update(mdms_o_supply, tenantNumId, dataSign, mdms_o_supply.getSERIES());
                } else {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据结果信息不存在");
                }
                // 更新mdms_o_sub_supply
                List<MDMS_O_SUB_SUPPLY> oldSubEntityList = mdmsOSubSupplyDao.selectBySupplyNumIdAndCortNumId(tenantNumId, dataSign, mdms_o_supply.getSUPPLY_NUM_ID(), mdmsBlProcessMasterHdr.getCORT_NUM_ID());
                MDMS_O_SUB_SUPPLY mdms_o_sub_supply = createMdmsOSubSupply(tenantNumId, dataSign, usrNumId, mdmsBlProcessMasterHdr.getCORT_NUM_ID(), mdms_o_supply.getSUPPLY_NUM_ID(), mdms_bl_supply);
                if (CollectionUtils.isNotEmpty(oldSubEntityList)) {
                    mdms_o_sub_supply.setSERIES(oldSubEntityList.get(0).getSERIES());
                    mdmsOSubSupplyDao.update(mdms_o_sub_supply, tenantNumId, dataSign, mdms_o_sub_supply.getSERIES());
                } else {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据结果信息不存在!");
                }
                // 证照
                List<MDMS_O_LICENSE_RESOURCES> licenseList = mdmsOLicenseResourcesDao.getLicenseListByRelationId(tenantNumId, dataSign, request.getReservedNo());
                if (CollectionUtils.isNotEmpty(licenseList)) {
                    if(mdmsOLicenseResourcesDao.checkLicenseExistByUnit(tenantNumId,dataSign,mdms_o_supply.getSUPPLY_NUM_ID())){
                        mdmsOLicenseResourcesDao.deleteLicenseByUnitId(tenantNumId,dataSign,mdms_o_supply.getSUPPLY_NUM_ID());
                    }
                    List<MDMS_O_LICENSE_RESOURCES> lList = new ArrayList<>();
                    for (MDMS_O_LICENSE_RESOURCES lr: licenseList) {
                        lr.setRELATION_ID(mdms_o_supply.getSUPPLY_NUM_ID());
                        lList.add(lr);
                    }
                    if (CollectionUtils.isNotEmpty(lList)) {
                        mdmsOLicenseResourcesDao.batchInsert(lList);
                    }
                }
                masterDataTransactionManager.commit(txStatus);
            } catch (Exception e) {
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end updateProcessHdrSupplyUpdateFinalStatus response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ProcessHdrUpdateFinalStatusUpdateResponse updateSupplyAuditStatus(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updateProcessHdrSubSupplyAuditStatus request:{}", JsonUtil.toJson(request));
        }
        ProcessHdrUpdateFinalStatusUpdateResponse response = new ProcessHdrUpdateFinalStatusUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            MDMS_BL_PROCESS_MASTER_HDR mdmsBlProcessMasterHdr = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId, dataSign, request.getReservedNo(),request.getCortNumId());
            if (Objects.isNull(mdmsBlProcessMasterHdr)) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据不存在!");
            }
            if(mdmsBlProcessMasterHdr.getTYPE_NUM_ID().compareTo(BlProcessTypeEnum.SUPPLY_HEAD_UPDATE.getBillType())!=0){
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("该单据类型错误，请确认，单号[%s]...", request.getReservedNo()));
            }

            // 获取mdms_bl_supply
            MDMS_BL_SUPPLY mdms_bl_supply = mdmsBlSupplyDao.selectByReservedNo(tenantNumId, dataSign, request.getReservedNo());
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if (Objects.equals(ProcessMasterSourceTypeEnums.BUSINESS.getId(), mdmsBlProcessMasterHdr.getSOURCE_TYPE())) {
                    // 修改单头状态
                    mdmsBlProcessMasterHdr.setLAST_UPDTME(new Date());
                    mdmsBlProcessMasterHdr.setLAST_UPDATE_USER_ID(request.getUserNumId());
                    mdmsBlProcessMasterHdr.setBILL_STATUS(ProcessMasterBillStatusEnums.PASS.getId());
                    mdmsBlProcessMasterHdrDao.update(mdmsBlProcessMasterHdr, tenantNumId, dataSign, mdmsBlProcessMasterHdr.getSERIES());
                }
                List<MDMS_O_SUPPLY> oldEntityList = mdmsOSupplyDao.selectByUnifiedCreditCode(tenantNumId, dataSign, mdms_bl_supply.getUNIFIED_SOCIAL_CREDIT_CODE());
                MDMS_O_SUPPLY mdms_o_supply = createMdmsOSupply(tenantNumId, dataSign, usrNumId, mdms_bl_supply);
                if (CollectionUtils.isNotEmpty(oldEntityList)) {
                    mdms_o_supply.setSERIES(oldEntityList.get(0).getSERIES());
                    mdms_o_supply.setSUPPLY_NUM_ID(oldEntityList.get(0).getSUPPLY_NUM_ID());
                    mdmsOSupplyDao.update(mdms_o_supply, tenantNumId, dataSign, mdms_o_supply.getSERIES());
                } else {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据结果信息不存在!");
                }
                masterDataTransactionManager.commit(txStatus);
            } catch (Exception e) {
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end updateProcessHdrSubSupplyAuditStatus response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ProcessHdrUpdateFinalStatusUpdateResponse updateSubSupplyAuditStatus(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updateProcessHdrSubSupplyAuditStatus request:{}", JsonUtil.toJson(request));
        }
        ProcessHdrUpdateFinalStatusUpdateResponse response = new ProcessHdrUpdateFinalStatusUpdateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            MDMS_BL_PROCESS_MASTER_HDR mdmsBlProcessMasterHdr = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId, dataSign, request.getReservedNo(),request.getCortNumId());
            if (Objects.isNull(mdmsBlProcessMasterHdr)) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据不存在!");
            }
            if(mdmsBlProcessMasterHdr.getTYPE_NUM_ID().compareTo(BlProcessTypeEnum.SUPPLY_SUB_UPDATE.getBillType())!=0){
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("该单据类型错误，请确认，单号[%s]...", request.getReservedNo()));
            }
            // 获取mdms_bl_supply
            MDMS_BL_SUPPLY mdms_bl_supply = mdmsBlSupplyDao.selectByReservedNo(tenantNumId, dataSign, request.getReservedNo());
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if (Objects.equals(ProcessMasterSourceTypeEnums.BUSINESS.getId(), mdmsBlProcessMasterHdr.getSOURCE_TYPE())) {
                    // 修改单头状态
                    mdmsBlProcessMasterHdr.setLAST_UPDTME(new Date());
                    mdmsBlProcessMasterHdr.setLAST_UPDATE_USER_ID(request.getUserNumId());
                    mdmsBlProcessMasterHdr.setBILL_STATUS(ProcessMasterBillStatusEnums.PASS.getId());
                    mdmsBlProcessMasterHdrDao.update(mdmsBlProcessMasterHdr, tenantNumId, dataSign, mdmsBlProcessMasterHdr.getSERIES());
                }
                List<MDMS_O_SUPPLY> oldEntityList = mdmsOSupplyDao.selectByUnifiedCreditCode(tenantNumId, dataSign, mdms_bl_supply.getUNIFIED_SOCIAL_CREDIT_CODE());
                List<MDMS_O_SUB_SUPPLY> oldSubEntityList = mdmsOSubSupplyDao.selectBySupplyNumIdAndCortNumId(tenantNumId, dataSign, oldEntityList.get(0).getSUPPLY_NUM_ID(), mdmsBlProcessMasterHdr.getCORT_NUM_ID());
                // 更新mdms_o_sub_supply
                MDMS_O_SUB_SUPPLY mdms_o_sub_supply = createMdmsOSubSupply(tenantNumId, dataSign, usrNumId, mdmsBlProcessMasterHdr.getCORT_NUM_ID(), oldEntityList.get(0).getSUPPLY_NUM_ID(), mdms_bl_supply);
                if (CollectionUtils.isNotEmpty(oldSubEntityList)) {
                    mdms_o_sub_supply.setSERIES(oldSubEntityList.get(0).getSERIES());
                    mdms_o_sub_supply.setSUPPLY_NUM_ID(oldSubEntityList.get(0).getSUPPLY_NUM_ID());
                    mdmsOSubSupplyDao.update(mdms_o_sub_supply, tenantNumId, dataSign, mdms_o_sub_supply.getSERIES());
                } else {
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该流程单据结果信息不存在!");
                }
                masterDataTransactionManager.commit(txStatus);
            } catch (Exception e) {
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end updateProcessHdrSubSupplyAuditStatus response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public SupplyGetResponse getSupply(SupplyGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getSupply request:{}", JsonUtil.toJson(request));
        }
        SupplyGetResponse response = new SupplyGetResponse();
        request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
        try {
            // 单头信息
            List<ProcessHdr> processHdrList = mdmsBlProcessMasterHdrDao.getByReservedNo(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
            if (CollectionUtils.isNotEmpty(processHdrList)) {
                ProcessHdr head = processHdrList.get(0);
                EmpeInfo cInfo = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), head.getCreateUserId());
                head.setCreateUserName(cInfo.getEmpeName());
                EmpeInfo uInfo = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), head.getLastUpdateUserId());
                head.setLastUpdateUserName(uInfo.getEmpeName());
                response.setHead(head);

                // 查详情信息
                List<MdmsBlSupply> details = mdmsBlSupplyDao.selectInfoByReservedNo(request.getTenantNumId(), request.getDataSign(), request.getReservedNo());
                for (MdmsBlSupply bl : details) {
                    EmpeInfo cEmpe = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), bl.getCreateUserId());
                    bl.setCreateUserName(cEmpe.getEmpeName());
                    EmpeInfo uEmpe = exArcEmpeDao.getSubUnitNumIdByUsrNumId(request.getTenantNumId(), request.getDataSign(), bl.getLastUpdateUserId());
                    bl.setLastUpdateUserName(uEmpe.getEmpeName());
                }
                response.setDetails(details);
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }

        if (log.isDebugEnabled()) {
            log.debug("end getSupply response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public BlCustomerSaveResponse saveBlCustomer(BlCustomerSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveBlCustomer request:{}", JsonUtil.toJson(request));
        }
        BlCustomerSaveResponse response = new BlCustomerSaveResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long userNumId = request.getUserNumId();
            ProcessHdr head = request.getHead();//流程头
            String reservedNo=head.getReservedNo();
            MDMS_BL_PROCESS_MASTER_HDR headHdr = createProcessHdr(tenantNumId,dataSign,userNumId,head);
            if(ValidatorUtils.isNullOrZero(reservedNo)){
                reservedNo=SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_PROCESS_MASTER_HDR_RESERVED_NO);
                headHdr.setRESERVED_NO(reservedNo);
            }
            BlCustomerInfo detail = request.getDetail();
            if(mdmsOCustomerDao.checkCustomerExist(tenantNumId,dataSign,detail.getUnifiedSocialCreditCode())){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"该客户已建档！客户统一信用码：" + detail.getUnifiedSocialCreditCode());
            }
            if(mdmsBlCustomerDao.checkBlCustomerExist(tenantNumId,dataSign,reservedNo,detail.getUnifiedSocialCreditCode(),detail.getSeries())){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25002,"同一个单据中统一社会信用编码不能重复！客户统一信用码：" + detail.getUnifiedSocialCreditCode());
            }
            MDMS_BL_PROCESS_MASTER_HDR oldHdrEntity = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId,dataSign,reservedNo,head.getCortNumId());
            MDMS_BL_CUSTOMER oldEntity = mdmsBlCustomerDao.getOldBlCustomer(tenantNumId,dataSign,reservedNo,detail.getSeries());

            MDMS_BL_CUSTOMER customerDetail = createMdmsBlCustomer(tenantNumId,dataSign,userNumId,reservedNo,detail);
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try{
                if(!Objects.isNull(oldHdrEntity)){
                    headHdr.setSERIES(oldHdrEntity.getSERIES());
                    headHdr.setCREATE_USER_ID(oldHdrEntity.getCREATE_USER_ID());
                    headHdr.setCREATE_DTME(oldHdrEntity.getCREATE_DTME());
                    mdmsBlProcessMasterHdrDao.update(headHdr,tenantNumId,dataSign,headHdr.getSERIES());
                }else{
                    mdmsBlProcessMasterHdrDao.insert(headHdr);
                }
                if(!Objects.isNull(customerDetail)){
                    customerDetail.setSERIES(oldEntity.getSERIES());
                    customerDetail.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                    customerDetail.setCREATE_DTME(oldEntity.getCREATE_DTME());
                    mdmsBlCustomerDao.update(customerDetail,tenantNumId,dataSign,customerDetail.getSERIES());
                }else{
                    mdmsBlCustomerDao.insert(customerDetail);
                }
                masterDataTransactionManager.commit(txStatus);
            }catch(Exception e){
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveBlCustomer response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public BlCustomerAuditResponse auditBlCustomer(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin auditBlCustomer request:{}", JsonUtil.toJson(request));
        }
        BlCustomerAuditResponse response = new BlCustomerAuditResponse();
        RedisLock redisLock = null;
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);//
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            String redisLockKey = String.join("-", "ykcloud.md.bl.customer.first.sale.audit",
                    request.getCortNumId(),request.getReservedNo());
            redisLock = redisService.getRedisLock(redisLockKey, 900);
            if (!redisLock.lock()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("客户首营审核中，单号[%s]...", request.getReservedNo()));
            }
            List<MDMS_O_CUSTOMER> custList= Lists.newArrayList();
            List<MDMS_O_SUB_CUSTOMER> subList= Lists.newArrayList();
            MDMS_BL_PROCESS_MASTER_HDR hdrEntity=mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId,dataSign,request.getReservedNo(),request.getCortNumId());
            List<MDMS_BL_CUSTOMER> customers=mdmsBlCustomerDao.getBlCustomerEntity( tenantNumId,  dataSign, request.getReservedNo());
            if(CollectionUtils.isNotEmpty(customers)){
                for(MDMS_BL_CUSTOMER entity : customers){
                    MDMS_O_CUSTOMER customer=createMdmsOCustomer(tenantNumId,dataSign,usrNumId, entity);
                    customer.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_CUSTOMER_SERIES));
                    String customerId=SeqUtil.getSeqAutoNextValue(tenantNumId, dataSign, SeqUtil.AUTO_MDMS_O_CUSTOMER_CUSTOMER_NUM_ID);
                    customer.setCUSTOMER_NUM_ID(customerId);
                    custList.add(customer);
                    MDMS_O_SUB_CUSTOMER subCustomer=createMdmsOSubCustomer(tenantNumId,dataSign,usrNumId,hdrEntity.getCORT_NUM_ID(),customerId, entity);
                    subList.add(subCustomer);
                }
            }
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if(CollectionUtils.isNotEmpty(custList)){
                    mdmsOCustomerDao.batchInsert(custList);
                    for(MDMS_O_CUSTOMER info : custList){
                        generateStorage(tenantNumId,dataSign,usrNumId,request.getCortNumId(),info.getCUSTOMER_NUM_ID(),info.getCUSTOMER_NAME(),4L,info.getPRV_NUM_ID(),info.getCITY_NUM_ID(),info.getCITY_AREA_NUM_ID(),info.getTOWN_NUM_ID(),info.getADR());
                    }
                }
                if(CollectionUtils.isNotEmpty(subList)){
                    mdmsOSubCustomerDao.batchInsert(subList);
                }
                masterDataTransactionManager.commit(txStatus);
            }catch (Exception e){
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        } finally {
            redisService.unLock(redisLock);
        }
        if (log.isDebugEnabled()) {
            log.debug("end auditBlCustomer response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public BlCustomerModifyAuditResponse auditBlCustomerModify(CamundaEndCallbackRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin auditBlCustomerModify request:{}", JsonUtil.toJson(request));
        }
        BlCustomerModifyAuditResponse response = new BlCustomerModifyAuditResponse();
        RedisLock redisLock = null;
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);//
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            String redisLockKey = String.join("-", "ykcloud.md.bl.customer.modify.audit",
                    request.getCortNumId(),request.getReservedNo());
            redisLock = redisService.getRedisLock(redisLockKey, 900);
            if (!redisLock.lock()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("客户资料修改审核中，单号[%s]...", request.getReservedNo()));
            }
            List<MDMS_O_CUSTOMER> custList= Lists.newArrayList();
            List<MDMS_O_SUB_CUSTOMER> subList= Lists.newArrayList();
            MDMS_BL_PROCESS_MASTER_HDR hdrEntity=mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId,dataSign,request.getReservedNo(),request.getCortNumId());
            List<MDMS_BL_CUSTOMER> customers=mdmsBlCustomerDao.getBlCustomerEntity(tenantNumId,dataSign, request.getReservedNo());
            if(CollectionUtils.isNotEmpty(customers)){
                for(MDMS_BL_CUSTOMER entity : customers){
                    MDMS_O_CUSTOMER oldEntity=mdmsOCustomerDao.getCustomerEntity(tenantNumId,dataSign,entity.getUNIFIED_SOCIAL_CREDIT_CODE());
                    if(!Objects.isNull(oldEntity)){
                        MDMS_O_CUSTOMER customer=createMdmsOCustomer(tenantNumId,dataSign,usrNumId, entity);
                        customer.setCUSTOMER_NUM_ID(oldEntity.getCUSTOMER_NUM_ID());
                        customer.setCREATE_DTME(oldEntity.getCREATE_DTME());
                        customer.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                        customer.setSERIES(oldEntity.getSERIES());
                        custList.add(customer);
                        MDMS_O_SUB_CUSTOMER oldSubEntity=mdmsOSubCustomerDao.getSubCustomerEntity(tenantNumId,dataSign,oldEntity.getCUSTOMER_NUM_ID(),request.getCortNumId());
                        if(!Objects.isNull(oldSubEntity)){
                            MDMS_O_SUB_CUSTOMER subCustomer=createMdmsOSubCustomer(tenantNumId,dataSign,usrNumId,hdrEntity.getCORT_NUM_ID(),oldEntity.getCUSTOMER_NUM_ID(), entity);
                            subCustomer.setCREATE_DTME(oldSubEntity.getCREATE_DTME());
                            subCustomer.setCREATE_USER_ID(oldSubEntity.getCREATE_USER_ID());
                            subCustomer.setSERIES(oldSubEntity.getSERIES());
                            subList.add(subCustomer);
                        }
                    }
                }
            }
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if(CollectionUtils.isNotEmpty(custList)){
                    for(MDMS_O_CUSTOMER entity: custList){
                        mdmsOCustomerDao.update(entity,tenantNumId,dataSign,entity.getSERIES());
                    }
                }
                if(CollectionUtils.isNotEmpty(subList)){
                    for(MDMS_O_SUB_CUSTOMER sub : subList){
                        mdmsOSubCustomerDao.update(sub,tenantNumId,dataSign,sub.getSERIES());
                    }
                }
                masterDataTransactionManager.commit(txStatus);
            }catch (Exception e){
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        } finally {
            redisLock.unlock();
        }
        if (log.isDebugEnabled()) {
            log.debug("end auditBlCustomerModify response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public BlCustomerModifyAuditResponse auditHeadCustomerModify(CustomerModifyAuditRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin auditHeadCustomerModify request:{}", JsonUtil.toJson(request));
        }
        BlCustomerModifyAuditResponse response = new BlCustomerModifyAuditResponse();
        RedisLock redisLock = null;
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);//
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            String redisLockKey = String.join("-", "ykcloud.md.head.customer.modify.audit",
                    request.getCortNumId(),request.getReservedNo());
            redisLock = redisService.getRedisLock(redisLockKey, 900);
            if (!redisLock.lock()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("客户总部资料修改审核中，单号[%s]...", request.getReservedNo()));
            }
            List<MDMS_O_CUSTOMER> custList= Lists.newArrayList();
            MDMS_BL_PROCESS_MASTER_HDR hdrEntity=mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId,dataSign,request.getReservedNo(),request.getCortNumId());
            if(Objects.isNull(hdrEntity)){
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("该单据不存在，请确认，单号[%s]...", request.getReservedNo()));
            }
            if(hdrEntity.getTYPE_NUM_ID().compareTo(BlProcessTypeEnum.CUSTOMER_HEAD_UPDATE.getBillType())!=0){
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("该单据类型错误，请确认，单号[%s]...", request.getReservedNo()));
            }
            List<MDMS_BL_CUSTOMER> customers=mdmsBlCustomerDao.getBlCustomerEntity(tenantNumId,dataSign, request.getReservedNo());
            if(CollectionUtils.isNotEmpty(customers)){
                for(MDMS_BL_CUSTOMER entity : customers){
                    MDMS_O_CUSTOMER oldEntity=mdmsOCustomerDao.getCustomerEntity(tenantNumId,dataSign,entity.getUNIFIED_SOCIAL_CREDIT_CODE());
                    if(!Objects.isNull(oldEntity)){
                        MDMS_O_CUSTOMER customer=createMdmsOCustomer(tenantNumId,dataSign,usrNumId, entity);
                        customer.setCUSTOMER_NUM_ID(oldEntity.getCUSTOMER_NUM_ID());
                        customer.setCREATE_DTME(oldEntity.getCREATE_DTME());
                        customer.setCREATE_USER_ID(oldEntity.getCREATE_USER_ID());
                        customer.setSERIES(oldEntity.getSERIES());
                        custList.add(customer);
                    }
                }
            }
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if(CollectionUtils.isNotEmpty(custList)){
                    for(MDMS_O_CUSTOMER entity: custList){
                        mdmsOCustomerDao.update(entity,tenantNumId,dataSign,entity.getSERIES());
                    }
                }
                masterDataTransactionManager.commit(txStatus);
            }catch (Exception e){
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        } finally {
            redisLock.unlock();
        }
        if (log.isDebugEnabled()) {
            log.debug("end auditHeadCustomerModify response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public BlCustomerModifyAuditResponse auditSubCustomerModify(CustomerModifyAuditRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin auditSubCustomerModify request:{}", JsonUtil.toJson(request));
        }
        BlCustomerModifyAuditResponse response = new BlCustomerModifyAuditResponse();
        RedisLock redisLock = null;
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);//
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            String redisLockKey = String.join("-", "ykcloud.md.sub.customer.modify.audit",
                    request.getCortNumId(),request.getReservedNo());
            redisLock = redisService.getRedisLock(redisLockKey, 900);
            if (!redisLock.lock()) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("客户分部资料修改审核中，单号[%s]...", request.getReservedNo()));
            }
            List<MDMS_O_SUB_CUSTOMER> subList= Lists.newArrayList();
            MDMS_BL_PROCESS_MASTER_HDR hdrEntity=mdmsBlProcessMasterHdrDao.getEntityByReservedNo(tenantNumId,dataSign,request.getReservedNo(),request.getCortNumId());
            if(Objects.isNull(hdrEntity)){
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("该单据不存在，请确认，单号[%s]...", request.getReservedNo()));
            }
            if(hdrEntity.getTYPE_NUM_ID().compareTo(BlProcessTypeEnum.CUSTOMER_SUB_UPDATE.getBillType())!=0){
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001,
                        String.format("该单据类型错误，请确认，单号[%s]...", request.getReservedNo()));
            }
            List<MDMS_BL_CUSTOMER> customers=mdmsBlCustomerDao.getBlCustomerEntity(tenantNumId,dataSign, request.getReservedNo());
            if(CollectionUtils.isNotEmpty(customers)){
                for(MDMS_BL_CUSTOMER entity : customers){
                    MDMS_O_CUSTOMER oldEntity=mdmsOCustomerDao.getCustomerEntity(tenantNumId,dataSign,entity.getUNIFIED_SOCIAL_CREDIT_CODE());
                    if(!Objects.isNull(oldEntity)){
                        MDMS_O_SUB_CUSTOMER oldSubEntity=mdmsOSubCustomerDao.getSubCustomerEntity(tenantNumId,dataSign,oldEntity.getCUSTOMER_NUM_ID(),request.getCortNumId());
                        if(!Objects.isNull(oldSubEntity)){
                            MDMS_O_SUB_CUSTOMER subCustomer=createMdmsOSubCustomer(tenantNumId,dataSign,usrNumId,hdrEntity.getCORT_NUM_ID(),oldEntity.getCUSTOMER_NUM_ID(), entity);
                            subCustomer.setCREATE_DTME(oldSubEntity.getCREATE_DTME());
                            subCustomer.setCREATE_USER_ID(oldSubEntity.getCREATE_USER_ID());
                            subCustomer.setSERIES(oldSubEntity.getSERIES());
                            subList.add(subCustomer);
                        }
                    }
                }
            }
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if(CollectionUtils.isNotEmpty(subList)){
                    for(MDMS_O_SUB_CUSTOMER sub : subList){
                        mdmsOSubCustomerDao.update(sub,tenantNumId,dataSign,sub.getSERIES());
                    }
                }
                masterDataTransactionManager.commit(txStatus);
            }catch (Exception e){
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        } finally {
            redisLock.unlock();
        }
        if (log.isDebugEnabled()) {
            log.debug("end auditSubCustomerModify response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public OCortListGetResponse getManagerCortList(OCortListGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getManagerCortList request:{}", JsonUtil.toJson(request));
        }
        OCortListGetResponse response = new OCortListGetResponse();
        try {
            List<MdmsOCort> list = mdmsOCortDao.getMdmsOCortList(request.getTenantNumId(), request.getDataSign());
            response.setList(list);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getManagerCortList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public OSupplyListGetResponse getSupplyList(OSupplyListGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getSupplyList request:{}", JsonUtil.toJson(request));
        }
        OSupplyListGetResponse response = new OSupplyListGetResponse();
        try {
            List<MdmsOSubSupply> list = mdmsOSubSupplyDao.getMdmsOSubSupplyList(request.getTenantNumId(), request.getDataSign(), request.getCortNumId());
            response.setList(list);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getSupplyList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    //客户档案查询--业务单据
    public CustomerListGetResponse getCustomerList(CustomerListGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCustomerList request:{}", JsonUtil.toJson(request));
        }
        CustomerListGetResponse response = new CustomerListGetResponse();
        try {
            List<BlCustomerInfo> list = mdmsOCustomerDao.getCustomerList(request.getTenantNumId(), request.getDataSign(), request.getUnifiedSocialCreditCode(), request.getCortNumId(), request.getCustomerNumId(), request.getCustomerName(), request.getCreateUserId(), request.getCreateDateBegin(),request.getCreateDateEnd(),request.getPageNum(),request.getPageSize());
            response.setResults(list);
            Integer count =mdmsOCustomerDao.getCustomerCount(request.getTenantNumId(), request.getDataSign(), request.getUnifiedSocialCreditCode(), request.getCortNumId(), request.getCustomerNumId(), request.getCustomerName(), request.getCreateUserId(), request.getCreateDateBegin(),request.getCreateDateEnd());
            response.setRecordCount(count);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public HdrStatusSaveResponse saveHdrStatus (HdrStarusSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveHdrStatus request:{}", JsonUtil.toJson(request));
        }
        HdrStatusSaveResponse response = new HdrStatusSaveResponse();
        try {
            if (StringUtils.isBlank(request.getReservedNo())) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "参数错误[缺少编号]");
            }
            if (StringUtils.isBlank(request.getCortNumId())) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "参数错误[缺少公司编码]");
            }
            if (null == request.getBillStatus() || request.getBillStatus() <= 0) {
                throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "参数错误[单据状态错误]");
            }
            MDMS_BL_PROCESS_MASTER_HDR headHdr = mdmsBlProcessMasterHdrDao.getEntityByReservedNo(request.getTenantNumId(), request.getDataSign(), request.getReservedNo(), request.getCortNumId());
            headHdr.setBILL_STATUS(request.getBillStatus());
            mdmsBlProcessMasterHdrDao.update(headHdr, request.getTenantNumId(), request.getDataSign(), headHdr.getSERIES());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveHdrStatus response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public PartnerPageGetResponse getPartnerPage(PartnerPageGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getPartnerPage request:{}", JsonUtil.toJson(request));
        }
        PartnerPageGetResponse response = new PartnerPageGetResponse();
        try {
            List<MdmsBlPartner> list = mdmsOPartnerDao.getPartnerPage(request.getTenantNumId(), request.getDataSign(), request.getUnifiedCreditCode(), request.getCortNumId(), request.getPartnerNumId(),
                    request.getPartnerName(), request.getCreateUserId(), request.getCreateDateBegin(),request.getCreateDateEnd(),request.getPageNum(),request.getPageSize());
            response.setResults(list);
            Integer count =mdmsOPartnerDao.getPartnerPageCount(request.getTenantNumId(), request.getDataSign(), request.getUnifiedCreditCode(), request.getCortNumId(),
                    request.getPartnerNumId(), request.getPartnerName(), request.getCreateUserId(), request.getCreateDateBegin(),request.getCreateDateEnd());
            response.setRecordCount(count);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getPartnerPage response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public SupplyPageGetResponse getSupplyPage(SupplyPageGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getSupplyPage request:{}", JsonUtil.toJson(request));
        }
        SupplyPageGetResponse response = new SupplyPageGetResponse();
        try {
            List<MdmsBlSupply> list = mdmsOSupplyDao.getSupplyPage(request.getTenantNumId(), request.getDataSign(), request.getUnifiedSocialCreditCode(), request.getCortNumId(), request.getSupplyNumId(), request.getSupplyName(), request.getCreateUserId(), request.getCreateDateBegin(),request.getCreateDateEnd(),request.getPageNum(),request.getPageSize());
            response.setResults(list);
            Integer count =mdmsOSupplyDao.getSupplyPageCount(request.getTenantNumId(), request.getDataSign(), request.getUnifiedSocialCreditCode(), request.getCortNumId(), request.getSupplyNumId(), request.getSupplyName(), request.getCreateUserId(), request.getCreateDateBegin(),request.getCreateDateEnd());
            response.setRecordCount(count);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getSupplyPage response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public BillMasterHdrListGetResponse getBillMasterHdrList(BillMasterHdrListGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getBillMasterHdrList request:{}", JsonUtil.toJson(request));
        }
        BillMasterHdrListGetResponse response = new BillMasterHdrListGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            List<BillMasterHdrInfo> list = mdmsBlProcessMasterHdrDao.getEntityList(tenantNumId,dataSign,request.getReservedNo(),request.getCreateUserId(), request.getCreateDtmeBegin(), request.getCreateDtmeEnd(), request.getBillStatus(),request.getCortNumId(),request.getTypeNumId(),request.getPageNum(),request.getPageSize());
            response.setResults(list);
            Integer count =mdmsBlProcessMasterHdrDao.getEntityCount(tenantNumId,dataSign,request.getReservedNo(),request.getCreateUserId(), request.getCreateDtmeBegin(), request.getCreateDtmeEnd(), request.getBillStatus(),request.getCortNumId(),request.getTypeNumId());
            response.setRecordCount(count);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getBillMasterHdrList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public CortPageGetResponse getCortPage(CortPageGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCortPage request:{}", JsonUtil.toJson(request));
        }
        CortPageGetResponse response = new CortPageGetResponse();
        try {
            List<MdmsBlCort> list = mdmsOCortDao.getCortPage(request.getTenantNumId(), request.getDataSign(), request.getUnifiedSocialCreditCode(), request.getCortNumId(), request.getCortName(), request.getCreateUserId(), request.getCreateDateBegin(),request.getCreateDateEnd(),request.getPageNum(),request.getPageSize());
            response.setResults(list);
            Integer count = mdmsOCortDao.getCortPageCount(request.getTenantNumId(), request.getDataSign(), request.getUnifiedSocialCreditCode(), request.getCortNumId(), request.getCortName(), request.getCreateUserId(), request.getCreateDateBegin(),request.getCreateDateEnd());
            response.setRecordCount(count);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCortPage response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public CortInfoGetResponse getCortInfo(CortInfoGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCortInfo request:{}", JsonUtil.toJson(request));
        }
        CortInfoGetResponse response = new CortInfoGetResponse();
        try {
            List<CortDropDown> list = mdmsOCortDao.getCortDropDown(request.getTenantNumId(), request.getDataSign(),request.getRegisterType());
            response.setResults(list);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCortInfo response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public AssistPageGetResponse getAssistPage(AssistPageGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getAssistPage request:{}", JsonUtil.toJson(request));
        }
        AssistPageGetResponse response = new AssistPageGetResponse();
        try {
            List<MdmsAssistOnOff> list = mdmsAssistOnOffDao.getAssistPage(request.getTenantNumId(), request.getDataSign(), request.getCortNumId(), request.getCortName(),request.getPageNum(),request.getPageSize());
            response.setResults(list);
            Integer count = mdmsAssistOnOffDao.getAssistPageCount(request.getTenantNumId(), request.getDataSign(), request.getCortNumId(), request.getCortName());
            response.setRecordCount(count);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getAssistPage response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public AssistStatusSaveResponse saveAssistStatus(AssistStatusSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveAssistStatus request:{}", JsonUtil.toJson(request));
        }
        AssistStatusSaveResponse response = new AssistStatusSaveResponse();
        TransactionStatus txStatus = masterDataTransactionManager.getTransaction(TransactionUtil.newTransactionDefinition(600));
        try {
            if (null == request.getSeries() || request.getSeries() <= 0) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "行号不能为空");
            }
            if (null == request.getAssistStatus()) {
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "状态不能为空");
            }
            MDMS_ASSIST_ON_OFF mdmsAssistOnOff = mdmsAssistOnOffDao.getAssistbySeries(request.getSeries(), request.getTenantNumId(), request.getDataSign());
            mdmsAssistOnOff.setASSIST_STATUS(request.getAssistStatus());
            mdmsAssistOnOff.setLAST_UPDTME(new Date());
            mdmsAssistOnOff.setLAST_UPDATE_USER_ID(request.getUserNumId());
            mdmsAssistOnOffDao.update(mdmsAssistOnOff,request.getTenantNumId(), request.getDataSign(), mdmsAssistOnOff.getSERIES().toString());

            List<ProcessOperateLog> logList = new ArrayList<>();
            String logInfo= "首营辅助开关" + AssistStatusEnums.getEnums(request.getAssistStatus());
            ProcessOperateLog operateLog=buildOperateLog(mdmsAssistOnOff.getCORT_NUM_ID(),mdmsAssistOnOff.getCORT_NUM_ID(), BlProcessTypeEnum.CORT_ASSIST.getBillType(),logInfo,ProcessMasterSourceTypeEnums.BUSINESS.getId(),null);
            logList.add(operateLog);
            if(CollectionUtils.isNotEmpty(logList)){
                saveBlProcessOperatelog(request.getTenantNumId(), request.getDataSign(),request.getUserNumId(),logList);
            }

            masterDataTransactionManager.commit(txStatus);
        } catch (Exception ex) {
            masterDataTransactionManager.rollback(txStatus);
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end saveAssistStatus response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public CustomerGenerateResponse generateCustomer(CustomerGenerateRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin generateCustomer request:{}", JsonUtil.toJson(request));
        }
        CustomerGenerateResponse response=new CustomerGenerateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            if(mdmsOCustomerDao.checkCustomerExist(tenantNumId,dataSign,request.getUnifiedSocialCreditCode())){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该客户已存在");
            }
            MDMS_O_CUSTOMER entity=buildCustomer(tenantNumId,dataSign,usrNumId,request);
            mdmsOCustomerDao.insert(entity);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end generateCustomer response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public SupplyGenerateResponse generateSupply(SupplyGenerateRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin generateSupply request:{}", JsonUtil.toJson(request));
        }
        SupplyGenerateResponse response=new SupplyGenerateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            if(mdmsOSupplyDao.checkSupplyExist(tenantNumId,dataSign,request.getUnifiedSocialCreditCode())){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该供应商已存在");
            }
            MDMS_O_SUPPLY entity=buildSupply(tenantNumId,dataSign,usrNumId,request);
            mdmsOSupplyDao.insert(entity);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end generateSupply response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public SupplyAnnualReportGenerateResponse generateSupplyAnnualReport(SupplyAnnualReportGenerateRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin generateSupplyAnnualReport request:{}", JsonUtil.toJson(request));
        }
        SupplyAnnualReportGenerateResponse response=new SupplyAnnualReportGenerateResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            DateTimeFormatter fmDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String reportYear=fmDate.format(LocalDate.now());
            List<SupplyAnnualReport> list=mdmsOSubSupplyDao.getSupplyAnnualReportList(tenantNumId,dataSign);
            List<MDMS_O_SUPPLY_ANNUAL_REPORT_HDR> hdrList=Lists.newArrayList();
            List<MDMS_O_SUPPLY_ANNUAL_REPORT_DTL> reportDtlList=Lists.newArrayList();
            if(CollectionUtils.isNotEmpty(list)){
                List<String> cortNumIds=list.stream().map(SupplyAnnualReport :: getCortNumId).distinct().collect(Collectors.toList());
                List<CortInfo> cortInfoList=mdmsOCortDao.getCortInfo(tenantNumId,dataSign,cortNumIds);
                if(CollectionUtils.isEmpty(cortInfoList)){
                    throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该公司不存在");
                }
                Map<String, String> cortMap=cortInfoList.stream().collect(Collectors.toMap(CortInfo::getCortNumId,CortInfo::getCortName));
                Map<String, List<SupplyAnnualReport>> map = list.stream().collect(Collectors.groupingBy(SupplyAnnualReport::getCortNumId));
                for (Map.Entry<String, List<SupplyAnnualReport>> entry : map.entrySet()) {
                    String cortNumId=entry.getKey();
                    List<SupplyAnnualReport> dtlList=entry.getValue();
                    MDMS_O_SUPPLY_ANNUAL_REPORT_HDR entity=this.buildSupplyReport(tenantNumId,dataSign,usrNumId,cortNumId,cortMap.get(cortNumId),reportYear,dtlList.size());
                    hdrList.add(entity);
                    dtlList.stream().forEach(dtl->{
                        MDMS_O_SUPPLY_ANNUAL_REPORT_DTL dtlEntity=buildSupplyReportDtl(tenantNumId,dataSign,usrNumId,cortNumId,reportYear,dtl);
                        reportDtlList.add(dtlEntity);
                    });
                }
            }
            //开启事务
            TransactionStatus txStatus = masterDataTransactionManager
                    .getTransaction(TransactionUtil.newTransactionDefinition(600));
            try {
                if(CollectionUtils.isNotEmpty(hdrList)){
                    mdmsOSupplyAnnualReportHdrDao.batchInsert(hdrList);
                }
                if(CollectionUtils.isNotEmpty(reportDtlList)){
                    mdmsOSupplyAnnualReportDtlDao.batchInsert(reportDtlList);
                }
                masterDataTransactionManager.commit(txStatus);
            }catch (Exception e){
                masterDataTransactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end generateSupplyAnnualReport response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public SupplyAnnualReportConfirmResponse confirmSupplyAnnualReport(SupplyAnnualReportConfirmRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin confirmSupplyAnnualReport request:{}", JsonUtil.toJson(request));
        }
        SupplyAnnualReportConfirmResponse response=new SupplyAnnualReportConfirmResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            Long usrNumId = request.getUserNumId();
            Integer confirmStatus=request.getConfirmStatus();
            String series=request.getSeries();
            String remark=request.getRemark();
            if(confirmStatus.compareTo(3)==0){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "当确认状态为无需上传时，无需上传原因必填");
            }
            MDMS_O_SUPPLY_ANNUAL_REPORT_DTL entity=mdmsOSupplyAnnualReportDtlDao.getReportDtlEntity(tenantNumId,dataSign,series);
            if(entity.getCONFIRM_STATUS()==2){
                throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该条数据已上传，无需确认上传");
            }else{
                MDMS_O_SUPPLY_ANNUAL_REPORT_HDR hdrEntity=mdmsOSupplyAnnualReportHdrDao.getHdrEntity(tenantNumId,dataSign,entity.getCORT_NUM_ID(),entity.getREPORT_YEAR());
                //开启事务
                TransactionStatus txStatus = masterDataTransactionManager
                        .getTransaction(TransactionUtil.newTransactionDefinition(600));
                try {
                    if(!Objects.isNull(hdrEntity)){
                        mdmsOSupplyAnnualReportDtlDao.updateEntity(tenantNumId,dataSign,usrNumId,series,confirmStatus,remark);
                        if(confirmStatus.compareTo(2)==0){
                            mdmsOSupplyAnnualReportHdrDao.updateConfirmQty(tenantNumId,dataSign,usrNumId,entity.getCORT_NUM_ID(),entity.getREPORT_YEAR());
                        }
                        MDMS_O_SUPPLY_ANNUAL_REPORT_LOG logEntity=this.buildReportLog(tenantNumId,dataSign,usrNumId,entity.getSUPPLY_NUM_ID(),entity.getSUPPLY_NAME(),entity.getUNIFIED_SOCIAL_CREDIT_CODE(),confirmStatus,hdrEntity.getSERIES());
                        mdmsOSupplyAnnualReportLogDao.insert(logEntity);
                    }
                    masterDataTransactionManager.commit(txStatus);
                }catch (Exception e){
                    masterDataTransactionManager.rollback(txStatus);
                    throw e;
                }
            }
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end confirmSupplyAnnualReport response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public SupplyAnnualReportQueryResponse querySupplyAnnualReport(SupplyAnnualReportQueryRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin querySupplyAnnualReport request:{}", JsonUtil.toJson(request));
        }
        SupplyAnnualReportQueryResponse response = new SupplyAnnualReportQueryResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            List<SupplyAnnualReportForQuery> list = mdmsOSupplyAnnualReportHdrDao.getAnnualReportPage(tenantNumId, dataSign,request.getCortNumId(), request.getCortName(), request.getReportYear(),request.getPageNum(),request.getPageSize());
            if(CollectionUtils.isNotEmpty(list)){
                for(SupplyAnnualReportForQuery sq : list){
                    sq.setConfirmProcess(sq.getConfirmQty()+"/"+sq.getTotalQty());
                }
                response.setResults(list);
            }
            Integer count = mdmsOSupplyAnnualReportHdrDao.getAnnualReportCount(request.getTenantNumId(), request.getDataSign(),request.getCortNumId(), request.getCortName(), request.getReportYear());
            response.setRecordCount(count);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end querySupplyAnnualReport response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public SupplyInfoQueryResponse querySupplyInfo(SupplyInfoQueryRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin querySupplyInfo request:{}", JsonUtil.toJson(request));
        }
        SupplyInfoQueryResponse response = new SupplyInfoQueryResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            String cortNumId=request.getCortNumId();
            String supplyNumId=request.getSupplyNumId();
            SupplyInfoForQuery supplyInfo=mdmsOSupplyDao.getSupplyBySupplyNumId(tenantNumId,dataSign,supplyNumId);
            if(!Objects.isNull(supplyInfo)){
                SubSupplyInfoForQuery subSupplyInfo=mdmsOSubSupplyDao.getSubSupplyBySupplyNumId(tenantNumId,dataSign,supplyNumId,cortNumId);
                supplyInfo.setSubSupplyInfo(subSupplyInfo);
            }
            response.setSupplyInfo(supplyInfo);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end querySupplyInfo response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    //批发公司关联连锁公司--批发公司查询
    @Override
    public WholesaleChainRelationResponse wholesaleRelatChain(WholesaleChainRelationRequest request) {
        // 不明确
        if (log.isDebugEnabled()) {
            log.debug("begin addManagerCompany request:{}", JsonUtil.toJson(request));
        }
        WholesaleChainRelationResponse response = new WholesaleChainRelationResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);

            List<MdmsOWholesaleChain> list = mdmsOWholesaleChainDao.checkExistWholesaleNumId(request.getWholesaleNumId(), request.getTenantNumId(), request.getDataSign());
            response.setResults(list);

        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerList response: {}", JsonUtil.toJson(response));
        }

        return response;
    }

    //批发公司关联连锁公司--连锁公司查询
    @Override
    public WholesaleChainRelationsResponse wholesaleRelatsChain(WholesaleChainRelationsRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin addManagerCompany request:{}", JsonUtil.toJson(request));
        }
        WholesaleChainRelationsResponse response=new WholesaleChainRelationsResponse();
        try {
            List<MdmsOWholesaleChain> list = mdmsOWholesaleChainDao.getWholesaleNumId(request.getTenantNumId(), request.getDataSign(), request.getWholesaleNumId());
            response.setList(list);

        } catch (Exception ex) {
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    //批发公司关联连锁公司--删除连锁公司
    @Override
    public WholesaleDeleteChainResponse wholesaleDeleteChain(WholesaleDeleteChainRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin deleteChannelPolicy request:{}", JsonUtil.toJson(request));
        }
        WholesaleDeleteChainResponse response=new WholesaleDeleteChainResponse();
        try {
            mdmsOWholesaleChainDao.deleteChainNumId(request.getTenantNumId(), request.getDataSign(),request.getChainNumId());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteChannelPolicy response: {}", JsonUtil.toJson(response));
        }
        return response;
    }


    private MDMS_O_WHOLESALE_CHAIN createCsProducts (WholesaleAddChainRequest request,String chainNumId,String chainName,String unifiedSocialCreditCode){
        MDMS_O_WHOLESALE_CHAIN product = JsonUtil.fromJsonCamel(JsonUtil.toJson(request), MDMS_O_WHOLESALE_CHAIN.class);
        product.setCHAIN_NUM_ID(chainNumId);
        product.setCHAIN_NAME(chainName);
        product.setUNIFIED_SOCIAL_CREDIT_CODE(unifiedSocialCreditCode);
        return product;

    }
    //批发公司关联连锁公司--新增连锁公司
    @Override


    public WholesaleAddChainResponse wholesaleAddChain(WholesaleAddChainRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveMdmsSConfig request: {}", JsonUtil.toJson(request));
        }
        WholesaleAddChainResponse response=new WholesaleAddChainResponse();
        try {
            MDMS_O_WHOLESALE_CHAIN hdr =createCsProducts(request,request.getChainNumId(),request.getChainName(),request.getUnifiedSocialCreditCode());
            mdmsOWholesaleChainDao.insertChainNumId(hdr);

        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteChannelPolicy response: {}", JsonUtil.toJson(response));
        }

        return response;
    }

    //批发公司关联连锁公司--查询添加连锁公司
    @Override
    public WholesaleSelectChainResponse wholesaleSelectChain(WholesaleSelectChainRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin addManagerCompany request:{}", JsonUtil.toJson(request));
        }
        WholesaleSelectChainResponse response =new WholesaleSelectChainResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);

            List<MdmsOWholesaleChain> list = mdmsOWholesaleChainDao.getWholesaleNumId(request.getTenantNumId(), request.getDataSign(),request.getWholesaleNumId());
            response.setResults(list);
        } catch (Exception ex) {
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    public void saveLicense( List<LicenseResourceDetails> licenseList, String reservedNo, Integer typeNumId, String cortNumId) {
        LicenseResSaveRequest licenseResSaveRequest = new LicenseResSaveRequest();
        licenseResSaveRequest.setResourcesInfoList(licenseList);
        licenseResSaveRequest.setRelationId(reservedNo);
        licenseResSaveRequest.setBusinessType(typeNumId);
        licenseResSaveRequest.setCortNumId(cortNumId);
        mdTenantService.saveLicenseRes(licenseResSaveRequest);
    }

    /*****
     * 客户、供应商 生成主档的时候，需要同步生成一个同一编号、同一名称的  逻辑仓。
     * 2021年10月30日17:02:16
     */
    private void  generateStorage(Long tenantNumId, Long dataSign,Long usrNumId,String cortNumId,String storageNumId,String storageName,Long businessType,Long prvNumId,Long cityNumId,Long cityAreaNumId,Long townNumId,String adr){
        MDMS_W_STORAGE entity = new MDMS_W_STORAGE();
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_W_STORAGE_SERIES));
        entity.setCREATE_DTME(new Date());
        entity.setCANCELSIGN(Constant.CANCEL_SIGN_N);
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_USER_ID(usrNumId);
        entity.setCORT_NUM_ID(cortNumId);
        entity.setSUB_UNIT_NUM_ID(" ");
        entity.setSTORAGE_NUM_ID(storageNumId);
        entity.setPRV_NUM_ID(prvNumId);
        entity.setCITY_NUM_ID(cityNumId);
        entity.setCITY_AREA_NUM_ID(cityAreaNumId);
        entity.setTOWN_NUM_ID(townNumId);
        entity.setADR(adr);
        entity.setSTORAGE_NAME(storageName);
        entity.setBUSINESS_TYPE(businessType);
        entity.setMANAGE_TYPE(3L);
        entity.setPHYSICAL_NUM_ID(" ");
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setLAST_UPDTME(new Date());
        if(!mdmsWStorageDao.checkStorageExist(tenantNumId,dataSign,cortNumId,storageNumId)){
            mdmsWStorageDao.insertEntity(entity);

        }
    }
    private MDMS_O_SUPPLY_ANNUAL_REPORT_HDR buildSupplyReport(Long tenantNumId, Long dataSign,Long usrNumId,String cortNumId,String cortName,String reportYear,Integer dtlSize){
        MDMS_O_SUPPLY_ANNUAL_REPORT_HDR entity = new MDMS_O_SUPPLY_ANNUAL_REPORT_HDR();
        entity.setCORT_NUM_ID(cortNumId);
        entity.setCORT_NAME(cortName);
        entity.setREPORT_YEAR(reportYear);
        entity.setREPORT_TITLE(reportYear+"年度供应商年报");
        entity.setCONFIRM_QTY(0);
        entity.setTOTAL_QTY(dtlSize);
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_SUPPLY_ANNUAL_REPORT_HDR_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }
    private MDMS_O_SUPPLY_ANNUAL_REPORT_DTL buildSupplyReportDtl(Long tenantNumId, Long dataSign,Long usrNumId,String cortNumId,String reportYear,SupplyAnnualReport dtl){
        MDMS_O_SUPPLY_ANNUAL_REPORT_DTL entity = new MDMS_O_SUPPLY_ANNUAL_REPORT_DTL();
        entity.setCORT_NUM_ID(cortNumId);
        entity.setREPORT_YEAR(reportYear);
        entity.setSUPPLY_NUM_ID(dtl.getSupplyNumId());
        entity.setSUPPLY_NAME(dtl.getSupplyName());
        entity.setUNIFIED_SOCIAL_CREDIT_CODE(dtl.getUnifiedSocialCreditCode());
        entity.setSUPPLY_STATUS(dtl.getSupplyStatus());
        entity.setSUB_SUPPLY_STATUS(dtl.getSubSupplyStatus());
        entity.setCONFIRM_STATUS(1);
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_SUPPLY_ANNUAL_REPORT_DTL_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }

    private MDMS_O_SUPPLY_ANNUAL_REPORT_LOG buildReportLog(Long tenantNumId, Long dataSign,Long usrNumId,String supplyNumId,String supplyName,String code,Integer confirmStatus,String relateSeries){
        MDMS_O_SUPPLY_ANNUAL_REPORT_LOG entity = new MDMS_O_SUPPLY_ANNUAL_REPORT_LOG();
        entity.setRELATE_SERIES(relateSeries);
        entity.setSUPPLY_NUM_ID(supplyNumId);
        entity.setSUPPLY_NAME(supplyName);
        entity.setUNIFIED_SOCIAL_CREDIT_CODE(code);
        entity.setCONFIRM_STATUS(confirmStatus);
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_SUPPLY_ANNUAL_REPORT_LOG_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;

    }
    public MDMS_ASSIST_ON_OFF createMdmsAssistOnOff(MDMS_O_CORT cort, Long tenantNumId, Long dataSign, Long usrNumId){
        MDMS_ASSIST_ON_OFF entity = new MDMS_ASSIST_ON_OFF();
        entity.setSERIES(Long.valueOf(SeqUtil.getSeqNextValue(SeqUtil.MDMS_ASSIST_ON_OFF_SERIES)));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign.intValue());
        entity.setCORT_NUM_ID(cort.getCORT_NUM_ID());
        entity.setCORT_NAME(cort.getCORT_NAME());
        entity.setASSIST_STATUS(AssistStatusEnums.CLOSE.getId());
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }

    private MDMS_O_CUSTOMER createMdmsOCustomer(Long tenantNumId, Long dataSign, Long usrNumId, MDMS_BL_CUSTOMER detail){
        MDMS_O_CUSTOMER entity = new MDMS_O_CUSTOMER();
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCUSTOMER_NAME(detail.getCUSTOMER_NAME());
        entity.setCUSTOMER_SIM_NAME(detail.getCUSTOMER_SIM_NAME());
        entity.setMNEMONIC_CODE(detail.getMNEMONIC_CODE());
        entity.setPOSTAL_CODE(detail.getPOSTAL_CODE());
        entity.setCONTACT_NUM(detail.getCONTACT_NUM());
        entity.setCUSTOMER_CLASSIFY(detail.getCUSTOMER_CLASSIFY());
        entity.setADR(detail.getADR());
        entity.setDEFAULT_HARVEST_ADR(detail.getDEFAULT_HARVEST_ADR());
        entity.setBUSINESS_SCOPE(detail.getBUSINESS_SCOPE());
        entity.setBUSINESS_PERMIT(detail.getBUSINESS_PERMIT());
        entity.setUNIFIED_SOCIAL_CREDIT_CODE(detail.getUNIFIED_SOCIAL_CREDIT_CODE());
        entity.setCUSTOMER_STATUS(detail.getCUSTOMER_STATUS());
        entity.setREGISTR_BEGIN_DATE(detail.getREGISTR_BEGIN_DATE());
        entity.setREGISTR_END_DATE(detail.getREGISTR_END_DATE());
        entity.setREGISTER_PLACE(detail.getREGISTER_PLACE());
        entity.setTAX_ACCOUNT(detail.getTAX_ACCOUNT());
        entity.setPRV_NUM_ID(detail.getPRV_NUM_ID());
        entity.setCITY_NUM_ID(detail.getCITY_NUM_ID());
        entity.setCITY_AREA_NUM_ID(detail.getCITY_AREA_NUM_ID());
        entity.setTOWN_NUM_ID(detail.getTOWN_NUM_ID());
        entity.setCREDIT_LINE(detail.getCREDIT_LINE());
        entity.setREGISTERED_CAPITAL(detail.getREGISTERED_CAPITAL());
        entity.setPAID_IN_CAPITAL(detail.getPAID_IN_CAPITAL());
        entity.setABC_MARK(detail.getABC_MARK());
        entity.setBASIC_BANK_NUM(detail.getBASIC_BANK_NUM());
        entity.setBASIC_BANK_NAME(detail.getBASIC_BANK_NAME());
        entity.setBASIC_BANK_ACCOUNT(detail.getBASIC_BANK_ACCOUNT());
        entity.setGOVERNMENT_RELATED_CUSTOMER(detail.getGOVERNMENT_RELATED_CUSTOMER());
        entity.setPOTENTIAL_LICHONG_CUSTOMER(detail.getPOTENTIAL_LICHONG_CUSTOMER());
        entity.setGOVERNMENT_RELATED_CUSTOMER_REMARK(detail.getGOVERNMENT_RELATED_CUSTOMER_REMARK());
        entity.setPOTENTIAL_LICHONG_CUSTOMER_REMARK(detail.getPOTENTIAL_LICHONG_CUSTOMER_REMARK());
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }

    private MDMS_O_SUB_CUSTOMER createMdmsOSubCustomer(Long tenantNumId, Long dataSign, Long usrNumId,String cortNumId,String customerNumId, MDMS_BL_CUSTOMER detail){
        MDMS_O_SUB_CUSTOMER entity = new MDMS_O_SUB_CUSTOMER();
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_SUB_CUSTOMER_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCUSTOMER_NUM_ID(customerNumId);
        entity.setCORT_NUM_ID(cortNumId);
        entity.setOLD_CUSTOMER_ID(detail.getOLD_CUSTOMER_ID());
        entity.setSUB_CREDIT_LINE(detail.getSUB_CREDIT_LINE());
        entity.setFINANCE_NAME(detail.getFINANCE_NAME());
        entity.setFINANCE_TEL(detail.getFINANCE_TEL());
        entity.setFILE_NO(detail.getFILE_NO());
        entity.setCORPORAT(detail.getCORPORAT());
        entity.setCORPORAT_PROXY_NAME(detail.getCORPORAT_PROXY_NAME());
        entity.setCORPORAT_PROXY_STAT_BEGIN(detail.getCORPORAT_PROXY_STAT_BEGIN());
        entity.setCORPORAT_PROXY_STAT_END(detail.getCORPORAT_PROXY_STAT_END());
        entity.setCORPORAT_PROXY_STAT_IDCARD(detail.getCORPORAT_PROXY_STAT_IDCARD());
        entity.setCORPORAT_PROXY_IDCARD_BEGIN(detail.getCORPORAT_PROXY_IDCARD_BEGIN());
        entity.setCORPORAT_PROXY_IDCARD_END(detail.getCORPORAT_PROXY_IDCARD_END());
        entity.setSUB_CUSTOMER_STATUS(detail.getSUB_CUSTOMER_STATUS());
        entity.setSALE_LIMIT_DAYS(detail.getSALE_LIMIT_DAYS());
        entity.setREMARK(detail.getREMARK());
        entity.setANNUAL_REPORT(detail.getANNUAL_REPORT());
        entity.setANNUAL_REPORT_BEGIN(detail.getANNUAL_REPORT_BEGIN());
        entity.setANNUAL_REPORT_END(detail.getANNUAL_REPORT_END());
        entity.setPAYMENT_TERM(detail.getPAYMENT_TERM());
        entity.setMONEY_TYPE(detail.getMONEY_TYPE());
        entity.setDRUG_BUSINESS_LICENSE(detail.getDRUG_BUSINESS_LICENSE());
        entity.setDRUG_BUSINESS_LICENSE_BEGIN(detail.getDRUG_BUSINESS_LICENSE_BEGIN());
        entity.setDRUG_BUSINESS_LICENSE_END(detail.getDRUG_BUSINESS_LICENSE_END());
        entity.setDRUG_PRODUCT_LICENSE(detail.getDRUG_PRODUCT_LICENSE());
        entity.setDRUG_PRODUCT_LICENSE_BEGIN(detail.getDRUG_PRODUCT_LICENSE_BEGIN());
        entity.setDRUG_PRODUCT_LICENSE_END(detail.getDRUG_PRODUCT_LICENSE_END());
        entity.setPURCHASE_LEADER(detail.getPURCHASE_LEADER());
        entity.setCLOSE_DAYS(detail.getCLOSE_DAYS());
        entity.setFORBID_RETURN_FACTORY(detail.getFORBID_RETURN_FACTORY());
        entity.setFORBID_SALE(detail.getFORBID_SALE());
        entity.setPRE_DISTRIBUTION(detail.getPRE_DISTRIBUTION());
        entity.setDIRECT_DELIVERY_FLAG(detail.getDIRECT_DELIVERY_FLAG());
        entity.setSTORAGE_ADR(detail.getSTORAGE_ADR());
        entity.setBUSINESS_CONTACT(detail.getBUSINESS_CONTACT());
        entity.setBUSINESS_CONTACT_TEL(detail.getBUSINESS_CONTACT_TEL());
        entity.setBUSINESS_CONTACT_IDCARD(detail.getBUSINESS_CONTACT_IDCARD());
        entity.setMEDICAL_DEVICE_PRO_REC2(detail.getMEDICAL_DEVICE_PRO_REC2());
        entity.setMEDICAL_DEVICE_PRO_REC2_SCOPE(detail.getMEDICAL_DEVICE_PRO_REC2_SCOPE());
        entity.setMEDICAL_DEVICE_BUS_LINCENSE(detail.getMEDICAL_DEVICE_BUS_LINCENSE());
        entity.setMEDICAL_DEVICE_BUS_SCOPE(detail.getMEDICAL_DEVICE_BUS_SCOPE());
        entity.setMEDICAL_DEVICE_BUS_BEGIN(detail.getMEDICAL_DEVICE_BUS_BEGIN());
        entity.setMEDICAL_DEVICE_BUS_END(detail.getMEDICAL_DEVICE_BUS_END());
        entity.setFOOD_BUS_LICENSE(detail.getFOOD_BUS_LICENSE());
        entity.setFOOD_BUS_LICENSE_SCOPE(detail.getFOOD_BUS_LICENSE_SCOPE());
        entity.setFOOD_BUS_LICENSE_BEGIN(detail.getFOOD_BUS_LICENSE_BEGIN());
        entity.setFOOD_BUS_LICENSE_END(detail.getFOOD_BUS_LICENSE_END());
        entity.setMEDICAL_INSTITUTION_LICENSE(detail.getMEDICAL_INSTITUTION_LICENSE());
        entity.setMEDICAL_INSTITUTION_BEGIN(detail.getMEDICAL_INSTITUTION_BEGIN());
        entity.setMEDICAL_INSTITUTION_END(detail.getMEDICAL_INSTITUTION_END());
        entity.setLICENCE_WARN_DAYS(detail.getLICENCE_WARN_DAYS());
        entity.setRECEIVING_BANK_NUM(detail.getRECEIVING_BANK_NUM());
        entity.setRECEIVING_BANK_NAME(detail.getRECEIVING_BANK_NAME());
        entity.setRECEIVING_BANK_ACCOUNT(detail.getRECEIVING_BANK_ACCOUNT());
        entity.setREPLENISHER(detail.getREPLENISHER());
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }

    private MDMS_BL_CUSTOMER createMdmsBlCustomer(Long tenantNumId, Long dataSign, Long usrNumId,String reservedNo, BlCustomerInfo detail){
        MDMS_BL_CUSTOMER entity = new MDMS_BL_CUSTOMER();
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_CUSTOMER_SERIES));
        entity.setRESERVED_NO(reservedNo);
        entity.setCUSTOMER_NAME(detail.getCustomerName());
        entity.setCUSTOMER_SIM_NAME(detail.getCustomerSimName());
        entity.setMNEMONIC_CODE(detail.getMnemonicCode());
        entity.setPOSTAL_CODE(detail.getPostalCode());
        entity.setCONTACT_NUM(detail.getContactNum());
        entity.setCUSTOMER_CLASSIFY(detail.getCustomerClassify());
        entity.setADR(detail.getAdr());
        entity.setQUALITY_AGREEMENT_BEGIN_DATE(detail.getQualityAgreementBeginDate());
        entity.setQUALITY_AGREEMENT_END_DATE(detail.getQualityAgreementEndDate());
        entity.setDEFAULT_HARVEST_ADR(detail.getDefaultHarvestAdr());
        entity.setBUSINESS_SCOPE(detail.getBusinessScope());
        entity.setBUSINESS_PERMIT(detail.getBusinessPermit());
        entity.setANNUAL_REPORT(detail.getAnnualReport());
        entity.setANNUAL_REPORT_BEGIN(detail.getAnnualReportBegin());
        entity.setANNUAL_REPORT_END(detail.getAnnualReportEnd());
        entity.setPAYMENT_TERM(detail.getPaymentTerm());
        entity.setUNIFIED_SOCIAL_CREDIT_CODE(detail.getUnifiedSocialCreditCode());
        entity.setREGISTR_BEGIN_DATE(detail.getRegistrBeginDate());
        entity.setREGISTR_END_DATE(detail.getRegistrEndDate());
        entity.setREGISTER_PLACE(detail.getRegisterPlace());
        entity.setTAX_ACCOUNT(detail.getTaxAccount());
        entity.setQUALITY_LEADER(detail.getQualityLeader());
        entity.setPRV_NUM_ID(detail.getPrvNumId());
        entity.setCITY_NUM_ID(detail.getCityNumId());
        entity.setCITY_AREA_NUM_ID(detail.getCityAreaNumId());
        entity.setTOWN_NUM_ID(detail.getTownNumId());
        entity.setCREDIT_LINE(detail.getCreditLine());
        entity.setREGISTERED_CAPITAL(detail.getRegisteredCapital());
        entity.setPAID_IN_CAPITAL(detail.getPaidInCapital());
        entity.setABC_MARK(detail.getAbcMark());
        entity.setBASIC_BANK_NUM(detail.getBasicBankNum());
        entity.setBASIC_BANK_NAME(detail.getBasicBankName());
        entity.setBASIC_BANK_ACCOUNT(detail.getBasicBankAccount());
        entity.setRECEIVING_BANK_NUM(detail.getReceivingBankNum());
        entity.setRECEIVING_BANK_NAME(detail.getReceivingBankName());
        entity.setRECEIVING_BANK_ACCOUNT(detail.getReceivingBankAccount());
        entity.setGOVERNMENT_RELATED_CUSTOMER(detail.getGovernmentRelatedCustomer());
        entity.setPOTENTIAL_LICHONG_CUSTOMER(detail.getPotentialLichongCustomer());
        entity.setGOVERNMENT_RELATED_CUSTOMER_REMARK(detail.getGovernmentRelatedCustomerRemark());
        entity.setPOTENTIAL_LICHONG_CUSTOMER_REMARK(detail.getPotentialLichongCustomerRemark());
        entity.setOLD_CUSTOMER_ID(detail.getOldCustomerId());
        entity.setSUB_CREDIT_LINE(detail.getSubCreditLine());
        entity.setFINANCE_NAME(detail.getFinanceName());
        entity.setFINANCE_TEL(detail.getFinanceTel());
        entity.setFILE_NO(detail.getFileNo());
        entity.setIS_MONOMER_DRUGSTORE(detail.getIsMonomerDrugstore());
        entity.setCORPORAT(detail.getCorporat());
        entity.setCORPORAT_PROXY_NAME(detail.getCorporatProxyName());
        entity.setCORPORAT_PROXY_STAT_BEGIN(detail.getCorporatProxyStatBegin());
        entity.setCORPORAT_PROXY_STAT_END(detail.getCorporatProxyStatEnd());
        entity.setCORPORAT_PROXY_STAT_IDCARD(detail.getCorporatProxyStatIdcard());
        entity.setCORPORAT_PROXY_IDCARD_BEGIN(detail.getCorporatProxyIdcardBegin());
        entity.setCORPORAT_PROXY_IDCARD_END(detail.getCorporatProxyIdcardEnd());
        entity.setSALE_LIMIT_DAYS(detail.getSaleLimitDays());
        entity.setREMARK(detail.getRemark());
        entity.setMONEY_TYPE(detail.getMoneyType());
        entity.setDRUG_BUSINESS_LICENSE(detail.getDrugBusinessLicense());
        entity.setDRUG_BUSINESS_LICENSE_BEGIN(detail.getDrugBusinessLicenseBegin());
        entity.setDRUG_BUSINESS_LICENSE_END(detail.getDrugBusinessLicenseEnd());
        entity.setDRUG_PRODUCT_LICENSE(detail.getDrugProductLicense());
        entity.setDRUG_PRODUCT_LICENSE_BEGIN(detail.getDrugProductLicenseBegin());
        entity.setDRUG_PRODUCT_LICENSE_END(detail.getDrugProductLicenseEnd());
        entity.setPURCHASE_LEADER(detail.getPurchaseLeader());
        entity.setCLOSE_DAYS(detail.getCloseDays());
        entity.setFORBID_RETURN_FACTORY(detail.getForbidReturnFactory());
        entity.setFORBID_SALE(detail.getForbidSale());
        entity.setPRE_DISTRIBUTION(detail.getPreDistribution());
        entity.setDIRECT_DELIVERY_FLAG(detail.getDirectDeliveryFlag());
        entity.setSTORAGE_ADR(detail.getStorageAdr());
        entity.setBUSINESS_CONTACT(detail.getBusinessContact());
        entity.setBUSINESS_CONTACT_TEL(detail.getBusinessContactTel());
        entity.setBUSINESS_CONTACT_IDCARD(detail.getBusinessContactIdcard());
        entity.setMEDICAL_DEVICE_PRO_REC2(detail.getMedicalDeviceProRec2());
        entity.setMEDICAL_DEVICE_PRO_REC2_SCOPE(detail.getMedicalDeviceProRec2Scope());
        entity.setMEDICAL_DEVICE_BUS_LINCENSE(detail.getMedicalDeviceBusLincense());
        entity.setMEDICAL_DEVICE_BUS_SCOPE(detail.getMedicalDeviceBusScope());
        entity.setMEDICAL_DEVICE_BUS_BEGIN(detail.getMedicalDeviceBusBegin());
        entity.setMEDICAL_DEVICE_BUS_END(detail.getMedicalDeviceBusEnd());
        entity.setFOOD_BUS_LICENSE(detail.getFoodBusLicense());
        entity.setFOOD_BUS_LICENSE_SCOPE(detail.getFoodBusLicenseScope());
        entity.setFOOD_BUS_LICENSE_BEGIN(detail.getFoodBusLicenseBegin());
        entity.setFOOD_BUS_LICENSE_END(detail.getFoodBusLicenseEnd());
        entity.setMEDICAL_INSTITUTION_LICENSE(detail.getMedicalInstitutionLicense());
        entity.setMEDICAL_INSTITUTION_BEGIN(detail.getMedicalInstitutionBegin());
        entity.setMEDICAL_INSTITUTION_END(detail.getMedicalInstitutionEnd());
        entity.setLICENCE_WARN_DAYS(detail.getLicenceWarnDays());
        entity.setREPLENISHER(detail.getReplenisher());
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }

    private MDMS_O_SUB_PARTNER createMdmsOSubPartner(Long tenantNumId, Long dataSign, Long usrNumId, String cortNumId, String partnerNumId, MDMS_BL_PARTNER mdms_bl_partner) {
        MDMS_O_SUB_PARTNER entity = new MDMS_O_SUB_PARTNER();
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_SUB_PARTNER_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setPARTNER_NUM_ID(partnerNumId);
        entity.setCORT_NUM_ID(cortNumId);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        entity.setCONTACTS_TEL(mdms_bl_partner.getCONTACTS_TEL());
        entity.setCONTACTS(mdms_bl_partner.getCONTACTS());
        return entity;
    }

    private MDMS_O_SUB_SUPPLY createMdmsOSubSupply(Long tenantNumId, Long dataSign, Long usrNumId,  String cortNumId, String supplyNumId, MDMS_BL_SUPPLY detail) {
        MDMS_O_SUB_SUPPLY entity = new MDMS_O_SUB_SUPPLY();
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_SUB_SUPPLY_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setSUPPLY_NUM_ID(supplyNumId);
        entity.setCORT_NUM_ID(cortNumId);
        entity.setOLD_SUPPLY_ID(detail.getOLD_SUPPLY_ID());
        entity.setCONTROL_SUBJECT(detail.getCONTROL_SUBJECT());
        entity.setREMARK(detail.getREMARK());
        entity.setMONEY_TYPE(detail.getMONEY_TYPE());
        entity.setPURCHASE_LEADER(detail.getPURCHASE_LEADER());
        entity.setPAYMENT_CONDITION(detail.getPAYMENT_CONDITION());
        entity.setCLOSE_DAYS(detail.getCLOSE_DAYS());
        entity.setFORBID_PURCHASE(detail.getFORBID_PURCHASE());
        entity.setFORBID_DISTRIBUTION(detail.getFORBID_DISTRIBUTION());
        entity.setFORBID_PAYMENT(detail.getFORBID_PAYMENT());
        entity.setFORBID_RETURN_FACTORY(detail.getFORBID_RETURN_FACTORY());
        entity.setFORBID_SALE(detail.getFORBID_SALE());
        entity.setFORBID_RETURN_STORE(detail.getFORBID_RETURN_STORE());
        entity.setPRE_DISTRIBUTION(detail.getPRE_DISTRIBUTION());
        entity.setDIRECT_DELIVERY_FLAG(detail.getDIRECT_DELIVERY_FLAG());
        entity.setSNNUAL_ANNOUNCEMEN(detail.getSNNUAL_ANNOUNCEMEN());
        entity.setSTORAGE_ADR(detail.getSTORAGE_ADR());
        entity.setCORPORAT_PROXY_STAT_IDCARD(detail.getCORPORAT_PROXY_STAT_IDCARD());
        entity.setSUB_SUPPLY_STATUS(detail.getSUB_SUPPLY_STATUS());
        entity.setCORPORAT(detail.getCORPORAT());
        entity.setCORPORAT_PROXY_NAME(detail.getCORPORAT_PROXY_NAME());
        entity.setCORPORAT_PROXY_STAT_BEGIN(detail.getCORPORAT_PROXY_STAT_BEGIN());
        entity.setCORPORAT_PROXY_STAT_END(detail.getCORPORAT_PROXY_STAT_END());
        entity.setQUALITY_LEADER(detail.getQUALITY_LEADER());
        entity.setBUSINESS_CONTACT(detail.getBUSINESS_CONTACT());
        entity.setBUSINESS_CONTACT_TEL(detail.getBUSINESS_CONTACT_TEL());
        entity.setBUSINESS_CONTACT_IDCARD(detail.getBUSINESS_CONTACT_IDCARD());
        entity.setSPDA_RECORD_BEGIN(detail.getSPDA_RECORD_BEGIN());
        entity.setSPDA_RECORD_END(detail.getSPDA_RECORD_END());
        entity.setSPDA_RECORD_STAFF(detail.getSPDA_RECORD_STAFF());
        entity.setSPDA_RECORD_IDCARD(detail.getSPDA_RECORD_IDCARD());
        entity.setBUSINESS_LICENSE_BEGIN(detail.getBUSINESS_LICENSE_BEGIN());
        entity.setBUSINESS_LICENSE_END(detail.getBUSINESS_LICENSE_END());
        entity.setQUALITY_AGREEMENT_BEGIN(detail.getQUALITY_AGREEMENT_BEGIN());
        entity.setQUALITY_AGREEMENT_END(detail.getQUALITY_AGREEMENT_END());
        entity.setLICENCE_WARN_DAYS(detail.getLICENCE_WARN_DAYS());
        entity.setREPLENISHER(detail.getREPLENISHER());
        entity.setDRUG_BUSINESS_LICENSE(detail.getDRUG_BUSINESS_LICENSE());
        entity.setDRUG_BUSINESS_LICENSE_BEGIN(detail.getDRUG_BUSINESS_LICENSE_BEGIN());
        entity.setDRUG_BUSINESS_LICENSE_END(detail.getDRUG_BUSINESS_LICENSE_END());
        entity.setDRUG_PRODUCT_LICENSE(detail.getDRUG_PRODUCT_LICENSE());
        entity.setDRUG_PRODUCT_LICENSE_BEGIN(detail.getDRUG_PRODUCT_LICENSE_BEGIN());
        entity.setDRUG_PRODUCT_LICENSE_END(detail.getDRUG_PRODUCT_LICENSE_END());
        entity.setHYGIENE_LICENSE_SCOPE(detail.getHYGIENE_LICENSE_SCOPE());
        entity.setHYGIENE_LICENSE_BEGIN(detail.getHYGIENE_LICENSE_BEGIN());
        entity.setHYGIENE_LICENSE_END(detail.getHYGIENE_LICENSE_END());
        entity.setMEDICAL_DEVICE_PRO_LINCENSE(detail.getMEDICAL_DEVICE_PRO_LINCENSE());
        entity.setMEDICAL_DEVICE_SCOPE(detail.getMEDICAL_DEVICE_SCOPE());
        entity.setMEDICAL_DEVICE_BEGIN(detail.getMEDICAL_DEVICE_BEGIN());
        entity.setMEDICAL_DEVICE_END(detail.getMEDICAL_DEVICE_END());
        entity.setMEDICAL_DEVICE_PRO_REC1(detail.getMEDICAL_DEVICE_PRO_REC1());
        entity.setMEDICAL_DEVICE_PRO_REC1_SCOPE(detail.getMEDICAL_DEVICE_PRO_REC1_SCOPE());
        entity.setMEDICAL_DEVICE_PRO_REC2(detail.getMEDICAL_DEVICE_PRO_REC2());
        entity.setMEDICAL_DEVICE_PRO_REC2_SCOPE(detail.getMEDICAL_DEVICE_PRO_REC2_SCOPE());
        entity.setMEDICAL_DEVICE_BUS_LINCENSE(detail.getMEDICAL_DEVICE_BUS_LINCENSE());
        entity.setMEDICAL_DEVICE_BUS_SCOPE(detail.getMEDICAL_DEVICE_BUS_SCOPE());
        entity.setMEDICAL_DEVICE_BUS_BEGIN(detail.getMEDICAL_DEVICE_BUS_BEGIN());
        entity.setMEDICAL_DEVICE_BUS_END(detail.getMEDICAL_DEVICE_BUS_END());
        entity.setFOOD_BUS_LICENSE_SCOPE(detail.getFOOD_BUS_LICENSE_SCOPE());
        entity.setFOOD_BUS_LICENSE_BEGIN(detail.getFOOD_BUS_LICENSE_BEGIN());
        entity.setFOOD_BUS_LICENSE_END(detail.getFOOD_BUS_LICENSE_END());
        entity.setRECEIVING_BANK_NAME(detail.getRECEIVING_BANK_NAME());
        entity.setRECEIVING_BANK_ACCOUNT(detail.getRECEIVING_BANK_ACCOUNT());
        entity.setRECEIVING_BANK_NUM(detail.getRECEIVING_BANK_NUM());
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }

    private MDMS_O_PARTNER createMdmsOPartner(Long tenantNumId, Long dataSign, Long usrNumId, MDMS_BL_PARTNER detail) {
        MDMS_O_PARTNER entity = new MDMS_O_PARTNER();
        entity.setSERIES(Long.valueOf(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_PARTNER_SERIES)));
        entity.setTENANT_NUM_ID(tenantNumId.intValue());
        entity.setDATA_SIGN(dataSign.byteValue());
        entity.setPARTNER_NUM_ID(SeqUtil.getSeqAutoNextValue(tenantNumId, dataSign, SeqUtil.AUTO_MDMS_O_PARTNER_PARTNER_NUM_ID));
        entity.setPARTNER_ID(detail.getPARTNER_ID());
        entity.setPARTNER_NAME(detail.getPARTNER_NAME());
        entity.setUNIFIED_CREDIT_CODE(detail.getUNIFIED_CREDIT_CODE());
        entity.setPARTNER_CLASSIFY(detail.getPARTNER_CLASSIFY());
        entity.setBUSINESS_SCOPE(detail.getBUSINESS_SCOPE());
        entity.setSALE_ALLOW(detail.getSALE_ALLOW());
        entity.setPARTNER_TYPE(detail.getPARTNER_TYPE());
        entity.setPARTNER_ADR(detail.getPARTNER_ADR());
        entity.setLEGAL_PERSON(detail.getLEGAL_PERSON());
        entity.setREGISTER_ADR(detail.getREGISTER_ADR());
        entity.setREGISTER_DATE(detail.getREGISTER_DATE());
        entity.setREGISTER_CAPITAL(detail.getREGISTER_CAPITAL());
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        entity.setPRV_NUM_ID(detail.getPRV_NUM_ID());
        entity.setCITY_NUM_ID(detail.getCITY_NUM_ID());
        entity.setCITY_AREA_NUM_ID(detail.getCITY_AREA_NUM_ID());
        entity.setTOWN_NUM_ID(detail.getTOWN_NUM_ID());
        return entity;
    }

    private MDMS_O_SUPPLY createMdmsOSupply(Long tenantNumId, Long dataSign, Long usrNumId, MDMS_BL_SUPPLY detail) {
        MDMS_O_SUPPLY entity = new MDMS_O_SUPPLY();
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_SUPPLY_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setSUPPLY_NUM_ID(SeqUtil.getSeqAutoNextValue(tenantNumId, dataSign, SeqUtil.AUTO_MDMS_O_SUPPLY_SUPPLY_NUM_ID));
        entity.setSUPPLY_NAME(detail.getSUPPLY_NAME());
        entity.setPARTNER_ROLE_GROUP(detail.getPARTNER_ROLE_GROUP());
        entity.setSUPPLY_CLASSIFY(detail.getSUPPLY_CLASSIFY());
        entity.setMNEMONIC_CODE(detail.getMNEMONIC_CODE());
        entity.setFINANCE_NAME(detail.getFINANCE_NAME());
        entity.setFINANCE_TEL(detail.getFINANCE_TEL());
        entity.setTAX_ACCOUNT(detail.getTAX_ACCOUNT());
        entity.setADR(detail.getADR());
        entity.setSUPPLY_TYPE(detail.getSUPPLY_TYPE());
        entity.setBUSINESS_SCOPE(detail.getBUSINESS_SCOPE());
        entity.setPRODUCTION_SCOPE(detail.getPRODUCTION_SCOPE());
        entity.setBUSINESS_PERMIT(detail.getBUSINESS_PERMIT());
        entity.setUNIFIED_SOCIAL_CREDIT_CODE(detail.getUNIFIED_SOCIAL_CREDIT_CODE());
        entity.setREGISTER_PLACE(detail.getREGISTER_PLACE());
        entity.setREGISTER_DATE(detail.getREGISTER_DATE());
        entity.setABC_MARK(detail.getABC_MARK());
        entity.setBASIC_BANK_NAME(detail.getBASIC_BANK_NAME());
        entity.setBASIC_BANK_ACCOUNT(detail.getBASIC_BANK_ACCOUNT());
        entity.setBASIC_BANK_NUM(detail.getBASIC_BANK_NUM());
        entity.setSUPPLY_STATUS(detail.getSUPPLY_STATUS());
        entity.setGOVERNMENT_RELATED_SUPPLIER(detail.getGOVERNMENT_RELATED_SUPPLIER());
        entity.setPOTENTIAL_LICHONG_SUPPLIER(detail.getPOTENTIAL_LICHONG_SUPPLIER());
        entity.setGOVERNMENT_RELATED_SUPPLIER_REMARK(detail.getGOVERNMENT_RELATED_SUPPLIER_REMARK());
        entity.setPOTENTIAL_LICHONG_SUPPLIER_REMARK(detail.getPOTENTIAL_LICHONG_SUPPLIER_REMARK());
        entity.setREGISTERED_CAPITAL(detail.getREGISTERED_CAPITAL());
        entity.setPAID_IN_CAPITAL(detail.getPAID_IN_CAPITAL());
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }

    private MDMS_BL_PARTNER createMdmsBlPartner(Long tenantNumId, Long dataSign, Long usrNumId, String reservedNo, MdmsBlPartner detail) {
        MDMS_BL_PARTNER entity = new MDMS_BL_PARTNER();
        entity.setSERIES(Long.valueOf(SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_PARTNER_SERIES)));
        entity.setTENANT_NUM_ID(tenantNumId.intValue());
        entity.setDATA_SIGN(dataSign.byteValue());
        entity.setRESERVED_NO(reservedNo);
        entity.setPARTNER_ID(detail.getPartnerId());
        entity.setPARTNER_NAME(detail.getPartnerName());
        entity.setUNIFIED_CREDIT_CODE(detail.getUnifiedCreditCode());
        entity.setPARTNER_CLASSIFY(detail.getPartnerClassify());
        entity.setBUSINESS_SCOPE(detail.getBusinessScope());
        entity.setSALE_ALLOW(detail.getSaleAllow());
        entity.setPARTNER_TYPE(detail.getPartnerType());
        entity.setPARTNER_ADR(detail.getPartnerAdr());
        entity.setLEGAL_PERSON(detail.getLegalPerson());
        entity.setREGISTER_ADR(detail.getRegisterAdr());
        entity.setREGISTER_DATE(detail.getRegisterDate());
        entity.setREGISTER_CAPITAL(detail.getRegisterCapital());
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        entity.setCONTACTS_TEL(detail.getContactsTel());
        entity.setCONTACTS(detail.getContacts());
        entity.setPRV_NUM_ID(detail.getPrvNumId());
        entity.setCITY_NUM_ID(detail.getCityNumId());
        entity.setCITY_AREA_NUM_ID(detail.getCityAreaNumId());
        entity.setTOWN_NUM_ID(detail.getTownNumId());
        return entity;
    }

    private MDMS_BL_SUPPLY createMdmsBlSupply(Long tenantNumId, Long dataSign, Long usrNumId, String reservedNo, MdmsBlSupply detail) {
        MDMS_BL_SUPPLY entity = new MDMS_BL_SUPPLY();
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_SUPPLY_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setRESERVED_NO(reservedNo);
        entity.setSUPPLY_NAME(detail.getSupplyName());
        entity.setPARTNER_ROLE_GROUP(detail.getPartnerRoleGroup());
        entity.setSUPPLY_CLASSIFY(detail.getSupplyClassify());
        entity.setMNEMONIC_CODE(detail.getMnemonicCode());
        entity.setFINANCE_NAME(detail.getFinanceName());
        entity.setFINANCE_TEL(detail.getFinanceTel());
        entity.setFILE_NO(detail.getFileNo());
        entity.setADR(detail.getAdr());
        entity.setTAX_ACCOUNT(detail.getTaxAccount());
        entity.setSUPPLY_TYPE(detail.getSupplyType());
        entity.setBUSINESS_SCOPE(detail.getBusinessScope());
        entity.setPRODUCTION_SCOPE(detail.getProductionScope());
        entity.setBUSINESS_PERMIT(detail.getBusinessPermit());
        entity.setUNIFIED_SOCIAL_CREDIT_CODE(detail.getUnifiedSocialCreditCode());
        entity.setREGISTER_PLACE(detail.getRegisterPlace());
        entity.setREGISTER_DATE(detail.getRegisterDate());
        entity.setABC_MARK(detail.getAbcMark());
        entity.setBASIC_BANK_NUM(detail.getBasicBankNum());
        entity.setBASIC_BANK_NAME(detail.getBasicBankName());
        entity.setBASIC_BANK_ACCOUNT(detail.getBasicBankAccount());
        entity.setRECEIVING_BANK_NUM(detail.getReceivingBankNum());
        entity.setRECEIVING_BANK_NAME(detail.getReceivingBankName());
        entity.setRECEIVING_BANK_ACCOUNT(detail.getReceivingBankAccount());
        entity.setSUPPLY_STATUS(detail.getSupplyStatus());
        entity.setGOVERNMENT_RELATED_SUPPLIER(detail.getGovernmentRelatedSupplier());
        entity.setPOTENTIAL_LICHONG_SUPPLIER(detail.getPotentialLichongSupplier());
        entity.setGOVERNMENT_RELATED_SUPPLIER_REMARK(detail.getGovernmentRelatedSupplierRemark());
        entity.setPOTENTIAL_LICHONG_SUPPLIER_REMARK(detail.getPotentialLichongSupplierRemark());
        entity.setREGISTERED_CAPITAL(detail.getRegisteredCapital());
        entity.setPAID_IN_CAPITAL(detail.getPaidInCapital());
        entity.setCONTROL_SUBJECT(detail.getControlSubject());
        entity.setREMARK(detail.getRemark());
        entity.setMONEY_TYPE(detail.getMoneyType());
        entity.setPURCHASE_LEADER(detail.getPurchaseleader());
        entity.setPAYMENT_CONDITION(detail.getPaymentCondition());
        entity.setCLOSE_DAYS(detail.getCloseDays());
        entity.setFORBID_PURCHASE(detail.getForbidPurchase());
        entity.setFORBID_RETURN_FACTORY(detail.getForbidReturnFactory());
        entity.setFORBID_DISTRIBUTION(detail.getForbidDistribution());
        entity.setFORBID_RETURN_STORE(detail.getForbidReturnStore());
        entity.setFORBID_SALE(detail.getForbidSale());
        entity.setFORBID_PAYMENT(detail.getForbidPayment());
        entity.setPRE_DISTRIBUTION(detail.getPreDistribution());
        entity.setDIRECT_DELIVERY_FLAG(detail.getDirectDeliveryFlag());
        entity.setSNNUAL_ANNOUNCEMEN(detail.getSnnualAnnouncemen());
        entity.setSTORAGE_ADR(detail.getStorageAdr());
        entity.setCORPORAT_PROXY_STAT_IDCARD(detail.getCorporatProxyStatIdcard());
        entity.setCORPORAT_PROXY_IDCARD_BEGIN(detail.getCorporatProxyIdcardBegin());
        entity.setCORPORAT_PROXY_IDCARD_END(detail.getCorporatProxyIdcardEnd());
        entity.setSUB_SUPPLY_STATUS(detail.getSubSupplyStatus());
        entity.setCORPORAT(detail.getCorporat());
        entity.setCORPORAT_PROXY_NAME(detail.getCorporatProxyName());
        entity.setCORPORAT_PROXY_STAT_BEGIN(detail.getCorporatProxyStatBegin());
        entity.setCORPORAT_PROXY_STAT_END(detail.getCorporatProxyStatEnd());
        entity.setQUALITY_LEADER(detail.getQualityLeader());
        entity.setBUSINESS_CONTACT(detail.getBusinessContact());
        entity.setBUSINESS_CONTACT_TEL(detail.getBusinessContactTel());
        entity.setBUSINESS_CONTACT_IDCARD(detail.getBusinessContactIdcard());
        entity.setSPDA_RECORD_BEGIN(detail.getSpdaRecordBegin());
        entity.setSPDA_RECORD_END(detail.getSpdaRecordEnd());
        entity.setSPDA_RECORD_STAFF(detail.getSpdaRecordStaff());
        entity.setSPDA_RECORD_IDCARD(detail.getSpdaRecordIdcard());
        entity.setBUSINESS_LICENSE_BEGIN(detail.getBusinessLicenseBegin());
        entity.setBUSINESS_LICENSE_END(detail.getBusinessLicenseEnd());
        entity.setQUALITY_AGREEMENT_BEGIN(detail.getQualityAgreementBegin());
        entity.setQUALITY_AGREEMENT_END(detail.getQualityAgreementEnd());
        entity.setLICENCE_WARN_DAYS(detail.getLicenceWarnDays());
        entity.setREPLENISHER(detail.getReplenisher());
        entity.setDRUG_BUSINESS_LICENSE(detail.getDrugBusinessLicense());
        entity.setDRUG_BUSINESS_LICENSE_BEGIN(detail.getDrugBusinessLicenseBegin());
        entity.setDRUG_BUSINESS_LICENSE_END(detail.getDrugBusinessLicenseEnd());
        entity.setDRUG_PRODUCT_LICENSE(detail.getSpdaRecordIdcard());
        entity.setDRUG_PRODUCT_LICENSE_BEGIN(detail.getDrugProductLicenseBegin());
        entity.setDRUG_PRODUCT_LICENSE_END(detail.getDrugProductLicenseEnd());
        entity.setHYGIENE_LICENSE_SCOPE(detail.getHygieneLicenseScope());
        entity.setHYGIENE_LICENSE_BEGIN(detail.getHygieneLicenseBegin());
        entity.setHYGIENE_LICENSE_END(detail.getHygieneLicenseEnd());
        entity.setMEDICAL_DEVICE_PRO_LINCENSE(detail.getMedicalDeviceProLincense());
        entity.setMEDICAL_DEVICE_SCOPE(detail.getMedicalDeviceScope());
        entity.setMEDICAL_DEVICE_BEGIN(detail.getMedicalDeviceBegin());
        entity.setMEDICAL_DEVICE_END(detail.getMedicalDeviceEnd());
        entity.setMEDICAL_DEVICE_PRO_REC1(detail.getMedicalDeviceProRec1());
        entity.setMEDICAL_DEVICE_PRO_REC1_SCOPE(detail.getMedicalDeviceProRec1Scope());
        entity.setMEDICAL_DEVICE_PRO_REC2(detail.getMedicalDeviceProRec2());
        entity.setMEDICAL_DEVICE_PRO_REC2_SCOPE(detail.getMedicalDeviceProRec2Scope());
        entity.setMEDICAL_DEVICE_BUS_LINCENSE(detail.getMedicalDeviceBusLincense());
        entity.setMEDICAL_DEVICE_BUS_SCOPE(detail.getMedicalDeviceBusScope());
        entity.setMEDICAL_DEVICE_BUS_BEGIN(detail.getMedicalDeviceBusBegin());
        entity.setMEDICAL_DEVICE_BUS_END(detail.getMedicalDeviceBusEnd());
        entity.setFOOD_BUS_LICENSE(detail.getFoodBusLicense());
        entity.setFOOD_BUS_LICENSE_SCOPE(detail.getFoodBusLicenseScope());
        entity.setFOOD_BUS_LICENSE_BEGIN(detail.getFoodBusLicenseBegin());
        entity.setFOOD_BUS_LICENSE_END(detail.getFoodBusLicenseEnd());
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        entity.setOLD_SUPPLY_ID(detail.getOldSupplyId());
        return entity;
    }

    private MDMS_BL_PROCESS_MASTER_HDR createProcessHdr(Long tenantNumId, Long dataSign, Long usrNumId, ProcessHdr head) {
        MDMS_BL_PROCESS_MASTER_HDR managerHdr = new MDMS_BL_PROCESS_MASTER_HDR();
        managerHdr.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_BL_PROCESS_MASTER_HDR_SERIES));
        managerHdr.setTENANT_NUM_ID(tenantNumId);
        managerHdr.setDATA_SIGN(dataSign);
        managerHdr.setCORT_NUM_ID(head.getCortNumId());
        managerHdr.setPROCESS_TITLE(head.getProcessTitle());
        managerHdr.setTYPE_NUM_ID(head.getTypeNumId());
        managerHdr.setREMARK(head.getRemark());
        managerHdr.setDEGREE_URGENCY(head.getDegreeUrgency());
        managerHdr.setFILE_URL(head.getFileUrl());
        managerHdr.setCREATE_DTME(new Date());
        managerHdr.setLAST_UPDTME(new Date());
        managerHdr.setCREATE_USER_ID(usrNumId);
        managerHdr.setLAST_UPDATE_USER_ID(usrNumId);
        managerHdr.setCANCELSIGN("N");
        managerHdr.setSOURCE_TYPE(head.getSourceType());
        return managerHdr;
    }

    public void insertLog(Long tenantNumId, Long dataSign, Long userNumId, int billType, Long reservedNo, String operateContent) {
        MDM_OPERATE_LOG mdmOperateLog = new MDM_OPERATE_LOG();
        mdmOperateLog.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_SUB_UNIT_RECSTORAGE_SERIES));
        mdmOperateLog.setTENANT_NUM_ID(tenantNumId);
        mdmOperateLog.setDATA_SIGN(dataSign);
        mdmOperateLog.setRESERVED_NO(reservedNo);
        mdmOperateLog.setBILL_TYPE(billType);
        mdmOperateLog.setBILL_TYPE_NAME(BillTypeEnums.getEnums(billType));
        mdmOperateLog.setOPERATE_CONTENT(operateContent);
        mdmOperateLog.setCREATE_DTME(new Date());
        mdmOperateLog.setCREATE_USER_ID(userNumId);
        mdmOperateLog.setLAST_UPDTME(new Date());
        mdmOperateLog.setLAST_UPDATE_USER_ID(userNumId);
        mdmOperateLog.setCANCELSIGN("N");
        mdmOperateLogDao.insterMdmOperateLog(mdmOperateLog);
    }

    private ProcessOperateLog buildOperateLog(String cortNumId, String reservedNo, int typeNumId, String content,int sourceType, String businessId){
        ProcessOperateLog logs=new ProcessOperateLog();
        logs.setOperateContent(content);
        logs.setCortNumId(cortNumId);
        logs.setTypeNumId(typeNumId);
        logs.setReservedNo(reservedNo);
        logs.setSourceType(sourceType);
        logs.setBusinessId(businessId);
        return logs;
    }

    public void saveBlProcessOperatelog(Long tenantNumId, Long dataSign, Long userNumId,  List<ProcessOperateLog> logList) {
        BlProcessOperateLogSaveRequest request=new BlProcessOperateLogSaveRequest();
        request.setTenantNumId(tenantNumId);
        request.setDataSign(dataSign);
        request.setUserNumId(userNumId);
        request.setLogList(logList);
        mdTenantService.saveBlProcessOperateLog(request);
    }

    public MDMS_BL_MANAGER_HDR createMdmsBlManagerHdr(Long series, String reservedNo, ManagerCortSaveRequest request) {
        MDMS_BL_MANAGER_HDR mdms_bl_manager_hdr = new MDMS_BL_MANAGER_HDR();
        mdms_bl_manager_hdr.setSERIES(series);
        mdms_bl_manager_hdr.setTENANT_NUM_ID(request.getTenantNumId());
        mdms_bl_manager_hdr.setDATA_SIGN(request.getDataSign().byteValue());
        mdms_bl_manager_hdr.setRESERVED_NO(reservedNo);
        mdms_bl_manager_hdr.setRESERVED_NAME(reservedNo);
        mdms_bl_manager_hdr.setTYPE_NUM_ID((byte) ManagerHdrTypeNumEnums.COMPANY.getId());
        mdms_bl_manager_hdr.setSTATUS_NUM_ID(ManagerHdrStatusNumEnums.CREATE.getId());
        mdms_bl_manager_hdr.setREMARK(StringUtils.isNotBlank(request.getRemark()) ? request.getRemark() : "");
        mdms_bl_manager_hdr.setLAST_UPDTME(new Date());
        mdms_bl_manager_hdr.setLAST_UPDATE_USER_ID(request.getUserNumId());
        mdms_bl_manager_hdr.setCANCELSIGN("N");
        mdms_bl_manager_hdr.setCREATE_DTME(new Date());
        mdms_bl_manager_hdr.setCREATE_USER_ID(request.getUserNumId());
        return mdms_bl_manager_hdr;
    }

    public MDMS_BL_CORT createMdmsBlCort(String dtlSeries, String reservedNo, ManagerCortSaveRequest request) {
        MDMS_BL_CORT mdms_bl_CORT = new MDMS_BL_CORT();
        mdms_bl_CORT.setSERIES(Long.valueOf(dtlSeries));
        mdms_bl_CORT.setTENANT_NUM_ID(request.getTenantNumId().intValue());
        mdms_bl_CORT.setDATA_SIGN(request.getDataSign().byteValue());
        mdms_bl_CORT.setRESERVED_NO(reservedNo);
        mdms_bl_CORT.setCORT_ID(request.getCortId());
        mdms_bl_CORT.setCORT_NAME(request.getCortName());
        mdms_bl_CORT.setUNIFIED_SOCIAL_CREDIT_CODE(request.getUnifiedSocialCreditCode());
        mdms_bl_CORT.setTYPE_NUM_ID(request.getTypeNumId());
        mdms_bl_CORT.setLEGAL_REPRESENTATIVE(request.getLegalRepresentative());
        mdms_bl_CORT.setREGISTR_STATUS(request.getRegistrStatus());
        mdms_bl_CORT.setINCORPOR_DATE(request.getIncorporDate());
        mdms_bl_CORT.setREGISTERED_CAPITAL(request.getRegisteredCapital());
        mdms_bl_CORT.setPAID_IN_CAPITAL(request.getPaidInCapital());
        mdms_bl_CORT.setAPPROVAL_DATE(request.getApprovalDate());
        mdms_bl_CORT.setORG_CODE(request.getOrgCode());
        mdms_bl_CORT.setBUSINESS_REGISTR_NO(request.getBusinessRegistrNo());
        mdms_bl_CORT.setTAXPAYER_NO(request.getTaxpayerNo());
        mdms_bl_CORT.setCORT_TYPE(request.getCortType());
        mdms_bl_CORT.setBUSINESS_BEGIN_TERM(request.getBusinessBeginTerm());
        mdms_bl_CORT.setBUSINESS_END_TERM(request.getBusinessEndTerm());
        mdms_bl_CORT.setTAXPAYER_QUALIFICAT(request.getTaxpayerQualificat());
        mdms_bl_CORT.setINDUSTRY(request.getIndustry());
        mdms_bl_CORT.setREGION(request.getRegion());
        mdms_bl_CORT.setPRV_NUM_ID(request.getPrvNumId());
        mdms_bl_CORT.setCITY_NUM_ID(request.getCityNumId());
        mdms_bl_CORT.setCITY_AREA_NUM_ID(request.getCityAreaNumId());
        mdms_bl_CORT.setTOWN_NUM_ID(request.getTownNumId());
        mdms_bl_CORT.setREGISTR_AUTHORITY(request.getRegistrAuthority());
        mdms_bl_CORT.setOLD_CORT_NAME(request.getOldCortName());
        mdms_bl_CORT.setIMPORT_ENTERPRISE_CODE(request.getImportEnterpriseCode());
        mdms_bl_CORT.setREGISTR_ADR(request.getRegistrAdr());
        mdms_bl_CORT.setBUSINESS_SCOPE(request.getBusinessScope());
        mdms_bl_CORT.setPHONE(request.getPhone());
        mdms_bl_CORT.setEMAIL(request.getEmail());
        mdms_bl_CORT.setBRIEF_INTRODUCTION(request.getBriefIntroduction());
        mdms_bl_CORT.setBANK_NAME(StringUtils.isNotBlank(request.getBankName()) ? request.getBankName() : "");
        mdms_bl_CORT.setBANK_ACCOUNT(StringUtils.isNotBlank(request.getBankAccount()) ? request.getBankAccount() : "");
        mdms_bl_CORT.setLAST_UPDTME(new Date());
        mdms_bl_CORT.setLAST_UPDATE_USER_ID(request.getUserNumId());
        mdms_bl_CORT.setCANCELSIGN("N");
        mdms_bl_CORT.setCREATE_DTME(new Date());
        mdms_bl_CORT.setCREATE_USER_ID(request.getUserNumId());
        return mdms_bl_CORT;
    }

    private UnitGenerateRequest buildUnitReq(Long tenantNumId, Long dataSign, Long usrNumId, String cortNumId,String cortName){
        UnitGenerateRequest unitRequest = new UnitGenerateRequest();
        unitRequest.setCortNumId(cortNumId);
        unitRequest.setUnitName(cortName);
        unitRequest.setUnitNumId(cortNumId);
        unitRequest.setUnitType(UnitTypeEnum.UNIT_ORG.getTypeNumId());
        unitRequest.setSubType(UnitSubTypeEnum.CORT.getSubType());
        unitRequest.setUserNumId(usrNumId);
        unitRequest.setTenantNumId(tenantNumId);
        unitRequest.setDataSign(dataSign);
        return unitRequest;
    }

    private MDMS_O_CUSTOMER buildCustomer(Long tenantNumId, Long dataSign, Long usrNumId, CustomerGenerateRequest req){
        MDMS_O_CUSTOMER entity = new MDMS_O_CUSTOMER();
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_CUSTOMER_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCUSTOMER_NUM_ID(req.getCustomerNumId());

        entity.setCUSTOMER_NAME(req.getCustomerName());
        entity.setCUSTOMER_SIM_NAME(req.getCustomerName());
        entity.setMNEMONIC_CODE(req.getMnemonicCode());
        entity.setPOSTAL_CODE(req.getPostalCode());
        entity.setCONTACT_NUM(req.getContactNum());
        entity.setCUSTOMER_CLASSIFY(req.getCustomerClassify());
        entity.setADR(req.getAdr());

        entity.setDEFAULT_HARVEST_ADR(req.getDefaultHarvestAdr());
        entity.setBUSINESS_SCOPE(req.getBusinessScope());
        entity.setBUSINESS_PERMIT(req.getBusinessPermit());
        entity.setUNIFIED_SOCIAL_CREDIT_CODE(req.getUnifiedSocialCreditCode());
        entity.setCUSTOMER_STATUS(1);
        entity.setREGISTR_BEGIN_DATE(req.getRegistrBeginDate());
        entity.setREGISTR_END_DATE(req.getRegistrEndDate());
        entity.setREGISTER_PLACE(req.getRegisterPlace());

        entity.setTAX_ACCOUNT(req.getTaxAccount());
        entity.setPRV_NUM_ID(req.getPrvNumId());
        entity.setCITY_NUM_ID(req.getCityNumId());
        entity.setCITY_AREA_NUM_ID(req.getCityAreaNumId());
        entity.setTOWN_NUM_ID(req.getTownNumId());
        entity.setCREDIT_LINE(req.getCreditLine());
        entity.setREGISTERED_CAPITAL(req.getRegisteredCapital());
        entity.setPAID_IN_CAPITAL(req.getPaidInCapital());
        entity.setABC_MARK(req.getAbcMark());
        entity.setBASIC_BANK_NUM(req.getBasicBankNum());
        entity.setBASIC_BANK_NAME(req.getBasicBankName());
        entity.setBASIC_BANK_ACCOUNT(req.getBasicBankAccount());
        entity.setGOVERNMENT_RELATED_CUSTOMER(req.getGovernmentRelatedCustomer());
        entity.setPOTENTIAL_LICHONG_CUSTOMER(req.getPotentialLichongCustomer());
        entity.setGOVERNMENT_RELATED_CUSTOMER_REMARK(req.getGovernmentRelatedCustomerRemark());
        entity.setPOTENTIAL_LICHONG_CUSTOMER_REMARK(req.getPotentialLichongCustomerRemark());
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }

    private MDMS_O_SUPPLY buildSupply(Long tenantNumId, Long dataSign, Long usrNumId, SupplyGenerateRequest req){
        MDMS_O_SUPPLY entity = new MDMS_O_SUPPLY();
        entity.setSUPPLY_NUM_ID(req.getSupplyNumId());
        entity.setSUPPLY_NAME(req.getSupplyName());
        entity.setPARTNER_ROLE_GROUP(req.getPartnerRoleGroup());
        entity.setSUPPLY_CLASSIFY(req.getSupplyClassify());
        entity.setMNEMONIC_CODE(req.getMnemonicCode());
        entity.setFINANCE_NAME(req.getFinanceName());
        entity.setFINANCE_TEL(req.getFinanceTel());
        entity.setADR(req.getAdr());
        entity.setSUPPLY_TYPE(req.getSupplyType());
        entity.setBUSINESS_SCOPE(req.getBusinessScope());
        entity.setBUSINESS_PERMIT(req.getBusinessPermit());
        entity.setPRODUCTION_SCOPE(req.getProductionScope());
        entity.setUNIFIED_SOCIAL_CREDIT_CODE(req.getUnifiedSocialCreditCode());
        entity.setREGISTER_PLACE(req.getRegisterPlace());
        entity.setREGISTER_DATE(req.getRegisterDate());
        entity.setABC_MARK(req.getAbcMark());
        entity.setTAX_ACCOUNT(req.getTaxAccount());
        entity.setBASIC_BANK_NUM(req.getBasicBankNum());
        entity.setBASIC_BANK_NAME(req.getBasicBankName());
        entity.setBASIC_BANK_ACCOUNT(req.getBasicBankAccount());
        entity.setSUPPLY_STATUS(1);
        entity.setGOVERNMENT_RELATED_SUPPLIER(req.getGovernmentRelatedSupplier());
        entity.setPOTENTIAL_LICHONG_SUPPLIER(req.getPotentialLichongSupplier());
        entity.setGOVERNMENT_RELATED_SUPPLIER_REMARK(req.getGovernmentRelatedSupplierRemark());
        entity.setPOTENTIAL_LICHONG_SUPPLIER_REMARK(req.getPotentialLichongSupplierRemark());
        entity.setREGISTERED_CAPITAL(req.getRegisteredCapital());
        entity.setPAID_IN_CAPITAL(req.getPaidInCapital());
        entity.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_O_SUPPLY_SERIES));
        entity.setTENANT_NUM_ID(tenantNumId);
        entity.setDATA_SIGN(dataSign);
        entity.setCREATE_DTME(new Date());
        entity.setLAST_UPDTME(new Date());
        entity.setCREATE_USER_ID(usrNumId);
        entity.setLAST_UPDATE_USER_ID(usrNumId);
        entity.setCANCELSIGN("N");
        return entity;
    }
}