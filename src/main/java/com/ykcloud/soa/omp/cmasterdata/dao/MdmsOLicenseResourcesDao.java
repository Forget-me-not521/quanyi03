package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.LicenseResourceDetails;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_LICENSE_RESOURCES;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author stark.jiang
 * @Date 2021/09/06/19:45
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsOLicenseResourcesDao extends Dao<MDMS_O_LICENSE_RESOURCES> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public boolean checkLicenseExistByUnit(Long tenantNumId, Long dataSign, String series) {
        String sql = "SELECT count(1) from MDMS_O_LICENSE_RESOURCES WHERE tenant_num_id = ? AND data_sign = ? AND relation_id = ?  and cancelsign = 'N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, Integer.class) > 0 ? true : false;
    }


    public void deleteLicenseByUnitId(Long tenantNumId, Long dataSign, String series) {
        String sql = "delete from MDMS_O_LICENSE_RESOURCES where tenant_num_id=? and data_sign=? and relation_id=? and cancelsign='N' ";
        jdbcTemplate.update(sql, new Object[]{tenantNumId, dataSign, series});
    }

    public List<MDMS_O_LICENSE_RESOURCES> getProductCodeLicenseList(Long tenantNumId, Long dataSign, String blProductSeries) {
        String sql="select * from MDMS_O_LICENSE_RESOURCES where tenant_Num_id=? and data_sign=? and relation_id=?  and cancelsign='N'  ";
        return  jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign,blProductSeries}, new BeanPropertyRowMapper<>(MDMS_O_LICENSE_RESOURCES.class));
    }

    public List<LicenseResourceDetails> getProductLicenseList(Long tenantNumId, Long dataSign, String blProductSeries) {
        String sql="select * from MDMS_O_LICENSE_RESOURCES where tenant_Num_id=? and data_sign=? and relation_id=?  and cancelsign='N'  ";
        return  jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign,blProductSeries}, new BeanPropertyRowMapper<>(LicenseResourceDetails.class));
    }

    public List<MDMS_O_LICENSE_RESOURCES> getLicenseListByRelationId(Long tenantNumId, Long dataSign, String relationId) {
        String sql="select * from MDMS_O_LICENSE_RESOURCES where tenant_Num_id=? and data_sign=? and relation_id=?  and cancelsign='N'  ";
        return  jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign,relationId}, new BeanPropertyRowMapper<>(MDMS_O_LICENSE_RESOURCES.class));
    }
}
