package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.ykcloud.soa.erp.common.utils.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOWholesaleChain;
import com.ykcloud.soa.omp.cmasterdata.api.model.SupplyAnnualReportForQuery;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_C_TYPE;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_WHOLESALE_CHAIN;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import springfox.documentation.service.ApiListing;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName MdmsOWholesaleChainDao
 * @Description TODO
 * @Author User
 * @Date 2021/11/8
 * @Version 1.0
 */
@Repository
public class MdmsOWholesaleChainDao {
    @Resource(name = "platFormJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_WHOLESALE_CHAIN.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_WHOLESALE_CHAIN.class, ",");
    private static final String TABLE_NAME = "MDMS_O_WHOLESALE_CHAIN";

    /*public boolean checkExistCmd(String cmd) {
        String sql = "select count(*) from commoncalltable where cmd = ? ";
        int number = jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{cmd});
        return number == 0 ? false : true;
    }*/



    //根据批发公司编号查出连锁公司数量
    public Long getChainNumId(Long tenantNumId, Long dataSign, Long subUnitNumId){
        String sql = "SELECT COUNT(chain_num_id) FROM mdms_o_wholesale_chain WHERE wholesale_num_id=? and data_sign = ? and reserved_no = ? ";
        List<Long> longs = jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, subUnitNumId}, Long.class);
        Long ChainNumId = 0L;
        if (longs.get(0) != null) {
            ChainNumId = longs.get(0);
        }
        return ChainNumId;
    }


    //根据批发公司编号查询出批发公司的公司编号，公司名称,公司名称统一信用码，关联连锁公司数量，最后更新人，最后更新时间
    public List<MdmsOWholesaleChain> checkExistWholesaleNumId(String wholesaleNumId, Long tenantNumId, Long dataSign){
        String sql ="SELECT wholesale_num_id ,chain_name ,unified_social_credit_code ,last_update_user_id ,last_updtme FROM  mdms_o_wholesale_chain WHERE wholesale_num_id=? and data_sign = ? and reserved_no = ? ";
        List<MdmsOWholesaleChain> list = jdbcTemplate.query(sql, new Object[]{wholesaleNumId, tenantNumId, dataSign}, new BeanPropertyRowMapper<>(MdmsOWholesaleChain.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }


    //  点击维护关联公司后   根据批发公司编号查询出连锁公司的公司编号，公司名称，统一信用码，关联人，关联时间
    public List<MdmsOWholesaleChain> getWholesaleNumId(Long tenantNumId, Long dataSign, String wholesaleNumId) {
        String sql ="  SELECT chain_num_id,chain_name,unified_social_credit_code,create_user_id,create_dtme FROM mdms_o_wholesale_chain WHERE wholesale_num_id= ? and data_sign = ? and reserved_no = ?  ";

        List<MdmsOWholesaleChain> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, wholesaleNumId}, new BeanPropertyRowMapper<>(MdmsOWholesaleChain.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    // 点击添加页面显示的查询 根据连锁公司编号和连锁公司名称查询出连锁公司编号和连锁公司名称和统一信用码         MDMS_O_WHOLESALE_CHAIN
    public List<MdmsOWholesaleChain> checkExistChainNumId(Long tenantNumId, Long dataSign,String chainNumId, String chainName){
        String sql ="SELECT chain_num_id , chain_name, unified_social_credit_code FROM mdms_o_wholesale_chain WHERE  chain_num_id=? OR chain_name = ? and data_sign = ? and reserved_no = ? ";

        List<MdmsOWholesaleChain> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, chainNumId, chainName}, new BeanPropertyRowMapper<>(MdmsOWholesaleChain.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }


    //  添加连锁公司
    public void insertChainNumId(MDMS_O_WHOLESALE_CHAIN chain_num_id) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_C_TYPE.class, chain_num_id));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "  数据插入失败! ");
        }
    }

    //根据连锁公司编码chain_num_id删除
    public int deleteChainNumId(Long tenantNumId, Long dataSign,String chainNumId) {
        String sql = "DELETE  FROM mdms_o_cort WHERE cort_num_id= ? and data_sign = ? and reserved_no = ? ";
        return jdbcTemplate.update(sql, new Object[]{tenantNumId, dataSign, chainNumId});
    }
}
