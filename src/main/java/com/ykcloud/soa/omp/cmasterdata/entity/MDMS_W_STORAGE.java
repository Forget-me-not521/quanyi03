package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.util.Date;

@Data
public class MDMS_W_STORAGE {

    private String SERIES; // '行号',
    private Long TENANT_NUM_ID; // '租户ID',
    private Long DATA_SIGN; //'0: 正式  1：测试'
    private String CORT_NUM_ID; // '公司'
    private String SUB_UNIT_NUM_ID; // '门店'
    private String STORAGE_NUM_ID; //'逻辑仓库主键'
    private String STORAGE_ID; //'逻辑仓库简码'
    private String STORAGE_NAME; //'逻辑仓库中文名称'
    private String PHYSICAL_NUM_ID; //'物理仓库编码'
    private String EN_STORAGE_NAME; //'逻辑仓库英文名称'
    private Long BUSINESS_TYPE; // '业务类型:1:零售,2:b2b, 3:b2c'
    private Long MANAGE_TYPE; //'管理类型:1:合格品仓，2待处理仓'
    private Long CUY_NUM_ID; // '国家编号'
    private Date CREATE_DTME; //'创建时间'
    private Date LAST_UPDTME; //'最后更新时间'
    private Long CREATE_USER_ID; //'用户'
    private Long LAST_UPDATE_USER_ID; //'更新用户'
    private String CANCELSIGN; //'删除'
    private Long PRV_NUM_ID;
    private Long CITY_NUM_ID;
    private Long CITY_AREA_NUM_ID;
    private Long TOWN_NUM_ID;
    private String CONT_EMPE;
    private String POSTCODE;
    private String TELEPHONE;
    private String ADR;
    private String MAPLOCATION_X;
    private String MAPLOCATION_Y;
    private String RETURN_CONT_EMPE;
    private String RETURN_POSTCODE;
    private String RETURN_TELEPHONE;
    private String RETURN_ADR;
    private Long STORAGE_DEPT_NUM_ID;
    private String REMARK; //备注'
    private Long PRIORITY; //'优先级(数字越大，优先级越高)'

}
