package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_W_SHIPMENT;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class MdmsWShipmentDao extends Dao<MDMS_W_SHIPMENT> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }


    public String checkExistShipment(Long tenantNumId, String cortNumId, Long dataSign, String physicalNumId, String shipmentNumId) {
        StringBuilder sb = new StringBuilder("select series from MDMS_W_SHIPMENT");
        sb.append(" where tenant_num_id=? and cort_num_id=? and data_sign=? and physical_num_id=?");
        sb.append(" and shipment_num_id=? and cancelsign='N' ");
        return jdbcTemplate.queryForObject(sb.toString(),
                new Object[]{tenantNumId, cortNumId, dataSign, physicalNumId, shipmentNumId}, String.class);

    }

}
