package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MDMS_O_NOTICE implements Serializable {
    private Long SERIES;

    private Long TENANT_NUM_ID;

    private Integer DATA_SIGN;

    private String NOTICE_NUM_ID;

    private Integer NOTICE_TYPE;

    private String NOTICE_TITLE;

    private String NOTICE_CONTENT;

    private String SHOW_TYPE;

    private Date BEGIN_DATE;

    private Date END_DATE;

    private String NOTICE_REASON;

    private Integer NOTICE_STATUS;

    private String NOTICE_OBJECT;

    private Integer READ_TYPE;

    private Date CREATE_DTME;

    private Date LAST_UPDTME;

    private Long CREATE_USER_ID;

    private Long LAST_UPDATE_USER_ID;

    private String CANCELSIGN;


}
