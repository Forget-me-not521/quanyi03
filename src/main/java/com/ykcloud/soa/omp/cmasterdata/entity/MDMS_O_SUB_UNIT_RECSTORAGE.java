package com.ykcloud.soa.omp.cmasterdata.entity;

import javax.validation.constraints.NotNull;

/**
 * 仓库商品路由表
 *
 * @author CharlonWang
 */
public class MDMS_O_SUB_UNIT_RECSTORAGE extends BaseEntity {
    @NotNull(message = "门店不能为空")
    private Long SUB_UNIT_NUM_ID;
    @NotNull(message = "商品编号不能为空")
    private Long ITEM_NUM_ID;
    @NotNull(message = "仓库编号不能为空")
    private Long STORAGE_NUM_ID;

    private Long FDC_SIGN;
    private Long GROUP_NUM_ID;//仓库集（如果0，则不限制）
    private Long BILL_TYPE_ID;//业务模式 1.门店补货直通物流中心收货仓 2:总部门店收货仓

    public Long getFDC_SIGN() {
        return FDC_SIGN;
    }

    public void setFDC_SIGN(Long fDC_SIGN) {
        FDC_SIGN = fDC_SIGN;
    }

    public Long getGROUP_NUM_ID() {
        return GROUP_NUM_ID;
    }

    public void setGROUP_NUM_ID(Long gROUP_NUM_ID) {
        GROUP_NUM_ID = gROUP_NUM_ID;
    }

    public Long getBILL_TYPE_ID() {
        return BILL_TYPE_ID;
    }

    public void setBILL_TYPE_ID(Long bILL_TYPE_ID) {
        BILL_TYPE_ID = bILL_TYPE_ID;
    }

    public Long getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(Long sUB_UNIT_NUM_ID) {
        SUB_UNIT_NUM_ID = sUB_UNIT_NUM_ID;
    }

    public Long getITEM_NUM_ID() {
        return ITEM_NUM_ID;
    }

    public void setITEM_NUM_ID(Long iTEM_NUM_ID) {
        ITEM_NUM_ID = iTEM_NUM_ID;
    }

    public Long getSTORAGE_NUM_ID() {
        return STORAGE_NUM_ID;
    }

    public void setSTORAGE_NUM_ID(Long sTORAGE_NUM_ID) {
        STORAGE_NUM_ID = sTORAGE_NUM_ID;
    }

}
