package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

@Data
public class EX_ARC_CORT_SUB_UNIT_TML extends BaseEntity {
    private String CORT_NUM_ID;
    private String SUB_UNIT_NUM_ID ; // '门店编号(子单元编号)'
    private Long TML_CLIENT_ID = 0l; // '终端号'
    private String TML_CLIENT_NAME = ""; // '终端名称'
    private Long TYPE_NUM_ID = 0l; // '收银台类型（1：综合，2：童车）'
    private String DESCRIPTION = ""; // 'POS描述'
    private Long BILL_COUNT = 0l; // '制单单量'
    private Long FINISHED_COUNT = 0l; // '结单单量'
    private Long CANCEL_COUNT = 0l; // '取消单量'
    private Long CURRENT_USER_ID = 0l; // '当前用户'
    private String TML_ADRESS = ""; // 'POS机所在地址'
    private String TML_BARCODE = ""; // '终端设备条码'
    private String PRINT_SALES_DEVICE_NAME = ""; // '小票打印机'
    private Long PRINT_SALES_DEVICE_TYPE = 0l; // '打印机类别 1: 针打 2：热敏 '
    private Long CUSTOMER_DISPLAY_SIGN = 0l;// '客显标识 0: 无客显 1：有客显'
    private Long KEYBOARD_TYPE_NUM_ID = 0l;// '键盘种类'
    private String COMPUTER_NAME = "";// '电脑名'
    private String COMPUTER_IP = "";// '电脑ip'
    private String COMPUTER_MAC = "";// '电脑mac地址'
    private String PRINT_INVOICE_DEVICE_NAME = "";// '发票打印机'
    private String PRINT_TRAN_DEVICE_NAME = "";// '快递打印机'
    private String DEVICE_CODE = "";// '终端设备识别码'
    private Long STATUS_NUM_ID = 0l;// '设备状态 1:空闲；2：在用；3：维修；4：报废'
    private String ADD_TYPE = "";

}
