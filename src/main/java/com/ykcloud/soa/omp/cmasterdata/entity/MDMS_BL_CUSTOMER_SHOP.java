package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.util.Date;

/**
 * @Author stark.jiang
 * @Date 2021/10/09/16:52
 * @Description:
 * @Version 1.0
 */
@Data
public class MDMS_BL_CUSTOMER_SHOP extends BaseEntity {
    private String RESERVED_NO;//单号',
    private String CORT_NUM_ID;
    private String CUSTOMER_NUM_ID;//客户连锁公司 ',
    private String SHOP_NUM_ID;//门店名称',
    private String SHOP_NAME;//门店名称',
    private String SIM_SHOP_NAME;//门店简称',
    private int SHOP_CATEGORY;//门店所属分类(虚拟店、线下店、B2C、门诊)',
    private Long PRV_NUM_ID;//省',
    private Long CITY_NUM_ID;//市',
    private Long CITY_AREA_NUM_ID;//县',
    private Long TOWN_NUM_ID;//镇',
    private String MAPLOCATION_X;//经度',
    private String MAPLOCATION_Y;//维度',
    private String ADR;//地址',
    private Date SIGNING_DATE;//签约日期',
    private int IS_DUAL_CHANNEL;//是否双通道(1是，0否)',
    private int IS_CHINESE_MEDICINE_APTITUDE;//中药资质(是否有销售中药的资质1是，0否)',
    private int IS_COLD_CHAIN_APTITUDE;//冷链资质(是否有销售冷链的资质1是，0否)',
    private int STORE_DISTRIBUTION_CYCLE;//门店配送周期',
    private int SALE_CODE;//经营资质代码',
    private String STORE_HEAD_LEADER;//门店企业负责人',
    private String QUALITY_LEADER;//质量负责人',
    private String BUSINESS_LICENSE;//营业执照',
    private String UNIFIED_SOCIAL_CREDIT_CODE;//统一社会信用代码',
    private String DRUG_BUSINESS_LICENSE;//药品经营许可证',
    private String DRUG_BUSINESS_SCOPE;//药品经营许可范围',
    private String MEDICAL_DEVICE_BUS_LINCENSE;//医疗器械经营许可证',
    private String MEDICAL_DEVICE_BUS_SCOPE;//医疗器械经营范围',
    private Date MEDICAL_DEVICE_BUS_BEGIN;//医疗器械经营许可证有效期开始',
    private Date MEDICAL_DEVICE_BUS_END;//医疗器械经营许可证有效期结束',
    private String MEDICAL_DEVICE_PRO_REC2;//二类医疗器械经营备案',
    private String MEDICAL_DEVICE_PRO_REC2_SCOPE;//二类医疗器械经营备案范围',
    private String FOOD_BUS_LICENSE;//食品经营许可证',
    private String FOOD_BUS_LICENSE_SCOPE;//食品经营许可证范围',
    private Date FOOD_BUS_LICENSE_BEGIN;//食品经营许可证有效期开始',
    private Date FOOD_BUS_LICENSE_END;//食品经营许可证有效期结束',
    private int SALE_LIMIT_DAYS;//限销近效期天数',
    private String REMARK;//备注',
}
