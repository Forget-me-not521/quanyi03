package com.ykcloud.soa.omp.cmasterdata.enums;

public enum NoticeStatusEnums {
    INACTIVE(1, "未激活"),
    ACTIVATION(2, "激活"),
    FAILURE(3, "失效"),
    EXPIRE(4, "到期");

    private int id;
    private String value;

    NoticeStatusEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (NoticeStatusEnums item : NoticeStatusEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
