package com.ykcloud.soa.omp.cmasterdata.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MDMS_O_CORT implements Serializable {
    private Long SERIES;

    private Integer TENANT_NUM_ID;

    private Byte DATA_SIGN;

    private String PARENT_CORT_NUM_ID;

    private String CORT_NUM_ID;

    private String CORT_ID;

    private String CORT_NAME;

    private String UNIFIED_SOCIAL_CREDIT_CODE;

    private Integer TYPE_NUM_ID;

    private String LEGAL_REPRESENTATIVE;

    private Byte REGISTR_STATUS;

    private Date INCORPOR_DATE;

    private String REGISTERED_CAPITAL;

    private String PAID_IN_CAPITAL;

    private Date APPROVAL_DATE;

    private String ORG_CODE;

    private String BUSINESS_REGISTR_NO;

    private String TAXPAYER_NO;

    private Byte CORT_TYPE;

    private Date BUSINESS_BEGIN_TERM;

    private Date BUSINESS_END_TERM;

    private Byte TAXPAYER_QUALIFICAT;

    private Byte INDUSTRY;

    private Integer REGION;

    private Long PRV_NUM_ID;

    private Integer CITY_NUM_ID;

    private Integer CITY_AREA_NUM_ID;

    private Long TOWN_NUM_ID;

    private String REGISTR_AUTHORITY;

    private String OLD_CORT_NAME;

    private String IMPORT_ENTERPRISE_CODE;

    private String EN_CORT_NAME;

    private String REGISTR_ADR;

    private String BUSINESS_SCOPE;

    private String BANK_NAME;

    private String BANK_ACCOUNT;

    private String PHONE;

    private String EMAIL;

    private String BRIEF_INTRODUCTION;

    private Date CREATE_DTME;

    private Date LAST_UPDTME;

    private Long CREATE_USER_ID;

    private Long LAST_UPDATE_USER_ID;

    private String CANCELSIGN;

}
