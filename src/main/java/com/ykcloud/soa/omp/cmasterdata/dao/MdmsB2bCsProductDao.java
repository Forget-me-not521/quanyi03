package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsB2bCsProduct;
import com.ykcloud.soa.omp.cmasterdata.api.request.B2bCsProductQyRequest;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_B2B_CS_PRODUCT;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.MdDaoUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class MdmsB2bCsProductDao extends Dao<MDMS_B2B_CS_PRODUCT> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    private static final String TABLE_NAME = "MDMS_B2B_CS_PRODUCT";

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }


    public List<MDMS_B2B_CS_PRODUCT> getListPage(B2bCsProductQyRequest r) {
        StringBuilder sb = new StringBuilder();
        sb.append("select a.*,b.item_name,style_desc,c.customer_name from mdms_b2b_cs_product a");
        sb.append("left join mdms_bl_product_basic b on b.item_num_id=a.item_num_id and b.tenant_num_id=a.tenant_num_id and b.data_sign=a.data_sign ");
        sb.append("left join mdms_o_customer c on c.customer_num_id=a.customer_num_id and c.tenant_num_id=a.tenant_num_id and c.data_sign=a.data_sign ");
        sb.append("where b.cancelsign = 'N'");
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("a", "unit_num_id", r.getUnitNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("a", "channel_num_id", r.getChannelNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("a", "customer_num_id", r.getCustomerNumId()));
        sb.append(MdDaoUtil.DATE_COL_SQL("a", "create_dtme", r.getCreateDtme()));
        sb.append(MdDaoUtil.TENANT_NUM_ID_DATA_SIGN_SQL("a"));
        Integer start = (r.getPageNum() - 1) * r.getPageSize();
        sb.append(" limit ?,? ");
        List<MDMS_B2B_CS_PRODUCT> list = jdbcTemplate.query(sb.toString(), new Object[]{r.getUnitNumId(), r.getChannelNumId(), r.getCustomerNumId(), r.getCreateDtme(), r.getTenantNumId(), r.getDataSign(), start, r.getPageSize()}, new BeanPropertyRowMapper<>(MDMS_B2B_CS_PRODUCT.class));
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list;
    }

    public Integer getListPageCount(B2bCsProductQyRequest r) {
        String sb = "SELECT count(1) FROM " +
                TABLE_NAME + "where cancelsign='N' and tenant_num_id =? and data_sign = ?" +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("unit_num_id", r.getUnitNumId()) +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("channel_num_id", r.getChannelNumId()) +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("customer_num_id", r.getCustomerNumId()) +
                MdDaoUtil.DATE_COL_SQL("create_dtme", r.getCreateDtme());
        return jdbcTemplate.queryForObject(sb, new Object[]{r.getTenantNumId(), r.getDataSign(), r.getUnitNumId(), r.getChannelNumId(), r.getCustomerNumId(), r.getCreateDtme()}, Integer.class);
    }

    public MdmsB2bCsProduct getB2bCsProduct(Long tenantNumId, Long dataSign, Long series) {
        String sql = "select * from MDMS_B2B_CS_PRODUCT where TENANT_NUM_ID = ? and DATA_SIGN = ? and cancelsign='N' AND series = ?";
        MdmsB2bCsProduct product = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, new BeanPropertyRowMapper<>(MdmsB2bCsProduct.class));
        if (null == product) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "不存在系统单号为" + series + "的表单数据！");
        }
        return product;
    }

    public int deleteB2bCsProduct(Long tenantNumId, Long dataSign, Long series) {
        String sql = "DELETE from MDMS_B2B_CS_PRODUCT WHERE tenant_num_id = ? and data_sign = ? and series = ? ";
        return jdbcTemplate.update(sql, new Object[]{tenantNumId, dataSign, series});
    }
}
