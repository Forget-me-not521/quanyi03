package com.ykcloud.soa.omp.cmasterdata.util;

import com.gb.soa.omp.ccommon.util.Assert;
import org.apache.commons.collections.FastHashMap;
import org.springframework.util.ClassUtils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author: fred.zhao
 * @Description:
 * @Date: Created in 20:53:2018/5/11
 */
public class UnderlineToCamelUtil {


    /**
     * The cache of PropertyDescriptor arrays for beans we have already
     * introspected, keyed by the java.lang.Class of this object.
     */
    private static FastHashMap descriptorsCache = null;
    private static FastHashMap mappedDescriptorsCache = null;

    private static Set notWritableType = null;

    static {
        descriptorsCache = new FastHashMap();
        descriptorsCache.setFast(true);
        mappedDescriptorsCache = new FastHashMap();
        mappedDescriptorsCache.setFast(true);

        registerNotWritableType();
    }

    private static final void registerNotWritableType() {
        notWritableType = new HashSet();
        // collection and Map Object are not writable
        notWritableType.add(Collection.class);
        notWritableType.add(Map.class);
    }

    /**
     * 下划线转驼峰法
     *
     * @param line       源字符串
     * @param smallCamel 大小驼峰,是否为小驼峰
     * @return 转换后的字符串
     */
    public static String underline2Camel(String line, boolean smallCamel) {
        if (line == null || "".equals(line)) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        Pattern pattern = Pattern.compile("([A-Za-z\\d]+)(_)?");
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            String word = matcher.group();
            sb.append(smallCamel && matcher.start() == 0 ? Character.toLowerCase(word.charAt(0)) : Character
                    .toUpperCase(word.charAt(0)));
            int index = word.lastIndexOf('_');
            if (index > 0) {
                sb.append(word.substring(1, index).toLowerCase());
            } else {
                sb.append(word.substring(1).toLowerCase());
            }
        }
        return sb.toString();
    }

    /**
     * 驼峰法转下划线
     *
     * @param line 源字符串
     * @return 转换后的字符串
     */
    public static String camel2Underline(String line) {
        if (line == null || "".equals(line)) {
            return "";
        }
        line = String.valueOf(line.charAt(0)).toUpperCase().concat(line.substring(1));
        StringBuffer sb = new StringBuffer();
        Pattern pattern = Pattern.compile("[A-Z]([a-z\\d]+)?");
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            String word = matcher.group();
            sb.append(word.toUpperCase());
            sb.append(matcher.end() == line.length() ? "" : "_");
        }
        return sb.toString();
    }

    /**
     * 拷贝下滑线entity属性至驼峰属性model中
     *
     * @param source
     * @param target
     */
    public static void copyYkEntityToModel(Object entity, Object model) {
        Assert.notNull(entity, "Source must not be null");
        Assert.notNull(model, "Target must not be null");

        Class<?> targetClass = model.getClass();
        PropertyDescriptor[] propertyDescriptors = getPropertyDescriptors(targetClass);
        for (PropertyDescriptor targetPd : propertyDescriptors) {
            Method writeMethod = targetPd.getWriteMethod();
            if (writeMethod != null) {
                PropertyDescriptor sourcePd = getPropertyDescriptor(entity, camel2Underline(targetPd
                        .getName()));
                if (sourcePd != null) {
                    Method readMethod = sourcePd.getReadMethod();
                    if (readMethod != null && ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                        try {
                            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                readMethod.setAccessible(true);
                            }
                            Object value = readMethod.invoke(entity);
                            if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                writeMethod.setAccessible(true);
                            }
                            writeMethod.invoke(model, value);
                        } catch (Throwable ex) {
                            throw new RuntimeException(
                                    "Could not copy property '" + targetPd.getName() + "' from source to target", ex);
                        }
                    }
                }

            }
        }
    }

    /**
     * 拷贝驼峰属性model至下滑线entity属性中
     *
     * @param source
     * @param target
     */
    public static void copyYkModelToEntity(Object model, Object entity) {
        Assert.notNull(entity, "Source must not be null");
        Assert.notNull(model, "Target must not be null");

        Class<?> targetClass = entity.getClass();
        PropertyDescriptor[] propertyDescriptors = getPropertyDescriptors(targetClass);
        for (PropertyDescriptor targetPd : propertyDescriptors) {
            Method writeMethod = targetPd.getWriteMethod();
            if (writeMethod != null) {
                PropertyDescriptor sourcePd = getPropertyDescriptor(model, underline2Camel(targetPd
                        .getName(), true));
                if (sourcePd != null) {
                    Method readMethod = sourcePd.getReadMethod();
                    if (readMethod != null && ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                        try {
                            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                readMethod.setAccessible(true);
                            }
                            Object value = readMethod.invoke(model);
                            if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                writeMethod.setAccessible(true);
                            }
                            writeMethod.invoke(entity, value);
                        } catch (Throwable ex) {
                            throw new RuntimeException(
                                    "Could not copy property '" + targetPd.getName() + "' from source to target", ex);
                        }
                    }
                }

            }
        }
    }

    private static PropertyDescriptor[] getPropertyDescriptors(Object bean) {
        Assert.notNull(bean, "No bean specified");
        return (getPropertyDescriptors(bean.getClass()));
    }

    private static PropertyDescriptor[] getPropertyDescriptors(Class beanClass) {
        Assert.notNull(beanClass, "No bean class specified");

        // Look up any cached descriptors for this bean class
        PropertyDescriptor descriptors[] = null;
        descriptors = (PropertyDescriptor[]) descriptorsCache.get(beanClass);
        if (descriptors != null) {
            return descriptors;
        }

        // Introspect the bean and cache the generated descriptors
        BeanInfo beanInfo = null;
        try {
            beanInfo = Introspector.getBeanInfo(beanClass);
        } catch (IntrospectionException e) {
            return new PropertyDescriptor[0];
        }
        descriptors = beanInfo.getPropertyDescriptors();
        if (descriptors == null) {
            descriptors = new PropertyDescriptor[0];
        }
        descriptorsCache.put(beanClass, descriptors);
        return descriptors;
    }

    private static PropertyDescriptor getPropertyDescriptor(Object bean,
                                                            String name) {
        Assert.notNull(bean, "No bean specified");
        Assert.notNull(name, "No name specified");

        PropertyDescriptor descriptors[] = getPropertyDescriptors(bean);
        if (descriptors != null) {
            for (PropertyDescriptor descriptor : descriptors) {
                if (name.equals(descriptor.getName())) {
                    return descriptor;
                }
            }
        }
        return null;
    }


    public static void main(String[] args) {
        String line = "I_HAVE_AN_IPANG3_PIG";
        String camel = underline2Camel(line, true);
        System.out.println(camel);
        System.out.println(camel2Underline(camel));
    }
}
