package com.ykcloud.soa.omp.cmasterdata.dao;

import java.util.Arrays;

import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.entity.COMMON_QUERY;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;

@Repository
public class CommonQueryDao {
    @Resource(name = "platFormJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(COMMON_QUERY.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(COMMON_QUERY.class, ",");
    private static final String TABLE_NAME = "COMMON_QUERY";

    public String checkExistsqlId(String sqlId, Long tenantNumId, Long dataSign) {
        String sql = "select sql_id from common_query where sql_id = ? and tenant_num_id=? and data_sign=? and cancelsign ='N' and cancel_sign ='N'";
        String oldSqlId = jdbcTemplate.queryForObject(sql, String.class, new Object[]{sqlId, tenantNumId, dataSign});
        return oldSqlId;
    }

    public String checkExistsqlName(String sqlName, Long tenantNumId, Long dataSign) {
        String sql = "select sql_name from common_query where sql_name = ? and tenant_num_id=? and data_sign=? and cancelsign ='N' and cancel_sign ='N'";
        String OldsqlName = jdbcTemplate.queryForObject(sql, String.class, new Object[]{sqlName, tenantNumId, dataSign});
        return OldsqlName;
    }

    public int insertCommonQuery(COMMON_QUERY query) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into ");
        sb.append(TABLE_NAME);
        sb.append(" ( ");
        sb.append(SQL_COLS);
        sb.append(") values (");
        sb.append(WILDCARDS);
        sb.append(")");
        return jdbcTemplate.update(sb.toString(), EntityFieldUtil.fieldSplitValue(COMMON_QUERY.class, query));
    }

    public COMMON_QUERY checkExistQueryBySeries(String series, Long tenantNumId, Long dataSign) {
        String sql = "select * from common_query where series = ? and tenant_num_id=? and data_sign=? and cancelsign ='N' and cancel_sign ='N'";
        COMMON_QUERY oldQuery = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(COMMON_QUERY.class), new Object[]{series, tenantNumId, dataSign});
        return oldQuery;
    }

    public int updateQueryDao(String sqlName, String sqlId, String sqlContent, String paramContent, String jdbcName,
                              String dbType, String annotatePrefix, String subSqlId, String noDataException, String methodName,
                              Long cacheLiveTime, String returnHandleContent, Long dbleOpTime, String remark, Long userNumId,
                              String series, Long tenantNumId, Long dataSign) {
        StringBuilder sb = new StringBuilder();
        sb.append("update common_query set ");
        sb.append("sql_name = ?, sql_id = ?,sql_content = ?,param_content =?,jdbc_name =?,");
        sb.append("db_type = ?,annotate_prefix = ?, sub_sql_id = ?, no_data_exception = ?,method_name =?,");
        sb.append("cache_live_time = ?,return_handle_content =?,_dble_op_time = ?,remark = ?,");
        sb.append("last_update_user_id =?,last_updtme = now ()");
        sb.append("where series =? and tenant_num_id = ? and data_sign =? and cancelsign = 'N' and cancel_sign = 'N' ");

        return jdbcTemplate.update(sb.toString(), new Object[]{sqlName, sqlId, sqlContent, paramContent, jdbcName,
                dbType, annotatePrefix, subSqlId, noDataException, methodName,
                cacheLiveTime, returnHandleContent, dbleOpTime, remark, userNumId,
                series, tenantNumId, dataSign});
    }

    public int deleteCommonQueryByseries(Long userNumId, String series, Long tenantNumId, Long dataSign) {
        String sql = "update common_query set cancelsign = 'Y',cancel_sign = 'Y' ,last_updtme= now() ,last_update_user_id= ? where series = ? and tenant_num_id = ? and data_sign =? and cancelsign = 'N' and cancel_sign = 'N' ";
        return jdbcTemplate.update(sql, new Object[]{userNumId, series, tenantNumId, dataSign});
    }


}
