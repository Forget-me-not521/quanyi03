package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.erp.common.utils.DaoUtil;
import com.ykcloud.soa.omp.cmasterdata.api.model.Storage;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_W_PHYSICAL;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_W_STORAGE;
import com.ykcloud.soa.omp.cmasterdata.service.model.StorageDept;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ykcloud.soa.omp.cmasterdata.util.Constant.SUB_SYSTEM;

@Repository
public class MdmsWStorageDao  extends Dao<MDMS_W_STORAGE> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_W_STORAGE.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_W_STORAGE.class, ",");
    private static final String TABLE_NAME = "MDMS_W_STORAGE";

    public Long getStorageNumIdBySubUnitNumId(Long tenantNumId, Long dataSign, Long subUnitNumId) {
        String sql = "select storage_num_id from mdms_w_storage "
                + "where tenant_num_id=? and data_sign=? and sub_unit_num_id=? "
                + "and storage_dept_num_id=4 and pur_sign=1 and cancelsign='N'";
        List<Long> storageNumIds = jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, subUnitNumId}, Long.class);
        if (storageNumIds.isEmpty()) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据门店未查到委外仓库编号!门店编号 :" + subUnitNumId);
        }
        if (storageNumIds.size() > 1) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据门店查到多笔委外仓库编号!门店编号 :" + subUnitNumId);
        }
        return storageNumIds.get(0);
    }


    public Long getStorageNumIdBySubUnitNumIdAndPhysicalId(Long tenantNumId, Long dataSign, Long subUnitNumId, Long physicalId) {
        String sql = "select storage_num_id from mdms_w_storage "
                + "where tenant_num_id=? and data_sign=? and sub_unit_num_id=? and physical_num_id=?  and cancelsign = 'N'";

        List<Long> storageNumIds = jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, subUnitNumId, physicalId}, Long.class);
        if (storageNumIds.isEmpty()) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据门店未查到委外仓库编号!门店编号 :" + subUnitNumId);
        }

        if (storageNumIds.size() > 1) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据门店查到多笔委外仓库编号!门店编号 :" + subUnitNumId);
        }

        return storageNumIds.get(0);
    }


    public MDMS_W_STORAGE selectMdmsWStorage(Long tenantNumId, Long dataSign, String StorageId) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select ");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(" from ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" where TENANT_NUM_ID=? and DATA_SIGN=? and STORAGE_NUM_ID=?");
        return jdbcTemplate.queryForObject(stringBuffer.toString(), new Object[]{tenantNumId, dataSign, StorageId}, new BeanPropertyRowMapper<>(MDMS_W_STORAGE.class));
    }

    public List<MDMS_W_STORAGE> getStorageByphysicalNumId(Long tenantNumId, Long dataSign, Long physicalNumId) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select ");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(" from ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" where TENANT_NUM_ID = ? AND DATA_SIGN = ? and  PHYSICAL_NUM_ID = ? ");
        return jdbcTemplate.query(stringBuffer.toString(), new Object[]{tenantNumId, dataSign, physicalNumId}, new BeanPropertyRowMapper<>(MDMS_W_STORAGE.class));
    }

    public int insertEntity(MDMS_W_STORAGE mdms_w_storage) {
        mdms_w_storage.setSERIES(SeqUtil.getSeqNextValue(SeqUtil.MDMS_W_STORAGE_SERIES));
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_W_STORAGE.class, mdms_w_storage));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "  数据插入失败! ");
        }
        return i;
    }

    //获取逻辑仓id
    public List<Long> getStorageIds(Long tenantNumId, Long dataSign) {
        String sql = " select g.storage_num_id from mdms_w_storage g  " +
                "inner join mdms_o_sub_unit md on g.sub_unit_num_id = md.sub_unit_num_id " +
                "where md.TENANT_NUM_ID=? and md.DATA_SIGN=? and md.sub_unit_type in (0,21) ";
        return jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign}, Long.class);
    }

    //获取逻辑仓NumId
    public MDMS_W_STORAGE getStorageNumId(Long tenantNumId, Long dataSign, String storageId) {
        String sql = "select storage_num_id from mdms_w_storage " +
                "where  TENANT_NUM_ID=? and DATA_SIGN=? and storage_id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, storageId}, new BeanPropertyRowMapper<>(MDMS_W_STORAGE.class));
    }

    public MDMS_W_STORAGE getStorageBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "select *  from mdms_w_storage " +
                "where  TENANT_NUM_ID=? and DATA_SIGN=? and series=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, new BeanPropertyRowMapper<>(MDMS_W_STORAGE.class));
    }


    /**
     * 根据门店+逻辑仓获取仓库分组
     *
     * @author tz.x
     * @date 2018年7月7日上午11:47:13
     */
    public StorageDept getStorageGroupNumId(Long tenantNumId, Long dataSign, Long storageNumId) {
        String sql = "select storage_dept_num_id, group_num_id from mdms_w_storage "
                + "where tenant_num_id = ? and data_sign = ? and storage_num_id = ? "
                + "and cancelsign = 'N' ";
        StorageDept storageDept = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, storageNumId},
                new BeanPropertyRowMapper<>(StorageDept.class));
        if (storageDept == null) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "获取逻辑仓信息失败,逻辑仓编号：" + storageNumId);
        }
        return storageDept;
    }

    /**
     * 根据storageNumId获取逻辑仓名字
     *
     * @author CharlonWang
     */
    public Storage getStorageByNumId(Long tenantNumId, Long dataSign, Long subUnitNumId,
                                     Long storageNumId, Long physicalNumId) {
        String sql = "select physical_num_id,storage_num_id,storage_name from MDMS_W_STORAGE " +
                " where TENANT_NUM_ID = ? AND DATA_SIGN = ?  and sub_unit_num_id = ? AND storage_num_id = ? and physical_num_id = ? and cancelsign = 'N'";
        Storage storage = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, subUnitNumId,
                storageNumId, physicalNumId}, new BeanPropertyRowMapper<Storage>(Storage.class));
        if (storage == null) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "获取逻辑仓信息失败,门店编号：" + subUnitNumId + ",逻辑仓编号：" + storageNumId);
        }
        return storage;
    }

    public Long getStorageByNumIdByStoregeName(Long tenantNumId, Long dataSign, String storageName) {
        String sql = " select storage_num_id from MDMS_W_STORAGE where TENANT_NUM_ID = ? AND DATA_SIGN = ?   AND storage_name = ? and cancelsign = 'N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, storageName}, Long.class);
    }

    public Long getSubUnitNumIdBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "select sub_unit_num_id from MDMS_W_STORAGE where tenant_num_id = ? and data_sign = ? and series = ? and cancelsign = 'N' ";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, series}, Long.class);
    }

    public int deleteRecordBySeries(Long tenantNumId, Long dataSign, Long series) {
        String sql = "delete  from MDMS_W_STORAGE where tenant_num_id=? and data_sign=? and series=? and cancelsign = 'N'";
        return jdbcTemplate.update(sql, new Object[]{tenantNumId, dataSign, series});
    }

    public int deleteRecordByPhysicalNumId(Long tenantNumId, Long dataSign, Long physicalNumId) {
        String sql = "delete  from MDMS_W_STORAGE where tenant_num_id=? and data_sign=? and physical_num_id=? and cancelsign = 'N'";
        return jdbcTemplate.update(sql, new Object[]{tenantNumId, dataSign, physicalNumId});
    }

    public boolean checkRecordIsExistByStorageNumId(Long tenantNumId, Long dataSign, Long storageNumId) {
        String sql = "select count(1) from MDMS_W_STORAGE where tenant_num_id=? and data_sign=? and storage_num_id=? and cancelsign = 'N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, storageNumId}, Integer.class) == 0 ? false : true;
    }

    public void updateEntityByStorageNumId(Long tenantNumId, Long dataSign, Long storageNumId, MDMS_W_STORAGE mdmsWStorage) {
        String cols = EntityFieldUtil.wildCardSplitUpdate(mdmsWStorage, ",");
        List<Object> list = new ArrayList<>(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_W_STORAGE.class, mdmsWStorage)));
        list.add(tenantNumId);
        list.add(dataSign);
        list.add(storageNumId);
        StringBuffer sb = new StringBuffer();
        sb.append("update mdms_w_storage set ");
        sb.append(cols);
        sb.append(" where tenant_num_id=? and data_sign=? and storage_num_id=? and cancelsign = 'N'");
        int row = jdbcTemplate.update(sb.toString(), list.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    String.format("更新逻辑仓资料失败!逻辑仓编码:%d", storageNumId));
        }
    }


    public int updateEntityByStorageId(String series, MDMS_W_STORAGE mdmsWStorage,String subUnitNumId,String cortNumId) {
        String cols = EntityFieldUtil.wildCardSplitUpdate(mdmsWStorage, ",");
        List<Object> list = new ArrayList<>(Arrays.asList(EntityFieldUtil.fieldSplitValueUpdate(MDMS_W_STORAGE.class, mdmsWStorage)));
        list.add(series);
        list.add(cortNumId);
        list.add(subUnitNumId);
        StringBuffer sb = new StringBuffer();
        sb.append("update mdms_w_storage set ");
        sb.append(cols);
        sb.append(" where  series=? and cancelsign = 'N' and cort_num_id=?  and  sub_unit_num_id=? ");
        int row = jdbcTemplate.update(sb.toString(), list.toArray());
        if (row <= 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001,
                    String.format("更新逻辑仓资料失败!逻辑仓行号:%d", series));
        }
        return row;
    }

    public Long getSeriesByStorageNumId(Long tenantNumId, Long dataSign, Long storageNumId) {
        String sql = "select series from mdms_w_storage where tenant_num_id=? and data_sign=? and storage_num_id=? and cancelsign = 'N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, storageNumId}, Long.class);
    }

    public void insertEntityAllFeild(MDMS_W_STORAGE mdmsWStorage) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("insert into ");
        stringBuffer.append(TABLE_NAME);
        stringBuffer.append(" (");
        stringBuffer.append(SQL_COLS);
        stringBuffer.append(") values (");
        stringBuffer.append(WILDCARDS);
        stringBuffer.append(")");
        int i = jdbcTemplate.update(stringBuffer.toString(), EntityFieldUtil.fieldSplitValue(MDMS_W_STORAGE.class, mdmsWStorage));
        if (i <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "  数据插入失败! ");
        }
    }

    public boolean checkRecordIsExistByPhysicalNumId(Long tenantNumId, Long dataSign, Long physicalNumId) {
        String sql = "select count(1) from mdms_w_storage where tenant_num_id=? and data_sign=? and physical_num_id=? and cancelsign = 'N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, physicalNumId}, Integer.class) == 0 ? false : true;
    }

    /**
     * 根据门店编号、逻辑仓编号、租户、标识查询物理仓编号
     */
    public Long getMdmsWPhysicalBySubUnitNumIdAndStorageNumId(Long tenantNumId, Long dataSign, Long subUnitNumId, Long storageNumId) {
        String sql = "SELECT physical_num_id FROM mdms_w_storage where sub_unit_num_id=? and storage_num_id=? and tenant_num_id=? and data_sign=? and cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{subUnitNumId, storageNumId, tenantNumId, dataSign}, Long.class);

    }

    public List<Long> getPurchaseStorageNumIds(Long tenantNumId, Long dataSign, Long subUnitNumId) {
        String sql = "select storage_num_id from mdms_w_storage where tenant_num_id=? and data_sign=? and sub_unit_num_id=? and pur_sign = 1 and cancelsign='N'";
        List<Long> purchaseStorageNumIds = jdbcTemplate.queryForList(sql, new Object[]{tenantNumId, dataSign, subUnitNumId}, Long.class);
        if (purchaseStorageNumIds.isEmpty()) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据门店未查到的默认采购逻辑仓!门店编号 :" + subUnitNumId);
        }
        return purchaseStorageNumIds;
    }


    public MDMS_W_STORAGE getEntity(Long tenantNumId, Long dataSign, String cortNumId, String subUnitNumId, String physicalNumId,
                              String storageNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select ");
        sb.append(SQL_COLS);
        sb.append(" from ");
        sb.append(TABLE_NAME);
        sb.append(" where tenant_num_id=? and data_sign=? and cort_num_id=? " +
                " and SUB_UNIT_NUM_ID=? and physical_num_id=? and storage_num_id = ? and cancelsign='N'  ");
        return this.get(sb.toString(),
                new Object[]{tenantNumId, dataSign, cortNumId, subUnitNumId, physicalNumId, storageNumId});

    }

    public String checkExistSeries(Long tenantNumId, Long dataSign,
                                    String storageNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select series from MDMS_W_STORAGE");
        sb.append(" where tenant_num_id=? and data_sign=? and storage_num_id = ? and cancelsign='N' ");
        return jdbcTemplate.queryForObject(sb.toString(),
                new Object[]{tenantNumId, dataSign, storageNumId}, String.class);

    }

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public String getPhysicalByStorage(Long tenantNumId, Long dataSign, String cortNumId,
                                       String subUnitNumId, String storageNumId) {
        StringBuilder sb = new StringBuilder("SELECT physical_num_id from mdms_w_storage WHERE tenant_num_id = ?");
        sb.append("  and data_sign = ? and cort_num_id = ? and sub_unit_num_id = ?");
        sb.append(" and storage_num_id = ? and cancelsign = 'N'");
        String physicalNumId = jdbcTemplate.queryForObject(sb.toString(),
                new Object[]{tenantNumId, dataSign, cortNumId, subUnitNumId, storageNumId}, String.class);
        if (StringUtils.isBlank(physicalNumId)) {
            throw new DatabaseOperateException(SUB_SYSTEM, ExceptionType.DOE35001,
                    "根据仓库编号获取大仓编号失败!仓库编号 :" + storageNumId);
        }
        return physicalNumId;
    }


    public boolean checkStorageExist(Long tenantNumId, Long dataSign, String cortNumId,String storageNumId) {
        String sql = "select count(1) from MDMS_W_STORAGE where tenant_num_id=? and data_sign=? and cort_num_id=? and storage_num_id=? and cancelsign = 'N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, cortNumId,storageNumId}, Integer.class) > 0 ? true : false;
    }
}
