package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;

public class MDMS_W_PHYSICAL {
    private String SERIES;
    private Long TENANT_NUM_ID;
    private Long DATA_SIGN;
    private String PHYSICAL_NUM_ID;
    private String PHYSICAL_ID;
    private String PHYSICAL_NAME;
    private String EN_STK_NAME;
    private String CORT_NUM_ID;
    private String POSTAL;
    private String SUB_UNIT_NUM_ID;
    private Long STORAGE_DEPT_NUM_ID;
    private Long PUR_SIGN;
    private Long CUY_NUM_ID;
    private Long PRV_NUM_ID;
    private Long CITY_NUM_ID;
    private Long CITY_AREA_NUM_ID;
    private Long TOWN_NUM_ID;
    private String AREA_ADR;
    private String EN_AREA_ADR;
    private String CONT_EMPE;
    private String TELEPHONE;
    private String FAX;
    private String MOBILENUM;
    private String MAILADR;
    private Long IS_INDEPENDENT_ACCOUNTING;
    private Long IS_PROCESSING_STORAGE;
    private String FNC;
    private Date CREATE_DTME;
    private Date LAST_UPDTME;
    private Long CREATE_USER_ID;
    private Long LAST_UPDATE_USER_ID;
    private String CANCELSIGN;
//    private String INSERTDATA;
//    private String UPDATEDATA;
//    private String ALREADYSEND;
//    private String SENDDATA;
    private Integer VIRTUAL_SIGN;

    private Long enable_sign;

    private String REMARK;

    public Integer getVIRTUAL_SIGN() {
        return VIRTUAL_SIGN;
    }

    public void setVIRTUAL_SIGN(Integer VIRTUAL_SIGN) {
        this.VIRTUAL_SIGN = VIRTUAL_SIGN;
    }

    public String getCORT_NUM_ID() {
        return CORT_NUM_ID;
    }

    public void setCORT_NUM_ID(String CORT_NUM_ID) {
        this.CORT_NUM_ID = CORT_NUM_ID;
    }

    public String getPOSTAL() {
        return POSTAL;
    }

    public void setPOSTAL(String POSTAL) {
        this.POSTAL = POSTAL;
    }

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String SERIES) {
        this.SERIES = SERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long TENANT_NUM_ID) {
        this.TENANT_NUM_ID = TENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long DATA_SIGN) {
        this.DATA_SIGN = DATA_SIGN;
    }



    public String getPHYSICAL_ID() {
        return PHYSICAL_ID;
    }

    public void setPHYSICAL_ID(String PHYSICAL_ID) {
        this.PHYSICAL_ID = PHYSICAL_ID;
    }

    public String getPHYSICAL_NAME() {
        return PHYSICAL_NAME;
    }

    public void setPHYSICAL_NAME(String PHYSICAL_NAME) {
        this.PHYSICAL_NAME = PHYSICAL_NAME;
    }

    public String getEN_STK_NAME() {
        return EN_STK_NAME;
    }

    public void setEN_STK_NAME(String EN_STK_NAME) {
        this.EN_STK_NAME = EN_STK_NAME;
    }

    public String getPHYSICAL_NUM_ID() {
        return PHYSICAL_NUM_ID;
    }

    public void setPHYSICAL_NUM_ID(String PHYSICAL_NUM_ID) {
        this.PHYSICAL_NUM_ID = PHYSICAL_NUM_ID;
    }

    public String getSUB_UNIT_NUM_ID() {
        return SUB_UNIT_NUM_ID;
    }

    public void setSUB_UNIT_NUM_ID(String SUB_UNIT_NUM_ID) {
        this.SUB_UNIT_NUM_ID = SUB_UNIT_NUM_ID;
    }

    public Long getSTORAGE_DEPT_NUM_ID() {
        return STORAGE_DEPT_NUM_ID;
    }

    public void setSTORAGE_DEPT_NUM_ID(Long STORAGE_DEPT_NUM_ID) {
        this.STORAGE_DEPT_NUM_ID = STORAGE_DEPT_NUM_ID;
    }

    public Long getPUR_SIGN() {
        return PUR_SIGN;
    }

    public void setPUR_SIGN(Long PUR_SIGN) {
        this.PUR_SIGN = PUR_SIGN;
    }

    public Long getCUY_NUM_ID() {
        return CUY_NUM_ID;
    }

    public void setCUY_NUM_ID(Long CUY_NUM_ID) {
        this.CUY_NUM_ID = CUY_NUM_ID;
    }

    public Long getPRV_NUM_ID() {
        return PRV_NUM_ID;
    }

    public void setPRV_NUM_ID(Long PRV_NUM_ID) {
        this.PRV_NUM_ID = PRV_NUM_ID;
    }

    public Long getCITY_NUM_ID() {
        return CITY_NUM_ID;
    }

    public void setCITY_NUM_ID(Long CITY_NUM_ID) {
        this.CITY_NUM_ID = CITY_NUM_ID;
    }

    public Long getCITY_AREA_NUM_ID() {
        return CITY_AREA_NUM_ID;
    }

    public void setCITY_AREA_NUM_ID(Long CITY_AREA_NUM_ID) {
        this.CITY_AREA_NUM_ID = CITY_AREA_NUM_ID;
    }

    public Long getTOWN_NUM_ID() {
        return TOWN_NUM_ID;
    }

    public void setTOWN_NUM_ID(Long TOWN_NUM_ID) {
        this.TOWN_NUM_ID = TOWN_NUM_ID;
    }

    public String getAREA_ADR() {
        return AREA_ADR;
    }

    public void setAREA_ADR(String AREA_ADR) {
        this.AREA_ADR = AREA_ADR;
    }

    public String getEN_AREA_ADR() {
        return EN_AREA_ADR;
    }

    public void setEN_AREA_ADR(String EN_AREA_ADR) {
        this.EN_AREA_ADR = EN_AREA_ADR;
    }

    public String getCONT_EMPE() {
        return CONT_EMPE;
    }

    public void setCONT_EMPE(String CONT_EMPE) {
        this.CONT_EMPE = CONT_EMPE;
    }

    public String getTELEPHONE() {
        return TELEPHONE;
    }

    public void setTELEPHONE(String TELEPHONE) {
        this.TELEPHONE = TELEPHONE;
    }

    public String getFAX() {
        return FAX;
    }

    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    public String getMOBILENUM() {
        return MOBILENUM;
    }

    public void setMOBILENUM(String MOBILENUM) {
        this.MOBILENUM = MOBILENUM;
    }

    public String getMAILADR() {
        return MAILADR;
    }

    public void setMAILADR(String MAILADR) {
        this.MAILADR = MAILADR;
    }

    public Long getIS_INDEPENDENT_ACCOUNTING() {
        return IS_INDEPENDENT_ACCOUNTING;
    }

    public void setIS_INDEPENDENT_ACCOUNTING(Long IS_INDEPENDENT_ACCOUNTING) {
        this.IS_INDEPENDENT_ACCOUNTING = IS_INDEPENDENT_ACCOUNTING;
    }

    public Long getIS_PROCESSING_STORAGE() {
        return IS_PROCESSING_STORAGE;
    }

    public void setIS_PROCESSING_STORAGE(Long IS_PROCESSING_STORAGE) {
        this.IS_PROCESSING_STORAGE = IS_PROCESSING_STORAGE;
    }

    public String getFNC() {
        return FNC;
    }

    public void setFNC(String FNC) {
        this.FNC = FNC;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date CREATE_DTME) {
        this.CREATE_DTME = CREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date LAST_UPDTME) {
        this.LAST_UPDTME = LAST_UPDTME;
    }

    public Long getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(Long CREATE_USER_ID) {
        this.CREATE_USER_ID = CREATE_USER_ID;
    }

    public Long getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(Long LAST_UPDATE_USER_ID) {
        this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
    }

    public String getCANCELSIGN() {
        return CANCELSIGN;
    }

    public void setCANCELSIGN(String CANCELSIGN) {
        this.CANCELSIGN = CANCELSIGN;
    }

//    public String getINSERTDATA() {
//        return INSERTDATA;
//    }
//
//    public void setINSERTDATA(String INSERTDATA) {
//        this.INSERTDATA = INSERTDATA;
//    }
//
//    public String getUPDATEDATA() {
//        return UPDATEDATA;
//    }
//
//    public void setUPDATEDATA(String UPDATEDATA) {
//        this.UPDATEDATA = UPDATEDATA;
//    }
//
//    public String getALREADYSEND() {
//        return ALREADYSEND;
//    }
//
//    public void setALREADYSEND(String ALREADYSEND) {
//        this.ALREADYSEND = ALREADYSEND;
//    }
//
//    public String getSENDDATA() {
//        return SENDDATA;
//    }
//
//    public void setSENDDATA(String SENDDATA) {
//        this.SENDDATA = SENDDATA;
//    }


    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public Long getEnable_sign() {
        return enable_sign;
    }

    public void setEnable_sign(Long enable_sign) {
        this.enable_sign = enable_sign;
    }
}
