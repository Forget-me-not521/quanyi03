package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.omp.cmasterdata.api.model.*;
import com.ykcloud.soa.omp.cmasterdata.api.request.MdmsB2bCsPolicyPriceRequest;
import com.ykcloud.soa.omp.cmasterdata.util.MdDaoUtil;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class B2bBlCommonQueryDao {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    /**
     * 查询销售组织
     *
     * @param tenantNumId
     * @param dataSign
     * @param cortNumId
     * @param unitNumId
     * @return
     */
    public List<MdmsSalesOrg> selectSalesOrgList(Long tenantNumId, Long dataSign, Integer cortNumId, String unitNumId) {
        String sql = "select unit_num_id,unit_name from MDMS_O_UNIT WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND unit_type=3 and cort_num_id = ? AND unit_num_id in (" + unitNumId + ")  AND cancelsign = 'N'";
        return jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, cortNumId, unitNumId}, new BeanPropertyRowMapper<>(MdmsSalesOrg.class));
    }

    /**
     * 数据字段
     *
     * @param tenantNumId
     * @param dataSign
     * @param tableName   表名称
     * @param fieldName   字段名称
     * @return
     */
    public List<MdmsCType> selectMdmsCType(Long tenantNumId, Long dataSign, String tableName, String fieldName) {
        String sql = "SELECT type_num_id,type_name,notes" +
                "FROM mdms_c_type" +
                "WHERE tenant_num_id = ? AND data_sign = ?" +
                "AND status_num_id = 1 AND cancelsign = 'N'" +
                "AND table_name = ? and field_name = ?";
        return jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, tableName, fieldName}, new BeanPropertyRowMapper<>(MdmsCType.class));
    }

    /**
     * 查询逻辑仓库
     *
     * @param tenantNumId
     * @param dataSign
     * @param cortNumId
     * @param unitNumId
     * @return
     */
    public List<MdmsWStorage> selectWStorageList(Long tenantNumId, Long dataSign, Integer cortNumId, String unitNumId) {
        String sql = "select storage_num_id,storage_name from mdms_w_storage WHERE TENANT_NUM_ID = ? AND DATA_SIGN = ? AND  cort_num_id = ? AND unit_num_id in (" + unitNumId + ")  AND cancelsign = 'N'";
        return jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, cortNumId, unitNumId}, new BeanPropertyRowMapper<>(MdmsWStorage.class));
    }

    /**
     * 查询客户条款中的协议
     * @param tenantNumId
     * @param dataSign
     * @param customerNumId
     * @return
     */
    public List<MdmsB2bCsProduct> getProtocolProductList(Long tenantNumId, Long dataSign, String customerNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select DISTINCT a.item_num_id from (SELECT item_num_id FROM mdms_b2b_cs_product WHERE tenant_num_id = ? AND data_sign =? AND effect_sign = 1 AND type_num_id = 2 AND customer_num_id = ? ");
        sb.append(" AND item_num_id NOT IN (SELECT item_num_id FROM mdms_b2b_cs_product WHERE tenant_num_id = ? AND data_sign =? AND effect_sign = 1 AND type_num_id = 1 AND customer_num_id <> ?) ");
        sb.append(" UNION ALL");
        sb.append(" SELECT item_num_id FROM mdms_b2b_cs_product WHERE tenant_num_id = ? AND data_sign =? AND effect_sign = 1 AND type_num_id = 1 AND customer_num_id = ?)a");
        return jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, customerNumId, tenantNumId, dataSign, customerNumId, tenantNumId, dataSign, customerNumId}, new BeanPropertyRowMapper<>(MdmsB2bCsProduct.class));
    }

    /**
     * 查询 渠道内的商品 effect_sign = 1为生效状态
     * @param tenantNumId
     * @param dataSign
     * @param cortNumId
     * @param channelNumId
     * @return
     */
    public List<MdmsB2bCsProduct> getProductChannelPolicyList(Long tenantNumId, Long dataSign, String cortNumId, Byte channelNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select DISTINCT a.item_num_id from (SELECT  item_num_id FROM mdms_b2b_channel_policy WHERE tenant_num_id = ? AND data_sign =? AND effect_sign = 1 AND cort_num_id =? AND channel_num_id =?");
        sb.append(" UNION ALL");
        sb.append(" SELECT b1.item_num_id FROM mdms_p_product_basic b1 INNER JOIN mdms_p_product_unit u1 ON b1.item_num_id = u1.item_num_id AND b1.tenant_num_id = u1.tenant_num_id AND b1.data_sign = u1.data_sign");
        sb.append(" WHERE b1.tenant_num_id = ? AND b1.data_sign = ?");
        sb.append(" AND b1.pty3_num_id IN(SELECT pty3_num_id FROM mdms_b2b_channel_policy WHERE tenant_num_id = ? AND data_sign =? AND effect_sign = 1) and AND cort_num_id =? and channel_num_id=?)a");
        return jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, cortNumId, channelNumId, tenantNumId, dataSign, tenantNumId, dataSign, cortNumId, channelNumId}, new BeanPropertyRowMapper<>(MdmsB2bCsProduct.class));
    }

    /**
     * 商品要求。商品属性  禁止的
     * @param tenantNumId
     * @param dataSign
     * @param cortNumId
     * @param channelNumId
     * @return
     */
    public List<MdmsB2bCsProduct> getProductProhibitList(Long tenantNumId, Long dataSign, String cortNumId, Byte channelNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select DISTINCT a.item_num_id from (SELECT  item_num_id FROM mdms_b2b_channel_policy WHERE tenant_num_id = ? AND data_sign =? AND effect_sign = 1 AND cort_num_id =? AND channel_num_id =?");
        sb.append(" UNION ALL");
        sb.append(" SELECT b1.item_num_id FROM mdms_p_product_basic b1 INNER JOIN mdms_p_product_unit u1 ON b1.item_num_id = u1.item_num_id AND b1.tenant_num_id = u1.tenant_num_id AND b1.data_sign = u1.data_sign");
        sb.append(" WHERE b1.tenant_num_id = ? AND b1.data_sign = ?");
        sb.append(" AND b1.pty3_num_id IN(SELECT pty3_num_id FROM mdms_b2b_channel_policy WHERE tenant_num_id = ? AND data_sign =? AND effect_sign = 1) and AND cort_num_id =? and channel_num_id=?)a");
        return jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, cortNumId, channelNumId, tenantNumId, dataSign, tenantNumId, dataSign, cortNumId, channelNumId}, new BeanPropertyRowMapper<>(MdmsB2bCsProduct.class));
    }

    /**
     * 商品要求。商品冻结的
     * @param tenantNumId
     * @param dataSign
     * @param cortNumId
     * @return
     */
    public List<MdmsB2bCsProduct> getProductFreezeList(Long tenantNumId, Long dataSign, String cortNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT item_num_id FROM  mdms_item_frozen_hdr h1");
        sb.append(" INNER JOIN mdms_item_frozen_dtl d1 ON h1.reserved_no = d1.reserved_no AND h1.tenant_num_id = d1.tenant_num_id AND h1.data_sign = d1.data_sign");
        sb.append(" WHERE h1.tenant_num_id = ? AND d1.data_sign = ? AND d1.batch_id = ''  AND h1.status_num_id = 3 AND d1.sell_sign = 1  AND d1.return_store_sign = 1 AND cort_num_id =?");
        return jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, cortNumId}, new BeanPropertyRowMapper<>(MdmsB2bCsProduct.class));
    }

    /**
     * 根据取价逻辑获取信息
     * @param r
     * @return
     */
    public MdmsB2bCsPolicyPrice getPolicyPriceByStore(MdmsB2bCsPolicyPriceRequest r) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT item_num_id,price_model,2 price_lv,(?  * ( 100 + price_add_rate)/100 + price_add_price) as result FROM mdms_b2b_cs_policy");
        sb.append(" WHERE tenant_num_id = ? AND data_sign = ? AND price_model = ? AND effect_sign = 1 ");
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("sub_unit_num_id",r.getSubUnitNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("cort_num_id",r.getCortNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("customer_num_id",r.getCustomerNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("item_num_id",r.getItemNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("pty3_num_id",r.getPty3NumId()));
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{r.getPrice(),r.getTenantNumId(),r.getDataSign(),r.getPriceModel(),r.getSubUnitNumId(),r.getCortNumId(),r.getCustomerNumId(),r.getItemNumId(),r.getPty3NumId()}, new BeanPropertyRowMapper<>(MdmsB2bCsPolicyPrice.class));
    }

    /**
     * 获取合同价格
     * @param tenantNumId
     * @param dataSign
     * @param unitNumId
     * @param itemNumId
     * @return
     */
    public MdmsB2bCsProduct getContractPrice(Long tenantNumId, Long dataSign,String unitNumId,String itemNumId){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT item_num_id,contract_price FROM mdm.mdms_b2b_cs_product WHERE tenant_num_id = ? AND data_sign = ? AND contract_sign = 1 AND effect_sign = 1");
        sb.append("AND unit_num_id = ? AND item_num_id = ? limit 1");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId,dataSign,unitNumId,itemNumId}, new BeanPropertyRowMapper<>(MdmsB2bCsProduct.class));
    }
}
