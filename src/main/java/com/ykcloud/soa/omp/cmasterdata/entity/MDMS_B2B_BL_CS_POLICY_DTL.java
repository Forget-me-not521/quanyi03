package com.ykcloud.soa.omp.cmasterdata.entity;

import com.ykcloud.soa.erp.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MDMS_B2B_BL_CS_POLICY_DTL extends BaseEntity {
    private static final long serialVersionUID=1L;

    /** 单据号 */
    @ApiModelProperty(value = "单据号")
    private String RESERVED_NO;

    /** 公司 */
    @ApiModelProperty(value = "公司")
    private String CORT_NUM_ID;

    /** 业务单元 */
    @ApiModelProperty(value = "业务单元")
    private String UNIT_NUM_ID;

    /** 组织 */
    @ApiModelProperty(value = "组织")
    private String ORG_UNIT;

    /** 渠道 */
    @ApiModelProperty(value = "渠道")
    private Integer CHANNEL_NUM_ID;

    /** 客户 */
    @ApiModelProperty(value = "客户")
    private String CUSTOMER_NUM_ID;

    /** 客户门店 */
    @ApiModelProperty(value = "客户门店")
    private String SUB_UNIT_NUM_ID;

    /** 是否生效。0不生效，1生效 */
    @ApiModelProperty(value = "是否生效。0不生效，1生效")
    private Integer EFFECT_SIGN;

    /** 商品 */
    @ApiModelProperty(value = "商品")
    private String ITEM_NUM_ID;

    /** 取价模式 */
    @ApiModelProperty(value = "取价模式")
    private Integer PRICE_MODEL_OLD;

    /** 取价模式 */
    @ApiModelProperty(value = "取价模式")
    private Integer PRICE_MODEL_NEW;

    /** 原加价毛利率 */
    @ApiModelProperty(value = "原加价毛利率")
    private BigDecimal PRICE_ADD_RATE_OLD;

    /** 新加价毛利率 */
    @ApiModelProperty(value = "新加价毛利率")
    private BigDecimal PRICE_ADD_RATE_NEW;

    /** 原固定加价 */
    @ApiModelProperty(value = "原固定加价")
    private BigDecimal PRICE_ADD_PRICE_OLD;

    /** 新固定加价 */
    @ApiModelProperty(value = "新固定加价")
    private BigDecimal PRICE_ADD_PRICE_NEW;

    /** 创建人 */
    @ApiModelProperty(value = "创建人")
    private Long CREATE_USER_ID;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private Date CREATE_DTME;

    /** 更新用户 */
    @ApiModelProperty(value = "更新用户")
    private Long UPDATE_USER_ID;

    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private Date UPDATE_DTME;

    /** 删除 */
    @ApiModelProperty(value = "删除")
    private String CANCELSIGN;
}
