package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsAssistOnOff;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_ASSIST_ON_OFF;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;


@Repository
public class MdmsAssistOnOffDao  extends Dao<MDMS_ASSIST_ON_OFF> {

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public List<MdmsAssistOnOff> getAssistPage(Long tenantNumId, Long dataSign, String cortNumId, String cortName, Integer pageNum, Integer pageSize) {
        String sql = "select * from mdms_assist_on_off where cancelsign='N' and tenant_num_id =? and data_sign = ? ";
        if(StringUtils.isNotEmpty(cortNumId)){
            sql+=" and cort_num_id='"+cortNumId+"' ";
        }
        if(StringUtils.isNotEmpty(cortName)){
            sql+=" and cort_name='"+cortName+"' ";
        }
        Integer start = (pageNum-1)*pageSize;

        sql+=" limit ?,? ";
        List<MdmsAssistOnOff> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign,start,pageSize}, new BeanPropertyRowMapper<>(MdmsAssistOnOff.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    public Integer getAssistPageCount(Long tenantNumId, Long dataSign, String cortNumId, String cortName) {
        String sql = "select count(1) from mdms_assist_on_off where cancelsign='N' and tenant_num_id =? and data_sign = ? ";
        if(StringUtils.isNotEmpty(cortNumId)){
            sql+=" and cort_num_id='"+cortNumId+"' ";
        }
        if(StringUtils.isNotEmpty(cortName)){
            sql+=" and cort_name='"+cortName+"' ";
        }
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign}, Integer.class);
    }

    public MDMS_ASSIST_ON_OFF getAssistbySeries(Long series, Long tenantNumId, Long dataSign) {
        String sql = "select * from mdms_assist_on_off where series = ? and cancelsign='N' and tenant_num_id = ? and data_sign = ? ";
        List<MDMS_ASSIST_ON_OFF> list = jdbcTemplate.query(sql, new Object[]{series, tenantNumId, dataSign}, new BeanPropertyRowMapper<>(MDMS_ASSIST_ON_OFF.class));
        if(CollectionUtils.isEmpty(list)){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该开关不存在!");
        }
        if(list.size()>1){
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "该开关有多条!");
        }
        return list.get(0);
    }

}
