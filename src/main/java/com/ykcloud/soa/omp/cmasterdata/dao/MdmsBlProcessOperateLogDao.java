package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsBlProcessOperateLog;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_BL_PROCESS_OPERATE_LOG;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author stark.jiang
 * @Date 2021/09/11/14:01
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsBlProcessOperateLogDao extends Dao<MDMS_BL_PROCESS_OPERATE_LOG> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public List<MdmsBlProcessOperateLog> getByReservedNo(Long tenantNumId, Long dataSign, String cortNumId, String reservedNo,Integer sourceType,Integer businessType,Integer billType,Integer pageNum, Integer pageSize) {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from mdms_bl_process_operate_log ");
        sb.append(" where tenant_Num_id = ? and data_sign = ? and cort_num_id = ? and reserved_no = ? and source_type=?  and cancelsign = 'N' ");
        if(!ValidatorUtils.isNullOrZero(businessType)){
            sb.append(" and business_type="+businessType);
        }
        if(!ValidatorUtils.isNullOrZero(billType)){
            sb.append(" and bill_type="+billType);
        }
        Integer start = (pageNum-1)*pageSize;

        sb.append(" limit ?,? ");
        List<MdmsBlProcessOperateLog> logList = jdbcTemplate.query(sb.toString(), new Object[]{tenantNumId, dataSign, cortNumId, reservedNo,sourceType,start,pageSize}, new BeanPropertyRowMapper<>(MdmsBlProcessOperateLog.class));
        return logList;
    }
    public Integer getOperateLogCount(Long tenantNumId, Long dataSign, String cortNumId, String reservedNo,Integer sourceType,Integer businessType,Integer billType) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1)  from mdms_bl_process_operate_log ");
        sb.append(" where tenant_Num_id = ? and data_sign = ? and cort_num_id = ? and reserved_no = ? and source_type=?  and cancelsign = 'N' ");
        if(!ValidatorUtils.isNullOrZero(businessType)){
            sb.append(" and business_type="+businessType);
        }
        if(!ValidatorUtils.isNullOrZero(billType)){
            sb.append(" and bill_type="+billType);
        }
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, cortNumId, reservedNo,sourceType}, Integer.class);
    }
    public List<MdmsBlProcessOperateLog> getByReservedNoPage(Long tenantNumId, Long dataSign, String cortNumId, String reservedNo, Integer pageNum, Integer pageSize) {
        String sql = "select * from mdms_bl_process_operate_log where tenant_Num_id = ? and data_sign = ? and cort_num_id = ? and reserved_no = ? and cancelsign = 'N'";
        Integer start = (pageNum-1)*pageSize;
        sql+=" limit ?,? ";
        List<MdmsBlProcessOperateLog> logList = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign, cortNumId, reservedNo}, new BeanPropertyRowMapper<>(MdmsBlProcessOperateLog.class));
        return logList;
    }

    public Integer getByReservedNoCount(Long tenantNumId, Long dataSign, String cortNumId, String reservedNo) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) from mdms_bl_process_operate_log ");
        sb.append(" where tenant_Num_id = ? and data_sign = ? and cort_num_id = ? and reserved_no = ? and cancelsign = 'N'");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, cortNumId, reservedNo}, Integer.class);
    }
}
