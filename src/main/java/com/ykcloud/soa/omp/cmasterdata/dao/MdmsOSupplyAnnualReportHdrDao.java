package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.SupplyAnnualReportForQuery;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUPPLY_ANNUAL_REPORT_HDR;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.ValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author stark.jiang
 * @Date 2021/10/22/9:34
 * @Description:
 * @Version 1.0
 */
@Repository
public class MdmsOSupplyAnnualReportHdrDao extends Dao<MDMS_O_SUPPLY_ANNUAL_REPORT_HDR> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }

    public int updateConfirmQty(Long tenantNumId, Long dataSign,Long userNumId,String cortNumId,String reportYear) {
        String sql="update mdms_o_supply_annual_report_hdr set confirm_qty=confirm_qty+1,last_update_user_id=? ,last_updtme=now() ";
        sql+=" where tenant_num_id=? and data_sign=? and cort_num_id=? and report_year=? and cancelsign='N'";
        int row = jdbcTemplate.update(sql, new Object[]{userNumId,tenantNumId, dataSign,cortNumId,reportYear});
        if (row <= 0) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "更新供应商年报表头失败!");
        }
        return row;
    }



    public MDMS_O_SUPPLY_ANNUAL_REPORT_HDR getHdrEntity(Long tenantNumId, Long dataSign,String cortNumId,String reportYear) {
        String sql = "select * from mdms_o_supply_annual_report_hdr WHERE tenant_num_id = ? AND data_sign = ? AND  cort_num_id = ? AND report_year=? and cancelsign = 'N'";

        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, cortNumId,reportYear}, new BeanPropertyRowMapper<>(MDMS_O_SUPPLY_ANNUAL_REPORT_HDR.class));
    }


    public List<SupplyAnnualReportForQuery> getAnnualReportPage(Long tenantNumId, Long dataSign, String cortNumId, String cortName, Date reportYear, Integer pageNum, Integer pageSize) {
        String sql = "select cort_num_id,cort_name,report_year,report_title,confirm_qty,total_qty from mdms_o_supply_annual_report_hdr where tenant_num_id =? and data_sign = ? and cancelsign='N'";

        if(StringUtils.isNotEmpty(cortNumId)){
            sql+=" and cort_num_id='"+cortNumId+"' ";
        }
        if(StringUtils.isNotEmpty(cortName)){
            sql+=" and cort_name='"+cortName+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(reportYear)){
            sql+=" and report_year='"+reportYear+"' ";
        }
        Integer start = (pageNum-1)*pageSize;

        sql+=" limit ?,? ";
        List<SupplyAnnualReportForQuery> list = jdbcTemplate.query(sql, new Object[]{tenantNumId, dataSign,start,pageSize}, new BeanPropertyRowMapper<>(SupplyAnnualReportForQuery.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    public Integer getAnnualReportCount(Long tenantNumId,Long dataSign,String cortNumId,String cortName,Date reportYear) {
        String sql = "select count(1) from mdms_o_supply_annual_report_hdr where tenant_num_id =? and data_sign = ? and cancelsign='N' ";
        if(StringUtils.isNotEmpty(cortNumId)){
            sql+=" and cort_num_id='"+cortNumId+"' ";
        }
        if(StringUtils.isNotEmpty(cortName)){
            sql+=" and cort_name='"+cortName+"' ";
        }
        if(!ValidatorUtils.isNullOrZero(reportYear)){
            sql+=" and report_year='"+reportYear+"' ";
        }
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign}, Integer.class);
    }
}
