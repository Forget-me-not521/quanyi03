package com.ykcloud.soa.omp.cmasterdata.enums;

public enum ManagerHdrTypeNumEnums {
    COMPANY(1, "公司"),;

    private int id;
    private String value;

    ManagerHdrTypeNumEnums(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnums(int id) {
        for (ManagerHdrTypeNumEnums item : ManagerHdrTypeNumEnums.values()) {
            if (item.getId() == id) {
                return item.getValue();
            }
        }
        return null;
    }

}
