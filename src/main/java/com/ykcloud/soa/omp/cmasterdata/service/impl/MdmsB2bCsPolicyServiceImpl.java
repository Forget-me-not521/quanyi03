package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.gb.soa.omp.ccommon.util.ExceptionUtil;
import com.gb.soa.omp.ccommon.util.JsonUtil;
import com.gb.soa.omp.ccommon.util.StringUtil;
import com.gb.soa.omp.ccommon.util.TransactionUtil;
import com.ykcloud.soa.erp.common.entity.EntityConverter;
import com.ykcloud.soa.omp.cmasterdata.api.request.MdmsB2bBlCsPolicyDtlImportRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.MdmsB2bBlCsPolicyHdrRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.MdmsB2bBlCsPolicyHdrSaveRequest;
import com.ykcloud.soa.omp.cmasterdata.api.response.MdmsB2bCsPolicyResponse;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdmsB2bCsPolicyService;
import com.ykcloud.soa.omp.cmasterdata.dao.MdmsB2bCsPolicyDtlDao;
import com.ykcloud.soa.omp.cmasterdata.dao.MdmsB2bCsPolicyHdrDao;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_B2B_BL_CS_POLICY_DTL;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_B2B_BL_CS_POLICY_HDR;
import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

@Service("mdmsB2bCsPolicyService")
@RestController
@Slf4j
public class MdmsB2bCsPolicyServiceImpl implements MdmsB2bCsPolicyService {

    @Resource(name = "masterDataTransactionManager")
    private DataSourceTransactionManager transactionManager;

    @Resource
    private MdmsB2bCsPolicyHdrDao mdmsB2bCsPolicyHdrDao;

    @Resource
    private MdmsB2bCsPolicyDtlDao mdmsB2bCsPolicyDtlDao;

    @Resource
    private HttpServletResponse response;


    @Override
    public MdmsB2bCsPolicyResponse saveMdmsB2bBlCsPolicyHdr(MdmsB2bBlCsPolicyHdrSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin saveMdmsB2bBlCsPolicyHdr request:{}", JsonUtil.toJson(request));
        }
        TransactionStatus txStatus = transactionManager.getTransaction(TransactionUtil.newTransactionDefinition(5 * 60));
        MdmsB2bCsPolicyResponse response = new MdmsB2bCsPolicyResponse();
        try{
            MDMS_B2B_BL_CS_POLICY_HDR entity = EntityConverter.get(request, new MDMS_B2B_BL_CS_POLICY_HDR());
            //添加
            if(StringUtil.isNull(entity.getSERIES())){
                String hdr_series = SeqUtil.getSeqNextValue(SeqUtil.MDMS_B2B_BL_CS_POLICY_HDR_SERIES);
                String hdr_reserved_no = SeqUtil.getSeqAutoNextValue(request.getTenantNumId(), request.getDataSign(), SeqUtil.MDMS_B2B_BL_CS_POLICY_HDR_RESERVED_NO);
                entity.setSERIES(hdr_series);
                entity.setRESERVED_NO(hdr_reserved_no);
                Date now = new Date();
                entity.setORDER_DATE(now);
                mdmsB2bCsPolicyHdrDao.insert(entity);
                mdmsB2bCsPolicyDtlDao.deletePolicyDtlByReservedNo(entity.getRESERVED_NO(),entity.getTENANT_NUM_ID(),entity.getDATA_SIGN());
                List<MDMS_B2B_BL_CS_POLICY_DTL> dtlList = entity.getMdmsB2bBlCsPolicyDtlList();
                dtlList.forEach(dtl->{
                    String dtl_series = SeqUtil.getSeqNextValue(SeqUtil.MDMS_B2B_BL_CS_POLICY_DTL_SERIES);
                    dtl.setSERIES(dtl_series);
                    dtl.setRESERVED_NO(entity.getRESERVED_NO());
                    dtl.setTENANT_NUM_ID(entity.getTENANT_NUM_ID());
                    dtl.setDATA_SIGN(entity.getDATA_SIGN());
                    dtl.setCORT_NUM_ID(entity.getCORT_NUM_ID());
                    dtl.setUNIT_NUM_ID(entity.getUNIT_NUM_ID());
                    dtl.setCREATE_USER_ID(entity.getCREATE_USER_ID());
                    dtl.setLAST_UPDATE_USER_ID(entity.getLAST_UPDATE_USER_ID());
                    dtl.setCREATE_DTME(now);
                    dtl.setLAST_UPDTME(now);
                    mdmsB2bCsPolicyDtlDao.insert(dtl);
                });
            }else {
                //修改hdr表
                mdmsB2bCsPolicyHdrDao.update(entity,entity.getTENANT_NUM_ID(),entity.getDATA_SIGN(),entity.getSERIES());
            }
            transactionManager.commit(txStatus);
        } catch (Exception e) {
            transactionManager.rollback(txStatus);
            ExceptionUtil.processException(e, response);
        }
            if (log.isDebugEnabled()) {
            log.debug("end saveMdmsB2bBlCsPolicyHdr response:{}", JsonUtil.toJson(response));
        }
            return response;
        }

    @Override
    public MdmsB2bCsPolicyResponse selectMdmsB2bBlCsPolicyHdrList(MdmsB2bBlCsPolicyHdrRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin selectMdmsB2bBlCsPolicyHdrList request:{}", JsonUtil.toJson(request));
        }
        MdmsB2bCsPolicyResponse response = new MdmsB2bCsPolicyResponse();
        try {
            response.setData(mdmsB2bCsPolicyHdrDao.pageQuery(request));
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end selectMdmsB2bBlCsPolicyHdrList response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsB2bCsPolicyResponse selectMdmsB2bBlCsPolicyHdrBySeries(MdmsB2bBlCsPolicyHdrSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin selectMdmsB2bBlCsPolicyHdrBySeries request:{}", JsonUtil.toJson(request));
        }
        MdmsB2bCsPolicyResponse response = new MdmsB2bCsPolicyResponse();
        try {
            MDMS_B2B_BL_CS_POLICY_HDR hdr = mdmsB2bCsPolicyHdrDao.getMdmsB2bCsPolicyHdrEntity(request.getTenantNumId(),request.getDataSign(),request.getSERIES());
            List<MDMS_B2B_BL_CS_POLICY_DTL> list = mdmsB2bCsPolicyDtlDao.getMdmsB2bCsPolicyDtlList(request.getTenantNumId(),request.getDataSign(),hdr.getRESERVED_NO());
            hdr.setMdmsB2bBlCsPolicyDtlList(list);
            response.setData(hdr);
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end selectMdmsB2bBlCsPolicyHdrBySeries response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsB2bCsPolicyResponse deleteMdmsB2bBlCsPolicyHdr(MdmsB2bBlCsPolicyHdrRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin deleteMdmsB2bBlCsPolicyHdr request:{}", JsonUtil.toJson(request));
        }
        TransactionStatus txStatus = transactionManager.getTransaction(TransactionUtil.newTransactionDefinition(5 * 60));
        MdmsB2bCsPolicyResponse response = new MdmsB2bCsPolicyResponse();
        try {
            Long tenantNumId = request.getTenantNumId();
            Long dataSign = request.getDataSign();
            List<String> series = request.getSeriesList();
            for(String s:series){
                MDMS_B2B_BL_CS_POLICY_HDR hdr = mdmsB2bCsPolicyHdrDao.getMdmsB2bCsPolicyHdrEntity(tenantNumId,dataSign,s);
                mdmsB2bCsPolicyDtlDao.deletePolicyDtlByReservedNo(hdr.getRESERVED_NO(),hdr.getTENANT_NUM_ID(),hdr.getDATA_SIGN());
                mdmsB2bCsPolicyHdrDao.deleteMdmsB2bCsPolicyHdr(tenantNumId,dataSign,s);
            }
            transactionManager.commit(txStatus);
        } catch (Exception e) {
            transactionManager.rollback(txStatus);
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteMdmsB2bBlCsPolicyHdr response:{}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsB2bCsPolicyResponse uploadExcel(MdmsB2bBlCsPolicyDtlImportRequest request) {
        return null;
    }

    @Override
    public String exportTemplate(MdmsB2bBlCsPolicyDtlImportRequest request) {
        return null;
    }

    @Override
    public String exportExcel(MdmsB2bBlCsPolicyDtlImportRequest request) {
        return null;
    }
}
