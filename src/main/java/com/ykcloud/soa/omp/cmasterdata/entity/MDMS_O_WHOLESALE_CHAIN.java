package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName MDMS_O_WHOLESALE_CHAIN
 * @Description TODO
 * @Author User
 * @Date 2021/11/8
 * @Version 1.0
 */

public class MDMS_O_WHOLESALE_CHAIN {

    private int  SERIES=0;   //行号
    private int TENANT_NUM_ID=0;  //租户ID
    private int  DATA_SIGN=0;   //0:正式 ,1:测试
    private String WHOLESALE_NUM_ID;   //批发公司编码
    private String WHOLESALE_NAME;   //批发公司名称
    private String CHAIN_NUM_ID;   //连锁公司编码
    private String CHAIN_NAME;   //连锁公司名称
    private String UNIFIED_SOCIAL_CREDIT_CODE;   //连锁公司统一社会信用代码
    private Date CREATE_DTME;   //创建时间
    private Date LAST_UPDTME;   //最后更新时间
    private int CREATE_USER_ID=0;    //用户
    private String CANCELSIGN="N";  //删除
    private int LAST_UPDATE_USER_ID=0;   //更新用户


    public int getSERIES() {
        return SERIES;
    }

    public void setSERIES(int SERIES) {
        this.SERIES = SERIES;
    }

    public int getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(int TENANT_NUM_ID) {
        this.TENANT_NUM_ID = TENANT_NUM_ID;
    }

    public int getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(int DATA_SIGN) {
        this.DATA_SIGN = DATA_SIGN;
    }

    public String getWHOLESALE_NUM_ID() {
        return WHOLESALE_NUM_ID;
    }

    public void setWHOLESALE_NUM_ID(String WHOLESALE_NUM_ID) {
        this.WHOLESALE_NUM_ID = WHOLESALE_NUM_ID;
    }

    public String getWHOLESALE_NAME() {
        return WHOLESALE_NAME;
    }

    public void setWHOLESALE_NAME(String WHOLESALE_NAME) {
        this.WHOLESALE_NAME = WHOLESALE_NAME;
    }

    public String getCHAIN_NUM_ID() {
        return CHAIN_NUM_ID;
    }

    public void setCHAIN_NUM_ID(String CHAIN_NUM_ID) {
        this.CHAIN_NUM_ID = CHAIN_NUM_ID;
    }

    public String getCHAIN_NAME() {
        return CHAIN_NAME;
    }

    public void setCHAIN_NAME(String CHAIN_NAME) {
        this.CHAIN_NAME = CHAIN_NAME;
    }

    public String getUNIFIED_SOCIAL_CREDIT_CODE() {
        return UNIFIED_SOCIAL_CREDIT_CODE;
    }

    public void setUNIFIED_SOCIAL_CREDIT_CODE(String UNIFIED_SOCIAL_CREDIT_CODE) {
        this.UNIFIED_SOCIAL_CREDIT_CODE = UNIFIED_SOCIAL_CREDIT_CODE;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date CREATE_DTME) {
        this.CREATE_DTME = CREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date LAST_UPDTME) {
        this.LAST_UPDTME = LAST_UPDTME;
    }

    public int getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(int CREATE_USER_ID) {
        this.CREATE_USER_ID = CREATE_USER_ID;
    }

    public String getCANCELSIGN() {
        return CANCELSIGN;
    }

    public void setCANCELSIGN(String CANCELSIGN) {
        this.CANCELSIGN = CANCELSIGN;
    }

    public int getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(int LAST_UPDATE_USER_ID) {
        this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
    }
}
