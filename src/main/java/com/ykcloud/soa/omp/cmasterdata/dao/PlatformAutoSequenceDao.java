package com.ykcloud.soa.omp.cmasterdata.dao;

import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ykcloud.soa.omp.cmasterdata.entity.PLATFORM_AUTO_SEQUENCE;

@Repository
public class PlatformAutoSequenceDao {

    @Resource(name = "platFormJdbcTemplate")
    private JdbcTemplate jdbcTemplate;


    public boolean checkExistSeqName(String seqName, Long tenantNumId, Long dataSign) {
        String sql = "select count(*) from platform_auto_sequence where SEQ_NAME = ? and TENANT_NUM_ID= ? and DATA_SIGN=?";
        int number = jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{seqName, tenantNumId, dataSign});
        return number == 0 ? false : true;
    }

    public boolean checkExistSeries(String series, Long tenantNumId, Long dataSign) {
        String sql = "select count(*) from platform_auto_sequence where SERIES = ? and TENANT_NUM_ID= ? and DATA_SIGN=?";
        int number = jdbcTemplate.queryForObject(sql, Integer.class, new Object[]{series, tenantNumId, dataSign});
        return number == 0 ? false : true;
    }

    public PLATFORM_AUTO_SEQUENCE findPlatformAutoSequenceBySeries(String series, Long tenantNumId, Long dataSign) throws Exception {
        String sql = "select * from platform_auto_sequence where SERIES = ? and TENANT_NUM_ID= ? and DATA_SIGN=?";
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(PLATFORM_AUTO_SEQUENCE.class), new Object[]{series, tenantNumId, dataSign});
    }

    public int insertPlatformAutoSequenceNew(String seqName, String seqProject, String seqPrefix, Long currentNum,
                                             String remark, Long initValue, Integer cacheNum, String series, Long tenantNumId, Long dataSign) {

        String sql = "insert into platform_auto_sequence(SEQ_NAME,SEQ_PROJECT,SEQ_PREFIX,CURRENT_NUM,REMARK,INIT_VALUE,CACHE_NUM,SERIES,TENANT_NUM_ID,DATA_SIGN) values(?,?,?,?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql,
                new Object[]{seqName, seqProject, seqPrefix, currentNum, remark, initValue, cacheNum, series, tenantNumId, dataSign});

    }

    /*
     * public int updatePlatformAutoSequence(String seqName,String seqProject,String
     * seqPrefix,Long currentNum,String remark,Long initValue,Integer
     * cacheNum,String series,Long tenantNumId,Long dataSign) {
     *
     * String sql =
     * "update platform_auto_sequence set SEQ_NAME=?,SEQ_PROJECT=?,SEQ_PREFIX=?,CURRENT_NUM=?,REMARK=?,INIT_VALUE=?,CACHE_NUM=? where SERIES =? and TENANT_NUM_ID= ? and DATA_SIGN=?"
     * ; return jdbcTemplate.update(sql, new Object[]
     * {seqName,seqProject,seqPrefix,currentNum,remark,initValue,cacheNum,series,
     * tenantNumId,dataSign});
     *
     * }
     */
    public int updatePlatformAutoSequence(Long currentNum, String
            remark, Integer cacheNum, String series, Long tenantNumId, Long dataSign) {

        String sql = "update platform_auto_sequence set CURRENT_NUM=?,REMARK=?,CACHE_NUM=? where SERIES =? and TENANT_NUM_ID= ? and DATA_SIGN=?";
        return jdbcTemplate.update(sql, new Object[]{currentNum, remark, cacheNum, series, tenantNumId, dataSign});

    }
}
