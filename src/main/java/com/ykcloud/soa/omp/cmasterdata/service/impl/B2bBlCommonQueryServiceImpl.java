package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.gb.soa.omp.ccommon.api.exception.BusinessException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.util.ExceptionUtil;
import com.gb.soa.omp.ccommon.util.JsonUtil;
import com.ykcloud.soa.omp.cmasterdata.api.model.*;
import com.ykcloud.soa.omp.cmasterdata.api.request.*;
import com.ykcloud.soa.omp.cmasterdata.api.response.*;
import com.ykcloud.soa.omp.cmasterdata.api.service.B2bBlCommonService;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdTenantService;
import com.ykcloud.soa.omp.cmasterdata.dao.*;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Service("b2bBlCommonService")
@RestController
@Slf4j
public class B2bBlCommonQueryServiceImpl implements B2bBlCommonService {
    @Resource
    private B2bBlCommonQueryDao b2bBlCommonQueryDao;
    @Resource
    private MdmsOCustomerDao mdmsOCustomerDao;
    @Resource
    private MdmsOCortDao mdmsOCortDao;
    @Resource
    private MdTenantService mdTenantService;

    @Resource
    private MdmsOSubUnitDao mdmsOSubUnitDao;

    @Resource
    private MdmsPProductUnitDao mdmsPProductUnitDao;

    @Override
    public MdmsSalesOrgGetResponse getSalesOrgList(MdmsSalesOrgGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getSalesOrgList request:{}", JsonUtil.toJson(request));
        }
        MdmsSalesOrgGetResponse response = new MdmsSalesOrgGetResponse();
        try {
            // 需要了解cort_num_id 、unit_num_id  值怎么获取
            response.setSalesOrgList(b2bBlCommonQueryDao.selectSalesOrgList(request.getTenantNumId(), request.getDataSign(), request.getCortNumId(), request.getUnitNumId()));
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getSalesOrgList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public CustomerListGetResponse getCustomerList(CustomerListGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCustomerList request:{}", JsonUtil.toJson(request));
        }
        CustomerListGetResponse response = new CustomerListGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            response.setResults(mdmsOCustomerDao.getCustomerList(request.getTenantNumId(), request.getDataSign(), request.getUnifiedSocialCreditCode(), request.getCortNumId(), request.getCustomerNumId(), request.getCustomerName(), request.getCreateUserId(), request.getCreateDateBegin(), request.getCreateDateEnd(), request.getPageNum(), request.getPageSize()));
            response.setRecordCount(mdmsOCustomerDao.getCustomerCount(request.getTenantNumId(), request.getDataSign(), request.getUnifiedSocialCreditCode(), request.getCortNumId(), request.getCustomerNumId(), request.getCustomerName(), request.getCreateUserId(), request.getCreateDateBegin(), request.getCreateDateEnd()));
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public CortInfoGetResponse getCortInfo(CortInfoGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCortInfo request:{}", JsonUtil.toJson(request));
        }
        CortInfoGetResponse response = new CortInfoGetResponse();
        try {
            List<CortDropDown> list = mdmsOCortDao.getCortDropDown(request.getTenantNumId(), request.getDataSign(), request.getRegisterType());
            response.setResults(list);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCortInfo response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsWStorageGetResponse getWStorageList(MdmsWStorageGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getWStorageList request:{}", JsonUtil.toJson(request));
        }
        MdmsWStorageGetResponse response = new MdmsWStorageGetResponse();
        try {
            List<MdmsWStorage> list = b2bBlCommonQueryDao.selectWStorageList(request.getTenantNumId(), request.getDataSign(), request.getCortNumId(), request.getUnitNumId());
            response.setStorageList(list);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getWStorageList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public UnitProductListGetResponse getUnitProductList(UnitProductListGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCustomerShopInfo request:{}", JsonUtil.toJson(request));
        }
        UnitProductListGetResponse response = new UnitProductListGetResponse();
        try {
            response.setResults(mdmsPProductUnitDao.getUnitProductList(request.getTenantNumId(), request.getDataSign(), request.getCortNumId(), request.getItemName(), request.getFactory(), request.getPageNum(), request.getPageSize()));
            response.setRecordCount(mdmsPProductUnitDao.getUnitProductCount(request.getTenantNumId(), request.getDataSign(), request.getCortNumId(), request.getItemName(), request.getFactory()));
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerShopInfo response: {}", JsonUtil.toJson(response));
        }
        return response;
    }


    @Override
    public CustomerSubUnitResponse getCustomerSubUnitInfo(OCustomerShopGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getCustomerSubUnitInfo request:{}", JsonUtil.toJson(request));
        }
        CustomerSubUnitResponse response = new CustomerSubUnitResponse();
        try {
            List<BlSubUnit> info = mdmsOSubUnitDao.getSubUnitByCustomerNumId(request.getTenantNumId(), request.getDataSign(), request.getCustomerNumId());
            response.setInfo(info);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getCustomerSubUnitInfo response: {}", JsonUtil.toJson(response));
        }
        return response;
    }


    @Override
    public MdmsB2bCsProductResponse getProtocolProductList(MdmsB2bCsProductRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getWStorageList request:{}", JsonUtil.toJson(request));
        }
        MdmsB2bCsProductResponse response = new MdmsB2bCsProductResponse();
        try {
            response.setResults(b2bBlCommonQueryDao.getProtocolProductList(request.getTenantNumId(), request.getDataSign(), request.getCustomerNumId()));
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getWStorageList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsB2bCsProductResponse getProductChannelPolicyList(MdmsB2bCsProductRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getProductChannelPolicyList request:{}", JsonUtil.toJson(request));
        }
        MdmsB2bCsProductResponse response = new MdmsB2bCsProductResponse();
        try {
            response.setResults(b2bBlCommonQueryDao.getProductChannelPolicyList(request.getTenantNumId(), request.getDataSign(), request.getCortNumId(), request.getChannelNumId()));
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getProductChannelPolicyList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsB2bCsProductResponse getProductProhibitList(MdmsB2bCsProductRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getProductProhibitList request:{}", JsonUtil.toJson(request));
        }
        MdmsB2bCsProductResponse response = new MdmsB2bCsProductResponse();
        try {
            response.setResults(b2bBlCommonQueryDao.getProductProhibitList(request.getTenantNumId(), request.getDataSign(), request.getCortNumId(), request.getChannelNumId()));
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getProductProhibitList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsB2bCsProductResponse getProductFreezeList(MdmsB2bCsProductRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getProductFreezeList request:{}", JsonUtil.toJson(request));
        }
        MdmsB2bCsProductResponse response = new MdmsB2bCsProductResponse();
        try {
            response.setResults(b2bBlCommonQueryDao.getProductFreezeList(request.getTenantNumId(), request.getDataSign(), request.getCustomerNumId()));
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getProductFreezeList response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public MdmsB2bCsPolicyPriceResponse getPolicyPriceByStore(MdmsB2bCsPolicyPriceRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getPolicyPriceByStore request:{}", JsonUtil.toJson(request));
        }
        MdmsB2bCsPolicyPriceResponse response = new MdmsB2bCsPolicyPriceResponse();
        try {
            double price = 0;
            if (request.getPriceModel() == 1 || request.getPriceModel() == 0) {//合同价
                MdmsB2bCsProduct csProduct = b2bBlCommonQueryDao.getContractPrice(request.getTenantNumId(), request.getDataSign(), request.getUnitNumId(), request.getItemNumId());
                if (null == csProduct) {
                    throw new BusinessException(Constant.SUB_SYSTEM, ExceptionType.BE40071, "未取到价格!");
                }
                price = csProduct.getContractPrice();
            } else if (request.getPriceModel() == 2) {//移动加权价
                //TODO 是否可以引入finance库，或者接口，目前查询不了
            }
            request.setPrice(price);
            MdmsB2bCsPolicyPrice policyPrice = b2bBlCommonQueryDao.getPolicyPriceByStore(request);
            response.setPolicyPrice(policyPrice);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getPolicyPriceByStore response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

}
