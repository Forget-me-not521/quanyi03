package com.ykcloud.soa.omp.cmasterdata.entity;

public class T_MENU {
    private String ID;
    private String LABEL;
    private String ENABLE;
    private String ISMENU;
    private String URL;
    private String MENUTYPE;
    private String CONTROLID;
    private String SYSCODE;
    private String PARENTID;
    private Long SEQ;
    private String ICON;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getLABEL() {
        return LABEL;
    }

    public void setLABEL(String LABEL) {
        this.LABEL = LABEL;
    }

    public String getENABLE() {
        return ENABLE;
    }

    public void setENABLE(String ENABLE) {
        this.ENABLE = ENABLE;
    }

    public String getISMENU() {
        return ISMENU;
    }

    public void setISMENU(String ISMENU) {
        this.ISMENU = ISMENU;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getMENUTYPE() {
        return MENUTYPE;
    }

    public void setMENUTYPE(String MENUTYPE) {
        this.MENUTYPE = MENUTYPE;
    }

    public String getCONTROLID() {
        return CONTROLID;
    }

    public void setCONTROLID(String CONTROLID) {
        this.CONTROLID = CONTROLID;
    }

    public String getSYSCODE() {
        return SYSCODE;
    }

    public void setSYSCODE(String SYSCODE) {
        this.SYSCODE = SYSCODE;
    }

    public String getPARENTID() {
        return PARENTID;
    }

    public void setPARENTID(String PARENTID) {
        this.PARENTID = PARENTID;
    }

    public Long getSEQ() {
        return SEQ;
    }

    public void setSEQ(Long SEQ) {
        this.SEQ = SEQ;
    }

    public String getICON() {
        return ICON;
    }

    public void setICON(String ICON) {
        this.ICON = ICON;
    }
}
