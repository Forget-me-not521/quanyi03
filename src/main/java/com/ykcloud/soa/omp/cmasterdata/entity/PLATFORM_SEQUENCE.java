package com.ykcloud.soa.omp.cmasterdata.entity;

import java.util.Date;


public class PLATFORM_SEQUENCE {

    private String SERIES;
    private String SEQ_NAME;
    private String SEQ_PROJECT;
    private String SEQ_PREFIX;
    private String SEQ_NUM;
    private String SEQ_VAL;
    private Long CURRENT_NUM;
    private Date CREATE_TIME;
    private Long SEQ_NUM_START;
    private Long SEQ_NUM_END;
    private Integer DISRUPT;
    private Integer IS_STORE_LOCAL;

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String SERIES) {
        this.SERIES = SERIES;
    }

    public String getSEQ_NAME() {
        return SEQ_NAME;
    }

    public void setSEQ_NAME(String SEQ_NAME) {
        this.SEQ_NAME = SEQ_NAME;
    }

    public String getSEQ_PROJECT() {
        return SEQ_PROJECT;
    }

    public void setSEQ_PROJECT(String SEQ_PROJECT) {
        this.SEQ_PROJECT = SEQ_PROJECT;
    }

    public String getSEQ_PREFIX() {
        return SEQ_PREFIX;
    }

    public void setSEQ_PREFIX(String SEQ_PREFIX) {
        this.SEQ_PREFIX = SEQ_PREFIX;
    }

    public String getSEQ_NUM() {
        return SEQ_NUM;
    }

    public void setSEQ_NUM(String SEQ_NUM) {
        this.SEQ_NUM = SEQ_NUM;
    }

    public String getSEQ_VAL() {
        return SEQ_VAL;
    }

    public void setSEQ_VAL(String SEQ_VAL) {
        this.SEQ_VAL = SEQ_VAL;
    }

    public Long getCURRENT_NUM() {
        return CURRENT_NUM;
    }

    public void setCURRENT_NUM(Long CURRENT_NUM) {
        this.CURRENT_NUM = CURRENT_NUM;
    }

    public Date getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(Date CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public Long getSEQ_NUM_START() {
        return SEQ_NUM_START;
    }

    public void setSEQ_NUM_START(Long SEQ_NUM_START) {
        this.SEQ_NUM_START = SEQ_NUM_START;
    }

    public Long getSEQ_NUM_END() {
        return SEQ_NUM_END;
    }

    public void setSEQ_NUM_END(Long SEQ_NUM_END) {
        this.SEQ_NUM_END = SEQ_NUM_END;
    }

    public Integer getDISRUPT() {
        return DISRUPT;
    }

    public void setDISRUPT(Integer DISRUPT) {
        this.DISRUPT = DISRUPT;
    }

    public Integer getIS_STORE_LOCAL() {
        return IS_STORE_LOCAL;
    }

    public void setIS_STORE_LOCAL(Integer IS_STORE_LOCAL) {
        this.IS_STORE_LOCAL = IS_STORE_LOCAL;
    }

}
