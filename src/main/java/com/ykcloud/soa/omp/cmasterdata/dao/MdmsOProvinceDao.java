package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;

import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOProvince;
import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_PROVINCE;
import com.ykcloud.soa.omp.cmasterdata.service.model.AdministrativeRegionQueryCondition;
import com.ykcloud.soa.omp.cmasterdata.util.EntityFieldUtil;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

import javax.annotation.Resource;

@Repository
public class MdmsOProvinceDao {
    private static final String SQL_COLS = EntityFieldUtil.fieldSplit(MDMS_O_PROVINCE.class, ",");
    private static final String WILDCARDS = EntityFieldUtil.wildcardSplit(MDMS_O_PROVINCE.class, ",");
    private static final String TABLE_NAME = "mdms_o_province";

    private static final String ALLREGIONFIENDJOINSQL = "SELECT " +
            " prv.prv_num_id, " +
            " prv.prv_sim_no, " +
            " prv.prv_name, " +
            " prv.series as prv_series, " +
            " city.series as city_series, " +
            " area.series as area_series, " +
            " town.series as town_series, " +
            " city.city_num_id, " +
            " city.city_sim_no, " +
            " city.city_name, " +
            " area.city_area_num_id, " +
            " area.city_area_sim_no, " +
            " area.city_area_name, " +
            " town.town_num_id, " +
            " town.town_sim_no, " +
            " town.town_name " +
            "FROM " +
            "mdms_o_province prv  " +
            "LEFT JOIN mdms_o_city city on prv.tenant_num_id = city.tenant_num_id and prv.data_sign = city.data_sign and prv.cancelsign = city.cancelsign and prv.prv_num_id = city.prv_num_id " +
            "LEFT JOIN mdms_o_city_area area on area.tenant_num_id = city.tenant_num_id and area.data_sign = city.data_sign and area.cancelsign = city.cancelsign and area.city_num_id = city.city_num_id " +
            "LEFT JOIN mdms_o_city_town town on town.tenant_num_id = area.tenant_num_id and town.data_sign = area.data_sign and town.cancelsign = area.cancelsign and town.city_area_num_id = area.city_area_num_id " +
            "WHERE " +
            " prv.tenant_num_id = 9 " +
            "AND prv.data_sign = 0 " +
            "AND prv.cancelsign = 'N' ";

    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;

    public boolean checkExistByPrvNumId(Long tenantNumId, Long dataSign, Long prvNumId) {
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1)");
        sb.append(" from ");
        sb.append(" mdms_o_province ");
        sb.append(" where tenant_num_id=? and data_sign=? and prv_num_id=? and cancelsign='N'");
        return jdbcTemplate.queryForObject(sb.toString(), new Object[]{tenantNumId, dataSign, prvNumId}, Integer.class) == 0 ? true : false;
    }

    public boolean checkRelation(Long tenantNumId, Long dataSign, Long prvNumId, Long cityNumId, Long cityAreaNumId, Long townNumId) {
        String sql = " select count(1) " +
                " from mdms_o_province p " +
                " left join mdms_o_city c on  c.prv_num_id=p.prv_num_id and c.tenant_num_id=p.tenant_num_id and c.data_sign=p.data_sign " +
                " left join mdms_o_city_area ca on ca.prv_num_id=p.prv_num_id and ca.tenant_num_id=p.tenant_num_id and ca.data_sign=p.data_sign " +
                " left join mdms_o_city_town ct on ct.prv_num_id=p.prv_num_id and ct.tenant_num_id=p.tenant_num_id and ct.data_sign=p.data_sign " +
                " where p.tenant_num_id=? and p.data_sign=? and p.prv_num_id=? and c.city_num_id=? and ca.city_area_num_id=? and ct.town_num_id=? and p.cancelsign='N'";
        return jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId, dataSign, prvNumId, cityNumId, cityAreaNumId, townNumId}, Integer.class) == 0 ? false : true;
    }

    public int insertMdmsOProvince(MDMS_O_PROVINCE prv) {
        StringBuilder sb = new StringBuilder();
        sb.append("insert into ");
        sb.append(TABLE_NAME);
        sb.append(" ( ");
        sb.append(SQL_COLS);
        sb.append(") values (");
        sb.append(WILDCARDS);
        sb.append(")");
        System.out.println(sb.toString());
        System.out.println(Arrays.asList(EntityFieldUtil.fieldSplitValue(MDMS_O_PROVINCE.class, prv)));

        return jdbcTemplate.update(sb.toString(), EntityFieldUtil.fieldSplitValue(MDMS_O_PROVINCE.class, prv));

    }

    public Long checkExistSeries(String series, Long tenantNumId, Long dataSign) {
        String sql = "select prv_num_id from mdms_o_province where SERIES = ? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        Long prvNumId = jdbcTemplate.queryForObject(sql, Long.class, new Object[]{series, tenantNumId, dataSign});
        return prvNumId;
    }

    public int updateMdmsOProvince(Long prvNumId, String prvName, String prvSimNo, Long userNumId, String series,
                                   Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_province set prv_num_id=?,prv_name=?,prv_sim_no=?,last_update_user_id=? ,last_updtme = now(),updatedata='Y' where series =? and tenant_num_id=? and data_sign=? and cancelsign ='N'";

        return jdbcTemplate.update(sql, new Object[]{prvNumId, prvName, prvSimNo, userNumId, series, tenantNumId, dataSign});
    }

    public int deleteByseries(Long userNumId, String series, Long tenantNumId, Long dataSign) {
        String sql = "update mdms_o_province set cancelsign ='Y' ,last_update_user_id= ?,last_updtme=now() where series= ? and tenant_num_id=? and data_sign=? and cancelsign ='N'";
        return jdbcTemplate.update(sql, new Object[]{userNumId, series, tenantNumId, dataSign});
    }





    private void joinRegionQueryConditionSql(StringBuilder sql, Map<String, Object> params, AdministrativeRegionQueryCondition condition) {
        if (Objects.nonNull(condition.getPrvNumId())) {
            sql.append(" and town.prv_num_id = :prvNumId ");
            params.put("prvNumId", condition.getPrvNumId());
        }
        if (Objects.nonNull(condition.getPrvSimNo())) {
            sql.append(" and locate(:prvSimNo, prv.prv_sim_no) > 0 ");
            params.put("prvSimNo", condition.getPrvSimNo());
        }
        if (Objects.nonNull(condition.getCityNumId())) {
            sql.append(" and town.city_num_id = :cityNumId ");
            params.put("cityNumId", condition.getPrvNumId());
        }
        if (Objects.nonNull(condition.getCitySimNo())) {
            sql.append(" and locate(:citySimNo, city.city_sim_no) > 0 ");
            params.put("citySimNo", condition.getCitySimNo());
        }
        if (Objects.nonNull(condition.getCityAreaNumId())) {
            sql.append(" and town.city_area_num_id = :cityAreaNumId ");
            params.put("cityAreaNumId", condition.getCityAreaNumId());
        }
        if (Objects.nonNull(condition.getCityAreaSimNo())) {
            sql.append(" and locate(:cityAreaSimNo, area.city_area_sim_no) > 0 ");
            params.put("cityAreaSimNo", condition.getCityAreaSimNo());
        }
        if (Objects.nonNull(condition.getTownNumId())) {
            sql.append(" and town.town_num_id = :townNumId ");
            params.put("townNumId", condition.getTownNumId());
        }
        if (Objects.nonNull(condition.getTownSimNo())) {
            sql.append(" and locate(:townSimNo, town.town_sim_no) > 0 ");
            params.put("townSimNo", condition.getTownSimNo());
        }
        if (Objects.nonNull(condition.getPrvName())) {
            sql.append(" and locate(:prvName, prv.prv_name) > 0 ");
            params.put("prvName", condition.getPrvName());
        }
        if (Objects.nonNull(condition.getCityName())) {
            sql.append(" and locate(:cityName, city.city_name) > 0 ");
            params.put("cityName", condition.getCityName());
        }
        if (Objects.nonNull(condition.getCityAreaName())) {
            sql.append(" and locate(:cityAreaName, area.city_area_name) > 0 ");
            params.put("cityAreaName", condition.getCityAreaName());
        }
        if (Objects.nonNull(condition.getTownName())) {
            sql.append(" and locate(:townName, town.town_name) > 0 ");
            params.put("townName", condition.getTownName());
        }
    }

    public Integer countLowerAdministrativeArea(Long tenantNumId, Long dataSign, AdministrativeRegionQueryCondition condition) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT " +
                " count(1) " +
                "FROM " +
                " mdms_o_city_town town " +
                "LEFT JOIN mdms_o_province prv ON prv.tenant_num_id = town.tenant_num_id " +
                "AND prv.data_sign = town.data_sign " +
                "AND town.cancelsign = prv.cancelsign " +
                "AND prv.prv_num_id = town.prv_num_id " +
                "LEFT JOIN mdms_o_city city ON town.tenant_num_id = city.tenant_num_id " +
                "AND town.data_sign = city.data_sign " +
                "AND town.cancelsign = city.cancelsign " +
                "and town.city_num_id = city.city_num_id " +
                "LEFT JOIN mdms_o_city_area area ON area.tenant_num_id = town.tenant_num_id " +
                "AND area.data_sign = town.data_sign " +
                "AND town.cancelsign = area.cancelsign " +
                "and town.city_area_num_id = area.city_area_num_id " +
                "WHERE " +
                " town.tenant_num_id = :tenantNumId " +
                "AND town.data_sign = :dataSign " +
                "AND town.cancelsign = 'N' ");
        Map<String, Object> params = new HashMap<>();
        params.put("tenantNumId", tenantNumId);
        params.put("dataSign", dataSign);
        joinRegionQueryConditionSql(sql, params, condition);
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
    }

    public Integer countProvinceInfo(Long tenantNumId, Long dataSign, AdministrativeRegionQueryCondition condition) {
        StringBuilder sql = new StringBuilder();
        sql.append("select count(1) from mdms_o_province where tenant_num_id = :tenantNumId and data_sign = :dataSign and cancelsign = 'N' ");

        Map<String, Object> params = new HashMap<>();
        params.put("tenantNumId", tenantNumId);
        params.put("dataSign", dataSign);

        if (Objects.nonNull(condition.getPrvNumId())) {
            sql.append(" and prv_num_id = :prvNumId ");
            params.put("prvNumId", condition.getPrvNumId());
        }
        if (Objects.nonNull(condition.getPrvSimNo())) {
            sql.append(" and locate(:prvSimNo, prv_sim_no) > 0 ");
            params.put("prvSimNo", condition.getPrvSimNo());
        }
        if (Objects.nonNull(condition.getPrvName())) {
            sql.append(" and locate(:prvName, prv_name) > 0 ");
            params.put("prvName", condition.getPrvName());
        }

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.TYPE);

    }

    public List<MdmsOProvince> queryProvinceInfo(Long tenantNumId, Long dataSign, AdministrativeRegionQueryCondition condition, Integer start, Integer pageSize) {
        StringBuilder sql = new StringBuilder();
        sql.append("select prv_num_id, prv_name, prv_sim_no, create_dtme, create_user_id, last_updtme, last_update_user_id " +
                "from mdms_o_province " +
                "where tenant_num_id = :tenantNumId and data_sign = :dataSign and cancelsign = 'N' ");

        Map<String, Object> params = new HashMap<>();
        params.put("tenantNumId", tenantNumId);
        params.put("dataSign", dataSign);

        if (Objects.nonNull(condition.getPrvNumId())) {
            sql.append(" and prv_num_id = :prvNumId ");
            params.put("prvNumId", condition.getPrvNumId());
        }
        if (Objects.nonNull(condition.getPrvSimNo())) {
            sql.append(" and locate(:prvSimNo, prv_sim_no) > 0 ");
            params.put("prvSimNo", condition.getPrvSimNo());
        }
        if (Objects.nonNull(condition.getPrvName())) {
            sql.append(" and locate(:prvName, prv_name) > 0 ");
            params.put("prvName", condition.getPrvName());
        }

        sql.append(" limit :start, :pageSize");
        params.put("start", start);
        params.put("pageSize", pageSize);

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(MdmsOProvince.class));
    }

}
