package com.ykcloud.soa.omp.cmasterdata.dao;

import com.gb.soa.omp.ccommon.api.exception.DatabaseOperateException;
import com.gb.soa.omp.ccommon.api.exception.ExceptionType;
import com.gb.soa.omp.ccommon.api.exception.ValidateBusinessException;
import com.gb.soa.omp.ccommon.util.MyJdbcTemplate;
import com.ykcloud.soa.erp.common.dao.Dao;
import com.ykcloud.soa.omp.cmasterdata.api.model.B2bBlChannelPolicyHdr;
import com.ykcloud.soa.omp.cmasterdata.api.request.BtobChannelPolicyGetRequest;
import com.ykcloud.soa.omp.cmasterdata.entity.ScmB2bBlChannelPolicyHdr;
import com.ykcloud.soa.omp.cmasterdata.util.Constant;
import com.ykcloud.soa.omp.cmasterdata.util.MdDaoUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class MdBtobWholesaleChannelPolicyDao extends Dao<ScmB2bBlChannelPolicyHdr> {
    @Resource(name = "masterDataJdbcTemplate")
    private MyJdbcTemplate jdbcTemplate;
    @Override
    public JdbcTemplate jdbcTemplate() {
        return jdbcTemplate;
    }
    private static final String TABLE_NAME = "mdms_b2b_bl_channel_policy_hdr";

    public List<ScmB2bBlChannelPolicyHdr> getChannelPolicyHdrListPage(BtobChannelPolicyGetRequest r) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT hdr.* FROM ");
        sb.append(TABLE_NAME + " hdr where 1=1");
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("hdr","unit_num_id",r.getUnitNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("hdr","channel_num_id",r.getChannelNumId()));
        sb.append(MdDaoUtil.IF_NOT_NULL_COL_SQL("hdr","reserved_no",r.getReservedNo()));
        sb.append(MdDaoUtil.DATE_COL_SQL("hdr","order_date",r.getOrderDate()));
        sb.append(MdDaoUtil.TENANT_NUM_ID_DATA_SIGN_SQL("hdr"));
        Integer start = (r.getPageNum()-1)*r.getPageSize();
        sb.append(" limit ?,? ");
        List<ScmB2bBlChannelPolicyHdr> list = jdbcTemplate.query(sb.toString(), new Object[]{r.getUnitNumId(), r.getChannelNumId(),r.getReservedNo(),r.getOrderDate(), r.getTenantNumId(), r.getDataSign(),start,r.getPageSize()}, new BeanPropertyRowMapper<>(ScmB2bBlChannelPolicyHdr.class));
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        return list;
    }

    public Integer getChannelPolicyHdrListPageCount(BtobChannelPolicyGetRequest r) {
        String sb = "SELECT count(1) FROM " +
                TABLE_NAME + "where cancelsign='N' and tenant_num_id =? and data_sign = ?" +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("unit_num_id", r.getUnitNumId()) +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("channel_num_id", r.getChannelNumId()) +
                MdDaoUtil.IF_NOT_NULL_COL_SQL("reserved_no", r.getReservedNo()) +
                MdDaoUtil.DATE_COL_SQL("order_date", r.getOrderDate());
        return jdbcTemplate.queryForObject(sb, new Object[]{r.getTenantNumId(), r.getDataSign(),r.getUnitNumId(), r.getChannelNumId(), r.getReservedNo(),r.getOrderDate()}, Integer.class);
    }

    public B2bBlChannelPolicyHdr getChannelPolicyHdr(Long tenantNumId, Long dataSign, Long series){
        String sql = "select * from mdms_b2b_bl_channel_policy_hdr where TENANT_NUM_ID = ? and DATA_SIGN = ? and cancelsign='N' AND series = ?";
        B2bBlChannelPolicyHdr hdr = jdbcTemplate.queryForObject(sql, new Object[]{tenantNumId,dataSign, series}, new BeanPropertyRowMapper<>(B2bBlChannelPolicyHdr.class));
        if (null == hdr) {
            throw new ValidateBusinessException(Constant.SUB_SYSTEM, ExceptionType.VBE25001, "不存在系统单号为" + series + "的表单数据！");
        }
        return hdr;
    }

    public void updateBySeries(Long tenantNumId, Long dataSign, Long series,Byte statusNumId) {
        String sql = "UPDATE mdms_b2b_bl_channel_policy_hdr SET status_num_id = ?,update_dtme=now() WHERE TENANT_NUM_ID = ? and DATA_SIGN = ? AND series = ?";
        if (jdbcTemplate.update(sql, statusNumId,tenantNumId, dataSign, series) < 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "行号" + series + "更新status_num_id失败!");
        }
    }

    public void deleteChannelPolicy(Long tenantNumId, Long dataSign, Long series,String reservedNo){
        String sql = "UPDATE mdms_b2b_bl_channel_policy_hdr SET cancelsign = 'Y',update_dtme=now() WHERE TENANT_NUM_ID = ? and DATA_SIGN = ? and cancelsign='N' AND series = ?";
        if (jdbcTemplate.update(sql, tenantNumId, dataSign, series) < 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "行号" + series + "删除失败!");
        }
        sql = "UPDATE mdms_b2b_bl_channel_policy_dtl SET cancelsign = 'Y',update_dtme=now() WHERE TENANT_NUM_ID = ? and DATA_SIGN = ? and cancelsign='N' AND reserved_no = ?";
        if (jdbcTemplate.update(sql, tenantNumId, dataSign, reservedNo) < 0) {
            throw new DatabaseOperateException(Constant.SUB_SYSTEM, ExceptionType.DOE35001, "单据号" + reservedNo + "删除失败!");
        }
    }

}
