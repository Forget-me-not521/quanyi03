package com.ykcloud.soa.omp.cmasterdata.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class MDMS_O_CITY_REGION {
    private String SERIES;
    private Long TENANT_NUM_ID;
    private Long DATA_SIGN;
    private Long REGION_NUM_ID;
    private String REGION_SIM_NO;
    private String REGION_NAME;
    private String EN_REGION_NAME;
    private Double AREA;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date CREATE_DTME;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date LAST_UPDTME;
    private Long CREATE_USER_ID;
    private Long LAST_UPDATE_USER_ID;
    private String CANCELSIGN;
    private String INSERTDATA;
    private String UPDATEDATA;
    private String SENDDATA;

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String SERIES) {
        this.SERIES = SERIES;
    }

    public Long getTENANT_NUM_ID() {
        return TENANT_NUM_ID;
    }

    public void setTENANT_NUM_ID(Long TENANT_NUM_ID) {
        this.TENANT_NUM_ID = TENANT_NUM_ID;
    }

    public Long getDATA_SIGN() {
        return DATA_SIGN;
    }

    public void setDATA_SIGN(Long DATA_SIGN) {
        this.DATA_SIGN = DATA_SIGN;
    }

    public Long getREGION_NUM_ID() {
        return REGION_NUM_ID;
    }

    public void setREGION_NUM_ID(Long REGION_NUM_ID) {
        this.REGION_NUM_ID = REGION_NUM_ID;
    }

    public String getREGION_SIM_NO() {
        return REGION_SIM_NO;
    }

    public void setREGION_SIM_NO(String REGION_SIM_NO) {
        this.REGION_SIM_NO = REGION_SIM_NO;
    }

    public String getREGION_NAME() {
        return REGION_NAME;
    }

    public void setREGION_NAME(String REGION_NAME) {
        this.REGION_NAME = REGION_NAME;
    }

    public String getEN_REGION_NAME() {
        return EN_REGION_NAME;
    }

    public void setEN_REGION_NAME(String EN_REGION_NAME) {
        this.EN_REGION_NAME = EN_REGION_NAME;
    }

    public Double getAREA() {
        return AREA;
    }

    public void setAREA(Double AREA) {
        this.AREA = AREA;
    }

    public Date getCREATE_DTME() {
        return CREATE_DTME;
    }

    public void setCREATE_DTME(Date CREATE_DTME) {
        this.CREATE_DTME = CREATE_DTME;
    }

    public Date getLAST_UPDTME() {
        return LAST_UPDTME;
    }

    public void setLAST_UPDTME(Date LAST_UPDTME) {
        this.LAST_UPDTME = LAST_UPDTME;
    }

    public Long getCREATE_USER_ID() {
        return CREATE_USER_ID;
    }

    public void setCREATE_USER_ID(Long CREATE_USER_ID) {
        this.CREATE_USER_ID = CREATE_USER_ID;
    }

    public Long getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }

    public void setLAST_UPDATE_USER_ID(Long LAST_UPDATE_USER_ID) {
        this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
    }

    public String getCANCELSIGN() {
        return CANCELSIGN;
    }

    public void setCANCELSIGN(String CANCELSIGN) {
        this.CANCELSIGN = CANCELSIGN;
    }

    public String getINSERTDATA() {
        return INSERTDATA;
    }

    public void setINSERTDATA(String INSERTDATA) {
        this.INSERTDATA = INSERTDATA;
    }

    public String getUPDATEDATA() {
        return UPDATEDATA;
    }

    public void setUPDATEDATA(String UPDATEDATA) {
        this.UPDATEDATA = UPDATEDATA;
    }

    public String getSENDDATA() {
        return SENDDATA;
    }

    public void setSENDDATA(String SENDDATA) {
        this.SENDDATA = SENDDATA;
    }
}
