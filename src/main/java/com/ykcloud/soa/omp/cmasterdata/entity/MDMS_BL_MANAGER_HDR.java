package com.ykcloud.soa.omp.cmasterdata.entity;

import lombok.Data;

import java.util.Date;

@Data
public class MDMS_BL_MANAGER_HDR {
    private Long SERIES;//行号
    private Long TENANT_NUM_ID;//租户ID
    private Byte DATA_SIGN;//正式或测试标识
    private String RESERVED_NO;//DM单号
    private String RESERVED_NAME;//单据名称
    private Integer STATUS_NUM_ID;
    private Byte TYPE_NUM_ID;//调整类型
    private String REMARK;//备注
    private Date CREATE_DTME;//创建时间
    private Date LAST_UPDTME;//最后更新时间
    private Long CREATE_USER_ID;//用户
    private String CANCELSIGN;//删除
    private Long LAST_UPDATE_USER_ID;//更新用户
}
