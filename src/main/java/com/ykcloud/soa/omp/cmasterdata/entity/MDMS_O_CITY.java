package com.ykcloud.soa.omp.cmasterdata.entity;

public class MDMS_O_CITY extends BaseEntity {
    private Long PRV_NUM_ID;
    private Long CITY_NUM_ID;
    private String CITY_SIM_NO;
    private String CITY_NAME;
    private Long EMPE_NUM_ID;
    private String CITY_POSTCODE;
    private String CITY_TELEPHONE;
    private String CITY_FAX;
    private String CITY_ADR;
    private String CITY_MAIL;
    private String INSERTDATA;
    private String UPDATEDATA;
    private String SENDDATA;
    private String CONS_ABILITY_NO;
    private String EN_CITY_NAME;
    private String EN_CITY_ADR;
    private String MAP_COORD;
    private Long LEVEL_TYPE;
    private Double MAC_HHS;
    private String TIER;
    private Long ZXS_SIGN;

    public Long getPRV_NUM_ID() {
        return PRV_NUM_ID;
    }

    public void setPRV_NUM_ID(Long PRV_NUM_ID) {
        this.PRV_NUM_ID = PRV_NUM_ID;
    }

    public Long getCITY_NUM_ID() {
        return CITY_NUM_ID;
    }

    public void setCITY_NUM_ID(Long CITY_NUM_ID) {
        this.CITY_NUM_ID = CITY_NUM_ID;
    }

    public String getCITY_SIM_NO() {
        return CITY_SIM_NO;
    }

    public void setCITY_SIM_NO(String CITY_SIM_NO) {
        this.CITY_SIM_NO = CITY_SIM_NO;
    }

    public String getCITY_NAME() {
        return CITY_NAME;
    }

    public void setCITY_NAME(String CITY_NAME) {
        this.CITY_NAME = CITY_NAME;
    }

    public Long getEMPE_NUM_ID() {
        return EMPE_NUM_ID;
    }

    public void setEMPE_NUM_ID(Long EMPE_NUM_ID) {
        this.EMPE_NUM_ID = EMPE_NUM_ID;
    }

    public String getCITY_POSTCODE() {
        return CITY_POSTCODE;
    }

    public void setCITY_POSTCODE(String CITY_POSTCODE) {
        this.CITY_POSTCODE = CITY_POSTCODE;
    }

    public String getCITY_TELEPHONE() {
        return CITY_TELEPHONE;
    }

    public void setCITY_TELEPHONE(String CITY_TELEPHONE) {
        this.CITY_TELEPHONE = CITY_TELEPHONE;
    }

    public String getCITY_FAX() {
        return CITY_FAX;
    }

    public void setCITY_FAX(String CITY_FAX) {
        this.CITY_FAX = CITY_FAX;
    }

    public String getCITY_ADR() {
        return CITY_ADR;
    }

    public void setCITY_ADR(String CITY_ADR) {
        this.CITY_ADR = CITY_ADR;
    }

    public String getCITY_MAIL() {
        return CITY_MAIL;
    }

    public void setCITY_MAIL(String CITY_MAIL) {
        this.CITY_MAIL = CITY_MAIL;
    }

    public String getINSERTDATA() {
        return INSERTDATA;
    }

    public void setINSERTDATA(String INSERTDATA) {
        this.INSERTDATA = INSERTDATA;
    }

    public String getUPDATEDATA() {
        return UPDATEDATA;
    }

    public void setUPDATEDATA(String UPDATEDATA) {
        this.UPDATEDATA = UPDATEDATA;
    }

    public String getSENDDATA() {
        return SENDDATA;
    }

    public void setSENDDATA(String SENDDATA) {
        this.SENDDATA = SENDDATA;
    }

    public String getCONS_ABILITY_NO() {
        return CONS_ABILITY_NO;
    }

    public void setCONS_ABILITY_NO(String CONS_ABILITY_NO) {
        this.CONS_ABILITY_NO = CONS_ABILITY_NO;
    }

    public String getEN_CITY_NAME() {
        return EN_CITY_NAME;
    }

    public void setEN_CITY_NAME(String EN_CITY_NAME) {
        this.EN_CITY_NAME = EN_CITY_NAME;
    }

    public String getEN_CITY_ADR() {
        return EN_CITY_ADR;
    }

    public void setEN_CITY_ADR(String EN_CITY_ADR) {
        this.EN_CITY_ADR = EN_CITY_ADR;
    }

    public String getMAP_COORD() {
        return MAP_COORD;
    }

    public void setMAP_COORD(String MAP_COORD) {
        this.MAP_COORD = MAP_COORD;
    }

    public Long getLEVEL_TYPE() {
        return LEVEL_TYPE;
    }

    public void setLEVEL_TYPE(Long LEVEL_TYPE) {
        this.LEVEL_TYPE = LEVEL_TYPE;
    }

    public Double getMAC_HHS() {
        return MAC_HHS;
    }

    public void setMAC_HHS(Double MAC_HHS) {
        this.MAC_HHS = MAC_HHS;
    }

    public String getTIER() {
        return TIER;
    }

    public void setTIER(String TIER) {
        this.TIER = TIER;
    }

    public Long getZXS_SIGN() {
        return ZXS_SIGN;
    }

    public void setZXS_SIGN(Long ZXS_SIGN) {
        this.ZXS_SIGN = ZXS_SIGN;
    }
}
