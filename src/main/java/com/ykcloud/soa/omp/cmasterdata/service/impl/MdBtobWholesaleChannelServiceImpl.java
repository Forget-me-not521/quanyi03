package com.ykcloud.soa.omp.cmasterdata.service.impl;

import com.gb.soa.omp.ccommon.util.ExceptionUtil;
import com.gb.soa.omp.ccommon.util.JsonUtil;
import com.gb.soa.omp.ccommon.util.TransactionUtil;
import com.gb.soa.sequence.util.SeqGetUtil;
import com.google.common.collect.Lists;
import com.ykcloud.soa.omp.cmasterdata.api.model.*;
import com.ykcloud.soa.omp.cmasterdata.api.request.B2bBlChannelPolicySaveRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.BtobChannelPolicyGetRequest;
import com.ykcloud.soa.omp.cmasterdata.api.response.*;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdBtobWholesaleChannelService;
import com.ykcloud.soa.omp.cmasterdata.dao.MdBtobWholesaleChannelPolicyDao;
import com.ykcloud.soa.omp.cmasterdata.dao.MdBtobWholesaleChannelPolicyDetailDao;
import com.ykcloud.soa.omp.cmasterdata.dao.MdScmB2bChannelPolicyDao;
import com.ykcloud.soa.omp.cmasterdata.entity.*;
import com.ykcloud.soa.omp.cmasterdata.util.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 批销-渠道政策
 */
@Service("scmBtobWholesaleChannelService")
@RestController
@Slf4j
public class MdBtobWholesaleChannelServiceImpl implements MdBtobWholesaleChannelService {

    @Resource(name = "masterDataTransactionManager")
    private DataSourceTransactionManager transactionManager;

    @Resource
    private MdBtobWholesaleChannelPolicyDao mdBtobWholesaleChannelPolicyDao;
    @Resource
    private MdBtobWholesaleChannelPolicyDetailDao mdBtobWholesaleChannelPolicyDetailDao;
    @Resource
    private MdScmB2bChannelPolicyDao mdScmB2bChannelPolicyDao;
    @Resource
    private HttpServletResponse response;

    @Override
    public ChannelResponse getChannelPolicyHdrListPage(BtobChannelPolicyGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getChannelPolicyHdrListPage request:{}", JsonUtil.toJson(request));
        }
        ChannelResponse response = new ChannelResponse();
        try {
            response.setResults(mdBtobWholesaleChannelPolicyDao.getChannelPolicyHdrListPage(request));
            response.setCount(mdBtobWholesaleChannelPolicyDao.getChannelPolicyHdrListPageCount(request));
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getChannelPolicyHdrListPage response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public BtobStateUpdateResponse updateChannelPolicyState(BtobChannelPolicyGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updateChannelPolicyDtlState request:{}", JsonUtil.toJson(request));
        }
        BtobStateUpdateResponse response = new BtobStateUpdateResponse();
        try {
            mdBtobWholesaleChannelPolicyDao.updateBySeries(request.getTenantNumId(), request.getDataSign(), request.getSeries(), request.getStatusNumId());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end updateChannelPolicyDtlState response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ChannelPolicyGetResponse getChannelPolicy(BtobChannelPolicyGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getChannelPolicy request:{}", JsonUtil.toJson(request));
        }
        ChannelPolicyGetResponse response = new ChannelPolicyGetResponse();
        try {
            B2bBlChannelPolicyHdr hdr = mdBtobWholesaleChannelPolicyDao.getChannelPolicyHdr(request.getTenantNumId(), request.getDataSign(), request.getSeries());
            List<B2bBlChannelPolicyDtl> dtlList = mdBtobWholesaleChannelPolicyDetailDao.getChannelPolicyDtlList(request.getTenantNumId(), request.getDataSign(), hdr.getReserved_no());
            response.setHdr(hdr);
            response.setDtlList(dtlList);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getChannelPolicy response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ChannelPolicySubmitResponse submitChannelPolicy(B2bBlChannelPolicySaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin submitChannelPolicy request:{}", JsonUtil.toJson(request));
        }
        ChannelPolicySubmitResponse response = new ChannelPolicySubmitResponse();
        try {
            TransactionStatus txStatus = transactionManager.getTransaction(TransactionUtil.newTransactionDefinition());
            try {
                ScmB2bBlChannelPolicyHdr policyHdr = JsonUtil.fromJsonCamel(JsonUtil.toJson(request.getHdr()), ScmB2bBlChannelPolicyHdr.class);
                int result;
                if(!ValidatorUtils.isNullOrZero(policyHdr.getSeries())){
                    policyHdr.setTenant_num_id(request.getTenantNumId());
                    policyHdr.setData_sign(request.getDataSign());
                    policyHdr.setUpdate_user_id(request.getUserNumId());
                    policyHdr.setUpdate_dtme(new Date());
                    result = mdBtobWholesaleChannelPolicyDao.update(policyHdr, request.getTenantNumId(), request.getDataSign(), policyHdr.getSeries().toString());
                }else{
                    policyHdr = createChannelPolicyHdr(request.getHdr(), request.getTenantNumId(), request.getDataSign(), request.getUserNumId());
                    String reservedNo = SeqUtil.getSeqAutoNextValue(request.getTenantNumId(), request.getDataSign(), SeqUtil.MDMS_SCM_B2B_BL_CHANNEL_POLICY_HDR_RESERVED_NO);
                    policyHdr.setReserved_no(reservedNo);
                    result = mdBtobWholesaleChannelPolicyDao.insert(policyHdr);
                }
                List<ScmB2bBlChannelPolicyDtl> detailList = Lists.newArrayList();
                if (result > 0) {
                    mdBtobWholesaleChannelPolicyDetailDao.deleteChannelPolicyDtl(request.getTenantNumId(), request.getDataSign(), policyHdr.getReserved_no());
                    List<ScmB2bChannelPolicy> policyList = Lists.newArrayList();
                    for (B2bBlChannelPolicyDtl detail : request.getDtlList()) {
                        ScmB2bBlChannelPolicyDtl dtl = JsonUtil.fromJsonCamel(JsonUtil.toJson(detail), ScmB2bBlChannelPolicyDtl.class);
                        dtl.setSeries(SeqGetUtil.getNoSubSequence(SeqUtil.MDMS_SCM_B2B_BL_CHANNEL_POLICY_DTL_SERIES));
                        dtl.setTenant_num_id(request.getTenantNumId());
                        dtl.setData_sign(request.getDataSign());
                        dtl.setCreate_user_id(request.getUserNumId());
                        dtl.setReserved_no(policyHdr.getReserved_no());
                        dtl.setCancelsign(Constant.CANCEL_SIGN_N);
                        detailList.add(dtl);
                        if (policyHdr.getStatus_num_id() == 3) {//提交
                            //写入数据到结构表
                            ScmB2bChannelPolicy channelPolicy = JsonUtil.fromJsonCamel(JsonUtil.toJson(detail),ScmB2bChannelPolicy.class);
                            channelPolicy.setSeries(SeqGetUtil.getNoSubSequence(SeqUtil.MDMS_SCM_B2B_BL_CHANNEL_POLICY_SERIES));
                            channelPolicy.setTenant_num_id(request.getTenantNumId());
                            channelPolicy.setData_sign(request.getDataSign());
                            channelPolicy.setCancelsign(Constant.CANCEL_SIGN_N);
                            channelPolicy.setForm_type(policyHdr.getSo_from_type().toString());
                            channelPolicy.setForm_bill(policyHdr.getReserved_no());
                            channelPolicy.setCreate_user_id(request.getUserNumId());
                            channelPolicy.setCreate_dtme(new Date());
                            policyList.add(channelPolicy);
                        }
                    }
                    if (CollectionUtils.isNotEmpty(detailList)) {
                        mdBtobWholesaleChannelPolicyDetailDao.batchInsert(detailList);
                        if (CollectionUtils.isNotEmpty(policyList)) {
                            mdScmB2bChannelPolicyDao.batchInsert(policyList);
                        }
                    }
                }
                response.setReservedNo(policyHdr.getReserved_no());
                transactionManager.commit(txStatus);
            } catch (Exception e) {
                transactionManager.rollback(txStatus);
                throw e;
            }
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        return response;
    }

    @Override
    public void importTemplate() {
        try {
            ExcelParam.Builder eBuilder = new ExcelParam.Builder("导入模版");
            List<String> headerList = new ArrayList<>();
            headerList.add("渠道ID");
            headerList.add("商品ID");
            headerList.add("商品范围层级");
            headerList.add("小类ID");
            headerList.add("加价毛利率");
            headerList.add("固定加价");
            eBuilder.headers(headerList.toArray(new String[headerList.size()]));
            eBuilder.data(new ArrayList<String[]>(0));
            ExcelUtil.exportOneBookForXlsx(eBuilder.build(), response, "导入模版");
        } catch (Exception e) {
            log.error("模版下载报错", e);
        }
    }

    @Override
    public BtobStateUpdateResponse deleteChannelPolicy(BtobChannelPolicyGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin deleteChannelPolicy request:{}", JsonUtil.toJson(request));
        }
        BtobStateUpdateResponse response = new BtobStateUpdateResponse();
        try {
            mdBtobWholesaleChannelPolicyDao.deleteChannelPolicy(request.getTenantNumId(), request.getDataSign(), request.getSeries(), request.getReservedNo());
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end deleteChannelPolicy response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ChannelResponse getChannelPolicyListPage(BtobChannelPolicyGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getChannelPolicyListPage request:{}", JsonUtil.toJson(request));
        }
        ChannelResponse response = new ChannelResponse();
        try {
            response.setResults(mdScmB2bChannelPolicyDao.getChannelPolicyListPage(request));
            response.setCount(mdScmB2bChannelPolicyDao.getChannelPolicyListPageCount(request));
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getChannelPolicyListPage response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

   /* @Override
    public ChannelResponse getChannelRouteListPage(BtobChannelPolicyGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getChannelRouteListPage request:{}", JsonUtil.toJson(request));
        }
        ChannelResponse response = new ChannelResponse();
        try {
            response.setResults(invScmB2bChannelRouteDao.getChannelRouteListPage(request));
            response.setCount(invScmB2bChannelRouteDao.getChannelRouteListPageCount(request));
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        if (log.isDebugEnabled()) {
            log.debug("end getChannelRouteListPage response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ChannelRouteAddResponse addChannelRoute(B2bBlChannelRouteAddRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin addChannelRoute request:{}", JsonUtil.toJson(request));
        }
        ChannelRouteAddResponse response = new ChannelRouteAddResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            ScmB2bChannelRoute route = createChannelRoute(request);
            invScmB2bChannelRouteDao.insert(route);
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        return response;
    }

    @Override
    public ChannelRouteGetResponse getChannelRoute(B2bBlChannelRouteGetRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin getChannelRoute request:{}", JsonUtil.toJson(request));
        }
        ChannelRouteGetResponse response = new ChannelRouteGetResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            B2bBlChannelRoute route = invScmB2bChannelRouteDao.getChannelRoute(request);
            response.setRoute(route);
        } catch (Exception ex) {
            ExceptionUtil.processException(ex, response);
        }

        if (log.isDebugEnabled()) {
            log.debug("end getChannelRoute response: {}", JsonUtil.toJson(response));
        }
        return response;
    }

    @Override
    public ChannelRouteAddResponse updateChannelRoute(B2bBlChannelRouteAddRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("begin updateChannelRoute request:{}", JsonUtil.toJson(request));
        }
        ChannelRouteAddResponse response = new ChannelRouteAddResponse();
        try {
            request.validate(Constant.SUB_SYSTEM, ExceptionType.VCE15001);
            invScmB2bChannelRouteDao.updateChannelRoute(request);
        } catch (Exception e) {
            ExceptionUtil.processException(e, response);
        }
        return response;
    }*/

    private ScmB2bBlChannelPolicyHdr createChannelPolicyHdr(B2bBlChannelPolicyHdr hdr, Long tenantNumId, Long dataSign, Long userNumId) {
        ScmB2bBlChannelPolicyHdr policyHdr = JsonUtil.fromJsonCamel(JsonUtil.toJson(hdr), ScmB2bBlChannelPolicyHdr.class);
        policyHdr.setSeries(SeqGetUtil.getNoSubSequence(SeqUtil.MDMS_SCM_B2B_BL_CHANNEL_POLICY_HDR_SERIES));
        policyHdr.setTenant_num_id(tenantNumId);
        policyHdr.setData_sign(dataSign);
        policyHdr.setCreate_user_id(userNumId);
        policyHdr.setCreate_dtme(new Date());
        policyHdr.setCancelsign(Constant.CANCEL_SIGN_N);
        return policyHdr;
    }

    /*private ScmB2bChannelRoute createChannelRoute(B2bBlChannelRouteAddRequest request) {
        ScmB2bChannelRoute route = JsonUtil.fromJsonCamel(JsonUtil.toJson(request), ScmB2bChannelRoute.class);
        route.setSeries(SeqGetUtil.getNoSubSequence(SeqUtil.INV_SCM_B2B_CHANNEL_ROUTE_SERIES));
        route.setTenant_num_id(request.getTenantNumId());
        route.setData_sign(request.getDataSign());
        route.setCreate_user_id(request.getUserNumId());
        route.setCreate_dtme(new Date());
        route.setCancelsign(Constant.CANCEL_SIGN_N);
        return route;
    }*/

}
