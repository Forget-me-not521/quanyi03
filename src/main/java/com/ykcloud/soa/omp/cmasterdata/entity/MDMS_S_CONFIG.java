package com.ykcloud.soa.omp.cmasterdata.entity;

public class MDMS_S_CONFIG extends BaseEntity {
    private String CONFIG_NAME;
    private String CONFIG_VALUE;
    private String CONFIG_DESCRIPTION;
    private String REF_ONE;

    public String getCONFIG_NAME() {
        return CONFIG_NAME;
    }

    public void setCONFIG_NAME(String CONFIG_NAME) {
        this.CONFIG_NAME = CONFIG_NAME;
    }

    public String getCONFIG_VALUE() {
        return CONFIG_VALUE;
    }

    public void setCONFIG_VALUE(String CONFIG_VALUE) {
        this.CONFIG_VALUE = CONFIG_VALUE;
    }

    public String getCONFIG_DESCRIPTION() {
        return CONFIG_DESCRIPTION;
    }

    public void setCONFIG_DESCRIPTION(String CONFIG_DESCRIPTION) {
        this.CONFIG_DESCRIPTION = CONFIG_DESCRIPTION;
    }

    public String getREF_ONE() {
        return REF_ONE;
    }

    public void setREF_ONE(String REF_ONE) {
        this.REF_ONE = REF_ONE;
    }
}
