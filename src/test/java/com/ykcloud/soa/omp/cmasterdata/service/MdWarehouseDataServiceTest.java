package com.ykcloud.soa.omp.cmasterdata.service;

import com.ykcloud.soa.omp.cmasterdata.MdMain;
import com.ykcloud.soa.omp.cmasterdata.api.request.PhysicalSaveOrUpdateRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.ShipmentSaveOrUpdateRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.StorageSaveOrUpdateRequest;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdWarehouseDataService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @ClassName MdWarehouseDataServiceTest
 * @Description
 * @Author Dan
 * @Date 2021/10/16 15:46
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MdMain.class)
@ActiveProfiles(value = "dev")
public class MdWarehouseDataServiceTest {

    @Resource
    private MdWarehouseDataService mdWarehouseDataService;

    @Test
    public void physicalSaveOrUpdate(){
        PhysicalSaveOrUpdateRequest request = new PhysicalSaveOrUpdateRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setCortNumId("123");
        //request.setSubUnitNumId("0");
        request.setPhysicalNumId("A01");
        request.setPhysicalName("A01测试");
        request.setVirtualSign(0);
        request.setRemark("11测试remark");
        request.setContEmpe("11联系人");
        request.setTelephone("12345678901");
//        request.setPrvNumId(1l);
//        request.setCityNumId(12l);
//        request.setCityAreaNumId(123l);
        request.setAreaAdr("江苏省苏州市花桥中信广场free");
        request.setSeries("2110190020001");
        mdWarehouseDataService.physicalSaveOrUpdate(request);
    }

    @Test
    public void storageSaveOrUpdate(){
        StorageSaveOrUpdateRequest request = new StorageSaveOrUpdateRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setCortNumId("123");
        //request.setSubUnitNumId("0");
        request.setPhysicalNumId("A01");
        request.setStorageNumId("101");
        request.setStorageName("101测试");
        request.setBusinessType(1l);
        request.setManageType(1l);
        mdWarehouseDataService.storageSaveOrUpdate(request);
    }

    @Test
    public void shipmentSaveOrUpdate() {
        ShipmentSaveOrUpdateRequest request = new ShipmentSaveOrUpdateRequest();
        request.setSeries("2111030020001");
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setCortNumId("123");
//        request.setSubUnitNumId("0");//
        request.setPhysicalNumId("A02");
        request.setPhysicalName("大仓1");
        request.setShipmentNumId("002");
        request.setShipmentName("装运点2222");
        request.setEnableSign(0L);
        mdWarehouseDataService.shipmentSaveOrUpdate(request);
    }
}
