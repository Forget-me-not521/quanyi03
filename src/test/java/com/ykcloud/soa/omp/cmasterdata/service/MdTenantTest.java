package com.ykcloud.soa.omp.cmasterdata.service;

import com.ykcloud.soa.omp.cmasterdata.MdMain;
import com.ykcloud.soa.omp.cmasterdata.api.request.AutomicSequenceGetRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.BlProcessOperateLogGetRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.CachePrefixDeleteRequest;
import com.ykcloud.soa.omp.cmasterdata.api.request.ConfigValueGetRequest;

import com.ykcloud.soa.omp.cmasterdata.api.service.MdTenantService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MdMain.class)
public class MdTenantTest {

    @Resource
    private MdTenantService mdTenantService;


    @Test
    public void getConfigValue() {
        ConfigValueGetRequest request = new ConfigValueGetRequest();
        request.setTenantNumId(6L);
        request.setDataSign(0L);
        request.setConfigName("get_dealing_by_date");
        mdTenantService.getConfigValue(request);
    }




    @Test
    public void getAutomicSequenceTest() {
        AutomicSequenceGetRequest request = new AutomicSequenceGetRequest();
        request.setTenantNumId(6L);
        request.setDataSign(1L);
        request.setSeriesName("auto_mdms_o_product_origin_product_origin_num_id");
        mdTenantService.getAutomicSequence(request);
    }


    //清除缓存
    @Test
    public void deleteCachePrefixTest() {
        CachePrefixDeleteRequest request = new CachePrefixDeleteRequest();
        request.setCachePrefix("POS");
        mdTenantService.deleteCacheByPrefix(request);
    }

    @Test
    public void getLog() {
        BlProcessOperateLogGetRequest request = new BlProcessOperateLogGetRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setReservedNo("CGHT-2110040010001");
        request.setCortNumId("1");
        request.setPageNum(1);
        request.setPageSize(10);
        request.setBusinessType(9);
        request.setSourceType(1);
        request.setUserNumId(1L);
        mdTenantService.getBlProcessOperateLog(request);
    }

}
