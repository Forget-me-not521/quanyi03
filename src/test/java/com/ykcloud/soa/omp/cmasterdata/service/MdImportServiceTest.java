//package com.ykcloud.soa.omp.cmasterdata.service;
//
//import com.google.common.collect.Lists;
//import com.ykcloud.soa.omp.cmasterdata.MdMain;
//import com.ykcloud.soa.omp.cmasterdata.api.model.ImportUnit;
//import com.ykcloud.soa.omp.cmasterdata.api.model.StorageAndPhysical;
//import com.ykcloud.soa.omp.cmasterdata.api.request.StorageAndPhysicalImportRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.UnitImportRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.service.MdImportService;
//import com.ykcloud.soa.omp.cmasterdata.entity.MDMS_O_SUB_UNIT;
//import com.ykcloud.soa.omp.cmasterdata.service.impl.MdUnitServiceImpl;
//import com.ykcloud.soa.omp.cmasterdata.util.SeqUtil;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import javax.annotation.Resource;
//import java.lang.reflect.Field;
//import java.lang.reflect.Modifier;
//import java.util.ArrayList;
//import java.util.List;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = MdMain.class)
//public class MdImportServiceTest {
//
//    private static Logger log = LoggerFactory.getLogger(MdUnitServiceImpl.class);
//    @Resource
//    MdImportService mdImportService;
//
//    @Test
//    public void toUp() {
//        Field[] field = MDMS_O_SUB_UNIT.class.getDeclaredFields();
//        for (Field field1 : field) {
//            System.out.println(Modifier.toString(field1.getModifiers()) + " " + field1.getType().toString().split("[.]")[2] + " " + field1.getName().toUpperCase() + ";");
//
//        }
//    }
//
//    //取流水号
//    @Test
//    public void getSeqAutoNextValueTest() {
//        String seq = SeqUtil.getSeqAutoNextValue(6L, 1L, "auto_style_num_id");
//        log.debug("seq:{}", seq);
//    }
//
//
//
//    @Test
//    public void importMdmsWStorageAndMdmsWPhysical() {
//        StorageAndPhysical storageAndPhysical = new StorageAndPhysical();
//        List<StorageAndPhysical> storageAndPhysicals = new ArrayList<>();
//        StorageAndPhysicalImportRequest request = new StorageAndPhysicalImportRequest();
//        storageAndPhysical.setSubUnitId("1");
//        storageAndPhysical.setPhysicalId("1");
//        storageAndPhysical.setPhysicalName("1");
//        storageAndPhysical.setPhysicalStorageDeptNumId(1L);
//        storageAndPhysical.setPhysicalPurSign(1L);
//        storageAndPhysical.setIsIndependentAccounting(1L);
//        storageAndPhysical.setIsProcessingStorage(1L);
//        storageAndPhysical.setPhysicalPrvNumId(140000L);
//        storageAndPhysical.setPhysicalCityNumId(141100L);
//        storageAndPhysical.setPhysicalCityAreaNumId(141122L);
//        storageAndPhysical.setPhysicalTownNumId(14112203L);
//        storageAndPhysical.setPhysicalAreaAdr("1");
//        storageAndPhysical.setContEmpe("1");
//        storageAndPhysical.setTelephone("1");
//
//        //逻辑仓
//        storageAndPhysical.setStorageId("1");
//        storageAndPhysical.setStorageName("1");
//        storageAndPhysical.setStorageDeptNumId(1L);
//        storageAndPhysical.setPurSign(1L);
//        storageAndPhysical.setSellSign(1L);
//        storageAndPhysical.setFromCityNumId(1L);
//        storageAndPhysical.setReplenishSign(1L);
//        storageAndPhysical.setReturnedToSupplierSign(1L);
//        storageAndPhysical.setPrvNumId(140000L);
//        storageAndPhysical.setCityNumId(141100L);
//        storageAndPhysical.setCityAreaNumId(141122L);
//        storageAndPhysical.setTownNumId(14112203L);
//        storageAndPhysical.setAdr("1");
//        storageAndPhysical.setMaplocationX("1");
//        storageAndPhysical.setMaplocationY("1");
//        storageAndPhysicals.add(storageAndPhysical);
//        request.setStorageAndPhysicals(storageAndPhysicals);
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        request.setUserNumId(9527L);
//        mdImportService.importStorageAndPhysical(request);
//    }
//
//    @Test
//    public void testImportUnit() {
//        UnitImportRequest request = new UnitImportRequest();
//        List<ImportUnit> importUnitList = Lists.newArrayList();
//        ImportUnit importUnit = new ImportUnit();
//        importUnit.setBankCityNumId(100000L);
//        importUnit.setBankPrvNumId(430000L);
//        importUnit.setBehfAdr("4444");
//        importUnit.setBehfCity(0L);
//        importUnit.setBehfFax("5555");
//        importUnit.setBehfMail("eeww");
//        importUnit.setBehfPostcode("sss");
//        importUnit.setBehfTelephone("55551");
//        importUnit.setBillingBank("中国银行上海市静");
//        importUnit.setBillingBankAccount("436463269429");
//        importUnit.setCityAreaNumId(430121L);
//        importUnit.setCityNumId(100000L);
//        importUnit.setContEmpe1("dss");
//        importUnit.setContEmpe1Adr("ssss");
//        importUnit.setContEmpe1Fax("ddd");
//        importUnit.setContEmpe1Mail("dwew");
//        importUnit.setContEmpe1Postcode("dssd");
//        importUnit.setContEmpe1Telephone("5555");
//        importUnit.setContEmpe2Telephone("14112203");
//        importUnit.setCortId("5100057");
//        importUnit.setCortName("南京溧水万达");
//        importUnit.setDeliveryEffectiveDays(7L);
//        importUnit.setEnBehfAdr("dsewew");
//        importUnit.setEnContEmpe1("dsse");
//        importUnit.setEnCortName("南京溧水万达");
//        importUnit.setEnRegisteredName("dssee");
//        importUnit.setEnSimCortName("南京溧水万达");
//        importUnit.setEnSimUnitName("ewewewe");
//        importUnit.setEnUnitName("ewewwe");
//        importUnit.setIdentityId("444444");
//        importUnit.setLegalBehf("seee");
//        importUnit.setOrgCode("746529462");
//        importUnit.setPrvNumId(430000L);
//        importUnit.setRegisteredName("dsds");
//        importUnit.setSettlementType(1L);
//        importUnit.setSimCortName("南京溧水万达");
//        importUnit.setSimUnitName("weew");
//        importUnit.setStatusNumId(3);
//        importUnit.setTaxAccount("9143010532933943XR");
//        importUnit.setTaxpayerType("一般纳税人");
//        importUnit.setTownNumId(14112203L);
//        importUnit.setTypeNumId(2L);
//        importUnit.setUnitName("南京溧水万达");
//        importUnit.setUnitId("3100057");
//
//
//        importUnitList.add(importUnit);
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        request.setImportUnitList(importUnitList);
//
////        UnitImportResponse response = mdImportService.importUnit(request);
////        System.out.println(response.getImportErrors().get(0).getErrorMsg());
//    }
//}
