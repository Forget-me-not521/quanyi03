package com.ykcloud.soa.omp.cmasterdata.service;

import com.ykcloud.soa.omp.cmasterdata.MdMain;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsBlPartner;
import com.ykcloud.soa.omp.cmasterdata.api.model.MdmsOWholesaleChain;
import com.ykcloud.soa.omp.cmasterdata.api.model.ProcessHdr;
import com.ykcloud.soa.omp.cmasterdata.api.request.*;
import com.ykcloud.soa.omp.cmasterdata.api.response.*;
import com.ykcloud.soa.omp.cmasterdata.api.service.MdManagerCompanyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MdMain.class)
public class MdManagerCompanyTest {

    @Resource
    private MdManagerCompanyService mdManagerCompanyService;

    // 公司档案创建
    @Test
    public void addManagerCompany() {
        ManagerCortSaveRequest request = new ManagerCortSaveRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setRemark("因为要测试创建公司档案");
        request.setUserNumId(1L);
        // 插入详情
        request.setCortId("1");
        request.setCortName("苏州公司");
        request.setUnifiedSocialCreditCode("123456789");
        request.setLegalRepresentative("李四");
        request.setRegistrStatus((byte)1);
        request.setIncorporDate(new Date());
        request.setRegisteredCapital("100万");
        request.setPaidInCapital("100万");
        request.setApprovalDate(new Date());
        request.setOrgCode("123");
        request.setBusinessRegistrNo("123456");
        request.setTaxpayerNo("4567");
        request.setCortType((byte) 1);
        request.setBusinessBeginTerm(new Date());
        request.setBusinessEndTerm(new Date());
        request.setTaxpayerQualificat((byte) 2);
        request.setIndustry((byte) 1);
        request.setRegion(1);
        request.setPrvNumId(12L);
        request.setCityNumId(13);
        request.setCityAreaNumId(14);
        request.setTownNumId(15L);
        request.setRegistrAuthority("苏州市部门");
        request.setOldCortName("");
        request.setImportEnterpriseCode("123456");
        request.setRegistrAdr("工业园区");
        request.setBusinessScope("药品");
        request.setPhone("18711112222");
        request.setEmail("wxxw@163.com");
        request.setBriefIntroduction("真的很好的");
        request.setBankName("");
        request.setBankAccount("");
        mdManagerCompanyService.addManagerCompany(request);
    }

    // 公司档案修改
    @Test
    public void saveManagerCompany() {
        ManagerCortSaveRequest request = new ManagerCortSaveRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setRemark("因为要测试更新公司档案");
        request.setUserNumId(1L);
        request.setReservedNo("2109070080001");
        // 插入详情
        request.setCortId("1");
        request.setCortName("苏州公司更新版");
        request.setUnifiedSocialCreditCode("123456789");
        request.setLegalRepresentative("李四更新版");
        request.setRegistrStatus((byte)1);
        request.setIncorporDate(new Date());
        request.setRegisteredCapital("200万");
        request.setPaidInCapital("100万");
        request.setApprovalDate(new Date());
        request.setOrgCode("12345");
        request.setBusinessRegistrNo("12345");
        request.setTaxpayerNo("12345");
        request.setCortType((byte) 1);
        request.setBusinessBeginTerm(new Date());
        request.setBusinessEndTerm(new Date());
        request.setTaxpayerQualificat((byte) 2);
        request.setIndustry((byte) 1);
        request.setRegion(1);
        request.setPrvNumId(12L);
        request.setCityNumId(13);
        request.setCityAreaNumId(14);
        request.setTownNumId(15L);
        request.setRegistrAuthority("苏州市部门");
        request.setOldCortName("");
        request.setImportEnterpriseCode("123456");
        request.setRegistrAdr("工业园区");
        request.setBusinessScope("药品");
        request.setPhone("18911112222");
        request.setEmail("wxxw@163.com");
        request.setBriefIntroduction("真的很好的更新版");
        request.setBankName("");
        request.setBankAccount("");
        mdManagerCompanyService.saveManagerCompany(request);
    }

    // 公司档案查看
    @Test
    public void getManagerCompany() {
        ManagerCortGetRequest request = new ManagerCortGetRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setUserNumId(1L);
        request.setReservedNo("2109070080001");
        ManagerCompanyGetResponse response = mdManagerCompanyService.getManagerCompany(request);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("##################" + response);
    }

    // 公司档案提交审核
    @Test
    public void saveSubmitAuditManagerCompanyHdr() {
        ManagerCortHdrSubmitAuditRequest request = new ManagerCortHdrSubmitAuditRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setUserNumId(1L);
        request.setReservedNo("2109070080001");
        mdManagerCompanyService.saveSubmitAuditManagerCompanyHdr(request);
    }

    // 公司档案审核
    @Test
    public void saveAuditManagerCompanyHdr() {
        ManagerCortHdrAuditRequest request = new ManagerCortHdrAuditRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setUserNumId(1L);
        request.setReservedNo("2109070080001");
        mdManagerCompanyService.saveAuditManagerCompanyHdr(request);
    }

    // 公司档案驳回
    @Test
    public void saveRejectedManagerCompanyHdr() {
        ManagerCortHdrRejectedRequest request = new ManagerCortHdrRejectedRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setUserNumId(1L);
        request.setReservedNo("2109070080001");
        mdManagerCompanyService.saveRejectedManagerCompanyHdr(request);
    }

    // 公司档案作废
    @Test
    public void saveInvalidManagerCompanyHdr() {
        ManagerCortHdrInvalidRequest request = new ManagerCortHdrInvalidRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setUserNumId(1L);
        request.setReservedNo("2109070080001");
        mdManagerCompanyService.saveInvalidManagerCompanyHdr(request);
    }

    // 公司档案日志列表
    @Test
    public void getManagerCompanyHdrLog() {
        ManagerCortHdrLogGetRequest request = new ManagerCortHdrLogGetRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setUserNumId(1L);
        request.setReservedNo("2109070080001");
        ManagerCompanyHdrLogGetResponse response = mdManagerCompanyService.getManagerCompanyHdrLog(request);
        System.out.println("####################" + response);

    }

    // 合作商保存
    @Test
    public void saveNewPartner() {
        NewPartnerSaveRequest request = new NewPartnerSaveRequest();
        request.setTenantNumId(18L);
        request.setDataSign(0L);
        request.setUserNumId(1L);
        ProcessHdr head = new ProcessHdr();
        head.setCortNumId("123");
        head.setProcessTitle("title");
        head.setTypeNumId((byte)1);
        head.setRemark("remark");
        head.setDegreeUrgency((byte)1);
        request.setHead(head);
        MdmsBlPartner entity = new MdmsBlPartner();
        entity.setPartnerId("123");
        entity.setPartnerName("合作");
        entity.setUnifiedCreditCode("sf234");
        entity.setPartnerClassify((byte)1);
        entity.setBusinessScope("药品");
        entity.setSaleAllow("许可");
        entity.setPartnerType((byte)1);
        entity.setPartnerAdr("吴中区");
        entity.setLegalPerson("哈哈");
        entity.setRegisterAdr("苏州市");
        entity.setRegisterDate(new Date());
        entity.setRegisterCapital(new BigDecimal(200));
        List<MdmsBlPartner> list = new ArrayList<>();
        list.add(entity);
        //request.setDetails(list);

        mdManagerCompanyService.saveNewPartner(request);
    }


    //       批发公司关联连锁公司--批发公司查询
    //        String sql ="SELECT wholesale_num_id ,chain_name ,unified_social_credit_code ,last_update_user_id ,last_updtme FROM  mdms_o_wholesale_chain WHERE wholesale_num_id=? and data_sign = ? and reserved_no = ? ";
    @Test
    public void WholesaleChainRelation(){
        WholesaleChainRelationRequest request =new WholesaleChainRelationRequest();
        request.setWholesaleNumId("pfname01");
        request.setChainName("lsname");
        request.setUnifiedSocialCreditCode("test");
        request.setLastUpdateUserId(0L);
        request.setLastUpdtme(new Date());
        WholesaleChainRelationResponse response = mdManagerCompanyService.wholesaleRelatChain(request);

        System.out.println("##################" + response);
    }


    //批发公司关联连锁公司--查询添加连锁公司wholesaleSelectChain
    @Test
    public void selectTest(){
        WholesaleSelectChainRequest request = new WholesaleSelectChainRequest();
        request.setChainNumId("numId");
        request.setChainName("chainName");
        request.setUnifiedSocialCreditCode("code");
        WholesaleSelectChainResponse response = mdManagerCompanyService.wholesaleSelectChain(request);

    }

    //批发公司关联连锁公司--连锁公司查询
    @Test
    public void wholesaleRelatsChainTest(){
        WholesaleChainRelationsRequest request = new WholesaleChainRelationsRequest();
        request.setChainNumId("ahwifhu");
        request.setChainName("name02");
        request.setUnifiedSocialCreditCode("xinyongma02");
        request.setCreateUserId(2L);
        request.setCreateDtme(new Date());
        WholesaleChainRelationsResponse response = mdManagerCompanyService.wholesaleRelatsChain(request);

        System.out.println("##################" + response);
    }

    //批发公司关联连锁公司--连锁公司删除
    @Test
    public void WholesaleDeleteChainTest(){
        WholesaleDeleteChainRequest request =new WholesaleDeleteChainRequest();
        //request.setChainNumId();
        WholesaleDeleteChainResponse response = mdManagerCompanyService.wholesaleDeleteChain(request);
        System.out.println(response);
    }

    //批发公司关联连锁公司--连锁公司添加
    @Test
    public void  add(){
        WholesaleAddChainRequest request= new WholesaleAddChainRequest();
        request.setChainNumId("shgy");
        request.setChainName("test");
        request.setUnifiedSocialCreditCode("bdBD");
        WholesaleAddChainResponse response = mdManagerCompanyService.wholesaleAddChain(request);
        System.out.println(response);
    }



}
