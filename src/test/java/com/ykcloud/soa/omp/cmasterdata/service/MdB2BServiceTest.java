//package com.ykcloud.soa.omp.cmasterdata.service;
//
//import javax.annotation.Resource;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.ykcloud.soa.omp.cmasterdata.MdMain;
//import com.ykcloud.soa.omp.cmasterdata.api.request.ClientRegionalDeleteRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.ClientRegionalSaveRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.request.RegionalClientAddRequest;
//import com.ykcloud.soa.omp.cmasterdata.api.response.ClientRegionalDeleteResponse;
//import com.ykcloud.soa.omp.cmasterdata.api.response.ClientRegionalSaveResponse;
//import com.ykcloud.soa.omp.cmasterdata.api.response.RegionalClientAddResponse;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = MdMain.class)
//public class MdB2BServiceTest {
//    @Resource
//    private MdB2BService mdB2BService;
//
//
//    @Test
//    public void saveClientArea() {
//        ClientRegionalSaveRequest request = new ClientRegionalSaveRequest();
//        request.setTenantNumId(9L);
//        request.setDataSign(0L);
//        request.setUserNumId(2016217511L);
//        request.setSeries("");
//        request.setTypeName("美区");
//        request.setTypeNumId(201627511L);
//        ClientRegionalSaveResponse response = mdB2BService.saveClientRegional(request);
//        System.out.println(response);
//    }
//
//    @Test
//    public void deleteClientRegional() {
//        ClientRegionalDeleteRequest request = new ClientRegionalDeleteRequest();
//        request.setTenantNumId(9L);
//        request.setDataSign(0L);
//        request.setUserNumId(2016217511L);
//        request.setCusArea(201627511L);
//        request.setSeries("2008280030001");
//        ClientRegionalDeleteResponse response = mdB2BService.deleteClientRegional(request);
//        System.out.println(response);
//    }

//
//    @Test
//    public void addClientToRegional() {
//        RegionalClientAddRequest request = new RegionalClientAddRequest();
//        request.setTenantNumId(9L);
//        request.setDataSign(0L);
//        request.setUserNumId(2016217511L);
//
//        RegionalClientAddResponse response = mdB2BService.addClientToRegional(request);
//        System.out.println(response);
//    }
//}
