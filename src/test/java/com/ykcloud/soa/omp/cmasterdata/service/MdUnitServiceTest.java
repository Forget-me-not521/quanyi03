//package com.ykcloud.soa.omp.cmasterdata.service;
//
//import javax.annotation.Resource;
//import javax.sound.midi.Soundbank;
//
//import com.ykcloud.soa.omp.cmasterdata.api.request.*;
//import com.ykcloud.soa.omp.cmasterdata.api.response.*;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.gb.soa.omp.ccommon.util.ExceptionUtil;
//import com.gb.soa.omp.ccommon.util.JsonUtil;
//import com.ykcloud.soa.omp.cmasterdata.MdMain;
//import com.ykcloud.soa.omp.cmasterdata.api.service.MdUnitService;
//
//import java.util.Arrays;
//import java.util.List;
//
///**
// * @ClassName: MdUnitServiceTest.java
// * @Description:
// * @version: v1.0.0
// * @author: fred.zhao
// * @date: 2018年3月19日 上午10:56:51
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = MdMain.class)
//public class MdUnitServiceTest {
//
//
//    @Resource
//    private MdUnitService mdUnitService;
//
//
//    @Test
//    public void getSupplyEffectiveDay() {
//        SupplyEffectiveDayGetRequest request = new SupplyEffectiveDayGetRequest();
//        request.setSupplyUnitNumId(100059L);
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        System.out.println(JsonUtil.toJson(mdUnitService.getSupplyEffectiveDay(request)));
//    }
//
//    @Test
//    public void getUnitInfoByUnitNumIdTest() {
//        UnitInfoByUnitNumIdGetRequest request = new UnitInfoByUnitNumIdGetRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        request.setUnitNumId(100001L);
//        mdUnitService.getUnitInfoByUnitNumId(request);
//    }
//
//    @Test
//    public void getSupplyStatusPermitActionActionTest() {
//        SupplyStatusPermitActionGetRequest request = new SupplyStatusPermitActionGetRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        request.setSupplyUnitNumId(100003L);
//        mdUnitService.getSupplyStatusPermitAction(request);
//    }
//
//    @Test
//    public void getTypeNumIdByUnitNumIdTest() {
//        TypeNumIdByUnitNumIdGetRequest request = new TypeNumIdByUnitNumIdGetRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        request.setUnitNumId(100049L);
//        TypeNumIdByUnitNumIdGetResponse count = mdUnitService.getTypeNumIdByUnitNumId(request);
//        System.out.println(count.getTypeNumId());
//    }
//
//    @Test
//    public void getSubUnitNumIdBySuperUnitNumIdTest() {
//        SubUnitNumIdBySuperUnitNumIdGetRequest request = new SubUnitNumIdBySuperUnitNumIdGetRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        request.setSuperUnitNumId(100001L);
//        SubUnitNumIdBySuperUnitNumIdGetResponse count = mdUnitService.getSubUnitNumIdBySuperUnitNumId(request);
//        System.out.println(count.getSubUnitNumIds().size());
//    }
//
//    // by 殷剑
//    @Test
//    public void getUnitNumIdByUnitIdTest() {
//        UnitNumIdByUnitIdGetRequest request = new UnitNumIdByUnitIdGetRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        request.setUnitId("A001");
//        mdUnitService.getUnitNumIdByUnitId(request);
//    }
//
//    @Test
//    public void getCortNumIdByCortIdTest() {
//        CortNumIdByCortIdGetRequest request = new CortNumIdByCortIdGetRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        request.setCortId("A801");
//        mdUnitService.getCortNumIdByCortId(request);
//    }
//
//
//    @Test
//    public void getSubUnitNumIdByUnitNumIdTest() {
//        SubUnitNumIdByUnitNumIdGetRequest request = new SubUnitNumIdByUnitNumIdGetRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        request.setUnitNumId(100049L);
//        mdUnitService.getSubUnitNumIdByUnitNumId(request);
//    }
//
//    @Test
//    public void getSubUnitNameByUnitNumIdTest() {
//        SubUnitNameBySubUnitNumIdGetRequest request = new SubUnitNameBySubUnitNumIdGetRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        request.setSubUnitNumId(100049L);
//        mdUnitService.getSubUnitNameBySubUnitNumId(request);
//    }
//
//    @Test
//    public void getBilling() {
//        BillingGetRequest request = new BillingGetRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        request.setUnitNumId(1355L);
//        mdUnitService.getBilling(request);
//    }
//
//    //获取用户的授权门店
//    @Test
//    public void testGetAuthorizeUnit() {
//        AuthorizeUnitGetRequest request = new AuthorizeUnitGetRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(1L);
//        //200001
//        //200002
//        //200003
//        //200004
//        //100055
//        //99956
//        //100056
//        //100157
//        List<Long> subUnitNumIds = Arrays.asList(200001L, 200002L, 100055L);//不能为空
//
//        List<Long> statusNumIds = Arrays.asList(1L, 2L, 3L, 4L);//可以为空
//        request.setSubUnitNumIds(subUnitNumIds);
//        request.setStatusNumIds(statusNumIds);
//
//        AuthorizeUnitGetResponse response = mdUnitService.getAuthorizeUnit(request);
//    }
//
//    @Test
//    public void getSupplyUnitname() {
//        SupplyUnitNameRequest request = new SupplyUnitNameRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        request.setSupplyUnitNumId(536L);
//        SoSupplyUnitNameResponse response = mdUnitService.getSupplyUnitName(request);
//    }
//
//    @Test
//    public void testSaveSupplyUnit() {
//        SupplyUnitSaveRequest request = new SupplyUnitSaveRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        request.setUserNumId(666L);
//        request.setReservedNo(1812290010005L);
//        request.setSettlementType(2L);
//        request.setCortNumId(5100398L);
//        request.setSupplyUnitNumId(3100389L);
//        request.setSupplyCortName("父供应商名称变更");
//        request.setEnSupplyCortName("父供应商英文名称");
//        request.setSimSupplyCortName("父供应商简称");
//        request.setEnSimSupplyCortName("父供应商英文简称");
//        request.setUnitName("供应商名称变更");
//        request.setEnUnitName("供应商英文名称");
//        request.setSimUnitName("供应商简称");
//        request.setEnSimUnitName("供应商英文简称");
//        request.setLegalPerson("法人代表不能为空");
//        request.setLegalPersonId("法人证号不能为空");
//        request.setRegisteredName("工商注册名称");
//        request.setEnRegisteredName("工商注册英文名称");
//        request.setOrganizationCode("组织机构编码");
//        request.setTradingCertificates("营业执照");
//        request.setTaxAccount("税号不能为空");
//        request.setBehfTelephone("法人电话不能为空");
//        request.setBehfFax("法人传真");
//        request.setBehfAdr("法人地址");
//        request.setBankPrvNumId(640000L);
//        request.setBankCityNumId(640200L);
//        request.setBillingBank("银行名不能为空");
//        request.setBillingBankAccount("银行账户不能为空");
////		request.setBankAccount("银行账户不能为空");
//        request.setTaxpayerType(1L);
//        request.setStatusNumId(2);
//        request.setReturnSign("Y");
//        request.setCompensateAmount(4616156.0);
//        request.setMarginAmount(4984.0);
//        request.setLogisticsPoint(6.0);
//        request.setItemDiscount(54.0);
//        request.setSupportLogisticsType("123");//配送方式  ：直通100，直送020，配送003；多选叠加如：120,123,023
//        request.setPrvNumId(640000L);
//        request.setCityNumId(640200L);
//        request.setCityAreaNumId(640205L);
//        request.setContEmpe1Adr("供应商-地址不能为空");
//        request.setContEmpe1("供方代表不能为空");
//        request.setContEmpe1Telephone("123456789");
//        request.setContEmpe1Postcode("4484849");
//        request.setContEmpe1Fax("484849");
//        request.setContEmpe1Mail("eww@163.com");
//        request.setUnitUrl("公司网址");
//        request.setSupplyTypeNumId("123");
//        request.setRemark("备注");
//        mdUnitService.saveSupplyUnit(request);
//    }
//
//    //供应商变更审核
//    @Test
//    public void modifySupplyUnit() {
//        SupplyUnitModifyRequest request = new SupplyUnitModifyRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        request.setReservedNo(1812290010005L);
//        request.setUserNumId(666L);
//        mdUnitService.modifySupplyUnit(request);
//    }
//
//    //父供应商保存或修改
//    @Test
//    public void testSaveCortUnit() {
//        CortUnitSaveRequest request = new CortUnitSaveRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        request.setUserNumId(666L);
//
//        request.setSeries(-1L);
//        request.setCortNumId(999999987L);
//        request.setCortId("A09999");
//        request.setCortName("上海仰空科技");
//        request.setEnCortName("ykcloud");
//        request.setSimCortName("仰空");
//        request.setEnSimCortName("yk");
//        request.setCuyNumId(0L);
//        request.setTypeNumId(2L);
//        request.setPrvNumId(640000L);
//        request.setCityNumId(6402000L);
//        request.setCityAreaNumId(640205L);
//        request.setTownNumId(14112203L);
//        request.setRegisteredName("仰空");
//        request.setEnRegisteredName("yk");
//        request.setTaxAccount("69Ad5826821151515");
//        request.setBillingBankAccount("上海仰空科技");
//        request.setStatusNumId(1);
//        request.setHeadquartersAdr("仰空集团");
//        request.setEnHeadquartersAdr("ykgroup");
//        request.setLegalBehf("胡总");
//        request.setEnLegalBehf("Mr.Hu");
//        request.setBehfPostcode("5588");
//        request.setBehfTelephone("666666");
//        request.setBehfFax("021-6666666");
//        request.setBehfAdr("浦江大厦");
//        request.setEnBehfAdr("pjds");
//        request.setBehfMail("yiako.com");
//        request.setBehfCity(6402001L);
//        request.setContEmpe1("胡总");
//        request.setEnContEmpe1("Mr.Hu");
//        request.setContEmpe1Postcode("98622");
//        request.setContEmpe1Telephone("13558952598513");
//        request.setContEmpe1Fax("0512-25632255");
//        request.setContEmpe1Adr("昆山花桥");
//        request.setEnContEmpe1Adr("kshq");
//        request.setContEmpe1Mail("ykllllllll");
//        request.setContEmpe1City(6402001L);
//        request.setContEmpe2("胡总");
//        request.setEnContEmpe2("Mr.Hu");
//        request.setContEmpe2Postcode("55881445");
//        request.setContEmpe2Telephone("13698425255");
//        request.setContEmpe2Fax("胡");
//        request.setContEmpe2Adr("昆山");
//        request.setEnContEmpe2Adr("kushan");
//        request.setContEmpe2Mail("ks.com");
//        request.setContEmpe2City(6402001L);
//        request.setEnContIm1("胡");
//        request.setEnContIm2("Hu");
//        request.setOrgCode("Ad598ad88484894");
//        request.setBankName("农业银行花桥支行");
//        request.setChainNumId(0L);
//        request.setChainSubNumId(0L);
//        request.setIdentityId("345689955892265632");
//        request.setInvoiceAdr("花桥");
//        request.setBankPrvNumId(6400001L);
//        request.setBankCityNumId(640200L);
//        request.setBillingBank("农业银行");
//        request.setBillingBankAccount("农业银行--上海仰空");
//        request.setTradingCertificates("ALOD56Kkkpwp9865KIo");
//        request.setTaxpayerType("小规模");
//        mdUnitService.saveCortUnit(request);
//    }
//
//    //父供应商删除
//    @Test
//    public void testDeleteCortUnit() {
//        CortUnitDeleteRequest request = new CortUnitDeleteRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        request.setSeriesList(Arrays.asList(19010800107L));
//        mdUnitService.deleteCortUnit(request);


//    }
//
//    @Test
//    public void settlementCompanyUpper() {
//        SettlementCompanyUpperRequest request = new SettlementCompanyUpperRequest();
//        request.setCortId("12");
//        request.setBillingBankAccount("A123213");
//        request.setCortName("测试法人");
//        request.setTaxAccount("12");
//        request.setBehfTelephone("3213");
//        request.setBehfAdr("测试地址");
//        request.setBillingBank("测试银行");
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        mdUnitService.settlementCompanyUpper(request);
//    }
//
//    @Test
//    public void settlementCompanyLower() {
//        SettlementCompanyLowerRequest request = new SettlementCompanyLowerRequest();
//        request.setCortId("1232134421441242141");
//        request.setCortName("cdshi");
//        request.setTaxAccount("1");
//        request.setBillingBankAccount("dasdf");
//        request.setBehfTelephone("213213");
//        request.setBehfAdr("213213");
//        request.setBillingBank("dasdsad");
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        request.setSuperCortNumId(12L);
//        mdUnitService.settlementCompanyLower(request);
//    }
//
//    @Test
//    public void deleteCompany() {
//        DeleteCompanyRequest request = new DeleteCompanyRequest();
//        request.setSuperCortNumId("12");
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        mdUnitService.deleteCompany(request);
//    }
//
//    @Test
//    public void modifyUnitStatus() {
//        UnitStatusModifyRequest request = new UnitStatusModifyRequest();
//        request.setTenantNumId(6L);
//        request.setDataSign(0L);
//        request.setUnitNumId(3100049L);
//        request.setStatusNumId(2l);
//        request.setUserNumId(123L);
//        mdUnitService.modifyUnitStatus(request);
//    }
//}
