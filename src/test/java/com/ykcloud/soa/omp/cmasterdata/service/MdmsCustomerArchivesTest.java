package com.ykcloud.soa.omp.cmasterdata.service;

import com.gb.soa.omp.ccommon.api.request.AbstractUserSessionRequest;
import com.ykcloud.soa.omp.cmasterdata.MdMain;
import com.ykcloud.soa.omp.cmasterdata.api.request.*;
import com.ykcloud.soa.omp.cmasterdata.service.impl.MdmsCustomerArchivesServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author wangcong
 * @date 2021/11/1 20:59
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MdMain.class)
@Slf4j
public class MdmsCustomerArchivesTest {
    @Resource
    MdmsCustomerArchivesServiceImpl mdmsCustomerArchivesService;

    @Test
    public void customerTest() {
        MdmsCustomerArchivesListRequest mcalr = new MdmsCustomerArchivesListRequest();
        mcalr.setPageNum(1);
        mcalr.setPageSize(10);
        mcalr.setTenantNumId(1L);
        mcalr.setDataSign(1L);
        log.debug("--getCustomerArchivesListPage res:{}", mdmsCustomerArchivesService.getCustomerArchivesListPage(mcalr));

        MdmsCustomerArchivesRequest mcar = new MdmsCustomerArchivesRequest();
        mcar.setTenantNumId(1L);
        mcar.setDataSign(1L);
        mcar.setSeries(1L);
        log.debug("--getCustomerArchives res:{}", mdmsCustomerArchivesService.getCustomerArchives(mcar));
    }

    @Test
    public void customerShopTest() {
        MdmsCustomerShopListRequest mcslr = new MdmsCustomerShopListRequest();
        mcslr.setTenantNumId(1L);
        mcslr.setDataSign(1L);
        mcslr.setPageNum(1);
        mcslr.setPageSize(10);
        log.debug("--getCustomerShopListPage res:{}", mdmsCustomerArchivesService.getCustomerShopListPage(mcslr));

        MdmsCustomerShopRequest mcsr = new MdmsCustomerShopRequest();
        mcsr.setTenantNumId(1L);
        mcsr.setDataSign(1L);
        mcsr.setSeries(1L);
        log.debug("--getCustomerShop res:{}", mdmsCustomerArchivesService.getCustomerShop(mcsr));
    }

    @Test
    public void customerShopRelTest() {
        MdmsCustomerShopRelListRequest mcsrlr = new MdmsCustomerShopRelListRequest();
        mcsrlr.setTenantNumId(1L);
        mcsrlr.setDataSign(1L);
        mcsrlr.setPageNum(1);
        mcsrlr.setPageSize(10);
        log.debug("--getCustomerShopRelListPage res:{}", mdmsCustomerArchivesService.getCustomerShopRelListPage(mcsrlr));


        MdmsCustomerShopRelRequest mcsrr = new MdmsCustomerShopRelRequest();
        mcsrr.setTenantNumId(1L);
        mcsrr.setDataSign(1L);
        mcsrr.setSeries(1L);
        log.debug("--getCustomerShopRel res:{}", mdmsCustomerArchivesService.getCustomerShopRel(mcsrr));
    }

    @Test
    public void insertCustomerShopRel() {
        MdmsCustomerShopRelSaveRequest mcsrr = new MdmsCustomerShopRelSaveRequest();
        mcsrr.setAnnex("https://ss3.baidu.com/9fo3dSag_xI4khGko9WTAnF6hhy/image/pic/item/730e0cf3d7ca7bcbb8ab733eb1096b63f724a8cb.jpg");
        mcsrr.setChannelNumId(1);
        mcsrr.setCortName("22233");
        mcsrr.setCortNumId("2111030020001");
        mcsrr.setCreateUserId(1L);
        mcsrr.setEffectSign(1);
        mcsrr.setLastUpdateUserId(1L);
        mcsrr.setOrgUnit("12");
        mcsrr.setRemark("12");
        mcsrr.setSubUnitNumId("12");
        mcsrr.setSubUnitName("sub-12");
        mcsrr.setUnitNumId("121");
        mcsrr.setDataSign(1L);
        mcsrr.setTenantNumId(1L);
        mdmsCustomerArchivesService.saveOrUpdateCustomerShopRel(mcsrr);
    }

    @Test
    public void updateCustomerShopRel() {
        MdmsCustomerShopRelSaveRequest mcsrr = new MdmsCustomerShopRelSaveRequest();
        mcsrr.setSeries("2021110301");
        mcsrr.setAnnex("https://ss3.baidu.com/9fo3dSag_xI4khGko9WTAnF6hhy/image/pic/item/730e0cf3d7ca7bcbb8ab733eb1096b63f724a8cb.jpg");
        mcsrr.setChannelNumId(1);
        mcsrr.setCortName("233333333333333");
        mcsrr.setCortNumId("2111030020001");
        mcsrr.setCreateUserId(1L);
        mcsrr.setEffectSign(1);
        mcsrr.setLastUpdateUserId(1L);
        mcsrr.setOrgUnit("12");
        mcsrr.setRemark("12");
        mcsrr.setSubUnitNumId("12");
        mcsrr.setSubUnitName("sub-12");
        mcsrr.setUnitNumId("121");
        mcsrr.setDataSign(1L);
        mcsrr.setTenantNumId(1L);
        mdmsCustomerArchivesService.saveOrUpdateCustomerShopRel(mcsrr);
    }
}
