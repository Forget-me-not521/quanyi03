//package com.ykcloud.soa.omp.cmasterdata.service;
//
//import com.fasterxml.jackson.databind.PropertyNamingStrategy;
//import com.gb.soa.omp.ccommon.util.JsonMapper;
//import com.ykcloud.soa.omp.cmasterdata.MdMain;
//import com.ykcloud.soa.omp.cmasterdata.api.request.CustUnitSaveRequest;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import javax.annotation.Resource;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = MdMain.class)
//@ActiveProfiles("test")
//public class MdCustUnitServiceTest {
//    private static Logger log = LoggerFactory.getLogger(MdCustUnitServiceImpl.class);
//    private static JsonMapper mapper;
//
//    static {
//        mapper = new JsonMapper();
//        mapper.getMapper().setPropertyNamingStrategy(PropertyNamingStrategy.LowerCaseStrategy.SNAKE_CASE);
//    }
//
//    @Resource
//    private MdCustUnitService mdCustUnitService;
//
//    @Test
//    public void saveCustUnitTest() {
//        CustUnitSaveRequest request = mapper.fromJson("{\"tenant_num_id\":9,\"data_sign\":0,\"message_series\":null,\"request_num_id\":null,\"check_repeat_sign\":null,\"expose_method\":null,\"appkey\":null,\"user_num_id\":1,\"reserved_no\":\"2008270010006\",\"theme\":\"11111\",\"type_num_id\":1,\"status_num_id\":null,\"so_from_type\":1,\"order_date\":1598486400000,\"unit_num_id\":104881,\"unit_name\":\"aaaaaa\",\"cort_num_id\":1,\"cort_name\":null,\"unit_status_num_id\":1,\"cus_name\":\"11111\",\"cus_tel\":\"2222\",\"emergency_cont\":\"33333\",\"emergency_cont_tel\":\"44444\",\"prv_num_id\":0,\"city_num_id\":0,\"city_area_num_id\":0,\"town_num_id\":0,\"adr\":\"\",\"legal_behf\":\"1\",\"behf_telephone\":\"1\",\"behf_id\":\"1\",\"behf_fax\":\"1\",\"behf_adr\":null,\"org_code\":\"1\",\"registered_name\":\"1\",\"trading_certificates\":\"33\",\"tax_account\":\"待后期维护\",\"taxpayer_type\":\"1\",\"reg_capital\":\"44\",\"reg_adr\":\"55555\",\"billing_bank\":\"开户行\",\"billing_bank_account\":\"1\",\"bank_prv_num_id\":330000,\"bank_city_num_id\":330200,\"cus_category\":0,\"cus_area\":null,\"cus_level\":null,\"remark\":\"\",\"settlement_cycle\":null,\"payment_days\":null,\"payment_method\":null}", CustUnitSaveRequest.class);
//        request.setTenantNumId(9L);
//        request.setDataSign(0L);
//        mdCustUnitService.saveCustUnit(request);
//    }
//
//}
